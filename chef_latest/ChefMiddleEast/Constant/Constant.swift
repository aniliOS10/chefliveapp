//
//  Constant.swift
//  ChefMiddleEast
//
//  Created by sandeep on 12/12/21.
//

import Foundation
struct Constants {
    
    static let deviceToken = "deviceToken"
    static let token = "token"
    static let userId = "userId"
    static let enterpriseId = "enterpriseId"
    static let phone = "phone"
    static let email = "email"
    static let name = "name"
    static let firstName = "firstName"
    static let lastName = "lastName"
    static let userImg = "userImg"
    static let fcmToken = "fcmToken"
    static let status = "status"
    static let ProfileImg = "ProfileImg"
    static let NotificationOnOff = "NotificationOnOff"
    static let NotificationOnOffSub = "NotificationOnOffSub"
    static let otp = "otp"
    static let phoneOtp = "phoneOtp"
    static let userModel = "userModel"
    static let ProductModel = "ProductModel"
    static let orderNumber = "orderNumber"
    static let guestUser = "guestUser"
    static let gender = "gender"
    static let dob = "dob"
    static let custAccount = "custAccount"

    

}
