//
//  ImageUploadManager.swift
//  ChefMiddleEast
//
//  Created by sandeep on 12/12/21.
//

import Foundation
import Alamofire
import SVProgressHUD
enum APIConstants :String {
    case
    isSuccess = "isSuccess",
    data = "data",
    error = "error",
    items = "items",
    pageNo = "pageNo",
    total = "total",
    totalRecords = "totalRecords"
}



class ImageUploadManager {
    let queue = DispatchQueue(label: "com.fileupload.queue", attributes: DispatchQueue.Attributes.concurrent)
    var downloadedImage : UIImage?
    private let baseUrl1 = EndPoint.BASE_API_URL
    fileprivate let imageUrl = "Auth/AddUserProfilePicture"
    fileprivate let imageUrl1 = ""
    var countValue: Int = 0
    var imageQuality : CGFloat = 0
    //var cld = CLDCloudinary(configuration: CLDConfiguration(cloudName: remoteConfig.cloudName,apiKey: remoteConfig.cloudKey,apiSecret: remoteConfig.cloudSecret, secure: true))
    
    //Utilize Singleton pattern by instanciating API only once.
    class var sharedInstance: ImageUploadManager {
        struct Singleton {
            static let instance = ImageUploadManager()
        }
        return Singleton.instance
    }
    
    init() {
        
    }
    
    func uploadImageRemote (imageh:UIImage?, callback:@escaping (_ url:  String?, _ error: String? ) -> Void){
        if let image = imageh, let data = image.jpegData(compressionQuality: 1.0) {
            let progressHandler = { (progress: Progress) in
                let ratio: CGFloat = CGFloat(progress.completedUnitCount) / CGFloat(progress.totalUnitCount)
                SVProgressHUD.showProgress(Float(ratio))
                
            }
        }
    }
    
    
    //MARK:upload image to server and returns dictionary containing url and image DATA
    
    //MARK:upload image to server and returns dictionary containing url and image DATA
    
    func uploadImageaaaRemote (image:UIImage,UserId:String, callback:@escaping (_ data: String?, _ error: NSError? ) -> Void){
        
        //    let imageData = UIImageJPEGRepresentation(image, 1.0)
        //    let imageInfo : UIImage = UIImage(data: imageData!)!
        
        let baseUrl = baseUrl1 + self.imageUrl
        
        let data = image.jpegData(compressionQuality: imageQuality)
        if data == nil{
            print("data return nil")
            return
        }
        var tokenHeaders:[String:String]! = [:]
        
        tokenHeaders = ["x-access-token" :"\(DataManager.token ?? "")"]
        
       // let parameters = tokenHeaders
        
        let parameters = ["UserId": UserId]
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 1000
        manager.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(data!, withName: "ProfilePic", fileName: "propic.jpeg", mimeType: "image/jpeg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:baseUrl,headers:parameters)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                //   SVProgressHUD.setDefaultMaskType(.custom)
                //   SVProgressHUD.setBackgroundLayerColor(#colorLiteral(red: 0.5704585314, green: 0.5704723597, blue: 0.5704649091, alpha: 0.3864301733))

               //     SVProgressHUD.showProgress(Float(Progress.fractionCompleted))

                })
                
                upload.responseJSON { response in
                    //self.delegate?.showSuccessAlert()
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    //                        self.showSuccesAlert()
                    //self.removeImage("frame", fileExtension: "txt")
                //    SVProgressHUD.dismiss()
                    switch response.result{
                    case .success(_):
                        print("successfull")
                    case.failure(let error):
                        print(error)
                        if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            callback("failure", nil)
                            
                        }else{
                            
                            NotificationAlert().NotificationAlert(titles: "The Internet connection appears to be offline")
                            callback("failure",nil)
                        }
                    }
                    
                    if let value = response.result.value {
                        print(response.result.value)
                        if let result = value as? Dictionary<String, AnyObject> {
                            
                            let dataImage = result["data"] as! String
                          /*  let urlModel = result[APIConstants.data.rawValue] as! [String:String]*/
                            print(dataImage)
                            callback(dataImage , nil )
                        }
                    }
                }
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
              //  SVProgressHUD.dismiss()
                if encodingError._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                }
                print(encodingError)
            }
        }
    }
    
    
    func uploadPDFFile (urlPath :URL, callback:@escaping (_ data: String?, _ error: NSError? ) -> Void){
        
        //    let imageData = UIImageJPEGRepresentation(image, 1.0)
        //    let imageInfo : UIImage = UIImage(data: imageData!)!
        
        let baseUrl = baseUrl1 + self.imageUrl
        
        let data = try! Data(contentsOf: urlPath)
        
        var tokenHeaders:[String:String]! = [:]
        
        tokenHeaders = ["x-access-token" :"\(DataManager.token ?? "")"]
        let parameters = tokenHeaders
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 1000
        manager.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(data, withName: "media", fileName: "media.pdf", mimeType: "application/pdf")
            for (key, value) in parameters! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:baseUrl,headers:parameters)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                   SVProgressHUD.setDefaultMaskType(.custom)
                   SVProgressHUD.setBackgroundLayerColor(#colorLiteral(red: 0.5704585314, green: 0.5704723597, blue: 0.5704649091, alpha: 0.3864301733))

                    SVProgressHUD.showProgress(Float(Progress.fractionCompleted))

                })
                
                upload.responseJSON { response in
                    //self.delegate?.showSuccessAlert()
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    //                        self.showSuccesAlert()
                    //self.removeImage("frame", fileExtension: "txt")
                    SVProgressHUD.dismiss()
                    switch response.result{
                    case .success(_):
                        print("successfull")
                    case.failure(let error):
                        print(error)
                        if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                           // NotificationAlert().NotificationAlert(titles: "Your Request timeout!")
                            callback("failure", nil)
                            
                        }else{
                            
                            NotificationAlert().NotificationAlert(titles: "The Internet connection appears to be offline")
                            callback("failure",nil)
                        }
                    }
                    
                    if let value = response.result.value {
                        print(response.result.value)
                        if let result = value as? Dictionary<String, AnyObject> {
                            let urlModel = result[APIConstants.data.rawValue] as! [String:String]
                            print(urlModel)
                            callback(urlModel["url"] , nil )
                        }
                    }
                }
                
            case .failure(let encodingError):
                SVProgressHUD.dismiss()
                if encodingError._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                }
                print(encodingError)
            }
        }
    }
    
    
    
    func uploadImageaaaRemote1(image:UIImage, callback:@escaping (_ data: Dictionary<String,AnyObject>, _ error: NSError? ) -> Void){
        
        
        
        let baseUrl = baseUrl1 + "agencyDocuments"
        let data = image.jpegData(compressionQuality: imageQuality)
        var tokenHeaders:[String:String]! = [:]
        
        tokenHeaders = ["x-access-token" :"\(DataManager.token ?? "")"]
        
        let parameters = tokenHeaders
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 1000
        
        manager.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(data!, withName: "media", fileName: "media.jpeg", mimeType: "media/jpeg")
            for (key, value) in parameters! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:baseUrl,headers:parameters)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    SVProgressHUD.showProgress(Float(Progress.fractionCompleted))
                })
                
                upload.responseJSON { response in
                    //self.delegate?.showSuccessAlert()
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    //                        self.showSuccesAlert()
                    //self.removeImage("frame", fileExtension: "txt")
                    SVProgressHUD.dismiss()
                    switch response.result{
                    case .success(_):
                        print("successfull")
                    case.failure(let error):
                        print(error)
                        if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                           // NotificationAlert().NotificationAlert(titles: "Your Request timeout!")
                            let urlModel = ["status":"failure"]
                            callback(urlModel as Dictionary<String, AnyObject>, nil)
                            
                        }else{
                            
                            NotificationAlert().NotificationAlert(titles: "The Internet connection appears to be offline")
                            let urlModel = ["status":"failure"]
                            callback(urlModel as Dictionary<String, AnyObject>, nil)
                        }
                    }
                    
                    if let value = response.result.value {
                        print(response.result.value)
                        if let result = value as? Dictionary<String, AnyObject> {
                            let urlModel = result[APIConstants.data.rawValue]
                            print(urlModel)
                            callback(result , nil )
                        }
                    }
                }
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                SVProgressHUD.dismiss()
                if encodingError._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                }
                print(encodingError)
            }
            
        }
        
    }
    
    func uploadImageaaaRemote2(image:UIImage,Url:String, callback:@escaping (_ data: Dictionary<String,AnyObject>, _ error: NSError? ) -> Void){
        
        
        //    let imageData = UIImageJPEGRepresentation(image, 1.0)
        //    let imageInfo : UIImage = UIImage(data: imageData!)!
        
        let baseUrl = baseUrl1 + Url
        let data = image.jpegData(compressionQuality: imageQuality)
        var tokenHeaders:[String:String]! = [:]
        
        tokenHeaders = ["x-access-token" :"\(DataManager.token ?? "")"]
        
        
        let parameters = tokenHeaders
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 1000
        
        manager.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(data!, withName: "media", fileName: "media.jpeg", mimeType: "media/jpeg")
            for (key, value) in parameters! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:baseUrl,headers:parameters)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    SVProgressHUD.showProgress(Float(Progress.fractionCompleted))
                })
                
                upload.responseJSON { response in
                    //self.delegate?.showSuccessAlert()
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    //                        self.showSuccesAlert()
                    //self.removeImage("frame", fileExtension: "txt")
                    SVProgressHUD.dismiss()
                    switch response.result{
                    case .success(_):
                        print("successfull")
                    case.failure(let error):
                        print(error)
                        if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                           // NotificationAlert().NotificationAlert(titles: "Your Request timeout!")
                            let urlModel = ["status":"failure"]
                            callback(urlModel as Dictionary<String, AnyObject>, nil)
                            
                        }else{
                            
                            NotificationAlert().NotificationAlert(titles: "The Internet connection appears to be offline")
                            let urlModel = ["status":"failure"]
                            callback(urlModel as Dictionary<String, AnyObject>, nil)
                        }
                    }
                    
                    if let value = response.result.value {
                        print(response.result.value)
                        if let result = value as? Dictionary<String, AnyObject> {
                            let urlModel = result[APIConstants.data.rawValue]
                            print(urlModel)
                            callback(result , nil )
                        }
                    }
                }
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                SVProgressHUD.dismiss()
                if encodingError._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                }
                print(encodingError)
            }
            
        }
        
    }
    
    
    func uploadImageaBeforeAfterRemote(image:UIImage,vehicleId:Int,Url:String, callback:@escaping (_ data: Dictionary<String,AnyObject>, _ error: NSError? ) -> Void){
        
        
        //    let imageData = UIImageJPEGRepresentation(image, 1.0)
        //    let imageInfo : UIImage = UIImage(data: imageData!)!
        
        let baseUrl = baseUrl1 + Url
        
        
        let data = image.jpegData(compressionQuality: imageQuality)
        var tokenHeaders:[String:String]! = [:]
        
        tokenHeaders = ["x-access-token" :"\(DataManager.token ?? "")"]
        
        
        let parameters = tokenHeaders
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 1000
        
        manager.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(data!, withName: "media", fileName: "media.jpeg", mimeType: "media/jpeg")
            for (key, value) in parameters! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:baseUrl,headers:parameters)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    SVProgressHUD.showProgress(Float(Progress.fractionCompleted))
                })
                
                upload.responseJSON { response in
                    //self.delegate?.showSuccessAlert()
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    //                        self.showSuccesAlert()
                    //self.removeImage("frame", fileExtension: "txt")
                    SVProgressHUD.dismiss()
                    switch response.result{
                    case .success(_):
                        print("successfull")
                    case.failure(let error):
                        print(error)
                        if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                           // NotificationAlert().NotificationAlert(titles: "Your Request timeout!")
                            let urlModel = ["status":"failure"]
                            callback(urlModel as Dictionary<String, AnyObject>, nil)
                            
                        }else{
                            
                            NotificationAlert().NotificationAlert(titles: "The Internet connection appears to be offline")
                            let urlModel = ["status":"failure"]
                            callback(urlModel as Dictionary<String, AnyObject>, nil)
                        }
                    }
                    
                    if let value = response.result.value {
                        print(response.result.value)
                        if let result = value as? Dictionary<String, AnyObject> {
                            let urlModel = result[APIConstants.data.rawValue]
                            print(urlModel)
                            callback(result , nil )
                        }
                    }
                }
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                SVProgressHUD.dismiss()
                if encodingError._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                }
                print(encodingError)
            }
            
        }
        
    }
    
    //
    
    //MARK:upload image to server and returns dictionary containing url and image DATA
    
    func uploadVideoRemote (postID:String = "5ba4bfb8e436301a7fc38019",videoURL:NSURL, callback:@escaping (_ data: Dictionary<String, String>?, _ error: NSError? ) -> Void){
        
        
        //    let imageData = UIImageJPEGRepresentation(image, 1.0)
        //    let imageInfo : UIImage = UIImage(data: imageData!)!
        
        let baseUrl = baseUrl1 + self.imageUrl + "\(postID)"
        
        //  let data = UIImageJPEGRepresentation(image,1)
        
        
        var data: NSData?
        do {
            data = try NSData(contentsOfFile: (videoURL.relativePath)!, options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            data = nil
            return
        }
        let parameters = ["x-access-token" :"\(DataManager.token ?? "")"]
        
        //let imageData = UIImageJPEGRepresentation(image, 1.0)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(data as! Data, withName: "media", fileName: "media.mov", mimeType: "video/mov")
            
            multipartFormData.append(("video" as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: "mediaType")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:baseUrl,headers:parameters)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    //self.delegate?.showSuccessAlert()
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    
                    //                        self.showSuccesAlert()
                    //self.removeImage("frame", fileExtension: "txt")
                    
                    
                    if let value = response.result.value {
                        if let result = value as? Dictionary<String, AnyObject> {
                            let urlModel = result[APIConstants.data.rawValue] as! [String:String]
                            callback(urlModel , nil )
                        }
                    }
                }
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print(encodingError)
            }
        }
    }
}
