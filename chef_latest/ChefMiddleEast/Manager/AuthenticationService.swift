//
//  AuthenticationManager.swift
//  ChefMiddleEast
//
//  Created by sandeep on 12/12/21.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper


enum CallbackProtocal<T> {
    case onSuccess(data: T?)
    case onFailure(error: String)
}
enum CallbackProtocalItem<T> {
    case onSuccess(items: [T])
    case onFailure(error: String)
}
enum CallbackProtocolWithIntegerValueInside<T>{
    case onSuccessWithIntegerValue(data: Int)
    case onFailure(error: String)
}

enum CallbackProtocolWithStringValueInside<T> {
    case onSuccessWithStringValue(data: String)
    case onFailure(error: String)
}


enum AuthenticationService:APIService {

    
    case login(params: UserModel)
    case verifyEmailSendOTP
    case verifyPhoneSendOTP
    case forgetPassword(params: UserModel)
    case resetPassword(params: UserModel)
    case changePassword(params: UserModel)
    case getCountryList
    case getCityList
    case signUP(params: SignUpModel)
    case addAddress(params: AddressModel)
    case getAddress
    case updateAddress(params: AddressModel)
    case updateProfile(params: UpdateProfileModel)
    case getNotificationList
    case readNotification
    case updateFCMToken
    case clearCart
    case guestSignUp
    case sendEmailToSupport(params: CustomerSupportModel)
    case getUserProfile

  
    var path: String {
        var path = ""
        switch self {
        case .login:
            path = EndPoint.BASE_API_URL + ("Auth/signin")
            return path
            
        case .guestSignUp:
            path = EndPoint.BASE_API_URL + ("Auth/guestSignup")
            return path
            
        case .signUP:
            path = EndPoint.BASE_API_URL + ("Auth/signup")
            return path
         
        case .verifyEmailSendOTP:
                        
            let verifyEmailURL = String(format: "Auth/VerifyEmailSendOTP?email=%@&checkExisting=%d", EndPoint.email,EndPoint.checkExisting)
            path = EndPoint.BASE_API_URL + verifyEmailURL

            return path
            
        case .verifyPhoneSendOTP:
            let verifyPhoneURL = String(format: "Auth/VerifyPhoneSendOTP?phoneNumber=%@&checkExisting=%d", EndPoint.phone,EndPoint.checkExisting)
            path = EndPoint.BASE_API_URL + verifyPhoneURL
            return path
        case .forgetPassword:
            path = EndPoint.BASE_API_URL + ("Auth/ForgotPassword")
            return path
            
        case .resetPassword:
            path = EndPoint.BASE_API_URL + ("Auth/ResetPassword")
            return path
            
        case .changePassword:
            path = EndPoint.BASE_API_URL + ("ChangePassword/changePassword")
            return path
        case .getCountryList:
            path = EndPoint.BASE_API_URL + ("Auth/getCountriesList")
            return path
            
        case .getCityList:
            path = EndPoint.BASE_API_URL + ("Auth/getCitiesList?countryId=1")
            return path
            
        case .addAddress:
            path = EndPoint.BASE_API_URL + ("CustomerAddress/addAddress")
            return path
            
        case .getAddress:
            path = EndPoint.BASE_API_URL + ("CustomerAddress/getAddressList?userId=") + EndPoint.userId
            return path
            
        case .getNotificationList:
            path = EndPoint.BASE_API_URL + ("Notification/getNotificationList?userId=") + EndPoint.userId
            return path
        case .readNotification:
            path = EndPoint.BASE_API_URL + ("Notification/readNotification?userId=") + EndPoint.userId
            return path
            
        case .updateAddress:
            path = EndPoint.BASE_API_URL + ("CustomerAddress/editAddress")
            return path
        
        case .getUserProfile:
            path = EndPoint.BASE_API_URL + ("Auth/userDetail?userId=") + EndPoint.userId
            return path
            
        case .updateProfile:
            path = EndPoint.BASE_API_URL + ("Auth/updateProfile")
            return path
            
        case .clearCart:
            path = EndPoint.BASE_API_URL + ("Cart/clearCart?userId=") + EndPoint.userId
            return path
            
        case .sendEmailToSupport:
            path = EndPoint.BASE_API_URL + ("AppInfo/sendEmailToSupport")
            return path
            
        case .updateFCMToken:
            
            let tokenURL = String(format: "Auth/updateFCMToken?userId=%@&token=%@", EndPoint.userId, EndPoint.fcmToken)
            print("EndPoint.fcmToken")
            
            path = EndPoint.BASE_API_URL + tokenURL
            print(path)
            return path
            
      /*      case .CreateComment:
            path = APIManager.BASE_API_URL + ("/communityComments")
            return path
        case .CommunityGetID:
            path = APIManager.BASE_API_URL + ("/communities") + APIManager.userId
            return path
            
        case .SearchCommunity:
            path = APIManager.BASE_API_URL + ("/communities?serverPaging=false&")
            return path
            
        case .likeUnlike:
            path = APIManager.BASE_API_URL + ("/communityLikes")
            return path*/
        }
    }
    
    var resource: Resource {
        var resource: Resource!
        
        switch self {
        
        case .login(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;
            
        case .sendEmailToSupport(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;
            
        case .guestSignUp:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .signUP(let params):
            print("params")
            print(params)
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;
            
        case .verifyEmailSendOTP:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .clearCart:
            resource = Resource(method: .GET, parameters: nil)
            return resource;
            
        case .verifyPhoneSendOTP:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .forgetPassword(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String: AnyObject])
            return resource;
            
        case .resetPassword(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String: AnyObject])
            return resource;
            
        case .changePassword(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String: AnyObject])
            return resource;
            
        case .getCountryList:
            resource = Resource(method: .GET, parameters: nil)
            return resource;
            
        case .getCityList:
            resource = Resource(method: .GET, parameters: nil)
            return resource;
            
        case .getNotificationList:
            resource = Resource(method: .GET, parameters: nil)
            return resource;
            
        case .readNotification:
            resource = Resource(method: .GET, parameters: nil)
            return resource;
            
        case .addAddress(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String: AnyObject])
            return resource;
            
        case .getAddress:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .updateFCMToken:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
            
        case .updateAddress(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String: AnyObject])
            return resource;
            
        case .updateProfile(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String: AnyObject])
            return resource;
            
        case .getUserProfile:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
            
            
        
     /*       case .CreateComment(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;
        case .CommunityGetID:
            resource = Resource(method: .GET, parameters: nil)
            return resource;
        case .SearchCommunity:
            resource = Resource(method: .GET, parameters: nil)
            return resource;
         
         case .updateUserData(let params):
             resource = Resource(method: .PUT, parameters: params.toJSON() as [String : AnyObject])
             return resource;
           
        case .likeUnlike(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;*/
        }
    }
}
extension EndPoint {
    
    //MARK:- LogIn
    class func LogIn<T: Mappable>(params: UserModel, t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        AuthenticationService.login(params: params).request(t:  UserModel.self) { (result) in
            switch result {
            case .success(let result):
                
                callback(.onSuccess(data: result.data as? T))
                
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- Send Email To Support
    class func SendEmailToSupport<T: Mappable>(params: CustomerSupportModel, t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        AuthenticationService.sendEmailToSupport(params: params).request(t:  CustomerSupportModel.self) { (result) in
            switch result {
            case .success(let result):
                
                callback(.onSuccess(data: result.data as? T))
                
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- Sign Up
    class func SignUP<T: Mappable>(params: SignUpModel, t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        AuthenticationService.signUP(params: params).request(t:  UserModel.self) { (result) in
            switch result {
            case .success(let result):
                
                callback(.onSuccess(data: result.data as? T))
                
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- Guest Sign Up
    class func GuestSignUP<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        AuthenticationService.guestSignUp.request(t:  UserModel.self) { (result) in
            switch result {
            case .success(let result):
                
                callback(.onSuccess(data: result.data as? T))
                
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
  
    //MARK:- VerifyEmailSendOTP
    class func VerifyEmailSendOTP<T: Mappable>( t: T.Type, callback: @escaping (CallbackProtocolWithIntegerValueInside<T>)->Void) {
        AuthenticationService.verifyEmailSendOTP.request(t: UserModel.self) { (result) in
            switch result {
            case .success(let user):
                callback(.onSuccessWithIntegerValue(data: user.integerData!))
                //callback(.onSuccess(data: user.message as? T))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- Clear Cart
    class func clearCart<T: Mappable>( t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        AuthenticationService.clearCart.request(t: AddToCartModel.self) { (result) in
            switch result {
            case .success(let user):
                callback(.onSuccessWithStringValue(data: user.message!))
                //callback(.onSuccess(data: user.message as? T))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    
    //MARK:- VerifyPhoneSendOTP
    class func VerifyPhoneSendOTP<T: Mappable>( t: T.Type, callback: @escaping (CallbackProtocolWithIntegerValueInside<T>)->Void) {
        AuthenticationService.verifyPhoneSendOTP.request(t: UserModel.self) { (result) in
            switch result {
            case .success(let user):
                callback(.onSuccessWithIntegerValue(data: user.integerData!))
                //callback(.onSuccess(data: user.message as? T))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    
    //MARK:- ForgetPassword
    class func ForgetPassword<T: Mappable>( params: UserModel,t: T.Type, callback: @escaping (CallbackProtocolWithIntegerValueInside<T>)->Void) {
        AuthenticationService.forgetPassword(params: params).request(t: UserModel.self) { (result) in
            switch result {
            case .success(let user):
                callback(.onSuccessWithIntegerValue(data: user.integerData!))
            //    callback(.onSuccess(data: user.strData as? T))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- ResetPassword
    class func ResetPassword<T: Mappable>( params: UserModel,t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        AuthenticationService.resetPassword(params: params).request(t: UserModel.self) { (result) in
            switch result {
            case .success(let user):
              //  callback(.onSuccess(data: user.message as? T))
                callback(.onSuccessWithStringValue(data: user.message!))

            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    
    //MARK:- Change Password
    class func ChangePassword<T: Mappable>( params: UserModel,t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        AuthenticationService.changePassword(params: params).request(t: UserModel.self) { (result) in
            switch result {
            case .success(let user):
               // callback(.onSuccess(data: user.message as? T))
                callback(.onSuccessWithStringValue(data: user.message!))
            case .failure(let errorMsg):
                print(errorMsg)
                callback(.onFailure(error: errorMsg))
            }
        }
    }
    
    //MARK:- Add Address
    class func AddAddress<T: Mappable>( params: AddressModel,t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        AuthenticationService.addAddress(params: params).request(t: AddressModel.self) { (result) in
            switch result {
            case .success(let user):
               // callback(.onSuccess(data: user.message as? T))
                callback(.onSuccessWithStringValue(data: user.message!))
            case .failure(let errorMsg):
                print(errorMsg)
                callback(.onFailure(error: errorMsg))
            }
        }
    }
    
    //MARK:- Update Address
    class func UpdateAddress<T: Mappable>( params: AddressModel,t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        AuthenticationService.updateAddress(params: params).request(t: AddressModel.self) { (result) in
            switch result {
            case .success(let user):
               // callback(.onSuccess(data: user.message as? T))
                callback(.onSuccessWithStringValue(data: user.message!))
            case .failure(let errorMsg):
                print(errorMsg)
                callback(.onFailure(error: errorMsg))
            }
        }
    }
    
    //MARK:- Update Profile
    class func UpdateProfile<T: Mappable>( params: UpdateProfileModel,t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        AuthenticationService.updateProfile(params: params).request(t: UpdateProfileModel.self) { (result) in
            switch result {
            case .success(let user):
                callback(.onSuccessWithStringValue(data: user.message!))
            case .failure(let errorMsg):
                print(errorMsg)
                callback(.onFailure(error: errorMsg))
            }
        }
    }
    
    //MARK:- Get User Profile
    class func GetProfile<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        AuthenticationService.getUserProfile.request(t: GetProfileModel.self) { (result) in
            switch result {
            case .success(let result):
                callback(.onSuccess(data: result.data as? T))
            case .failure(let errorMsg):
                print(errorMsg)
                callback(.onFailure(error: errorMsg))
            }
        }
    }
    
    //MARK:- Get Address
    class func GetAddress<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        AuthenticationService.getAddress.request(t: AddressModel.self) { (result) in
            switch result {
            case .success(let result):
               // callback(.onSuccess(data: user.message as? T))
                callback(.onSuccess(items: result.items as? [T] ?? []))
            case .failure(let errorMsg):
                print(errorMsg)
                callback(.onFailure(error: errorMsg))
            }
        }
    }
    
    //MARK:- Update FCM Token
    class func UpdateFCMToken<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        AuthenticationService.updateFCMToken.request(t: NotificationsModel.self) { (result) in
            switch result {
            case .success(let result):
               // callback(.onSuccess(data: user.message as? T))
                callback(.onSuccessWithStringValue(data: result.message!))
            case .failure(let errorMsg):
                print(errorMsg)
                callback(.onFailure(error: errorMsg))
            }
        }
    }
    
    //MARK:- Get Notification List
    class func GetNotification<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        AuthenticationService.getNotificationList.request(t: NotificationsModel.self) { (result) in
            switch result {
            case .success(let result):
               // callback(.onSuccess(data: user.message as? T))
                callback(.onSuccess(items: result.items as? [T] ?? []))
            case .failure(let errorMsg):
                print(errorMsg)
                callback(.onFailure(error: errorMsg))
            }
        }
    }
    
    //MARK:- Read Notification
    class func ReadNotification<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        AuthenticationService.readNotification.request(t: NotificationsModel.self) { (result) in
            switch result {
            case .success(let result):
                print(result.data)
               // callback(.onSuccess(data: user.message as? T))
                callback(.onSuccess(items: result.items as? [T] ?? []))
            case .failure(let errorMsg):
                print(errorMsg)
                callback(.onFailure(error: errorMsg))
            }
        }
    }
    
    
    //MARK:- Get Country List
    class func GetCountryList<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        AuthenticationService.getCountryList.request(t: CountryListModel.self) { (result) in
            switch result {
            
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))

            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- Get Country List
    class func GetCityList<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        AuthenticationService.getCityList.request(t: CitiesListModel.self) { (result) in
            switch result {
            
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))

            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
   /* class func SearchCommunity<T: Mappable>( t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        CommunityAPIServices.SearchCommunity.request(t: CommunityModel.self) { (result) in
            switch result {
            case .success(let user):
                callback(.onSuccess(items: user.items as? [T] ?? []))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    
    */
    
}


