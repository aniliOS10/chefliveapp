//
//  Constants.swift
//  ChefMiddleEast
//
//  Created by sandeep on 12/12/21.
//

import Foundation
struct Constants {
    
    static let deviceToken = "deviceToken"
    static let token = "token"
    static let userId = "userId"
    static let enterpriseId = "enterpriseId"
    static let phone = "phone"
    static let email = "email"
    static let name = "name"
    static let fcmToken = "fcmToken"
    static let status = "status"
    static let ProfileImg = "ProfileImg"
    static let NotificationOnOff = "NotificationOnOff"

}
