//
//  DataManager.swift
//  ChefMiddleEast
//
//  Created by sandeep on 12/12/21.
//

import Foundation
class DataManager {
    
    static var deviceToken:String? {
        set {
            UserDefaults.setValue(newValue, forKey: Constants.deviceToken)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: Constants.deviceToken)
        }
    }
    
    static var token:String? {
        set {
            UserDefaults.setValue(newValue, forKey: Constants.token)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: Constants.token)
        }
    }
}
