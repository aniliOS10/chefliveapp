//
//  DashBoardManager.swift
//  ChefMiddleEast
//
//  Created by sandeep on 12/12/21.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper


enum DashBoardManager:APIService {
    
    
    case dashBoard
    case collectionList(params: CollectionListModel)
    case recipeList(params: CollectionListModel)
    case markRecipeFavourite
    case markRecipeBookmark
    case recipeGetFavouritList
    case recipeGetBookmarkList
    case recipeDetail
    case getFavouritList
    case allProductList(params: CollectionListModel)
    case productDetail
    case addToCart(params: AddToCartModel)
    case getCartList(params: AddToCartModel)
    case updateQuantity(params: AddToCartModel)
    case afterCart
    case cartCount
    case couponDiscount
    case getAppInfo
    case getPrivacyPolicy
    case getFaqList
    case getTermsAndConditions
    case getOrderList
    case getOrderDetail
    case cancelOrder
    case createOrder(params: CreateOrderModel)
    case onlinePayment(params: OnlinePaymentModel)
    case generateSalesOrderId
    case getCategoryFeatureImage
    case getCategoryList
    case applyCoupon(params: ApplyCoupanModel)
    case getInvoice
    case notificationSetting(params: SettingModel)

    
    var path: String {
        var path = ""
        
        switch self {
        
        case .dashBoard:
            path = EndPoint.BASE_API_URL + ("Dashboard/getDashboardData?deviceType=") + EndPoint.deviceType + "&userId=" + EndPoint.userId
            return path
        
        case .collectionList:
            path = EndPoint.BASE_API_URL + ("Collection/CollectionList")
            return path
            
        case .getCategoryList:
            path = EndPoint.BASE_API_URL + ("Category/categoryList")
            return path
            
        case .recipeList:
            path = EndPoint.BASE_API_URL + ("Recipe/RecipesList?searchQuery=") + EndPoint.searchText
            return path
            
        case .markRecipeFavourite:
            path = EndPoint.BASE_API_URL + ("Recipe/markRecipeFavourite?recipeId=") + EndPoint.recipeId + "&favouriteStatus=" + EndPoint.favouriteStatus + "&userId=" + EndPoint.userId
            return path
            
        case .markRecipeBookmark:
            path = EndPoint.BASE_API_URL + ("Recipe/markRecipeBookmark?recipeId=") + EndPoint.recipeId + "&bookmarkStatus=" + EndPoint.bookmarkStatus + "&userId=" + EndPoint.userId
            return path
            
        case .recipeGetFavouritList:
            path = EndPoint.BASE_API_URL + ("Recipe/getFavouriteList?") + EndPoint.favouriteType
            return path
            
        case .recipeGetBookmarkList:
            path = EndPoint.BASE_API_URL + ("Recipe/getBookmarkList")
            return path
            
        case .recipeDetail:
            path = EndPoint.BASE_API_URL + ("Recipe/GetRecipeDetail?recipeId=") + EndPoint.recipeId + "&deviceType=" + EndPoint.deviceType
            return path

        case .getFavouritList:
            path = EndPoint.BASE_API_URL + ("Recipe/getFavouriteList?favouriteType=") + EndPoint.favouriteType + "&userId=" + EndPoint.userId
            return path
            
        case .allProductList:
            path = EndPoint.BASE_API_URL + ("Product/GetProductsList")
            return path
            
        case .productDetail:
            path = EndPoint.BASE_API_URL + ("Product/GetProductDetail?deviceType=") + EndPoint.deviceType + "&productId=" + EndPoint.userId
            return path
            
        case .getOrderList:
            path = EndPoint.BASE_API_URL + ("Order/getOrderList?filterWith=") + EndPoint.filterWith + "&userId=" + EndPoint.userId
            return path
            
        case .getOrderDetail:
            path = EndPoint.BASE_API_URL + ("Order/getOrderDetail?salesOrderId=") + EndPoint.salesOrderId + "&userId=" + EndPoint.userId
            return path
            
        case .addToCart:
            path = EndPoint.BASE_API_URL + ("Cart/addToCart")
            return path
            
        case .getCartList:
            path = EndPoint.BASE_API_URL + ("Cart/getCartList")
            return path
            
        case .updateQuantity:
            path = EndPoint.BASE_API_URL + ("Cart/updateQuantity")
            return path
            
        case .cartCount:
            path = EndPoint.BASE_API_URL + ("Cart/count?userId=") + EndPoint.userId
            return path
            
        case .afterCart:
            path = EndPoint.BASE_API_URL + ("Cart/afterCart?userId=") + EndPoint.userId + "&currentDateTime=" + EndPoint.currentDateTime
            return path
            
        case .couponDiscount:
            path = EndPoint.BASE_API_URL + ("Coupon/couponDiscount?couponCode=") + EndPoint.couponCode
            return path
            
        case .getAppInfo:
            path = EndPoint.BASE_API_URL + ("AppInfo/getAppInfo")
            return path
            
        case .getPrivacyPolicy:
            path = EndPoint.BASE_API_URL + ("PrivacyPolicy/getPrivacyPolicy")
            return path
            
        case .getFaqList:
            path = EndPoint.BASE_API_URL + ("Faq/getFaqList")
            return path
            
        case .getTermsAndConditions:
            path = EndPoint.BASE_API_URL + ("TermsAndConditions/getTermsAndConditions")
            return path
        case .generateSalesOrderId:
            path = EndPoint.BASE_API_URL + ("Order/generateSalesOrderId")
            return path
        case .cancelOrder:
            path = EndPoint.BASE_API_URL + ("Order/cancelOrder?salesOrderId=") + EndPoint.salesOrderId + "&dataOrigin=" + EndPoint.dataOrigin
            return path
            
        case .createOrder:
            path = EndPoint.BASE_API_URL + ("Order/createOrder")
            return path
            
        case .onlinePayment:
            path = EndPoint.BASE_API_URL + ("Payment/payment")
            return path
            
        case .getCategoryFeatureImage:
            path = EndPoint.BASE_API_URL + ("AdminFeatureBanner/featureBannerCategoryList")
            return path
            
        case .applyCoupon:
            path = EndPoint.BASE_API_URL + ("Coupon/applyCoupon?couponCode")
            //+ EndPoint.couponCode + "&cartId=" + String("\(EndPoint.cartId)")
            return path
            
            
        case .getInvoice:
            path = EndPoint.BASE_API_URL + ("Order/getInvoice") + "?salesOrderId=" + EndPoint.salesOrderId + "&userId=" + EndPoint.userId
            return path
            
        case .notificationSetting:
            path = EndPoint.BASE_API_URL + ("Notification/setNotificationStatus")
            return path
            
            
        }
    }
    
    var resource: Resource {
        var resource: Resource!
        
        switch self {
        
        case .dashBoard:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
        
        case .collectionList(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;
            
        case .recipeList(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;
            
        case .markRecipeFavourite:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .markRecipeBookmark:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .recipeGetFavouritList:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .recipeGetBookmarkList:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .recipeDetail:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .getFavouritList:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .allProductList(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;
            
        case .productDetail, .getOrderList, .getOrderDetail:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .addToCart(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;
            
        case .getCartList(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;
            
        case .updateQuantity(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;
            
        case .afterCart:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .cartCount:
            resource = Resource(method: .GET, parameters: nil)
            return resource;
            
        case .getCategoryList:
            resource = Resource(method: .GET, parameters: nil)
            return resource;
            
        case .couponDiscount:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .getAppInfo:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .getPrivacyPolicy, .getTermsAndConditions, .generateSalesOrderId:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
       
        case . getFaqList:
            resource = Resource(method: .GET, parameters: nil)
            return resource;
       
        case .cancelOrder:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .createOrder(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;
       
        case .onlinePayment(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource;
            
        case .getCategoryFeatureImage:
             resource = Resource(method: .POST, parameters: nil)
            return resource
            
        case .applyCoupon(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource
            
            
        case .getInvoice:
            resource = Resource(method: .POST, parameters: nil)
            return resource;
            
        case .notificationSetting(let params):
            resource = Resource(method: .POST, parameters: params.toJSON() as [String : AnyObject])
            return resource
            
            
        }
    }
}

extension EndPoint {
    
    //MARK:- DashBoard
    class func DashBoard<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        DashBoardManager.dashBoard.request(t:  DashBoardModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(data: result.itemsData as? T ))
                
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    
    //MARK:- Collection List
    class func CollectionList<T: Mappable>(params: CollectionListModel,t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        DashBoardManager.collectionList(params: params).request(t:  CollectionListModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))
                
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  Recipe List
    class func RecipeList<T: Mappable>(params: CollectionListModel,t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        DashBoardManager.recipeList(params: params).request(t:  RecipeListModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))
                
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  Get All Category List
    class func GetAllCategoryList<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        DashBoardManager.getCategoryList.request(t:  CategoryListingModel.self) { (result) in
            switch result {
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  Get All Product List
    class func AllProductList<T: Mappable>(params: CollectionListModel,t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        DashBoardManager.allProductList(params: params).request(t:  NewArrivalsModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    
    
    //MARK:- Mark Recipe Favourite
    class func MarkRecipeFavourite<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        DashBoardManager.markRecipeFavourite.request(t:  CollectionListModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))
                
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- Mark Recipe Bookmark
    class func MarkRecipeBookmark<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        DashBoardManager.markRecipeBookmark.request(t:  CollectionListModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))
                
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- Recipe Get FavouritList
    class func RecipeGetFavouritList<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        DashBoardManager.recipeGetFavouritList.request(t:  CollectionListModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))
                
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- Recipe Get Bookmark List
    class func RecipeGetBookmarkList<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        DashBoardManager.recipeGetBookmarkList.request(t:  CollectionListModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))
                
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- Recipe Detail
    class func RecipeDetail<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        DashBoardManager.recipeDetail.request(t:  RecipeDetailModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(data: result.data as? T))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    
    //MARK:- Product Detail
    class func ProductDetail<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        DashBoardManager.productDetail.request(t:  NewArrivalsModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(data: result.data as? T))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- Product Detail
    class func getOrderList<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        DashBoardManager.getOrderList.request(t:  MyOrdersModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- Get Order Detail
    class func GetOrderDetail<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        DashBoardManager.getOrderDetail.request(t:  OrderDetailModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(data: result.data as? T))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:- Get Favourit List
    class func GetFavouritList<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        DashBoardManager.getFavouritList.request(t:  FavouriteModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    
    //MARK:-  Add To Cart
    class func AddToCart<T: Mappable>(params: AddToCartModel,t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        DashBoardManager.addToCart(params: params).request(t:  AddToCartModel.self) { (result) in
            
            switch result {
            case .success(let result):
              //  callback(.onSuccess(items: result.items as? [T] ?? []))
                callback(.onSuccessWithStringValue(data: result.message!))

            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  Get Cart List
    class func GetCartList<T: Mappable>(params: AddToCartModel,t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        DashBoardManager.getCartList(params: params).request(t:  AddToCartModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(data: result.itemsData as? T))

            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  Update Quantity
    class func UpdateQuantity<T: Mappable>(params: AddToCartModel,t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        DashBoardManager.updateQuantity(params: params).request(t:  AddToCartModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccessWithStringValue(data: result.message!))

            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  After Count
    class func AfterCart<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        DashBoardManager.afterCart.request(t:  DeliverySummaryModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(data: result.data as? T))

            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  Create Count
    class func CreateOrder<T: Mappable>(params: CreateOrderModel,t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        DashBoardManager.createOrder(params: params).request(t:  CreateOrderModel.self) { (result) in
            
            switch result {
            case .success(let result):
                UserDefaults.standard.set(result.stringData, forKey: Constants.orderNumber)
                callback(.onSuccessWithStringValue(data: result.message!))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  Create Count
    class func OnlinePayment<T: Mappable>(params: OnlinePaymentModel,t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        DashBoardManager.onlinePayment(params: params).request(t:  OnlinePaymentModel.self) { (result) in
            switch result {
            case .success(let result):
                //UserDefaults.standard.set(result.stringData, forKey: Constants.orderNumber)
                callback(.onSuccessWithStringValue(data: result.message!))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    
    //MARK:-  Cart Count
    class func CartCount<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        DashBoardManager.cartCount.request(t: CartCountModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(data: result.itemsData as? T))

            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    
    //MARK:-  Coupon Discount 
    class func CouponDiscount<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        DashBoardManager.couponDiscount.request(t: CouponModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(data: result.data as? T))

            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  Coupon Discount
    class func ApplyCoupon<T: Mappable>(params: ApplyCoupanModel, t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        DashBoardManager.applyCoupon(params: params).request(t: AppliedCoupanDetail.self) { (result) in
            switch result {
            case .success(let result):
                callback(.onSuccess(data: result.data as? T))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    
    //MARK:-  Get App Info
    class func GetAppInfo<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocal<T>)->Void) {
        DashBoardManager.getAppInfo.request(t:  AppInfoModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(data: result.data as? T))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  Get Privacy Policy
    class func GetPrivacyPolicy<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        DashBoardManager.getPrivacyPolicy.request(t:  AppInfoModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccessWithStringValue(data: result.stringData!))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }

    //MARK:-  Get FAQ  List
    class func GetFaqList<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        DashBoardManager.getFaqList.request(t:  FAQModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }

    //MARK:-  Get Terms and Conditions
    class func GetTermsAndConditions<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        DashBoardManager.getTermsAndConditions.request(t:  AppInfoModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccessWithStringValue(data: result.stringData!))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    
    //MARK:-  Cancel Order
    class func CancelOrder<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        DashBoardManager.cancelOrder.request(t:  AddToCartModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccessWithStringValue(data: result.message!))

            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK: -  Get Category Feature Images
    class func GetCategoryFeatureImages<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocalItem<T>)->Void) {
        DashBoardManager.getCategoryFeatureImage.request(t:  CategoryFeatureBannerImages.self) { (result) in
            switch result {
            case .success(let result):
                callback(.onSuccess(items: result.items as? [T] ?? []))

            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  GenerateSalesOrderId
    class func GenerateSalesOrderId<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        DashBoardManager.generateSalesOrderId.request(t:  AppInfoModel.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccessWithStringValue(data: result.stringData!))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  GenerateSalesOrderId
    class func pdfSalesOrderId<T: Mappable>(t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        DashBoardManager.getInvoice.request(t:  BaseResponse.self) { (result) in
            
            switch result {
            case .success(let result):
                callback(.onSuccessWithStringValue(data: result.stringData ?? ""))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }
    
    //MARK:-  Coupon Discount
    class func setNotificationStatus<T: Mappable>(params: SettingModel, t: T.Type, callback: @escaping (CallbackProtocolWithStringValueInside<T>)->Void) {
        DashBoardManager.notificationSetting(params: params).request(t: BaseResponse.self) { (result) in
            switch result {
            case .success(let result):
                callback(.onSuccessWithStringValue(data: result.message!))
            case .failure(let error):
                print(error)
                callback(.onFailure(error: error))
            }
        }
    }

}


