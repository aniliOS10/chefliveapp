//
//  AppDelegate.swift
//  ChefMiddleEast
//
//  Created by Apple on 22/10/21.
//



import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import FirebaseMessaging
import FirebaseCore

var dictAppInfo = AppInfoModel()

@available(iOS 13.0, *)
@main
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {
    let notificationCenter = UNUserNotificationCenter.current()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        UIApplication.shared.applicationIconBadgeNumber = 0
        self.setParamForAllProductsListApi()
        self.getAppInfoAPI()
        registerForPushNotifications()

        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
           
           return .portrait;
       }
    
    func registerForPushNotifications() {
            if #available(iOS 10.0, *) {
                // For iOS 10 display notification (sent via APNS)
                notificationCenter.delegate = self
                let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                notificationCenter.requestAuthorization(
                    options: authOptions,
                    completionHandler: {_, _ in })
                // For iOS 10 data message (sent via FCM)
                Messaging.messaging().delegate = self
            } else {
                let settings: UIUserNotificationSettings =
                    UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                
                    UIApplication.shared.registerUserNotificationSettings(settings)
            }
        DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
      print("Firebase registration token: \(String(describing: fcmToken))")

      let dataDict: [String: String] = ["token": fcmToken ?? ""]
      NotificationCenter.default.post(
        name: Notification.Name("FCMToken"),
        object: nil,
        userInfo: dataDict
      )
    
        EndPoint.fcmToken = fcmToken ?? ""
        
    }
    
    internal func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      Messaging.messaging().apnsToken = deviceToken
    }
    
    //Receive Remote Notification on Background
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void)
    {
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }


    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        print(#function)
        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication,didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler:@escaping (UIBackgroundFetchResult) -> Void) {

           let state : UIApplication.State = application.applicationState
            if (state == .inactive || state == .background) {
                print("background")
            } else {
                print("foreground")
            }
    }
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response.notification.request.content.userInfo)
    }
    
    
    //MARK:- Param for All ProductsList Api
    func setParamForAllProductsListApi() {
        let params = CollectionListModel()
        params.pageNumber = 1
        params.pageSize = 3000
        self.getProductsList(Model: params) { sucess in }
    }
    
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
       
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            CoreDataManager.shared.deleteObject_string(name: Entity.user.rawValue)
            UserDefaults.standard.set(false, forKey: Constants.guestUser)
            UserDefaults.standard.removeObject(forKey: Constants.token)
            UserDefaults.standard.removeObject(forKey: Constants.userId)
            UserDefaults.standard.removeObject(forKey: Constants.userImg)
            UserDefaults.standard.removeObject(forKey: Constants.firstName)
            UserDefaults.standard.removeObject(forKey: Constants.lastName)
            UserDefaults.standard.removeObject(forKey: Constants.email)
            UserDefaults.standard.removeObject(forKey: Constants.phone)
            UserDefaults.standard.removeObject(forKey: Constants.custAccount)
            UserDefaults.standard.set(true, forKey: Constants.NotificationOnOff)
            UserDefaults.standard.set(false, forKey: Constants.NotificationOnOffSub)
            RootManager().setLoginRoot()
        }
    }
    

    // MARK: - Core Data stack

      lazy var persistentContainer: NSPersistentContainer = {
          /*
           The persistent container for the application. This implementation
           creates and returns a container, having loaded the store for the
           application to it. This property is optional since there are legitimate
           error conditions that could cause the creation of the store to fail.
          */
          let container = NSPersistentContainer(name: "ChefMiddleEast")
          container.loadPersistentStores(completionHandler: { (storeDescription, error) in
              if let error = error as NSError? {

                  fatalError("Unresolved error \(error), \(error.userInfo)")
              }
          })
          return container
      }()

      // MARK: - Core Data Saving support

      func saveContext () {
          let context = persistentContainer.viewContext
          if context.hasChanges {
              do {
                  try context.save()
              } catch {
                  // Replace this implementation with code to handle the error appropriately.
                  // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                  let nserror = error as NSError
                  fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
              }
          }
      }

    //MARK:- Product List Api
    func getProductsList(Model: CollectionListModel, response:((_ success:Bool)->Void)!) {
        EndPoint.AllProductList(params: Model, t: NewArrivalsModel.self) { [self] result in
               switch result {
               case .onSuccess(let items):
                   if items.count > 0 {
                       saveProductToLocal(product: items)
                       response(true)
                   }
               case .onFailure(let error):
                   print(error)
               }
           }
       }
    
    //MARK:- Get App Info  Api
    func getAppInfoAPI() {
        EndPoint.GetAppInfo(t: AppInfoModel.self) { result in
               switch result {
               case .onSuccess(let data):
                   if data != nil {
                    dictAppInfo = data!
                   }
               case .onFailure(let error):
                   print(error)
               }
           }
       }
    
    
    func saveProductToLocal(product:[NewArrivalsModel]) {
       // var arrProduct = [NewArrivalsModel]()
      //  let temp = product
      //  arrProduct = temp.filter{ $0.salesPrice ?? 0.0 > 0.0 }
        
        let sync = try! NSKeyedArchiver.archivedData(withRootObject: product.toJSON(), requiringSecureCoding: false)
        UserDefaults.standard.set(sync, forKey: Constants.ProductModel)
    }
}

