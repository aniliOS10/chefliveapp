//
//  AddressModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 25/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class AddressModel: BaseResponse{
    
    var city : String?
    var address : String?
    var countryName: String?
    var countryRegionId : String?
    var countryId : String?
    var AddAdresscountryId : String?

    var street : String?
    var descrptn : String?
    var isPrimary : Int?
    var userId: String?
    var taxGroup: String?
    var logisticsLocationRoleName : String?
    var dobName : String?
    var gender : String?

    //----------- aftar cart api response ---------------//

    var addressType: String?
    var customerAddressId: Int?

    
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        city <- map["city"]
        address <- map["address"]
        countryName <- map["countryName"]

        countryRegionId <- map["countryRegionId"]
        street <- map["street"]
        descrptn <- map["description"]
        countryId <- map["countryId"]
        AddAdresscountryId <- map["countryId"]
        isPrimary <- map["isPrimary"]
        userId <- map["userId"]
        taxGroup <- map["taxGroup"]
        logisticsLocationRoleName <- map["logisticsLocationRoleName"]
        
        //----------- aftar cart api response ---------------//

        
        addressType <- map["addressType"]
        customerAddressId <- map["customerAddressId"]

        
    }
}


class UpdateProfileModel: BaseResponse{

    var userId : String?
    var firstName : String?
    var lastName: String?
    var email: String?
    var phoneNumber: String?
    var birthDate: String?
    var gender: String?

    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        userId <- map["userId"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        email <- map["email"]
        phoneNumber <- map["phoneNumber"]
        birthDate <- map["birthDate"]
        gender <- map["gender"]

    }
}


class  GetProfileModel: BaseResponse {
    
  
    var firstName : String?
    var lastName: String?
    var email: String?
    var phoneNumber: String?
    var birthDate: String?
    var gender: String?
    var profilePicture : String?
    
    override init() {
         super.init()
    }
    convenience required init?(map: Map) {
        self.init()
    }
    override func mapping(map: Map) {
       
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        email <- map["email"]
        phoneNumber <- map["phoneNumber"]
        birthDate <- map["birthDate"]
        gender <- map["gender"]
        profilePicture <- map["profilePicture"]
    }
    
}
