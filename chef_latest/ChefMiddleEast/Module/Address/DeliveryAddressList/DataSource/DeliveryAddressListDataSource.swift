//
//  DeliveryAddressListDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import Foundation

import UIKit
@available(iOS 13.0, *)
@available(iOS 13.0, *)
class DeliveryAddressListDataSource:GenericDataSource<MockUp_DeliveryAddressList>,UITableViewDataSource,UITableViewDelegate{
   
    var didSelectRow: ((_ cell:UITableViewCell, _ indexPath:IndexPath) -> Void)?
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryListingSectionTableViewCell") as! DeliveryListingSectionTableViewCell
        cell.titleLabel.text = "Select Delivery Address"
    
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 40.0 :0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 15
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryListingTableViewCell") as! DeliveryListingTableViewCell
        Utility.shared.makeRoundCorner(layer: cell.cellViewRound.layer, color: UIColor.lightGray.withAlphaComponent(0.2), radius: 5.0,borderWidth: 1.0)
        
    
        return cell
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       // return 200
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath){
            self.didSelectRow!(cell,indexPath)
        }
       
    }
}
