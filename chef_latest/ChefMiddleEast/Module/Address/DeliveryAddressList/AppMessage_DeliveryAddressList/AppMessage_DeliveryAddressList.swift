//
//  AppMessage_DeliveryAddressList.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import Foundation
import  UIKit
class AppMessage_DeliveryAddressList: NSObject {
    struct Placeholder {
        static let SelectDeliveryAddress = NSLocalizedString("SelectDeliveryAddress", comment: "")
        static let Addresses = NSLocalizedString("Addresses", comment: "")
        
    }
}
