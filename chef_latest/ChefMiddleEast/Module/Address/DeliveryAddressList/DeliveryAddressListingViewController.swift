//
//  DeliveryAddressListingViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import UIKit

@available(iOS 13.0, *)
class DeliveryAddressListingViewController:BaseViewController {
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var search : UISearchBar!
    
    @IBOutlet weak var buttonSave: UIButton!
 
    @IBOutlet weak var buttonAddNewAddress: UIButton!
    
    
    @IBOutlet weak var segmentController: UISegmentedControl!
   private let dataSource:DeliveryAddressListDataSource?
    
    lazy var viewModel:DeliveryAddressListingViewModel = {
        let viewModel = DeliveryAddressListingViewModel(dataSource: self.dataSource)
        return viewModel
    }()
    init(dashBoardViewDataSource:DeliveryAddressListDataSource)
    {
        self.dataSource = dashBoardViewDataSource
        super.init(nibName: nil, bundle: nil)
    }
  
    convenience init(dataSourceCollectionView1:DeliveryAddressListDataSource) {
        self.init(dashBoardViewDataSource: dataSourceCollectionView1)
    }
    required init?(coder: NSCoder) {
         self.dataSource = DeliveryAddressListDataSource()
         super.init(coder: coder)
     }
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        search.searchBarStyle = .minimal
        search.backgroundColor = UIColor.white
        search.tintColor = UIColor.white
      
      if #available(iOS 13.0, *) {
          search.searchTextField.backgroundColor = UIColor().getSearchColor()
          search.searchTextField.textColor =  UIColor().getThemeColor()
          search.searchTextField.tintColor =  UIColor().getThemeColor()
          Utility.shared.makeRoundCorner(layer: search.searchTextField.layer, color:UIColor.white.withAlphaComponent(0.5), radius: 5)
       //   Utility.shared.makeShadowsOfView_roundCorner(view: search, shadowRadius: 4.0, cornerRadius: 30, borderWidth:0.0, borderColor: UIColor.lightGray)
          let white = UIColor.red
          search.searchTextField.background = UIImage.init(named: "imageV1") //white.imageWithColor(width: Int(self.view.bounds.width), height: Int(self.search.searchTextField.frame.height))
          
          
         
    } else {
          // Fallback on earlier versions
      }
        
        
      
        self.setSegmentControllerUI()
        
       
        
        self.tableView.delegate = self.dataSource
        self.tableView.dataSource = self.dataSource
        
        
        
        self.viewModel.dataSource?.data.addAndNotify(observer: self, completionHandler: { mockUp_Dashboard in
            self.tableView.reloadData()
        })
        self.viewModel.makeDashboardList()
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
                            
       // menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
      //  self.initBellButton()
       //bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
       
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: AppMessage_DeliveryAddressList.Placeholder.Addresses)
       
        
         didSelectRow()
        // Do any additional setup after loading the view.
    }
    @objc func menuButtonPressed(){
        self.sideMenuController?.revealMenu()
    }
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
    
    @IBAction func buttonSavePressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonNewAddressPressed(_ sender: Any) {
        let controller:AddNewAddressViewController =  UIStoryboard(storyboard: .Address).initVC()
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func setSegmentControllerUI(){
        if #available(iOS 13.0, *) {
        
          
          let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)]
          segmentController.setTitleTextAttributes(titleTextAttributes, for:.normal)

            let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)]
          segmentController.setTitleTextAttributes(titleTextAttributes1, for:.selected)
      } else {
            // Fallback on earlier versions
        }
          segmentController.tintColor = UIColor.white
          
    }
    @IBAction func segmentControllerValueChanged(_ sender: Any) {
       
            
    }
    func didSelectRow(){
        self.dataSource?.didSelectRow = { cell,indexPath in
            
            let controller:AddNewAddressViewController =  UIStoryboard(storyboard: .Address).initVC()
            controller.titleString = AppMessage_NewAddress.Placeholder.EditAddress
            controller.subTitleString = AppMessage_NewAddress.Placeholder.EditAddress
            controller.saveTitleString = AppMessage_NewAddress.Placeholder.UpdateAddress
            
            self.navigationController?.pushViewController(controller, animated: true)
            
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
