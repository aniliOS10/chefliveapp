//
//  AddNewAddressTogelTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import UIKit

class AddNewAddressTogelTableViewCell: UITableViewCell {

    
    @IBOutlet var makeAddressDefaultSwitch: UISwitch!
    @IBOutlet var lblMakeAddressPrimary: UILabel!
    
    @IBOutlet var vwHome: UIView!
    
    @IBOutlet var btnHome: UIButton!
    
    @IBOutlet var vwWork: UIView!
    
    @IBOutlet var btnWork: UIButton!
    
    @IBOutlet var vwOffice: UIView!
    
    @IBOutlet var btnOffice: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBAction func makeAddressDefaultSwitch(_ sender: UISwitch) {
        
    }
    
}
