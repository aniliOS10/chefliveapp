//
//  AddNewAddressSaveTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import UIKit

class AddNewAddressSaveTableViewCell: UITableViewCell {
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
