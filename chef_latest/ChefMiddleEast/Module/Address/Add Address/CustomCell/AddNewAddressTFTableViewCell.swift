//
//  AddNewAddressTFTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import UIKit

class AddNewAddressTFTableViewCell: UITableViewCell {
    
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var streetTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var cellCityViewRound: UIView!
    @IBOutlet weak var cellStreetViewRound: UIView!
    @IBOutlet weak var cellAddressViewRound: UIView!
    
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var cityButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
