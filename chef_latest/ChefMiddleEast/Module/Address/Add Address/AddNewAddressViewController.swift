//
//  AddNewAddressViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import UIKit
import DropDown

protocol EditNewAddressViewControllerDelegate {
    func EditAddress(model: AddressModel)
}
class AddNewAddressViewController: BaseViewController {
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var search : UISearchBar!
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var segmentController: UISegmentedControl!
    
    @IBOutlet var countryPicker: UIPickerView!
    @IBOutlet weak var VWCountryPicker: UIView!
    var arrCountryList = [CountryListModel]()
    var arrCityList = [CitiesListModel]()

    @IBOutlet var selectedTF: UITextField!

    var titleString:String = "New address"
    var subTitleString:String = AppMessage_NewAddress.Placeholder.AddAddress
    var saveTitleString:String = AppMessage_NewAddress.Placeholder.SaveAddress
   
    var isEdit:Bool = false
    var dictPasswordInfo = [String:String]()

    var dictAddress = AddressModel()
    var screenComeFrom = String()
    var selectedCountryName = String()
    var selectedCountryID = String()
    var selectedCountryCode = String()

    var selectedCity = String()
    var selectedAddressType = String()
    var selectedCityCode = String()
        
    var delegate : EditNewAddressViewControllerDelegate!
    let dropCountry = DropDown()
    var dropView = UIView()
    //MARK:- VC Life Cycle
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.GetCountryList()
        GetCityList()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        let screenSize: CGRect = UIScreen.main.bounds
        self.dropView = UIView(frame: CGRect(x: 40, y: 200, width: screenSize.width - 80, height: 0))
        self.view.addSubview(dropView)
        
        if dictPasswordInfo.isEmpty {
            self.setUpIntialData()
        }
        
        if screenComeFrom == "EditButtonTap" {
            selectedAddressType = dictAddress.addressType ?? ""
            selectedCountryName = dictAddress.countryName ?? ""
            selectedCity = dictAddress.city ?? ""
            selectedCityCode = dictAddress.taxGroup ?? ""
            selectedCountryCode = dictAddress.countryId ?? "UAE"
            if selectedCountryCode == "" || selectedCountryCode == nil{
                selectedCountryCode = "UAE"
            }
            if dictAddress.isPrimary == 1 {
                dictPasswordInfo["isPrimary"] = "1"
            }
            else{
                dictPasswordInfo["isPrimary"] = "0"
            }
        }
        self.countryPicker.delegate = self
        self.countryPicker.dataSource = self
        self.countryPicker.isHidden = false
        self.setNAvigation()
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
    
    
    func setupCountryDropDown(_ button: UIButton, _ index : Int) {
        
        if index == 0 {
        
            var countryArray = [String]()
            var countryArrayID = [String]()
            var selectedCountryCodeArray = [String]()

            
            for i in 0..<arrCountryList.count
            {
                let dict =  arrCountryList[i]
                countryArray.append(dict.countryName ?? "United Arab Emirates")
                countryArrayID.append(String(dict.countryId ?? 1))
                selectedCountryCodeArray.append(dict.countryCode ?? "UAE")

            }
      
            
            if countryArray.count == 0 {
                self.GetCountryList()
                return
            }
            
            dropCountry.anchorView = dropView
            dropCountry.dataSource = countryArray
            dropCountry.selectionAction = { [weak self] (index, item) in
                self?.selectedCountryName = item
                self?.selectedCountryID = String(countryArrayID[index])
                self?.selectedCountryCode = selectedCountryCodeArray[index]
                self?.tableView.reloadData()
            }
            
            dropCountry.show()
        }
        else if index == 1 {
            
            var selectedCityArray = [String]()
            var selectedCityCodeArray = [String]()

            for i in 0..<arrCityList.count
            {
                let dict =  arrCityList[i]
                selectedCityArray.append(dict.cityName ?? "Abu Dhabi")
                selectedCityCodeArray.append(dict.cityCode ?? "C-L-ADB-N")

            }
            
            if selectedCityArray.count == 0 {
                GetCityList()
                return
            }
            
            dropCountry.anchorView = dropView
            dropCountry.dataSource = selectedCityArray
            dropCountry.selectionAction = { [weak self] (index, item) in
                self?.selectedCity = item
                self?.selectedCityCode = selectedCityCodeArray[index]

                self?.tableView.reloadData()
            }
            dropCountry.show()
        }
    }
    
    @IBAction func Gender(_ sender: Any) {
        dropCountry.show()
       }
    
    func pickUp(_ textField : UITextField){

        // UIPickerView
        self.countryPicker = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.countryPicker.delegate = self
        self.countryPicker.dataSource = self
        self.countryPicker.backgroundColor = UIColor.white
        textField.inputView = self.countryPicker

        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .black
        toolBar.isTranslucent = true
        toolBar.tintColor = .black
            //UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()

        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(AddNewAddressViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(AddNewAddressViewController.cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    @objc func donePicker() {
        print("done")
       /* if let sCountry = selectCountryTF.text, !sCountry.isEmpty {
            self.selectedCountry = sCountry
        }*/
        VWCountryPicker.isHidden = true
        countryPicker.removeFromSuperview()
      //  self.selectCountryTF.resignFirstResponder()
    }
    
    @objc func cancelPicker() {
        print("cancel")
     //   self.selectedCountry = ""
        VWCountryPicker.isHidden = true
        countryPicker.removeFromSuperview()
      //  self.selectCountryTF.resignFirstResponder()
    }
    
  
    
    
    //MARK:- Param Inital Setup
    func setUpIntialData() ->Void {
        
        dictPasswordInfo["userId"] = ""
        dictPasswordInfo["address"] = ""
        dictPasswordInfo["city"] = ""
        dictPasswordInfo["street"] = ""
        dictPasswordInfo["countryName"] = ""
        dictPasswordInfo["countryId"] = ""
        dictPasswordInfo["taxGroup"] = ""
        dictPasswordInfo["isPrimary"] = ""
    }
    
    
    //MARK:- Add Address Params
    func paramResetPassword(){
        view.endEditing(true)

        if !dictPasswordInfo.isEmpty {
            if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                let address = dictPasswordInfo["address"]
                
                if address == ""{
                    NotificationAlert().NotificationAlert(titles: "Enter Address")
                        return
                }
                    
                let street = dictPasswordInfo["street"]
                
                if street == ""{
                    NotificationAlert().NotificationAlert(titles: "Enter House/Apartment Number")
                        return
                }
                
                let isPrimary = dictPasswordInfo["isPrimary"]
                
                let param = AddressModel()
                param.userId = userId
                param.address = address
                if !selectedCity.trimmingCharacters(in: .whitespaces).isEmpty {
                    param.city = selectedCity
                }
                param.street = street
                if !selectedCityCode.isEmpty{
                    param.taxGroup = selectedCityCode
                }
                else{
                    param.taxGroup = ""
                }
                param.logisticsLocationRoleName = "Invoice"
                if !selectedAddressType.trimmingCharacters(in: .whitespaces).isEmpty {
                    param.addressType = selectedAddressType
                }
                else{
                    param.addressType = "Home"
                }
                if isPrimary == "1" {
                    param.isPrimary = 1
                }
                else{
                    param.isPrimary = 0
                }
                if UserDefaults.standard.bool(forKey: Constants.guestUser) {
                    param.isPrimary = 0
                }
                if !selectedCountryName.trimmingCharacters(in: .whitespaces).isEmpty {
                    param.countryName = selectedCountryName
                }
                if selectedCountryCode.count > 0 {
                    param.countryId = selectedCountryCode
                }
                print(param.toJSONString())
                
                if screenComeFrom == "EditButtonTap" {
                    param.customerAddressId = dictAddress.customerAddressId
                    self.updateAddressAPI(Model: param)
                }
                else{
                    self.addAddressAPI(Model: param)
                }
            }
        }
        else {
            NotificationAlert().NotificationAlert(titles: "Please fill information")
        }
        
    }
    
    
    
    //MARK:- Set Navigation
    func setNAvigation(){
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: self.titleString)
        self.navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    
    //MARK:- Button Action
    @IBAction func buttonSaveAddressPressed(_ sender: Any) {
        
        self.paramResetPassword()
    }
    
    @IBAction func buttonCancelPressed(_ sender: Any) {
       
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func segmentControllerValueChanged(_ sender: Any) {
       
            
    }
    
    // MARK: - Add Address API
    func addAddressAPI(Model: AddressModel){
        self.showLoader()
        EndPoint.AddAddress(params: Model,t: AddressModel.self) { result in
            switch result {
            case .onSuccessWithStringValue(let msg):
                self.hideLoader()
                NotificationAlert().NotificationAlert(titles: msg)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.navigationController?.popViewController(animated: true)
                }
            case .onFailure(let errorMsg):
                print(errorMsg)
                NotificationAlert().NotificationAlert(titles: errorMsg)
                self.hideLoader()
            }
        }
    }
    
    // MARK: - Update Address API
    func updateAddressAPI(Model: AddressModel){
        self.showLoader()
        EndPoint.UpdateAddress(params: Model,t: AddressModel.self) { result in
            switch result {
            case .onSuccessWithStringValue(let msg):
                self.hideLoader()
                NotificationAlert().NotificationAlert(titles: msg)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.delegate.EditAddress(model: Model)
                    self.navigationController?.popViewController(animated: true)
                }
            case .onFailure(let errorMsg):
                print(errorMsg)
                NotificationAlert().NotificationAlert(titles: errorMsg)
                self.hideLoader()
            }
        }
    }
    
    //MARK:- Get CountryList API
    func GetCountryList(){
        EndPoint.GetCountryList(t: CountryListModel.self) { (result) in
            switch result {
            
            case .onSuccess(let items):
                if items.count > 0 {
                    self.arrCountryList = items

                    if self.screenComeFrom != "EditButtonTap" {
                        self.selectedCountryName = self.arrCountryList[0].countryName!
                        self.selectedCountryID = String(self.arrCountryList[0].countryId!)
                        self.selectedCountryCode =  self.arrCountryList[0].countryCode!
                        
                    }
                    self.tableView.reloadData()
                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- Get CityList API
    func GetCityList(){
        EndPoint.GetCityList(t: CitiesListModel.self) { (result) in
            switch result {
            case .onSuccess(let items):
                if items.count > 0 {
                    self.arrCityList = items
                    if self.screenComeFrom != "EditButtonTap" {
                        self.selectedCity = ""
                        self.selectedCity = items[0].cityName!
                        self.selectedCityCode = items[0].cityCode ?? ""
                    }
                  
                    self.tableView.reloadData()
                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
}


extension AddNewAddressViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .done {
            
            textField.resignFirstResponder()
        }
        else {
            
            let textTag = textField.tag + 1
            
            if let nextResponder = textField.superview?.superview?.superview?.superview?.superview?.viewWithTag(textTag) {
                
                nextResponder.becomeFirstResponder()
            }
            else {
                
                textField.resignFirstResponder()
            }
        }
        
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        selectedTF = textField
        if textField.tag == 103  || textField.tag == 104{
            VWCountryPicker.isHidden = true
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        VWCountryPicker.isHidden = true
        
        if textField.tag == 0 {
            
          //  pickUp(textField)
        }

        else if textField.tag == 104 {
            
            if textField.text!.trimmingCharacters(in: .whitespaces).count > 0 {
                
                dictPasswordInfo["street"] = textField.text!.trimmingCharacters(in: .whitespaces)
                
                dictAddress.street = textField.text!.trimmingCharacters(in: .whitespaces)
            }
            else {
                NotificationAlert().NotificationAlert(titles: "Enter House/Apartment Number")
                dictPasswordInfo["street"] = ""

                return
            }
        }
        else if textField.tag == 103{
            
            if textField.text!.trimmingCharacters(in: .whitespaces).count > 0 {
                
                dictPasswordInfo["address"] = textField.text!.trimmingCharacters(in: .whitespaces)
                
                dictAddress.address = textField.text!.trimmingCharacters(in: .whitespaces)

            }
            else {
                NotificationAlert().NotificationAlert(titles: "Enter Address")
                dictPasswordInfo["address"] = ""
                return
            }
        }
    }
}


extension AddNewAddressViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCountryList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let countryName = arrCountryList[row].countryName!
        let countryCode = arrCountryList[row].countryCode!

        return "\(countryCode) " + " \(countryName)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
     //   self.selectCountryTF.text = arrCountryList[row].countryName!
      //  self.countryRegionId = arrCountryList[row].countryCode!
        self.selectedCountryName = arrCountryList[row].countryName!
      //  self.selectedCountryID = arrCountryList[row].countryId!
        self.tableView.reloadData()
        EndPoint.userId = "\(arrCountryList[row].countryId!)"
        self.GetCityList()
    }
}
