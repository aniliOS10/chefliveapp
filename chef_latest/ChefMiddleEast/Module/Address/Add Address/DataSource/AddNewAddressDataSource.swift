//
//  AddNewAddressDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import Foundation
import UIKit
@available(iOS 13.0, *)

extension AddNewAddressViewController: UITableViewDataSource,UITableViewDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewAddressSectionTableViewCell") as! AddNewAddressSectionTableViewCell
        
        cell.titleLabel.text = subTitleString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return section == 0 ? 40.0 :0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        
        if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewAddressTogelTableViewCell") as! AddNewAddressTogelTableViewCell
            cell.selectionStyle = .none
            cell.vwHome.layer.cornerRadius = cell.vwHome.layer.frame.height/2
            cell.vwWork.layer.cornerRadius = cell.vwWork.layer.frame.height/2
            cell.vwOffice.layer.cornerRadius = cell.vwOffice.layer.frame.height/2
            cell.makeAddressDefaultSwitch.tag = indexPath.row
            cell.makeAddressDefaultSwitch.addTarget(self, action: #selector(primaryAddressSwitch(sender:)), for: .valueChanged)
            cell.btnHome.tag =  99
            cell.btnWork.tag =  98
            cell.btnOffice.tag =  97
            cell.btnHome.addTarget(self, action: #selector(btnHomeTap(sender:)), for: .touchUpInside)
            cell.btnWork.addTarget(self, action: #selector(btnWorkTap(sender:)), for: .touchUpInside)
            cell.btnOffice.addTarget(self, action: #selector(btnOfficeTap(sender:)), for: .touchUpInside)
            
            if UserDefaults.standard.bool(forKey: Constants.guestUser) {
                cell.makeAddressDefaultSwitch.isHidden = true
                cell.lblMakeAddressPrimary.isHidden = true
            }else {
                cell.makeAddressDefaultSwitch.isHidden = false
                cell.lblMakeAddressPrimary.isHidden = false
            }
            
            if screenComeFrom == "EditButtonTap" {
                if let isOn = dictAddress.isPrimary, isOn == 1 {
                    cell.makeAddressDefaultSwitch.isOn = true
                }
                else{
                    cell.makeAddressDefaultSwitch.isOn = false
                }
            }
            else{
                cell.makeAddressDefaultSwitch.isOn = false
            }
            
            if  selectedAddressType == "Home" {
                cell.vwHome.backgroundColor = #colorLiteral(red: 0.6274509804, green: 0.01960784314, blue: 0.1529411765, alpha: 1)
                cell.btnHome.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
                cell.btnWork.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                cell.btnOffice.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                cell.vwWork.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                cell.vwOffice.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            }
            
            if  selectedAddressType == "Work" {
                cell.vwWork.backgroundColor = #colorLiteral(red: 0.6274509804, green: 0.01960784314, blue: 0.1529411765, alpha: 1)
                cell.btnWork.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
                cell.btnHome.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                cell.btnOffice.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                cell.vwHome.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                cell.vwOffice.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            }
            
            if  selectedAddressType == "Office" {
                cell.vwOffice.backgroundColor = #colorLiteral(red: 0.6274509804, green: 0.01960784314, blue: 0.1529411765, alpha: 1)
                cell.btnOffice.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
                cell.btnHome.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                cell.btnWork.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                cell.vwHome.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                cell.vwWork.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            }
            
            return cell
        }
        //if indexPath.row == 5
        else  if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewAddressTFTableViewCell") as! AddNewAddressTFTableViewCell
            
            
            cell.countryTF.tag = 101
            cell.cityTF.tag = 102
            cell.streetTF.tag = 103
            cell.addressTF.tag = 104
            
            cell.countryTF.delegate = self
            cell.cityTF.delegate = self
            cell.streetTF.delegate = self
            cell.addressTF.delegate = self
            cell.cellViewRound.layer.cornerRadius = 30.0
            cell.cellViewRound.addShadow()
            cell.cellCityViewRound.layer.cornerRadius = 30.0
            cell.cellCityViewRound.addShadow()
            cell.cellStreetViewRound.layer.cornerRadius = 30.0
            cell.cellStreetViewRound.addShadow()
            cell.cellAddressViewRound.layer.cornerRadius = 30.0
            cell.cellAddressViewRound.addShadow()
            cell.selectionStyle = .none
          //  cell.textField.tag = indexPath.row
            
            cell.countryTF.isUserInteractionEnabled = false
            cell.cityTF.isUserInteractionEnabled = false
            
            cell.countryButton.addTarget(self, action: #selector(btnCountry(sender:)), for: .touchUpInside)
            cell.cityButton.addTarget(self, action: #selector(btnCity(sender:)), for: .touchUpInside)


            if screenComeFrom == "EditButtonTap" {
                cell.countryTF.text = selectedCountryName
                cell.cityTF.text = selectedCity
                
                cell.streetTF.text = dictAddress.address
                dictPasswordInfo["address"] = cell.streetTF.text?.trimmingCharacters(in: .whitespaces)

                cell.addressTF.text = dictAddress.street
                dictPasswordInfo["street"] = cell.addressTF.text?.trimmingCharacters(in: .whitespaces)

            }
            else{
                if !selectedCountryName.trimmingCharacters(in: .whitespaces).isEmpty {
                    cell.countryTF.text = selectedCountryName
                }
                else{
                    selectedCountryName = ""
                    cell.countryTF.placeholder = "Select Country"
                }
                
                if !selectedCity.trimmingCharacters(in: .whitespaces).isEmpty {
                    cell.cityTF.text = selectedCity
                }
                else{
                    cell.cityTF.placeholder = "Select City Name"
                }
                
               
            }
            
            cell.streetTF.placeholder = "Address"
            cell.addressTF.placeholder = "House/Apartment Number"
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewAddressSaveTableViewCell") as! AddNewAddressSaveTableViewCell
           
            cell.saveButton.setTitle(self.saveTitleString, for: .normal)
            cell.selectionStyle = .none

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewAddressTFTableViewCell") as! AddNewAddressTFTableViewCell

        if indexPath.row == 0 {
            
//            let tf = cell.contentView.viewWithTag(101) as! UITextField
        }
        else if indexPath.row == 1  {
            VWCountryPicker.isHidden = true
           // NotificationAlert().NotificationAlert(titles: "Select Country First")
        }
        else{
            VWCountryPicker.isHidden = true
        }
    }
    
    //mARK:- Switch primary Select
    @objc func primaryAddressSwitch(sender:UISwitch) {
        
        if sender.isOn == true {
            dictPasswordInfo["isPrimary"] = "1"
        }
        else{
            dictPasswordInfo["isPrimary"] = "0"
        }
    }
    
    //MARk:- Home Tap
    @objc func btnHomeTap(sender: UIButton) {
        selectedAddressType = "Home"
        tableView.reloadData()
    }
    
    //MARk:- Work Tap
    @objc func btnWorkTap(sender: UIButton) {
        selectedAddressType = "Work"
        tableView.reloadData()
    }
    
    //MARk:- Office Tap
    @objc func btnOfficeTap(sender: UIButton) {
        selectedAddressType = "Office"
        tableView.reloadData()
    }
    
    
    //MARk:- Office Tap
    @objc func btnCountry(sender: UIButton) {
        setupCountryDropDown(sender,0)
    }
    
    //MARk:- Office Tap
    @objc func btnCity(sender: UIButton) {
        setupCountryDropDown(sender, 1)
    }

}
