//
//  AppMessage_NewAddress.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import Foundation


class AppMessage_NewAddress: NSObject {
    struct Placeholder {
        static let AddNewAddress = NSLocalizedString("AddNewAddress", comment: "")
        static let AddAddress = NSLocalizedString("AddAddress", comment: "")
        static let SaveAddress = NSLocalizedString("SaveAddress", comment: "")
      
      
        static let EditAddress = NSLocalizedString("EditAddress", comment: "")
        static let UpdateAddress = NSLocalizedString("UpdateAddress", comment: "")
    }
}
