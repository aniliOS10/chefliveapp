//
//  AddressInfoViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import UIKit

@available(iOS 13.0, *)
class AddressInfoViewController:BaseViewController {
    @IBOutlet weak var tableView : UITableView!
   
   private let dataSource:AddressInfoDataSource?
    
    lazy var viewModel:AddressInfoViewModel = {
        let viewModel = AddressInfoViewModel(dataSource: self.dataSource)
        return viewModel
    }()
    init(dashBoardViewDataSource:AddressInfoDataSource)
    {
        self.dataSource = dashBoardViewDataSource
        super.init(nibName: nil, bundle: nil)
    }
  
    convenience init(dataSourceCollectionView1:AddressInfoDataSource) {
        self.init(dashBoardViewDataSource: dataSourceCollectionView1)
    }
    required init?(coder: NSCoder) {
         self.dataSource = AddressInfoDataSource()
         super.init(coder: coder)
     }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource?.tableView = self.tableView
        self.tableView.delegate = self.dataSource
        self.tableView.dataSource = self.dataSource
      
        self.viewModel.dataSource?.data.addAndNotify(observer: self, completionHandler: { mockUp_Dashboard in
            self.tableView.reloadData()
        })
        self.viewModel.makeDashboardList()
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
      
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: AppMessage_Setting.Placeholder.Settings)
        
        
      
        // Do any additional setup after loading the view.
    }
   
   
    
    @IBAction func buttonChangePressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  
            
            
            
            
        
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
