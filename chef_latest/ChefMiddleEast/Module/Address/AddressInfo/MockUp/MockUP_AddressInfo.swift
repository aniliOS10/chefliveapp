//
//  MockUP_AddressInfo.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation

class MockUP_AddressInfo{
    var description : String!
    var title : String!
    var PlaceHolder : String!
    init(description: String, title: String,PlaceHolder : String!)
    {
        self.description = description
        self.title = title
        self.PlaceHolder = PlaceHolder
    }
}
