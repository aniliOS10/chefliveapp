//
//  AddressInfoViewModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
//AddressInfoViewModel
class AddressInfoViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    weak var dataSource:GenericDataSource<MockUP_AddressInfo>?
    
    init(dataSource : GenericDataSource<MockUP_AddressInfo>?) {
        self.dataSource = dataSource    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(){
        self.dataSource?.data.value.append(
            MockUP_AddressInfo.init(description: "", title: "140401 Rajpura", PlaceHolder: AppMessage_ChangePasswordSetting.Placeholder.CurrentPassword))
        self.dataSource?.data.value.append(
            MockUP_AddressInfo.init(description: "", title: "Patiala", PlaceHolder: AppMessage_ChangePasswordSetting.Placeholder.CurrentPassword))
        self.dataSource?.data.value.append(
            MockUP_AddressInfo.init(description: "", title: "Punjab", PlaceHolder: AppMessage_ChangePasswordSetting.Placeholder.CurrentPassword))
        self.dataSource?.data.value.append(
            MockUP_AddressInfo.init(description: "", title: "India", PlaceHolder: AppMessage_ChangePasswordSetting.Placeholder.CurrentPassword))
    
        
    }
}
