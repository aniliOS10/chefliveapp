//
//  AddressRowTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import UIKit

class AddressRowTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPrimaryAdress: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var lblFullAddress: UILabel!
    
    @IBOutlet weak var btnCountry: UIButton!
    
    @IBOutlet var heightPrimary: NSLayoutConstraint!
    
    @IBOutlet weak var vwAddressType: UIView!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        Utility.shared.makeShadowsOfView_roundCorner(view: btnEdit, shadowRadius: 1.0, cornerRadius: 3, borderColor: UIColor.lightGray)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
