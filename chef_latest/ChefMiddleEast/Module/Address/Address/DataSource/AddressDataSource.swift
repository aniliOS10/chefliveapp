//
//  AddressDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
import UIKit

extension AddressViewController: UITableViewDataSource,UITableViewDelegate{
    
    
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressSectionTableViewCell") as! AddressSectionTableViewCell
        cell.titleLabel.text = "Shipping Address"
        return cell
      
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressRowTableViewCell") as! AddressRowTableViewCell
        
        cell.vwAddressType.layer.cornerRadius = cell.vwAddressType.frame.size.height/2
        
        if let addressType = self.arrAddress[indexPath.row].addressType, !addressType.isEmpty {
            cell.btnCountry.setTitle(addressType, for: .normal)
            cell.vwAddressType.isHidden = false
        }
        else{
            cell.vwAddressType.isHidden = true

        }
        
        if let address = self.arrAddress[indexPath.row].address, !address.isEmpty {
            cell.lblAddress.text = address
        }
        else{
            cell.lblAddress.text = "Add Address"
        }
        if let isPrimary = self.arrAddress[indexPath.row].isPrimary, isPrimary == 1 {
            cell.lblPrimaryAdress.text = "Primary"
            cell.heightPrimary.constant = 14
            cell.lblPrimaryAdress.isHidden = false

        }
        else{
            cell.lblPrimaryAdress.isHidden = true
            cell.heightPrimary.constant = 0
        }
        if let street = self.arrAddress[indexPath.row].street, !street.isEmpty {
            if let city = self.arrAddress[indexPath.row].city
               , !city.isEmpty {
                if let countryName = self.arrAddress[indexPath.row].countryName, !countryName.isEmpty {
                    cell.lblFullAddress.text = street + ", " + city + ", " + countryName
                }
            }
        }
        cell.btnCountry.layer.cornerRadius = 10
        cell.btnEdit.layer.cornerRadius = 5
        cell.btnEdit.addShadow()
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(btnEdiTtap(sender:)), for: .touchUpInside)
        Utility.shared.makeShadowsOfView_roundCorner(view: cell.roundView,shadowColor:UIColor.black.withAlphaComponent(0.2), shadowRadius: 2.0, cornerRadius: 4, borderColor: UIColor.black.withAlphaComponent(0.25))
        
       // cell.roundView.addShadowDash()

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       // return 120
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if screenCommingFrom == "DeliverySummaryChooseAddress" {
            
            delegate.chooseAddress(model: arrAddress[indexPath.row])
        }
       
    }
   
    //MARK:- Edit Button
    @objc func btnEdiTtap(sender: UIButton){
        let controller:AddNewAddressViewController =  UIStoryboard(storyboard: .Address).initVC()
        controller.titleString = "Edit Address"
        controller.subTitleString = AppMessage_NewAddress.Placeholder.EditAddress
        controller.saveTitleString = AppMessage_NewAddress.Placeholder.UpdateAddress
        controller.dictAddress = arrAddress[sender.tag]
        controller.screenComeFrom = "EditButtonTap"
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

