//
//  AddressViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import UIKit

protocol AddressViewControllerDelegate {
    func chooseAddress(model: AddressModel)
}




class AddressViewController:BaseViewController,EditNewAddressViewControllerDelegate {
   
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var lbeNoData : UILabel!
    @IBOutlet weak var imgNoData : UIImageView!

    
    var arrAddress = [AddressModel]()
    var delegate : AddressViewControllerDelegate!
    var screenCommingFrom = String()
    var isTabBarHide = true
    
    //MARK:- VC Life Cycle
   // hi iOS
    override func viewWillAppear(_ animated: Bool) {
        
        if isTabBarHide {
            self.tabBarController?.tabBar.isHidden = false
        }
        else{
            self.tabBarController?.tabBar.isHidden = false
        }
        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            EndPoint.userId = userId
            self.getAddressAPI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.setNavigation()
    }
    
    //MARK:- Custome delegate Add AddressViewController (Edit Tap)
    func EditAddress(model: AddressModel) {
        
//        print(model.toJSONString())
//        arrAddress[0].street = model.street
//        arrAddress[0].city = model.city
//        arrAddress[0].AddAdresscountryId = model.AddAdresscountryId
//        arrAddress[0].countryName = model.countryName
//        arrAddress[0].customerAddressId = model.customerAddressId
//        tableView.reloadData()
    }
    
    
    
    //MARK:- Setup Navigation
    func setNavigation(){
        
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: Appmessage_Address.Placeholder.MyAddresses)
    }
   
    //MARK:- Button Action
    @IBAction func buttonChangeAddressPressed(_ sender: Any) {
        
    }
    
    @IBAction func buttonADDNewAddressPressed(_ sender: Any) {
        
        let controller:AddNewAddressViewController =  UIStoryboard(storyboard: .Address).initVC()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func buttonCancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - get Address API
    func getAddressAPI(){
        self.tableView.isHidden = true
        self.lbeNoData.isHidden = false
        self.imgNoData.isHidden = false
        self.showLoader()
        
        EndPoint.GetAddress(t: AddressModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccess(let items):
                if items.count > 0 {
                    self.arrAddress = items
                    self.tableView.isHidden = false
                    self.lbeNoData.isHidden = true
                    self.imgNoData.isHidden = true
                    self.tableView.reloadData()
                }
                self.hideLoader()
            case .onFailure(let errorMsg):
                print(errorMsg)
                NotificationAlert().NotificationAlert(titles: errorMsg)
                self.hideLoader()
            }
        }
    }

}
