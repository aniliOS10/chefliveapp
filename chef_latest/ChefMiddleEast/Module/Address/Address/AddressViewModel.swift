//
//  AddressViewModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
class AddressViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    weak var dataSource:GenericDataSource<MockUP_Address>?
    
    init(dataSource : GenericDataSource<MockUP_Address>?) {
        self.dataSource = dataSource    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(){
        self.dataSource?.data.value.append(
            MockUP_Address.init(description: "Chino hills, CA 9109, Australia", title: "John Doe"))
        self.dataSource?.data.value.append(
            MockUP_Address.init(description: "Chino hills, CA 9109, Australia", title: "John Doe"))
        self.dataSource?.data.value.append(
            MockUP_Address.init(description: "Chino hills, CA 9109, Australia", title: "John Doe"))
        
    }
}
