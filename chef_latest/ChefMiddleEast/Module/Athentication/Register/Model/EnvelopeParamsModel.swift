//
//  EnvelopeParamsModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 25/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class EnvelopeParams: BaseResponse {
    
    var dataOrigin : String?
    var custName : String?
    var lastName : String?
    var customerType : String?
    var taxGroup : String?
    var email : String?
    var gender : String?
    var birthDate : String?
    var phoneMobile : String?
    var address = AddressModel()
    var card = CardModel()

   
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        dataOrigin <- map["dataOrigin"]
        custName <- map["custName"]
        lastName <- map["lastName"]
        customerType <- map["customerType"]
        taxGroup <- map["taxGroup"]
        email <- map["email"]
        gender <- map["gender"]
        phoneMobile <- map["phoneMobile"]
        address <- map["address"]
        card <- map["card"]
        birthDate <- map["birthDate"]
        gender <- map["gender"]

    }
}
