//
//  MockUpSendOtp.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import Foundation
struct MockUp_SendOTP{
    var code : Int!
    var data : Int!
    var message : String!
    var status : Int!
    init(fromDictionary dictionary: [String:AnyObject]){
            code = dictionary["code"] as? Int
            data = dictionary["data"] as? Int
            message = dictionary["message"] as? String
            status = dictionary["status"] as? Int
        }
}
extension MockUp_SendOTP:Parceable {
    static func parseObject(dictionary: [String : AnyObject]) -> Result<MockUp_SendOTP, ErrorResult> {
        let object =   MockUp_SendOTP.init(fromDictionary: dictionary)
        if object.status == 1 {
            return Result.success(object)
        }else{
            return Result.failure(ErrorResult.parser(string: "Unable to parse conversion rate"))
        }
    }
}
