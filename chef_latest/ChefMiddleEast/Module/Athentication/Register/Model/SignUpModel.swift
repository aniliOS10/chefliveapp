//
//  SignUpModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 25/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class SignUpModel: BaseResponse {
    
    var password : String?
    var deviceToken : String?
    var deviceType : String?
    var envelopeParams = EnvelopeParams()

    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        password <- map["password"]
        deviceToken <- map["deviceToken"]
        deviceType <- map["deviceType"]
        envelopeParams <- map["envelopeParams"]

    }
}
