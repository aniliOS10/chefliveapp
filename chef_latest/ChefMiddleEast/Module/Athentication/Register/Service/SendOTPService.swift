//
//  RegisterService.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import UIKit
import Alamofire
protocol SendOTPServiceProtocol :class {
    func hitSendOTP( _ para:[String:Any]?,completion:@escaping((Result<MockUp_SendOTP,ErrorResult>)->Void))
}
class SendOTPService: RequestHandler,SendOTPServiceProtocol  {
    
    
    
    static let shared = SendOTPService()
    let endpoint = APIUrl.baseUrl
    let sendOtpUrl = APIUrl.SendOtpUrl().url
    var request:DataRequest?
    
    func hitSendOTP(_ para: [String : Any]?, completion: @escaping ((Result<MockUp_SendOTP, ErrorResult>) -> Void)) {
        self.cancelSendOtpService()
        //hitting service
        request =  RequestService().loadData_Alamofire(urlString: sendOtpUrl, method: .post, parameters: para, completion: self.networkResult(completion: completion))
    }
    
    func cancelSendOtpService(){
        self.request?.cancel()
    }
    
   
    

}

