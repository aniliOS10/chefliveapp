//
//  AppMessageRegisterUser.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import Foundation
class AppMessageRegisterUser{
    struct Placeholder {
        static let FirstName = NSLocalizedString("FirstName", comment: "")
        static let LastName = NSLocalizedString("LastName", comment: "")
        
        static let Phonenumber = NSLocalizedString("Phonenumber", comment: "")
        static let Email = NSLocalizedString("EmailAddress", comment: "")
        static let Password = NSLocalizedString("SetPassword", comment: "")
        static let ConfirmPassword = NSLocalizedString("ConfirmPassword", comment: "")
        static let signin = NSLocalizedString("signin", comment: "")
        
      
    }
    struct ValidationError {
    static let EmptyfullName = NSLocalizedString("EmptyfullName", comment: "")
    static let EmptyPhonenumber = NSLocalizedString("EmptyPhonenumber", comment: "")
        
    static let EmptyEmail = NSLocalizedString("EmptyEmail", comment: "")
    static let EmptyPassword = NSLocalizedString("EmptyPassword", comment: "")
    static let EmptyConfirmPassword = NSLocalizedString("EmptyConfirmPassword", comment: "")
    static let PasswordDonotMatch = NSLocalizedString("PasswordDonotMatch", comment: "")
    static let continueButton = NSLocalizedString("continue", comment: "")
   
        
        static let InvalidEmail = NSLocalizedString("InvalidEmail", comment: "")
        static let InvalidPhone = NSLocalizedString("InvalidPhone", comment: "")
        static let passwordTooShort = NSLocalizedString("passwordTooShort", comment: "")
 
    static let fullNameTooShort = NSLocalizedString("fullNameTooShort", comment: "")

    }
    
}
