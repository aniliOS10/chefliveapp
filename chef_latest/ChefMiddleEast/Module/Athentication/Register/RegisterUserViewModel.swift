//
//  RegisterViewModel.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import Foundation
struct RegisterUserViewModel {
    weak var userInfo:UserInfo?
    weak var service: SendOTPService?
   
     var validation: ValidationRegisterVC?
    
    var onCompletionHandling : ((MockUp_SendOTP) -> Void)?
    var  compeltionBlockLocaliable: ((AppMessageRegisterUser) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    
    
    init(service:SendOTPService,validation:ValidationRegisterVC) {
        self.service = service
        self.validation = validation
    }
    func hitSendOTP(param:[String:Any]?){
        guard let service = self.service else {
            onErrorHandling?(ErrorResult.custom(string: "Missing Service"))
            return
        }
        guard let param1 = param else {
            onErrorHandling?(ErrorResult.custom(string: "Missing Parmeters"))
            return
        }
      
        service.hitSendOTP(param1) { results in
          
            switch results{
            case .success(let mockUp_SendOTP):
                onCompletionHandling!(mockUp_SendOTP)
              break
            
            case .failure(let error):
                self.onErrorHandling?(error)
            }
        }
        
    }
    func  validateUserName(_ inputString:String?)throws -> String?
    {
        do{
            guard let fullName = try  self.validation?.validateUserName(inputString)else{
                return nil
            }
            return fullName
        }catch{
            onErrorHandling?(ErrorResult.custom(string: error.localizedDescription))
            throw ErrorResult.custom(string: error.localizedDescription)
        }
        
      
    }
    func  validatePhone(_ inputString:String?)throws -> String?
    {
        do{
            guard  let phone =  try self.validation?.validatePhone(inputString) else { return nil }
            return phone
        }catch{
            onErrorHandling?(ErrorResult.custom(string: error.localizedDescription))
            throw ErrorResult.custom(string: error.localizedDescription)
        }
       
    }
    func  validatePassword(_ inputString:String?)throws -> String?
    {
        do{
            guard  let value =  try self.validation?.validatePassword(inputString) else { return nil }
            return value
        }catch{
            onErrorHandling?(ErrorResult.custom(string: error.localizedDescription))
            throw ErrorResult.custom(string: error.localizedDescription)
        }
        
    
    }
    func  validateConfirmPassword(_ inputString:String?,password:String?)throws -> String?
    {
        do{
            let value = try self.validation?.validateConfirmPassword(inputString, password: password) 
           return value ?? ""
          
        }catch{
            onErrorHandling?(ErrorResult.custom(string: error.localizedDescription))
            throw ErrorResult.custom(string: error.localizedDescription)
        }
      
    }
}
