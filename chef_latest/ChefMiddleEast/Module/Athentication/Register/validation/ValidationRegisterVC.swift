//
//  ValidationRegisterVC.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import Foundation
struct ValidationRegisterVC {
    func validateUserName(_ fullName:String?) throws -> String {
        guard let fullNameValue = fullName else { throw ValidationErrorRegisterVC_Fullname.emptyFullName }
        guard fullNameValue.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorRegisterVC_Fullname.emptyFullName }
        guard fullNameValue.count > 3 else { throw ValidationErrorRegisterVC_Fullname.fullNameTooShort }
        return fullNameValue
    }
    func validatePhone(_ phone:String?) throws -> String {
        guard let phoneValue = phone else { throw ValidationErrorRegisterVC_Phone.emptyPhone }
        guard phoneValue.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorRegisterVC_Phone.emptyPhone }
        guard phoneValue.isValidPhoneNumber() else {
            throw ValidationErrorRegisterVC_Phone.phoneIsNotValid
        }
        return phoneValue
      
    }
    func validateEmail(_ email:String?) throws -> String {
        guard let emailValue = email else { throw ValidationErrorRegisterVC_Email.emptyEmail }
        guard emailValue.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorRegisterVC_Email.emptyEmail }
        guard emailValue.isValidEmail() else {
            throw ValidationErrorRegisterVC_Email.emailIsNotValid
        }
        return emailValue
    }
    func validatePassword(_ password:String?) throws -> String {
        guard let upasswordValue = password else { throw ValidationErrorRegisterVC_password.emptyPassword }
        guard upasswordValue.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorRegisterVC_password.emptyPassword }
        guard upasswordValue.count >= 8 else { throw ValidationErrorRegisterVC_password.passwordTooShort }
        return upasswordValue
    }
    func validateConfirmPassword(_ confrimePassword:String?,password:String?) throws -> String {
        
        guard let passwordValue = password else { throw ValidationErrorRegisterVC_password.emptyPassword }
        
        guard let confrimValue = confrimePassword else { throw ValidationErrorRegisterVC_password.emptyConfirmPassword }
        
        guard confrimValue.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorRegisterVC_password.emptyConfirmPassword }
        
      
        
        if  confrimValue == passwordValue{
            return confrimValue
        } else { throw ValidationErrorRegisterVC_password.passwordDonotMatch }
        
       
      
    }
}
enum ValidationErrorRegisterVC_Fullname:LocalizedError {
    case emptyFullName
    case fullNameTooShort
    var errorDescription: String? {
        switch self {
        case .emptyFullName:
            return AppMessageRegisterUser.ValidationError.EmptyfullName
        case .fullNameTooShort:
            return AppMessageRegisterUser.ValidationError.fullNameTooShort
        }
    }
}
enum ValidationErrorRegisterVC_Email:LocalizedError {
    case emptyEmail
    case emailIsNotValid
    var errorDescription: String? {
        switch self {
        case .emptyEmail:
            return AppMessageRegisterUser.ValidationError.EmptyEmail
        case .emailIsNotValid:
            return AppMessageRegisterUser.ValidationError.InvalidEmail
        }
    }
}
enum ValidationErrorRegisterVC_Phone:LocalizedError {
    case emptyPhone
    case phoneIsNotValid
    var errorDescription: String? {
        switch self {
        case .emptyPhone:
            return AppMessageRegisterUser.ValidationError.EmptyPhonenumber
        case .phoneIsNotValid:
            return AppMessageRegisterUser.ValidationError.InvalidPhone
        }
    }
}
enum ValidationErrorRegisterVC_password:LocalizedError {
  
    case passwordTooShort
    case emptyPassword
    case passwordDonotMatch
    case emptyConfirmPassword
    var errorDescription: String? {
        switch self {
        case .emptyPassword:
            return AppMessageRegisterUser.ValidationError.EmptyPassword
        case .passwordTooShort:
            return AppMessageRegisterUser.ValidationError.passwordTooShort
        case .emptyConfirmPassword:
            return AppMessageRegisterUser.ValidationError.EmptyConfirmPassword
        case .passwordDonotMatch:
            return AppMessageRegisterUser.ValidationError.PasswordDonotMatch
        }
    }
}

