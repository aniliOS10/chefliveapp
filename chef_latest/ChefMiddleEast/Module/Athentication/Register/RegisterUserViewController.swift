//
//  ResgisterUserViewController.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import UIKit
import CocoaTextField
import CountryPickerView
import AVFoundation
import Photos


var signUpParams = SignUpModel()

class RegisterUserViewController: BaseViewController {
    
    @IBOutlet weak var firstNameTF: CocoaTextField!
    @IBOutlet weak var lastNameTF: CocoaTextField!
    @IBOutlet weak var emailTF: CocoaTextField!
    @IBOutlet weak var phoneTF: CocoaTextField!
    @IBOutlet weak var tfConfirmPassword: CocoaTextField!
    @IBOutlet weak var tfPassword: CocoaTextField!
    @IBOutlet weak var titleLabel :UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var alreadyHaveAnAccountLabel: UILabel!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var userProfile: UIButton!
    @IBOutlet weak var uploadUserPhoto: UILabel!
    @IBOutlet weak var backViewWhite: UIView!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var btnSigIn: UIButton!
    @IBOutlet var myScrollVW: UIScrollView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var password1View: UIView!
    @IBOutlet weak var password2View: UIView!

    @IBOutlet weak var imgUserProfile: UIImageView!

    
    var service: SendOTPService?
    var eyeClick = true
    var Profileimg : UIImage?

    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       // userProfile.layer.borderWidth = 1
        userProfile.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
        userProfile.layer.cornerRadius = userProfile.frame.size.width/2
        
        imgUserProfile.layer.borderWidth = 1
        imgUserProfile.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.size.width/2
        
        tfPassword.delegate = self
        tfConfirmPassword.delegate = self
        emailTF.delegate = self
        firstNameTF.delegate = self
        lastNameTF.delegate = self
        phoneTF.delegate = self
        self.addEye()
        self.setUI()
        
        tfPassword.isSecureTextEntry = true
        tfConfirmPassword.isSecureTextEntry = true

    }
    
    func setUI(){
        self.backViewWhite .layer.masksToBounds = true
        self.backViewWhite.layoutIfNeeded()
        Utility.shared.roundParticular(corners: [.topLeft,.topRight], cornerRadius: 40, view1: backViewWhite)
        
        self.btnSigIn.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessageRegisterUser.Placeholder.signin, size: 18.0, fontName: FontName.Optima.Bold), for: .normal)
        Utility.shared.makeShadowsOfView_roundCorner(view: firstNameTF, shadowRadius: 4.0, cornerRadius: 26.5, borderColor: UIColor.lightGray)
        Utility.shared.makeShadowsOfView_roundCorner(view: lastNameTF, shadowRadius: 4.0, cornerRadius: 26.5, borderColor: UIColor.lightGray)
        Utility.shared.makeShadowsOfView_roundCorner(view: emailTF, shadowRadius: 4.0, cornerRadius: 26.5, borderColor: UIColor.lightGray)
       // Utility.shared.makeShadowsOfView_roundCorner(view: phoneTF, shadowRadius: 4.0, cornerRadius: 26.5, borderColor: UIColor.lightGray)
        Utility.shared.makeShadowsOfView_roundCorner(view: phoneView, shadowRadius: 4.0, cornerRadius: 26.5, borderColor: UIColor.lightGray)

        Utility.shared.makeShadowsOfView_roundCorner(view: tfPassword, shadowRadius: 0.0, cornerRadius: 26.5, borderColor: UIColor.white)

        tfPassword.clipsToBounds = true
        
        Utility.shared.makeShadowsOfView_roundCorner(view: password1View, shadowRadius: 4.0, cornerRadius: 26.5, borderColor: UIColor.lightGray)
        
        Utility.shared.makeShadowsOfView_roundCorner(view: tfConfirmPassword, shadowRadius: 4.0, cornerRadius: 26.5, borderColor: UIColor.white)
        tfConfirmPassword.clipsToBounds = true

        Utility.shared.makeShadowsOfView_roundCorner(view: password2View, shadowRadius: 4.0, cornerRadius: 26.5, borderColor: UIColor.lightGray)


        applyStyle(to: firstNameTF)
        firstNameTF.placeholder = AppMessageRegisterUser.Placeholder.FirstName
        applyStyle(to: lastNameTF)
        lastNameTF.placeholder = AppMessageRegisterUser.Placeholder.LastName
        applyStyle(to: emailTF)
        emailTF.placeholder = AppMessageRegisterUser.Placeholder.Email
        applyStyle(to: tfPassword)
        phoneTF.placeholder = AppMessageRegisterUser.Placeholder.Phonenumber
        applyStyle(to: phoneTF)
        tfPassword.placeholder = AppMessageRegisterUser.Placeholder.Password
        applyStyle(to: tfConfirmPassword)
        tfConfirmPassword.placeholder = AppMessageRegisterUser.Placeholder.ConfirmPassword
    }
    
    func addEye(){
        let buttonNew = UIButton(type: .custom)
        buttonNew.setImage(UIImage(named: "hiddenPass"), for: .normal)
        buttonNew.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        buttonNew.frame = CGRect(x: CGFloat(tfPassword.frame.size.width - 35), y: CGFloat(5), width: CGFloat(35), height: CGFloat(35))
        buttonNew.addTarget(self, action: #selector(self.reveal1), for: .touchUpInside)
        tfPassword.rightView = buttonNew
        tfPassword.rightViewMode = .always
        
        let button1 = UIButton(type: .custom)
        button1.setImage(UIImage(named: "hiddenPass"), for: .normal)
        button1.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        button1.frame = CGRect(x: CGFloat(tfConfirmPassword.frame.size.width - 35), y: CGFloat(5), width: CGFloat(35), height: CGFloat(35))
        button1.addTarget(self, action: #selector(self.reveal2), for: .touchUpInside)
        tfConfirmPassword.rightView = button1
        tfConfirmPassword.rightViewMode = .always
    }
    
    func signupValidation(){
        
        if firstNameTF.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please enter first name")
            return
        }
        else if lastNameTF.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please enter last name")
            return
        }
        else if emailTF.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please enter email")
            return
        }
        else if phoneTF.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please enter phone number")
            return
        }
        else if phoneTF.text?.trimmingCharacters(in: .whitespaces).count ?? 0 < 9 {
            NotificationAlert().NotificationAlert(titles: "Please enter valid phone number")
            return
        }
        else if phoneTF.text?.trimmingCharacters(in: .whitespaces).count ?? 0 > 10 {
            NotificationAlert().NotificationAlert(titles: "Please enter valid phone number")
            return
        }
        else if tfPassword.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please enter password")
            return
        }
        else  if (tfPassword.text?.trimmingCharacters(in: .whitespaces).count)! < 6 {
            NotificationAlert().NotificationAlert(titles: "Password should be 6 characters or more")
            return
        }
        else if tfConfirmPassword.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please enter confirm password")
            return
        }
        else  if (tfPassword.text?.trimmingCharacters(in: .whitespaces)) != (tfConfirmPassword.text?.trimmingCharacters(in: .whitespaces)){
           
            NotificationAlert().NotificationAlert(titles: "Password not match")
            return
        }
        else {
            let number = phoneTF.text!.trimmingCharacters(in: .whitespaces)
           // EndPoint.phone = String("\("%2B91")\(number)")
            EndPoint.phone = String("\("%2B971")\(number)")
            let email = emailTF.text!.trimmingCharacters(in: .whitespaces)
            EndPoint.email = email
            EndPoint.checkExisting = true
            self.verifyEmailSendOTP()
        }
    }
    
    
    
    
    // MARK: - BUtton Action
    @IBAction func reveal1(_ sender: UIButton) {
        if(eyeClick == true) {
            sender.setImage(UIImage(named: "viewPass"), for: .normal)
            tfPassword.isSecureTextEntry = false
        } else {
            sender.setImage(UIImage(named: "hiddenPass"), for: .normal)
            tfPassword.isSecureTextEntry = true
        }
        eyeClick = !eyeClick
    }
    
    @IBAction func reveal2(_ sender: UIButton) {
        if(eyeClick == true) {
            sender.setImage(UIImage(named: "viewPass"), for: .normal)
            tfConfirmPassword.isSecureTextEntry = false
        } else {
            sender.setImage(UIImage(named: "hiddenPass"), for: .normal)
            tfConfirmPassword.isSecureTextEntry = true
        }
        eyeClick = !eyeClick
    }
   
    @IBAction func btnContinue(_ sender: Any){
        
        self.signupValidation()
        
    }
    
    @IBAction func btnUserProfilePressed(_ sender: Any){
        
        addImage()
    }
    
    @IBAction func btnSignInPressed(_ sender: Any){
        RootManager().setLoginRoot()
    }
    
    @IBAction func btnBackPreessed(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonSignInWithFacebookPressed(_ sender: Any) {
        
    }
    
    @IBAction func buttonSignInWithGooglePressed(_ sender: Any) {
        
    }
    
    
    
}

//MARK:- UITextFieldDelegate
@available(iOS 13.0, *)
extension RegisterUserViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == ""{
            return true
        }
        if textField.text == "" && string == " "{
            return false
        }
        if textField == phoneTF {
           if textField.text?.count ?? 0 > 9{
               return false
           }
        }
        if textField.text?.count ?? 0 > 40{
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
         if textField == tfPassword {
           // tfPassword.isSecureTextEntry = true
        }
        else if textField == tfConfirmPassword {
           // tfConfirmPassword.isSecureTextEntry = true
        }
    }
}

@available(iOS 13.0, *)
extension RegisterUserViewController: AlertDelegate {
    func okButtonPressed(alertController: UIAlertController, alert: AlertVC) {
        
    }
    
    func cancelButtonPressed(alertController: UIAlertController, alert: AlertVC) {
        
    }
}


extension RegisterUserViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func addImage(){
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        let takePic = UIAlertAction(title: "Take Photo", style: .default,handler: {
            (alert: UIAlertAction!) -> Void in
            self.checkCameraAccess()
        })
        let choseAction = UIAlertAction(title: "Choose from Library",style: .default,handler: {
            (alert: UIAlertAction!) -> Void in
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
            myPickerController.modalPresentationStyle = .fullScreen
            self.present(myPickerController, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(takePic)
        optionMenu.addAction(choseAction)
        optionMenu.addAction(cancelAction)
        
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func checkCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        
        case .authorized:
            print("Authorized, proceed")
            DispatchQueue.main.async {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = UIImagePickerController.SourceType.camera
                myPickerController.modalPresentationStyle = .fullScreen
                myPickerController.showsCameraControls = true
                self.present(myPickerController, animated: true, completion: nil)
            }
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    print("Permission granted, proceed")
                    DispatchQueue.main.async {
                        let myPickerController = UIImagePickerController()
                        myPickerController.delegate = self
                        myPickerController.sourceType = UIImagePickerController.SourceType.camera
                        myPickerController.modalPresentationStyle = .fullScreen
                        myPickerController.showsCameraControls = true
                        self.present(myPickerController, animated: true, completion: nil)
                    }
                }
                else{
                    self.dismiss(animated: false, completion: nil)
                }
            }
        default:
            self.alertToEncourageCameraAccessInitially()
        }
    }
    
    func alertToEncourageCameraAccessInitially() {
        
        let alert = UIAlertController(
            title: "Alert",
            message: "ChefMiddleEast app requires to access your camera to capture image on your business profile and service.",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let originalImage = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage else { return }
        
        self.Profileimg = originalImage
       // self.userProfile.setImage(originalImage, for: .normal)
        imgUserProfile.image = originalImage
        imgUserProfile.layer.borderWidth = 1
        imgUserProfile.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
        imgUserProfile.layer.cornerRadius = userProfile.frame.size.width/2
        imgUserProfile.clipsToBounds = true
        
        self.dismiss(animated: false, completion: { [weak self] in
        })
    }
}
extension RegisterUserViewController {
    // MARK: - Verify Email Send OTP API
    func verifyEmailSendOTP(){
        self.showLoader()
        EndPoint.VerifyEmailSendOTP(t: UserModel.self) { result in
            switch result {
            case .onSuccessWithIntegerValue(let strOtp):
                NotificationAlert().NotificationAlert(titles: "Verification code sent on email")
                UserDefaults.standard.set(strOtp, forKey: Constants.otp)
                let controller:OTPVerifyViewController = UIStoryboard(storyboard: .athentication).initVC()
                controller.isRegisterNewUser = true
                EndPoint.checkExisting = true
                controller.password = self.tfPassword.text?.trimmingCharacters(in: .whitespaces)
                controller.fName = self.firstNameTF.text?.trimmingCharacters(in: .whitespaces)
                controller.lName = self.lastNameTF.text?.trimmingCharacters(in: .whitespaces)
                controller.email = self.emailTF.text?.trimmingCharacters(in: .whitespaces)
                controller.phoneNo = self.phoneTF.text?.trimmingCharacters(in: .whitespaces)
                if self.Profileimg != nil {
                    controller.Profileimg = self.Profileimg
                }
                
                controller.isPhoneVerification = false
                self.navigationController?.pushViewController(controller, animated: true)
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                NotificationAlert().NotificationAlert(titles: error)
                print(error)
                self.hideLoader()
            }
        }
    }
    
    func verifyPhoneSendOTP(){
        self.showLoader()
        EndPoint.VerifyPhoneSendOTP(t: UserModel.self) { result in
            switch result {
            case .onSuccessWithIntegerValue(let strOtp):
                NotificationAlert().NotificationAlert(titles: "Verification code sent on phone")
                UserDefaults.standard.set(strOtp, forKey: Constants.phoneOtp)
                self.hideLoader()
                EndPoint.phoneNumber = self.phoneTF.text!.trimmingCharacters(in: .whitespaces)
                EndPoint.checkExisting = true
             //   self.verifyEmailSendOTP()
            case .onFailure(let error):
                print(error)
                NotificationAlert().NotificationAlert(titles: error)
                print(error)
                self.hideLoader()
            }
        }
    }
    
}
