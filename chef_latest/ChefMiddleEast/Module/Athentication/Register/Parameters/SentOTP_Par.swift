//
//  SentOTP_Par.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import Foundation
struct SentOTP_Par: DictionaryEncodable {
    var dialCode: String?
    var phoneNumber: String?
    var email: String?
    func toDict()-> [String:Any]{
        var dict  = [String:Any]()
        if self.dialCode != nil{
            dict["dialCode"] =  self.dialCode
        }
        if self.phoneNumber != nil{
            dict["phoneNumber"] =  self.phoneNumber
        }
        if self.email != nil{
            dict["email"] =  self.email
        }
        
        return dict
    }
}
