//
//  AppMessagesWelcome.swift
//  ChefMiddleEast
//
//  Created by Apple on 26/10/21.
//

import Foundation
class AppMessagesWelcome{
    struct Placeholder {
        static let login = NSLocalizedString("LOGIN", comment: "")
        static let SIGNUP = NSLocalizedString("SIGNUP", comment: "")
        static let Welcometo = NSLocalizedString("Welcometo", comment: "")
        
        static let ContinueAsAGuest = NSLocalizedString("ContinueAsAGuest", comment: "")
    }
    
}
