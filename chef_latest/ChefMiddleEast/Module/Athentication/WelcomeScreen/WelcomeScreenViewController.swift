//
//  WelcomeScreenViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 26/10/21.
//

import UIKit

@available(iOS 13.0, *)
class WelcomeScreenViewController: UIViewController {
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var buttonSignUP: UIButton!
    @IBOutlet weak var welcomeLogo: UILabel!
    @IBOutlet weak var buttonGuest: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent

        Utility.shared.makeRoundCorner(layer: buttonLogin.layer, color: UIColor.clear, radius: 25.0)
        Utility.shared.makeRoundCorner(layer: buttonSignUP.layer, color: UIColor.clear, radius: 25.0)
        
        self.welcomeLogo.text = AppMessagesWelcome.Placeholder.Welcometo
       
        self.buttonLogin.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessagesWelcome.Placeholder.login, size: 18.0, fontName: FontName.Optima.Bold), for: .normal)
       
        self.buttonSignUP.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessagesWelcome.Placeholder.SIGNUP, size: 18.0, fontName: FontName.Optima.Bold), for: .normal)
        
        self.buttonGuest.setAttributedTitle (Utility.shared.provideAttributedFamily(text: AppMessagesWelcome.Placeholder.ContinueAsAGuest, size: 16.0, fontName: FontName.Optima.Bold), for: .normal)
   
    }
    func pushToRoot(){
        RootManager().SideRootScreen()
    }
    
    @IBAction func buttonLoginPressed(_ sender: Any) {
        let controller:LoginViewController =  UIStoryboard(storyboard: .athentication).initVC()
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func buttonSignUpPressed(_ sender: Any) {
        let controller:RegisterUserViewController =  UIStoryboard(storyboard: .athentication).initVC()
        self.navigationController?.pushViewController(controller, animated: true)
    }

    @IBAction func btnGuestLogIn(_ sender: UIButton){
        
        
        UserDefaults.standard.removeObject(forKey: Constants.token)
        UserDefaults.standard.removeObject(forKey: Constants.userId)
        UserDefaults.standard.removeObject(forKey: Constants.userImg)
        UserDefaults.standard.removeObject(forKey: Constants.firstName)
        UserDefaults.standard.removeObject(forKey: Constants.lastName)
        UserDefaults.standard.removeObject(forKey: Constants.email)
        UserDefaults.standard.removeObject(forKey: Constants.phone)
        UserDefaults.standard.removeObject(forKey: Constants.gender)
        UserDefaults.standard.removeObject(forKey: Constants.dob)
        UserDefaults.standard.removeObject(forKey: Constants.custAccount)

        UserDefaults.standard.set(true, forKey: Constants.NotificationOnOff)
        UserDefaults.standard.set(false, forKey: Constants.NotificationOnOffSub)


        GuestSignupAPI()
    }
    
    func GuestSignupAPI(){
        
        EndPoint.GuestSignUP(t: UserModel.self) { (result) in
            switch result {
            
            case .onSuccess(let user):
                print(user)
                
                if user != nil{
                    UserDefaults.standard.set(true, forKey: Constants.guestUser)
                    UserDefaults.standard.set(user?.email, forKey: Constants.email)
                    UserDefaults.standard.set(user?.userId, forKey: Constants.userId)
                    UserDefaults.standard.set(user?.token, forKey: Constants.token)
                    UserDefaults.standard.set(user?.firstName, forKey: Constants.firstName)
                    UserDefaults.standard.set(user?.lastName, forKey: Constants.lastName)
                    UserDefaults.standard.set(user?.gender, forKey: Constants.gender)
                    UserDefaults.standard.set(user?.dob, forKey: Constants.dob)
                    UserDefaults.standard.set(user?.custAccount, forKey: Constants.custAccount)

                    UserDefaults.standard.set(true, forKey: Constants.NotificationOnOff)
                    UserDefaults.standard.set(false, forKey: Constants.NotificationOnOffSub)
                    UserDefaults.standard.synchronize()

                    NotificationAlert().NotificationAlert(titles: "User created successfully")
                    self.pushToRoot()

                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }

}
