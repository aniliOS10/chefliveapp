//
//  AppMessageRegisterUser.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import Foundation
class AppMessageOTPVerify{
    struct Placeholder {
        static let EnterEmailOtp = NSLocalizedString("EnterEmailOtp", comment: "")
        static let EnterPhoneOtp = NSLocalizedString("EnterPhoneOtp", comment: "")
        static let EnterOTP = NSLocalizedString("EnterOTP", comment: "")
        static let continueButton = NSLocalizedString("continue", comment: "")
        static let submitButton = NSLocalizedString("submit", comment: "")
        static let DidNotReceiveOTP = NSLocalizedString("DidNotReceiveOTP", comment: "")
        static let ResendOTP = NSLocalizedString("ResendOTP", comment: "")
    }
    struct ValidationError {
    static let PersonalInformation = NSLocalizedString("PersonalInformation", comment: "")
    static let PleaseFillBelowForm = NSLocalizedString("PleaseFillBelowForm", comment: "")
   
    static let EmptyEmailOtp = NSLocalizedString("EmptyEmailOtp", comment: "")
    static let EmptyPhoneOtp = NSLocalizedString("EmptyPhoneOtp", comment: "")
        
    static let IncorrectEmailOtp = NSLocalizedString("IncorrectEmailOtp", comment: "")
    static let IncorrectPhoneOtp = NSLocalizedString("IncorrectPhoneOtp", comment: "")
  
    }
    
}
