//
//  MockUp_User.swift
//  PetCare
//
//  Created by Apple on 14/09/21.
//

import Foundation
struct MockUp_User{
    var accessToken : String!
        var dialCode : Int!
        var email : String!
        var isEmailVerified : Int!
        var isPhoneNoVerified : Int!
        var isSocialUser : Int!
        var phoneNumber : String!
        var profilePic : String!
        var userId : String!
        var userName : String!
    init() {
        
    }
    init(fromDictionary dictionary: [String:Any],message1:String){
        accessToken = dictionary["accessToken"] as? String
                dialCode = dictionary["dialCode"] as? Int
                email = dictionary["email"] as? String
                isEmailVerified = dictionary["isEmailVerified"] as? Int
                isPhoneNoVerified = dictionary["isPhoneNoVerified"] as? Int
                isSocialUser = dictionary["isSocialUser"] as? Int
                phoneNumber = dictionary["phoneNumber"] as? String
                profilePic = dictionary["profilePic"] as? String
                userId = dictionary["userId"] as? String
                userName = dictionary["userName"] as? String
        }

}
extension MockUp_User:Parceable {
    static func parseObject(dictionary: [String : AnyObject]) -> Result<MockUp_User, ErrorResult> {
        if let dict = dictionary["data"] as? [String:Any],let message =  dictionary["message"] as? String{
          
                let object =   MockUp_User.init(fromDictionary: dict,message1: message)
                return Result.success(object)
            
          
        }else{
            return Result.failure(ErrorResult.parser(string: "User json is not valid"))
        }
    }
}
