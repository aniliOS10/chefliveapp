//
//  MockUpVerifyPhone.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import Foundation
struct MockUp_VerifyPhone{
    var code : Int!
 //   var data : Data!
    var message : String!
        var status : Int!
    init(fromDictionary dictionary: [String:Any]){
            code = dictionary["code"] as? Int
          /*  if let dataData = dictionary["data"] as? [String:Any]{
                   // data = Data(fromDictionary: dataData)
                }*/
            message = dictionary["message"] as? String
            status = dictionary["status"] as? Int
        }

}
extension MockUp_VerifyPhone:Parceable {
    static func parseObject(dictionary: [String : AnyObject]) -> Result<MockUp_VerifyPhone, ErrorResult> {
        if (dictionary["message"] as? String) != nil{
            let object =   MockUp_VerifyPhone.init(fromDictionary: dictionary)
            return Result.success(object)
        }else{
            return Result.failure(ErrorResult.parser(string: "Unable to parse conversion rate"))
        }
    }
}
