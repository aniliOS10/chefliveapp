//
//  RegisterUser_Register.swift
//  PetCare
//
//  Created by Apple on 06/09/21.
//

import Foundation
import Alamofire
protocol RegisterUserServiceServiceProtocol :class {
    func hitRegisterUser( _ para:[String:AnyObject]?,completion:@escaping((Result<MockUp_User,ErrorResult>)->Void))
}
class RegisterUserService: RequestHandler,RegisterUserServiceServiceProtocol  {
    
    
    
    static let shared = RegisterUserService()
    let endpoint = APIUrl.baseUrl
    let verifyPhone = APIUrl.verifyPhone()
    let registerUser = APIUrl.registerUser(parameter: nil).url
    
    var request:DataRequest?
    
    func hitRegisterUser(_ para: [String : AnyObject]?, completion: @escaping ((Result<MockUp_User, ErrorResult>) -> Void)) {
        self.cancelSendOtpService()
        //hitting service
        request =  RequestService().loadData_Alamofire(urlString: registerUser, method: .post, parameters: para, completion: self.networkResult(completion: completion))
    }
    
   
    
    
    func cancelSendOtpService(){
        self.request?.cancel()
    }
    
   
    

}
