//
//  VerifyPhone.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import Foundation
import Alamofire
protocol VerifyPhoneOTPServiceProtocol :class {
    func hitVerifyPhone( _ para:[String:AnyObject]?,completion:@escaping((Result<MockUp_VerifyPhone,ErrorResult>)->Void))
}
class VerifyPhoneService: RequestHandler,VerifyPhoneOTPServiceProtocol  {
    
    
    
    static let shared = VerifyPhoneService()
    let endpoint = APIUrl.baseUrl
    let verifyPhone = APIUrl.verifyPhone().url
    var request:DataRequest?
    
    func hitVerifyPhone(_ para: [String : AnyObject]?, completion: @escaping ((Result<MockUp_VerifyPhone, ErrorResult>) -> Void)) {
        self.cancelSendOtpService()
        //hitting service
        request =  RequestService().loadData_Alamofire(urlString: verifyPhone, method: .post, parameters: para, completion: self.networkResult(completion: completion))
    }
    
   
    
    
    func cancelSendOtpService(){
        self.request?.cancel()
    }
    
   
    

}
