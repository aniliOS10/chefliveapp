//
//  OTPVerifyViewModel.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import Foundation
protocol ViewModel {
    var onCompletionHandling : ((Any) -> Void)? { get set }
    var  compeltionBlockLocaliable: ((Any) -> Void)? { get set }
    var onErrorHandling : ((ErrorResult?) -> Void)? { get set }
    func updateLocalizable()
}
@available(iOS 13.0, *)
class OTPverifyViewModel:ViewModel {
    var onCompletionHandling: ((Any) -> Void)?
    
    var compeltionBlockLocaliable: ((Any) -> Void)?
   
    var onErrorHandling : ((ErrorResult?) -> Void)?

    weak var userInfo:UserInfo?
    weak var service: VerifyPhoneService?
    weak var registerUserService: RegisterUserService?
    private let validation:ValidationOTPverifyVC?
    
    init(service:VerifyPhoneService, registerUserService:RegisterUserService,validation:ValidationOTPverifyVC) {
        self.service = service
        self.registerUserService = registerUserService
        self.validation =  validation
    }
    func hitVerifyPhone(param:[String:AnyObject]?){
        guard let service = self.service else {
            onErrorHandling?(ErrorResult.custom(string: "Missing Service"))
            return
        }
        guard let param1 = param else {
            onErrorHandling?(ErrorResult.custom(string: "Missing Parmeters"))
            return
        }
      
        service.hitVerifyPhone(param1) { results in
          
            switch results{
            case .success(let object):
               
                self.onCompletionHandling!(object)
              break
            
            case .failure(let error):
                self.onErrorHandling?(error)
            }
        }
        
    }
    func hitRegisterUser(param:[String:AnyObject]?){
        guard let service = self.registerUserService else {
            onErrorHandling?(ErrorResult.custom(string: "Missing Service"))
            return
        }
        guard let param1 = param else {
            onErrorHandling?(ErrorResult.custom(string: "Missing Parmeters"))
            return
        }
        
        service.hitRegisterUser(param1) { results in
          
            switch results{
            case .success(let object):
                CoreDataManager.shared.deleteObject_string(name:"UserInfo")
                CoreDataManager.shared.insertEntity(entityName: "UserInfo", mockUp: object)
                self.onCompletionHandling!(object as AnyObject)
              break
            
            case .failure(let error):
                self.onErrorHandling?(error)
            }
        }
        
    }
    func  validatePhoneOTP(_ inputString:String,phone:String)throws -> String?
    {
        do{
            let validate = try self.validation?.validatePhoneOTP(inputString, phoneCode: phone) ?? nil
            return validate
        }
        catch{
            onErrorHandling?(ErrorResult.custom(string: error.localizedDescription))
            throw ErrorResult.custom(string: error.localizedDescription)
        }
        
    }
    func  validateEmailOTP(_ inputString:String,otp:String)throws -> String?
    {
        do{
            let validate = try self.validation?.validateEmailOTP(inputString, emailCode: otp) ?? nil
            return validate
            
        }
        catch{
            onErrorHandling?(ErrorResult.custom(string: error.localizedDescription))
            throw ErrorResult.custom(string: error.localizedDescription)
        }
     
    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(AppMessageOTPVerify())
    }
}
