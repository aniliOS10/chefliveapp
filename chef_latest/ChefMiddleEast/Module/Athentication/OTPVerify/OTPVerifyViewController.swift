//
//  OTPVerifyViewController.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import UIKit
import SVPinView
@available(iOS 13.0, *)
class OTPVerifyViewController: BaseViewController {
    
    var emailOtp:String = ""
    var phoneOtp:String = ""
    var code:String = ""
    var regierUser_par:RegierUser_par?
    var emailPinOtp:String = ""
    var phonePinOtp:String = ""
    var isRegisterNewUser:Bool = false
    var isPhoneVerification:Bool = false
    var password:String?
    var fName:String?
    var lName:String?
    var email:String?
    var phoneNo:String?
    var Profileimg : UIImage?

    @IBOutlet weak var heightOfPinView: NSLayoutConstraint!
    
    
    //  @IBOutlet weak var buttonContinue: UIButton!
    //  @IBOutlet weak var labelMobileNumberConstant: UILabel!
    //  @IBOutlet weak var labelEmailConstant: UILabel!
    //  @IBOutlet weak var pinViewEmail: SVPinView!
    @IBOutlet weak var pinViewPhone: SVPinView!
    @IBOutlet weak var labelDidNotGetOTP: UILabel!
    @IBOutlet weak var buttonResendOTP: UIButton!
    @IBOutlet weak var buttonVerifyOTP: UIButton!
    
    //  @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var titleString: UILabel!
    @IBOutlet weak var subTitleString: UILabel!
    @IBOutlet weak var backViewWhite: UIView!
    @IBOutlet weak var lblEnterPin: UILabel!
    @IBOutlet weak var btnCountTime: UIButton!
    
    var countdownTimer: Timer!
    var totalTime = 30
    
    
    var validation:ValidationOTPverifyVC
    var registerUserService: RegisterUserService?
    var service: VerifyPhoneService?
    var isForgotPassword:Bool = false
    
    
    //
    @IBOutlet weak var pinViewEmail: SVPinView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var basePhonePin: UIView!
    @IBOutlet weak var baseEmailPin: UIView!
    
    
    lazy var viewModel : OTPverifyViewModel = {
        // let userInfoObj = CoreDataManager.shared.userInfoObj
        let viewModel = OTPverifyViewModel(service: self.service ?? VerifyPhoneService(), registerUserService: self.registerUserService ?? RegisterUserService(), validation: self.validation)
        return viewModel
    }()
    
    init(validation:ValidationOTPverifyVC,service: VerifyPhoneService,registerUserService: RegisterUserService) {
        self.validation = validation
        self.service = service
        self.registerUserService = registerUserService
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        self.validation = ValidationOTPverifyVC()
        self.service = VerifyPhoneService()
        self.registerUserService = RegisterUserService()
        super.init(coder: coder)
    }
    
    //MARK:- VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.startTimer()
        
        self.buttonResendOTP.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessageOTPVerify.Placeholder.ResendOTP, size: 14, fontName: FontName.Optima.Bold), for: .normal)
        
        self.buttonVerifyOTP.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessageOTPVerify.Placeholder.submitButton, size: 21.0, fontName: FontName.Optima.Bold), for: .normal)
        
        self.backViewWhite .layer.masksToBounds = true
        self.backViewWhite.layoutIfNeeded()
        Utility.shared.roundParticular(corners: [.topLeft,.topRight], cornerRadius: 40, view1: backViewWhite)
        
        
        
        if isRegisterNewUser  {
            subTitleString.text = "Please enter 6 digits code we send to your email"
            
//            var intvalue = 0
//            intvalue = UserDefaults.standard.value(forKey: Constants.phoneOtp) as? Int ?? 0
//
//            let alert = UIAlertController(title: "Phone OTP", message: String(intvalue), preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default))
//            self.present(alert, animated: true)
            
        } else if isPhoneVerification {
            subTitleString.text = "Please enter 6 digits code we send to your phone"
        } else {
            subTitleString.text = "Please enter 6 digits code we send to your email"
        }
        
        
        //  Utility.shared.makeShadowsOfView_roundCorner(view: forgotTextField, shadowRadius: 4.0, cornerRadius: 30, borderColor: UIColor.lightGray.withAlphaComponent(0.5))
        
        /*  self.viewModel.compeltionBlockLocaliable = {[weak self] object in
         
         // self?.labelTitle.text = AppMessageOTPVerify.Placeholder.EnterOTP
         
         //    self.labelEmailConstant.text = AppMessageOTPVerify.Placeholder.EnterEmailOtp
         // self?.labelMobileNumberConstant.text = AppMessageOTPVerify.Placeholder.EnterEmailOtp
         // self?.buttonContinue.setTitle(AppMessageOTPVerify.Placeholder.continueButton, for: .normal)
         //  self?.labelDidNotGetOTP.text = AppMessageOTPVerify.Placeholder.DidNotReceiveOTP
         // self?.buttonResendOTP.setTitle(AppMessageOTPVerify.Placeholder.ResendOTP, for: .normal)
         
         self?.buttonResendOTP.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessageOTPVerify.Placeholder.ResendOTP, size: 14, fontName: FontName.Optima.Bold), for: .normal)
         }
         self.viewModel.updateLocalizable()
         
         self.viewModel.onErrorHandling = {[weak self] error in
         let alertVC  = AlertVC()
         alertVC.delegateAlert = self
         var errorMessage = ""
         switch error {
         case .custom(let message):
         errorMessage = message
         case .network(let message):
         errorMessage = message
         case .parser(let message):
         errorMessage = message
         default:
         errorMessage = ""
         }
         guard let strongSelf = self else { return }
         DispatchQueue.main.async {
         self?.hideLoader()
         alertVC.withOk(message: errorMessage, vc: strongSelf, okStyle: .default, controllerStyle: .alert)
         }
         }*/
        /*  self.viewModel.onCompletionHandling = {[weak self] object in
         
         print(object)
         guard  let forgot = self?.isForgotPassword else {
         return
         }
         if forgot
         {
         DispatchQueue.main.async {
         self?.hideLoader()
         print("forgotPassword")
         
         print("OTP is verified")
         
         let controller:ResetPasswordViewController = UIStoryboard(storyboard: .athentication).initVC()
         controller.regierUser_par = self?.regierUser_par
         self?.navigationController?.pushViewController(controller, animated: true)
         }
         }else{
         guard let strongSelf = self else { return }
         DispatchQueue.main.async {
         self?.hideLoader()
         print( CoreDataManager.shared.userInfoObj.userInfo.phoneNumber)
         let mockUp = object as! MockUp_User
         let alertVC  = AlertVC()
         alertVC.delegateAlert = self
         alertVC.typeString = "Registered"
         alertVC.withOk(message: "User have registered successfully", vc: strongSelf, okStyle: .default, controllerStyle: .alert)
         
         }
         }
         
         }*/
        if isRegisterNewUser {
            basePhonePin.isHidden = true
            baseEmailPin.isHidden = false
            pinViewPhone.isHidden = true

          //  configurePhonePinView()
            configureEmailPinView()
        } else if isPhoneVerification {
            pinViewEmail.isHidden = true
            basePhonePin.isHidden = false
            baseEmailPin.isHidden = true
            configurePhonePinView()
        } else {
            basePhonePin.isHidden = true
            baseEmailPin.isHidden = false
            pinViewPhone.isHidden = true
            configureEmailPinView()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        
        btnCountTime.setTitle("(\(timeFormatted(totalTime)))", for: .normal)
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
     //   UserDefaults.standard.removeObject(forKey: Constants.otp)
        countdownTimer.invalidate()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
      //  let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "00:%02d", seconds)

      //  return String(format: "%02d:%02d", minutes, seconds)
    }
    
    func configurePhonePinView(){
        self.pinViewPhone.font = UIFont.init(name: FontName.Optima.Bold, size: 26.0) ?? UIFont.systemFont(ofSize: 2)
        self.pinViewPhone.borderLineColor = UIColor.lightGray.withAlphaComponent(0.2)
        self.pinViewPhone.activeBorderLineColor = buttonVerifyOTP.backgroundColor ?? UIColor.red
        self.pinViewPhone.borderLineThickness = 1.5
        self.pinViewPhone.activeBorderLineThickness = 2
        
        self.pinViewPhone.layer.shadowColor = UIColor.lightGray.cgColor
        self.pinViewPhone.layer.shadowOpacity = 1
        self.pinViewPhone.layer.shadowOffset = .zero
        self.pinViewPhone.layer.shadowRadius = 3
        
        let viewWidth = self.pinViewPhone.frame.width
        let spaceBetweenViews:CGFloat = 5.0
        let oneViewWidth:CGFloat = (((viewWidth/6)) - ((spaceBetweenViews * 3)/6))
        print(viewWidth)
        print(spaceBetweenViews)
        print(oneViewWidth)
        self.pinViewPhone.interSpace = spaceBetweenViews
        self.pinViewPhone.style = .box
        
        print("oneViewWidth")
        print(oneViewWidth * 2 + 90)
        
        if isRegisterNewUser {
            self.heightOfPinView.constant = oneViewWidth * 2 + 90
        } else if isPhoneVerification {
            self.heightOfPinView.constant = oneViewWidth + 80
        }
        
       // self.pinViewPhone.fieldCornerRadius = (oneViewWidth)/2
        self.pinViewPhone.fieldCornerRadius = 30
        pinViewPhone.textColor = buttonVerifyOTP.backgroundColor ?? UIColor.red
        pinViewPhone.shouldSecureText = false
        pinViewPhone.pinLength = 6
        pinViewPhone.keyboardType = .numberPad
        pinViewPhone.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 360, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        
        pinViewPhone.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
            self.phonePinOtp = String(pin)
        }
    }
    
    
    func configureEmailPinView(){
        self.pinViewEmail.font = UIFont.init(name: FontName.Optima.Bold, size: 26.0) ?? UIFont.systemFont(ofSize: 2)
        self.pinViewEmail.borderLineColor = UIColor.lightGray.withAlphaComponent(0.2)
        self.pinViewEmail.activeBorderLineColor = buttonVerifyOTP.backgroundColor ?? UIColor.red
        self.pinViewEmail.borderLineThickness = 1.5
        self.pinViewEmail.activeBorderLineThickness = 2
        
        self.pinViewEmail.layer.shadowColor = UIColor.lightGray.cgColor
        self.pinViewEmail.layer.shadowOpacity = 1
        self.pinViewEmail.layer.shadowOffset = .zero
        self.pinViewEmail.layer.shadowRadius = 3
        
        let viewWidth = self.pinViewEmail.frame.width
        let spaceBetweenViews:CGFloat = 5.0
        let oneViewWidth:CGFloat = (((viewWidth/6)) - ((spaceBetweenViews * 3)/6))
        print(viewWidth)
        print(spaceBetweenViews)
        print(oneViewWidth)
        self.pinViewEmail.interSpace = spaceBetweenViews
        self.pinViewEmail.style = .box
        
        
        if isRegisterNewUser {
            self.heightOfPinView.constant = oneViewWidth + 70
            self.view.layoutIfNeeded()

        } else if isPhoneVerification {
            self.heightOfPinView.constant = oneViewWidth
            self.view.layoutIfNeeded()

        }
        
        
        
        self.pinViewEmail.fieldCornerRadius = (self.pinViewEmail.frame.height)/2
        
        pinViewEmail.textColor = buttonVerifyOTP.backgroundColor ?? UIColor.red
        pinViewEmail.shouldSecureText = false
        pinViewEmail.pinLength = 6
        pinViewEmail.keyboardType = .numberPad
        pinViewEmail.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 360, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        
        pinViewEmail.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
            self.emailPinOtp = String(pin)
        }
    }
    
    /*  func configureEmailPinView(){
     
     pinViewEmail.borderLineColor = UIColor.black
     pinViewEmail.activeBorderLineColor = UIColor.black
     //        pinVw.font = UIFont(name: "CircularStd-Bold", size: 17)!
     pinViewEmail.keyboardType = .numberPad
     pinViewEmail.pinInputAccessoryView = { () -> UIView in
     let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 360, height: 50))
     doneToolbar.barStyle = UIBarStyle.default
     let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
     let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
     
     var items = [UIBarButtonItem]()
     items.append(flexSpace)
     items.append(done)
     
     doneToolbar.items = items
     doneToolbar.sizeToFit()
     return doneToolbar
     }()
     
     pinViewEmail.didChangeCallback = { pin in
     print("The entered pin is \(pin)")
     self.emailPinOtp = String(pin)
     }
     
     }*/
    
    
    //MARK:-  Dismiss Keyboard Action
    @objc func dismissKeyboard(){
        self.view.endEditing(false)
    }
    
    @IBAction func btnConfirm(_ sender: Any){
        
        if isRegisterNewUser {
           if let otp = UserDefaults.standard.value(forKey: Constants.otp) as? Int, otp != 0 {
              //  if let phoneOtp = UserDefaults.standard.value(forKey: Constants.phoneOtp) as? Int, phoneOtp != 0 {
                 //   if (otp == Int(self.emailPinOtp)) && (phoneOtp == Int(self.phonePinOtp)) {
                
             
                    if (otp == Int(self.emailPinOtp)) {
                        
                        
                        let params = SignUpModel()
                        params.deviceType = "ios"
                        params.deviceToken = Utility.shared.getAppUniqueId()
                        params.password = password?.trimmingCharacters(in: .whitespaces)
                       
                        let envelopeParams = EnvelopeParams()
                        envelopeParams.custName = fName?.trimmingCharacters(in: .whitespaces)
                        envelopeParams.lastName = lName?.trimmingCharacters(in: .whitespaces)
                        envelopeParams.email = email?.trimmingCharacters(in: .whitespaces)
                        envelopeParams.phoneMobile = phoneNo?.trimmingCharacters(in: .whitespaces)

                        
                        envelopeParams.dataOrigin = "mobile"
                        envelopeParams.customerType = "b2c"
                        params.envelopeParams = envelopeParams
                        
                        signUpParams = params
                        
                        let controller:AddressSignUPViewController =  UIStoryboard(storyboard: .athentication).initVC()
                         
                        if self.Profileimg != nil{
                            controller.imgProfile = self.Profileimg
                        }
                        self.navigationController?.pushViewController(controller, animated: true)
                        
//                    } else {
//                        NotificationAlert().NotificationAlert(titles: "Enter OTP is incorrect")
//                    }
//                }else {
//                    NotificationAlert().NotificationAlert(titles: "Enter OTP is incorrect")
//                }
                
            } else {
                NotificationAlert().NotificationAlert(titles: "Enter OTP is incorrect")
            }
           }
            else {
                NotificationAlert().NotificationAlert(titles: "Enter OTP is incorrect")
            }
        }
        else if isForgotPassword {
            if let otp = UserDefaults.standard.value(forKey: Constants.otp) as? Int, otp != 0 {
                if (otp == Int(self.emailPinOtp)) {
                    let controller:ResetPasswordViewController = UIStoryboard(storyboard: .athentication).initVC()
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                else{
                    NotificationAlert().NotificationAlert(titles: "Enter OTP is incorrect")
                }
            }
            else{
                NotificationAlert().NotificationAlert(titles: "Enter OTP is incorrect")
            }
        }
        else if isPhoneVerification {
            if let phoneOtp = UserDefaults.standard.value(forKey: Constants.phoneOtp) as? Int, phoneOtp != 0 {
                if (phoneOtp == Int(self.phonePinOtp)) {
                    UserDefaults.standard.set(EndPoint.phoneNumber, forKey: Constants.phone)
                    self.navigationController?.popViewController(animated: true)
                } else {
                    NotificationAlert().NotificationAlert(titles: "Enter OTP is incorrect")
                }
            } else {
                NotificationAlert().NotificationAlert(titles: "Enter OTP is incorrect")
            }
        } else {
            if let emailOtp = UserDefaults.standard.value(forKey: Constants.otp) as? Int, emailOtp != 0 {
                if (emailOtp == Int(self.emailPinOtp)) {
                    UserDefaults.standard.set(EndPoint.email, forKey: Constants.email)
                    self.navigationController?.popViewController(animated: true)
                } else {
                    NotificationAlert().NotificationAlert(titles: "Enter OTP is incorrect")
                }
            } else {
                NotificationAlert().NotificationAlert(titles: "Enter OTP is incorrect")
            }
        }
        
        /*
        if let otp = UserDefaults.standard.value(forKey: Constants.otp) as? Int, otp != 0 {
            
            if otp == Int(self.phonePinOtp) {
                if isRegisterNewUser ?? false {
                    let params = SignUpModel()
                    params.deviceType = "ios"
                    params.deviceToken = Utility.shared.getAppUniqueId()
                    params.password = password?.trimmingCharacters(in: .whitespaces)
                   
                    let envelopeParams = EnvelopeParams()
                    envelopeParams.custName = fName?.trimmingCharacters(in: .whitespaces)
                    envelopeParams.lastName = lName?.trimmingCharacters(in: .whitespaces)
                    envelopeParams.email = email?.trimmingCharacters(in: .whitespaces)
                    envelopeParams.dataOrigin = "mobile"
                    envelopeParams.customerType = "b2c"
                    params.envelopeParams = envelopeParams
                    
                    signUpParams = params
                    
                    let controller:AddressSignUPViewController =  UIStoryboard(storyboard: .athentication).initVC()
                     
                    if self.Profileimg != nil{
                        controller.imgProfile = self.Profileimg
                    }
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                else {
                    let controller:ResetPasswordViewController = UIStoryboard(storyboard: .athentication).initVC()
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
            else{
                NotificationAlert().NotificationAlert(titles: "Enter OTP is incorrect")
            }
        }
        else{
            NotificationAlert().NotificationAlert(titles: "Enter OTP is incorrect")
        }
         */
    }
    
    @IBAction func buttonBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonResendOTPPressed(_ sender: Any) {
        
        if let email = UserDefaults.standard.string(forKey: Constants.email), !email.isEmpty {
            
            let params = UserModel()
            params.email = email
            UserDefaults.standard.set(email, forKey: Constants.email)
            self.resendOTPAPI(Model: params)
        }
        else{
            if email != nil {
                let params = UserModel()
                params.email = email
                self.verifyEmailSendOTP()
            }
        }
    }
    
    // MARK: - Verify Email Send OTP API
    func verifyEmailSendOTP(){
        self.showLoader()
        EndPoint.VerifyEmailSendOTP(t: UserModel.self) { result in
            switch result {
            case .onSuccessWithIntegerValue(let strOtp):
                print(strOtp)
                self.hideLoader()

                self.pinViewPhone.clearPin()
                UserDefaults.standard.set(strOtp, forKey: Constants.otp)
              //  self.totalTime = 30
              //  self.startTimer()
                NotificationAlert().NotificationAlert(titles: "OTP sent successfully")
            case .onFailure(let error):
                NotificationAlert().NotificationAlert(titles: error)
                print(error)
                self.hideLoader()
            }

        }
    }
    
    
    // MARK: - Resend OTP API
    func resendOTPAPI(Model: UserModel){
        self.showLoader()
        EndPoint.ForgetPassword(params: Model,t: UserModel.self) { result in
            switch result {
            case .onSuccessWithIntegerValue(let strOtp):
                print(strOtp)
                self.pinViewPhone.clearPin()
                UserDefaults.standard.set(strOtp, forKey: Constants.otp)
                self.totalTime = 30
                self.startTimer()
                NotificationAlert().NotificationAlert(titles: "OTP sent successfully")
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                NotificationAlert().NotificationAlert(titles: error)
                print(error)
                self.hideLoader()
            }
        }
    }
}

@available(iOS 13.0, *)
extension OTPVerifyViewController: AlertDelegate {
    func okButtonPressed(alertController: UIAlertController, alert: AlertVC) {
        if alert.typeString == "Registered"{
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func cancelButtonPressed(alertController: UIAlertController, alert: AlertVC) {
        
    }
}
