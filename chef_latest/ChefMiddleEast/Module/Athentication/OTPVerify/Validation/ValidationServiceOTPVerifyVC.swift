//
//  ValidationServiceOTPVerifyVC.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import Foundation
struct ValidationOTPverifyVC {
    func validateEmailOTP(_ emailOtp:String?,emailCode:String?) throws -> String {
        guard let emailValue = emailOtp else { throw ValidationErrorOTPverifyVC_Email.emptyEmailOtp }
        guard emailValue.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorOTPverifyVC_Email.emptyEmailOtp }
       
        guard emailValue.count >= 1 else { throw ValidationErrorOTPverifyVC_Email.emptyEmailOtp }
        
        guard let emailCodeValue = emailCode else { throw ValidationErrorOTPverifyVC_Email.emptyCode }
        
        if  emailValue == emailCodeValue {}else{ throw ValidationErrorOTPverifyVC_Email.incorrectEmailOtp }
        
        return emailValue
    }
    func validatePhoneOTP(_ phoneOTP:String?,phoneCode:String?) throws -> String {
        guard let phoneOTPValue = phoneOTP else { throw ValidationErrorOTPverifyVC_phone.emptyPhoneOtp }
        guard phoneOTPValue.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorOTPverifyVC_phone.emptyPhoneOtp }
       
        guard phoneOTPValue.count >= 4 else { throw ValidationErrorOTPverifyVC_phone.emptyPhoneOtp }
       
       /* if let code = phoneCode{
            if  phoneOTPValue == code{} else { throw ValidationErrorOTPverifyVC_phone.incorrectPhoneOtp }
        }*/
      
        
      
 
        return phoneOTPValue
    }
}
enum ValidationErrorOTPverifyVC_Email:LocalizedError {
    case emptyEmailOtp
    case emptyCode
    case incorrectEmailOtp
    var errorDescription: String? {
        switch self {
        case .emptyEmailOtp:
            return AppMessageOTPVerify.ValidationError.EmptyEmailOtp
        case .emptyCode:
            return  NSLocalizedString("Email code is null", comment: "")
        case .incorrectEmailOtp:
            return AppMessageOTPVerify.ValidationError.IncorrectEmailOtp
        }
    }
}
enum ValidationErrorOTPverifyVC_phone:LocalizedError {
    case emptyPhoneOtp
    case incorrectPhoneOtp
    case emptyCode
    var errorDescription: String? {
        switch self {
        case .emptyPhoneOtp:
            return AppMessageOTPVerify.ValidationError.EmptyPhoneOtp
        case .emptyCode:
            return  NSLocalizedString("Phone code is null", comment: "")
        case .incorrectPhoneOtp:
            return AppMessageOTPVerify.ValidationError.IncorrectPhoneOtp
        }
    }
}

