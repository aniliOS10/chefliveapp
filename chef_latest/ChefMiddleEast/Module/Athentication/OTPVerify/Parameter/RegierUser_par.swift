//
//  RegierUser_par.swift
//  PetCare
//
//  Created by Apple on 06/09/21.
//

import Foundation
struct RegierUser_par: DictionaryEncodable {
var userName: String?
var dialCode: String?
var phoneNumber: String?
var verifyCode: String?
var password: String?
var deviceType: String?
var deviceToken: String?
var isSocialUser: Bool?
var email: String?
}
