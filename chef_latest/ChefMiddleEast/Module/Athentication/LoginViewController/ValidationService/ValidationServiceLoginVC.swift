//
//  ValidationServiceLoginVC.swift
//  BaseProject
//
//  Created by rupinder singh on 06/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import Foundation
struct ValidationServiceLoginVC {
    func validateUserID(_ id:String?) throws -> String {
        guard let userNameValue = id else { throw ValidationErrorLoginVC.invalidUserName }
        guard id?.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorLoginVC.emptyEmailOrPhone }
        guard userNameValue.count > 3 else { throw ValidationErrorLoginVC.userNameTooShort }
        guard userNameValue.count < 40 else { throw ValidationErrorLoginVC.userNameTooLong }
        return userNameValue
    }
    func validatePassword(_ password:String?) throws -> String {
        guard let upasswordValue = password else { throw ValidationErrorLoginVC.invalidUserName }
        guard password?.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorLoginVC.emptPassword }
        guard upasswordValue.count >= 5 else { throw ValidationErrorLoginVC.passwordTooShort }
        guard upasswordValue.count < 20 else { throw ValidationErrorLoginVC.passwordTooLong }
        return upasswordValue
    }
}
enum ValidationErrorLoginVC:LocalizedError {
    case invalidUserName
    case invalidPassword
    case emptyUserName
    case emptyEmailOrPhone
    case emptPassword
    case userNameTooLong
    case userNameTooShort
    case passwordTooShort
    case passwordTooLong
    var errorDescription: String? {
        switch self {
        case .invalidUserName:
            return AppMessageLoginVC.ValidationError.invalidUserName
        case .invalidPassword:
            return AppMessageLoginVC.ValidationError.invalidPassword
        case.emptyEmailOrPhone:
        return AppMessageLoginVC.ValidationError.EmptyEmailOrPhone
        case .emptyUserName:
            return AppMessageLoginVC.ValidationError.emptyUserName
        case .emptPassword:
            return AppMessageLoginVC.ValidationError.emptyPassword
        case .userNameTooShort:
            return AppMessageLoginVC.ValidationError.userNameTooShort
        case .userNameTooLong:
            return AppMessageLoginVC.ValidationError.userNameTooLong
        case .passwordTooShort:
            return AppMessageLoginVC.ValidationError.passwordTooShort
        case .passwordTooLong:
            return AppMessageLoginVC.ValidationError.passwordTooLong
        }
    }
}
enum loginError:LocalizedError {
   
    case invalidCreditional
    var errorDescription: String?{
        switch self {
        case .invalidCreditional:
            return AppMessageLoginVC.ValidationError.invalidCreditional
        }
    }
}
