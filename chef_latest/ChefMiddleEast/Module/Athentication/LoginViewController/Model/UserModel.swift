//
//  UserModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 12/12/21.
//

import Foundation
import ObjectMapper

class UserModel : BaseResponse {
   
    var email: String?
    var password: String?
    var name: String?
    var firstName: String?
    var lastName: String?
    var custAccount: String?
    var isEmailVerified: Bool?
    var phoneNumber: String?
    var isPhoneVerified: Bool?
    var dob: String?
    var userImg: String?
    var isNotificationOn: Bool?
    var status: String?
    var token: String?
    var userId: String?
    var isUserNameUpdated : Bool?
    var createdAt: String?
    var deviceToken : String?
    var devicetype : String?
    var currentPassword : String?
    var newPassword : String?
    var confirmPassword : String?
    var gender : String?
    var isNewsletterSubscribed: Bool?
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        email <- map["email"]
        isUserNameUpdated <- map["isUserNameUpdated"]
        password <- map["password"]
        userId <- map["userId"]
        name <- map["name"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        custAccount <- map["custAccount"]
        isEmailVerified <- map["isEmailVerified"]
        phoneNumber <- map["phoneNumber"]
        isPhoneVerified <- map["isPhoneVerified"]
        dob <- map["birthDate"]
        userImg <- map["userImgPath"]
        isNotificationOn <- map["isNotificationEnabled"]
        isNewsletterSubscribed <- map["isNewsletterSubscribed"]

        status <- map["status"]
        token <- map["token"]
        createdAt <- map["createdAt"]
        deviceToken <- map["deviceToken"]
        devicetype <- map["devicetype"]
        currentPassword <- map["currentPassword"]
        newPassword <- map["newPassword"]
        confirmPassword <- map["confirmPassword"]
        gender  <- map["gender"]
        
    }
}
