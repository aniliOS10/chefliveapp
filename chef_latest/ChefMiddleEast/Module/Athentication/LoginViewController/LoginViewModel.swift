//
//  LoginViewModel.swift
//  BaseProject
//
//  Created by rupinder singh on 17/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import Foundation
@available(iOS 13.0, *)
class LoginViewModel:ViewModel {
    
    
    var onErrorHandling: ((ErrorResult?) -> Void)?
    
    var onCompletionHandling: ((Any) -> Void)?
    
    var compeltionBlockLocaliable: ((Any) -> Void)?
    
    weak var userInfo:UserInfo?
    weak var service: LoginService?
    
    private let validationServiceLoginVC:ValidationServiceLoginVC
   // var onErrorHandling : ((ErrorResult?) -> Void)?
   // var completion:((MockUp_User)-> Void)?
    
    init(service:LoginService ,validation:ValidationServiceLoginVC) {
        self.service = service
        self.validationServiceLoginVC = validation
    }
    func hitLoginUser(param:[String:Any]?){
        guard let service = self.service else {
            onErrorHandling?(ErrorResult.custom(string: "Missing Service"))
            return
        }
        guard let param1 = param else {
            onErrorHandling?(ErrorResult.custom(string: "Missing Parmeters"))
            return
        }
     
        service.hitLoginUser(param1) { results in
          
            switch results{
            case .success(let mockUp_UserInfo):
                CoreDataManager.shared.deleteObject_string(name: Entity.user.rawValue)
                CoreDataManager.shared.insertEntity(entityName: Entity.user.rawValue, mockUp: mockUp_UserInfo)
                self.onCompletionHandling!(mockUp_UserInfo)
                
            case .failure(let error):
                self.onErrorHandling?(error)
            }
        }
        
    }
    func updateLocalizable() {
        self.compeltionBlockLocaliable!(AppMessageLoginVC())
    }
   
    
    func  validateUserID(_ inputString:String)throws -> String?
    {
        do{
            let validate = try self.validationServiceLoginVC.validateUserID(inputString)
            return validate
        }
        catch{
            onErrorHandling?(ErrorResult.custom(string: error.localizedDescription))
            throw ErrorResult.custom(string: error.localizedDescription)
        }
        
    }
    func  validatePassword(_ inputString:String)throws -> String?
    {
        do{
            let validate = try self.validationServiceLoginVC.validatePassword(inputString)
            return validate
        }
        catch{
            onErrorHandling?(ErrorResult.custom(string: error.localizedDescription))
            throw ErrorResult.custom(string: error.localizedDescription)
        }
    }
}
