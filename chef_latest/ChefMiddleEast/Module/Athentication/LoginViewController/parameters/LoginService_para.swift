//
//  LoginService_para.swift
//  PetCare
//
//  Created by Apple on 15/09/21.
//

import Foundation

struct LoginService_para: DictionaryEncodable {
        var deviceToken : String!
        var deviceType : String!
        var emailPhone : String!
        var password : String!
}
