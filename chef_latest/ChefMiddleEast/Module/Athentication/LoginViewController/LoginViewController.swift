//
//  LoginViewController.swift
//  BaseProject
//
//  Created by rupinder singh on 06/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import UIKit
import CocoaTextField

@available(iOS 13.0, *)
class LoginViewController: BaseViewController,AlertDelegate {
    
    @IBOutlet weak var textFieldUserName: CocoaTextField!
    @IBOutlet weak var textFieldPassword: CocoaTextField!
    @IBOutlet weak var textFieldUserNameV: UIView!
    @IBOutlet weak var textFieldPasswordV: UIView!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var buttonRegister: UIButtonX!
    @IBOutlet weak var titleString: UILabel!
    @IBOutlet weak var subTitleString: UILabel!
    @IBOutlet weak var doYouHaveAnAccountLabel: UILabel!
    @IBOutlet weak var buttonForgotPassword: UIButton!
    @IBOutlet weak var backViewWhite: UIView!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var buttonSignUp: UIButton!
    
    var eyeClick = true
    private let validationServiceLoginVC:ValidationServiceLoginVC
    var service: LoginService?
    lazy var viewModel : LoginViewModel = {
        // let userInfoObj = CoreDataManager.shared.userInfoObj
        let viewModel = LoginViewModel(service: self.service ?? LoginService(), validation: self.validationServiceLoginVC )
        return viewModel
    }()
    
    init(validationServiceLoginVCItem:ValidationServiceLoginVC) {
        self.validationServiceLoginVC = validationServiceLoginVCItem
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        self.validationServiceLoginVC = ValidationServiceLoginVC()
        self.service = LoginService()
        super.init(coder: coder)
    }
    
    //MARK:- VC Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.addEye()
        self.setPlaceHolderText()
        self.setUpShadow()
        
        self.viewModel.compeltionBlockLocaliable = {[weak self] obj in
            self?.textFieldUserName.placeholder = AppMessageLoginVC.Placeholder.UserName
            self?.textFieldPassword.placeholder = AppMessageLoginVC.Placeholder.Password
            self?.buttonLogin.setTitle(AppMessageLoginVC.Placeholder.Login, for: .normal)
            self?.titleString.text = AppMessageLoginVC.Placeholder.loginTitle
            
            self?.subTitleString.text = AppMessageLoginVC.Placeholder.Logintoyouraccount
        }
        self.viewModel.updateLocalizable()
        self.viewModel.onErrorHandling = {[weak self] error in
            let alertVC  = AlertVC()
            alertVC.delegateAlert = self
            var errorMessage = ""
            switch error {
            case .custom(let message):
                errorMessage = message
            case .network(let message):
                errorMessage = message
            case .parser(let message):
                errorMessage = message
            default:
                errorMessage = ""
            }
            guard let strongSelf = self else { return }
            self?.hideLoader()
            alertVC.withOk(message: errorMessage, vc: strongSelf, okStyle: .default, controllerStyle: .alert)
        }
        self.viewModel.onCompletionHandling = { [weak self] mockUser in
            DispatchQueue.main.async {
                self?.hideLoader()
                let alertVC  = AlertVC()
                alertVC.delegateAlert = self
                print("you have login successfully")
                guard let strongSelf = self else { return }
                UserDefaults.standard.set(false, forKey: Constants.guestUser)
                UserDefaults.standard.synchronize()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self?.pushToRoot()
                }
            }
        }
    }
    
    //MARK:- Set Up Shadow
    func setUpShadow(){
        textFieldUserName.delegate = self
        textFieldPassword.delegate = self
        applyStyle(to: textFieldUserName)
        applyStyle(to: textFieldPassword)
        self.backViewWhite .layer.masksToBounds = true
        self.backViewWhite.layoutIfNeeded()
        Utility.shared.roundParticular(corners: [.topLeft,.topRight], cornerRadius: 40, view1: backViewWhite)
        
        Utility.shared.makeShadowsOfView_roundCorner(view: textFieldUserNameV, shadowRadius: 4.0, cornerRadius: 27.5, borderColor: UIColor.lightGray.withAlphaComponent(0.2))
        
        Utility.shared.makeShadowsOfView_roundCorner(view: textFieldPasswordV, shadowRadius: 4.0, cornerRadius: 27.5, borderColor: UIColor.lightGray.withAlphaComponent(0.2))
    }
    
    //MARK:- Set PlaceHolder Text
    func setPlaceHolderText(){
        textFieldPassword.placeholder = AppMessageLoginVC.Placeholder.Password
        textFieldUserName.placeholder = AppMessageLoginVC.Placeholder.UserName
        self.doYouHaveAnAccountLabel.text = AppMessageLoginVC.Placeholder.Donthaveanaccount
        self.buttonSignUp.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessageLoginVC.Placeholder.SignUp, size: 17.0, fontName: FontName.Optima.Bold), for: .normal)
    }
    
    func addEye() {
        textFieldPassword.isSecureTextEntry = true

        let buttonNew = UIButton(type: .custom)
        buttonNew.setImage(UIImage(named: "hiddenPass"), for: .normal)
        buttonNew.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        buttonNew.frame = CGRect(x: CGFloat(textFieldPassword.frame.size.width - 35), y: CGFloat(5), width: CGFloat(35), height: CGFloat(35))
        buttonNew.addTarget(self, action: #selector(self.reveal1), for: .touchUpInside)
        textFieldPassword.rightView = buttonNew
        textFieldPassword.rightViewMode = .always
    }
    
    func pushToRoot(){
        RootManager().SideRootScreen()
    }
    
    //MARK:- Log In Params
    func paramForLogIn(){
        
        if textFieldUserName.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please Enter Email")
            return
        }
        else if textFieldPassword.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please Enter Password ")
            return
        }
        else {
            let params = UserModel()
            if let validEmail = textFieldUserName.text?.trimmingCharacters(in: .whitespaces).isValidEmail(){
                if validEmail == true {
                    params.email = self.textFieldUserName.text?.trimmingCharacters(in: .whitespaces)
                    params.password = self.textFieldPassword.text?.trimmingCharacters(in: .whitespaces)
                    params.devicetype = "ios" //Utility.shared.DivceTypeString()
                    params.deviceToken = Utility.shared.getAppUniqueId()
                    LoginAPI(Model: params)
                }
                else {
                    NotificationAlert().NotificationAlert(titles: "Please Enter valid Email")
                    return
                }
            }
        }
    }
    
    //MARK:- Button Action
    @IBAction func buttonLoginPressed(_ sender: Any) {
        
        self.paramForLogIn()
    }
    
    @IBAction func buttonRegisterPressed(_ sender: Any) {
        
        let controller:RegisterUserViewController =  UIStoryboard(storyboard: .athentication).initVC() 
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func buttonForgetPressed(_ sender: Any) {
        
        let controller:ForgotPasswordViewController = UIStoryboard(storyboard: .athentication).initVC()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func buttonSignInWithFacebookPressed(_ sender: Any) {
        
    }
    
    @IBAction func buttonSignInWithGooglePressed(_ sender: Any) {
        
    }
    
    @IBAction func buttonBackPressed(_ sender: Any) {
        
          let controller:WelcomeScreenViewController = UIStoryboard(storyboard: .athentication).initVC()
          self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @IBAction func buttonSignUpPressed(_ sender: Any) {
        
        let controller:RegisterUserViewController = UIStoryboard(storyboard: .athentication).initVC()
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func reveal1(_ sender: UIButton) {
        
        if(eyeClick == true) {
            sender.setImage(UIImage(named: "viewPass"), for: .normal)
            textFieldPassword.isSecureTextEntry = false
        } else {
            sender.setImage(UIImage(named: "hiddenPass"), for: .normal)
            textFieldPassword.isSecureTextEntry = true
        }
        eyeClick = !eyeClick
    }
    
    // MARK: - AlertViewDelegate
    func okButtonPressed(alertController: UIAlertController, alert: AlertVC) {
        
    }
    
    func cancelButtonPressed(alertController: UIAlertController, alert: AlertVC) {
        
    }
    
    // MARK: - LogIn API
    func LoginAPI(Model: UserModel){
        self.showLoader()
        EndPoint.LogIn(params: Model, t: UserModel.self) { result in
            switch result {
            case .onSuccess(let user):
                if user != nil{
                    UserDefaults.standard.set(false, forKey: Constants.guestUser)
                    UserDefaults.standard.set(user?.email, forKey: Constants.email)
                    UserDefaults.standard.set(user?.userId, forKey: Constants.userId)
                    UserDefaults.standard.set(user?.token, forKey: Constants.token)
                    UserDefaults.standard.set(user?.firstName, forKey: Constants.firstName)
                    UserDefaults.standard.set(user?.lastName, forKey: Constants.lastName)
                    UserDefaults.standard.set(user?.userImg, forKey: Constants.userImg)
                    UserDefaults.standard.set(user?.phoneNumber, forKey: Constants.phone)
                    UserDefaults.standard.set(user?.gender, forKey: Constants.gender)
                    UserDefaults.standard.set(user?.dob, forKey: Constants.dob)
                    UserDefaults.standard.set(user?.custAccount, forKey: Constants.custAccount)

                    UserDefaults.standard.set(user?.isNotificationOn, forKey: Constants.NotificationOnOff)
                    
                    UserDefaults.standard.set(user?.isNewsletterSubscribed, forKey: Constants.NotificationOnOffSub)
                    
                    if ((user?.userImg?.contains("http:")) != nil) {
                        UserDefaults.standard.set(user?.userImg, forKey: Constants.userImg)
                    }
                    else{
                        var string = ""
                        if user?.userImg != nil {
                            string =  EndPoint.BASE_API_IMAGE_URL + (user?.userImg!)!
                            UserDefaults.standard.set(string, forKey: Constants.userImg)
                        }
                    }
                    UserDefaults.standard.synchronize()
                    self.pushToRoot()
                }
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                NotificationAlert().NotificationAlert(titles: error)
                print(error)
                self.hideLoader()
            }
        }
    }
}

@available(iOS 13.0, *)
extension LoginViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool{
        
        
        
       if string == ""{
            return true
        }
        if textField.text?.count ?? 0 > 40{
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
}


