//
//  AppMessageLoginVC.swift
//  BaseProject
//
//  Created by rupinder singh on 07/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import Foundation
class AppMessageLoginVC{
    struct Placeholder {
        static let UserName = NSLocalizedString("UserName", comment: "")
        static let Password = NSLocalizedString("Password", comment: "")
        static let loginTitle = NSLocalizedString("loginTitle", comment: "")
        static let loginRegister = NSLocalizedString("loginRegister", comment: "")
        static let loginForgot = NSLocalizedString("loginForgot", comment: "")
        static let Login = NSLocalizedString("Login", comment: "")
        static let Logintoyouraccount = NSLocalizedString("Logintoyouraccount", comment: "")
        static let Donthaveanaccount = NSLocalizedString("Donthaveanaccount", comment: "")
        static let SignUp = NSLocalizedString("SignUp", comment: "")
      
        
    }
    struct ValidationError {
    static let emptyUserName = NSLocalizedString("emptyUserName", comment: "")
    static let emptyPassword = NSLocalizedString("emptyPassword", comment: "")
    static let invalidUserName = NSLocalizedString("invalidUserName", comment: "")
    static let EmptyEmailOrPhone = NSLocalizedString("EmptyEmailOrPhone", comment: "")
    static let invalidPassword = NSLocalizedString("invalidPassword", comment: "")
    static let userNameTooShort = NSLocalizedString("userNameTooShort", comment: "")
    static let userNameTooLong = NSLocalizedString("userNameTooLong", comment: "")
    static let passwordTooShort = NSLocalizedString("passwordTooShort", comment: "")
    static let passwordTooLong = NSLocalizedString("passwordTooLong", comment: "")
    static let invalidCreditional = NSLocalizedString("invalidCreditional", comment: "")
    static let pleaseEnterUserName = NSLocalizedString("pleaseEnterUserName", comment: "")
    static let pleaseEnterPassword = NSLocalizedString("pleaseEnterPassword", comment: "")
    }
    
}
