//
//  LoginService.swift
//  BaseProject
//
//  Created by rupinder singh on 17/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import UIKit
import Alamofire
protocol LoginServiceProtocol :class {
    func hitLoginUser( _ para:[String:Any]?,completion:@escaping((Result<MockUp_User,ErrorResult>)->Void))
}
class LoginService: RequestHandler,LoginServiceProtocol {
    static let shared = LoginService()
    let endpoint = APIUrl.baseUrl
    let loginUrl = APIUrl.LoginUrl().url
    var request:DataRequest?
    
    func hitLoginUser(_ para: [String : Any]?, completion: @escaping ((Result<MockUp_User, ErrorResult>) -> Void)) {
        self.cancelLoginService()
        //hitting service
       request =  RequestService().loadData_Alamofire(urlString: loginUrl, method: .post, parameters: para, completion: self.networkResult(completion: completion))
    }
    
    func cancelLoginService(){
        self.request?.cancel()
    }
    
   
    

}
