//
//  ForgotPasswordViewController.swift
//  PetCare
//
//  Created by Apple on 24/09/21.
//

import UIKit
import CocoaTextField


@available(iOS 13.0, *)
class ForgotPasswordViewController: BaseViewController {
    @IBOutlet weak var labelTitle: UILabel!
   
    @IBOutlet weak var labelMobileNumberConstant: UILabel!
    @IBOutlet weak var buttonSendOTP: UIButton!
    @IBOutlet weak var forgotTextField: CocoaTextField!
    @IBOutlet weak var buttonBack: UIButton!
    
    @IBOutlet weak var titleString: UILabel!
    @IBOutlet weak var subTitleString: UILabel!
    @IBOutlet weak var backViewWhite: UIView!
    
     var validation:ForgotValidation?
     var service: ForgotSendOTPService?
    
    lazy var viewModel : ForgotPasswordViewModel = {
       // let userInfoObj = CoreDataManager.shared.userInfoObj
        let viewModel = ForgotPasswordViewModel(service:  self.service ?? ForgotSendOTPService(),validation:  self.validation ?? ForgotValidation() )
        return viewModel
    }()
    
    init(validation:ForgotValidation,service: ForgotSendOTPService) {
        self.validation = validation
        self.service = service
       
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        self.validation = ForgotValidation()
        self.service = ForgotSendOTPService()
        super.init(coder: coder)
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.applyStyle(to:  self.forgotTextField)
        
        self.buttonSendOTP.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessageForgotPassword.Placeholder.SendOTP, size: 21.0, fontName: FontName.Optima.Bold), for: .normal)
        
        self.backViewWhite .layer.masksToBounds = true
        self.backViewWhite.layoutIfNeeded()
        Utility.shared.roundParticular(corners: [.topLeft,.topRight], cornerRadius: 40, view1: backViewWhite)
       
        Utility.shared.makeShadowsOfView_roundCorner(view: forgotTextField, shadowRadius: 4.0, cornerRadius: 30, borderColor: UIColor.lightGray.withAlphaComponent(0.5))
        
        
        
        self.viewModel.compeltionBlockLocaliable = { [weak self] object in
            self?.forgotTextField.placeholder =  AppMessageForgotPassword.Placeholder.Email
            self?.buttonSendOTP.setTitle(AppMessageForgotPassword.Placeholder.SendOTP, for: .normal)
          
            
        }
        self.viewModel.updateLocalizable()
        self.viewModel.onErrorHandling = {[weak self] error in
            let alertVC  = AlertVC()
            alertVC.delegateAlert = self
            var errorMessage = ""
            switch error {
            case .custom(let message):
                errorMessage = message
            case .network(let message):
                errorMessage = message
            case .parser(let message):
                errorMessage = message
            default:
                errorMessage = ""
            }
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                self?.hideLoader()
                alertVC.withOk(message: errorMessage, vc: strongSelf, okStyle: .default, controllerStyle: .alert)
                
                
            }
        }
        self.viewModel.onCompletionHandling = {[weak self] object in
            let alertVC  = AlertVC()
            alertVC.delegateAlert = self
            guard let strongSelf = self else { return }
           
            DispatchQueue.main.async {
                self?.hideLoader()
                let controller:OTPVerifyViewController = UIStoryboard(storyboard: .athentication).initVC()
                if let otp = object.otpCode{
                    controller.emailOtp = "\(otp)"
                }
               var regierUser_par = RegierUser_par()
               
                if let _ = Int(self?.forgotTextField.text ?? ""){
                    regierUser_par.phoneNumber = self?.forgotTextField.text ?? ""
                }else{
                    regierUser_par.email = self?.forgotTextField.text ?? ""
                }
                controller.isForgotPassword = true
                controller.isPhoneVerification = false
                controller.regierUser_par = regierUser_par
                self?.navigationController?.pushViewController(controller, animated: true)
                
            }
            print(object)
        }
        Utility.shared.makeRoundCorner(layer: self.buttonSendOTP.layer, color: UIColor.lightGray, radius: 10.0,borderWidth: 0.0)
        // Do any additional setup after loading the view.
    }
    @IBAction func buttonBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSendOTPPressed(_ sender: Any) {
        
        if let email = forgotTextField.text?.trimmingCharacters(in: .whitespaces), !email.isEmpty {
            
           // EndPoint.email = forgotTextField.text!.trimmingCharacters(in: .whitespaces)
           // self.verifyEmailSendOTP()
            let params = UserModel()
            params.email = email
            UserDefaults.standard.set(email, forKey: Constants.email)
            self.forgetPasswordAPI(Model: params)
        }
        else {
             if forgotTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
                NotificationAlert().NotificationAlert(titles: "Please Enter email")
                return
            }
        }
       
    }
    
    
    // MARK: - Forget Password API
    func forgetPasswordAPI(Model: UserModel){
        self.showLoader()
        EndPoint.ForgetPassword(params: Model,t: UserModel.self) { [self] result in
            switch result {
            case .onSuccessWithIntegerValue(let strOtp):
            print(strOtp)
            UserDefaults.standard.set(strOtp, forKey: Constants.otp)
                
            UserDefaults.standard.set(forgotTextField.text?.trimmingCharacters(in: .whitespaces), forKey: Constants.email)
                
            NotificationAlert().NotificationAlert(titles: "OTP sent successfully")
                let controller:OTPVerifyViewController = UIStoryboard(storyboard: .athentication).initVC()
                controller.isForgotPassword = true
                controller.isPhoneVerification = false
                self.navigationController?.pushViewController(controller, animated: true)
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                self.hideLoader()
                NotificationAlert().NotificationAlert(titles: error)
                
            }
        }
    }
    
}


@available(iOS 13.0, *)
extension ForgotPasswordViewController: AlertDelegate {
    func okButtonPressed(alertController: UIAlertController, alert: AlertVC) {
        
    }
    
    func cancelButtonPressed(alertController: UIAlertController, alert: AlertVC) {
        
    }
}
@available(iOS 13.0, *)
extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == ""{
            return true
        }
        if string == " "{
            return false
        }
        if textField.text?.count ?? 0 > 40{
            return false
        }
       
        return true
    }
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
