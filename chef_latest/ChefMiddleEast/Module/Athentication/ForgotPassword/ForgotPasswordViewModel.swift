//
//  ForgotPasswordViewModel.swift
//  PetCare
//
//  Created by Apple on 24/09/21.
//

import Foundation
struct ForgotPasswordViewModel {

    weak var service: ForgotSendOTPService?
    weak var validation: ForgotValidation?
    var onCompletionHandling : ((Mock_ForgotPassword) -> Void)?
    
    var  compeltionBlockLocaliable: ((AppMessageForgotPassword) -> Void)?
    
    
    var onErrorHandling : ((ErrorResult?) -> Void)?
    init(service:ForgotSendOTPService, validation:ForgotValidation) {
        self.service = service
        self.validation = validation
    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(AppMessageForgotPassword.shared)
    }
    
    func hitForgotSendOTPService(param:[String:AnyObject]?){
    
        guard let service = self.service else {
            onErrorHandling?(ErrorResult.custom(string: "Missing Service"))
            return
        }
        guard let param1 = param else {
            onErrorHandling?(ErrorResult.custom(string: "Missing Parmeters"))
            return
        }
        
        service.hitForgotSendOTPService(param1) { results in
          
            switch results{
            case .success(let object):
            
                onCompletionHandling!(object)
              break
            
            case .failure(let error):
               
                self.onErrorHandling?(error)
            }
        }
        
    }
    func  validatePhoneOrEmail(_ inputString:String)throws -> String?
    {
        do{
            let validatte = try self.validation?.validatePhoneOrEmail(inputString) ?? nil
            return validatte
        }
        catch{
            onErrorHandling?(ErrorResult.custom(string: error.localizedDescription))
            throw ErrorResult.custom(string: error.localizedDescription)
        }
       
    }
   
}
