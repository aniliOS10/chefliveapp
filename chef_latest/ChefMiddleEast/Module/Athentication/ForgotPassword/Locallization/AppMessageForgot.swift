//
//  AppMessageForgot.swift
//  PetCare
//
//  Created by Apple on 24/09/21.
//

import Foundation

class AppMessageForgotPassword {
    static let shared = AppMessageForgotPassword()
    
    
struct Placeholder {
    static let SendOTP = NSLocalizedString("SendOTP", comment: "")
    static let ForgotPasswordEnterPhoneOrEmail = NSLocalizedString("ForgotPasswordEnterPhoneOrEmail", comment: "")
    static let EmptyEmailOrPhone = NSLocalizedString("EmptyEmailOrPhone", comment: "")
    static let Email = NSLocalizedString("Email", comment: "")
}
}

