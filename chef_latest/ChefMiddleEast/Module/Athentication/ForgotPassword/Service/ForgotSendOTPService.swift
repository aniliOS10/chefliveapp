//
//  Forgot_sendOTP_Service.swift
//  PetCare
//
//  Created by Apple on 24/09/21.
//

import Foundation
import Alamofire
protocol ForgotSendOTPServiceProtocol  {
    func hitForgotSendOTPService( _ para:[String:AnyObject]?,completion:@escaping((Result<Mock_ForgotPassword,ErrorResult>)->Void))
}
class ForgotSendOTPService: RequestHandler,ForgotSendOTPServiceProtocol  {
    
    
    
    static let shared = ForgotSendOTPService()
    let endpoint = APIUrl.baseUrl
    let forgotPassword = APIUrl.ForgorPassword(parameter: nil).url

    var request:DataRequest?
    
    func hitForgotSendOTPService(_ para: [String : AnyObject]?, completion: @escaping ((Result<Mock_ForgotPassword, ErrorResult>) -> Void)) {
        self.cancelSendOtpService()
        //hitting service
        
       
        
        request =  RequestService().loadData_Alamofire(urlString: forgotPassword , method: .post, parameters: para, completion: self.networkResult(completion: completion))
    }
    
   
    
    
    func cancelSendOtpService(){
        self.request?.cancel()
    }
    
   
    

}
