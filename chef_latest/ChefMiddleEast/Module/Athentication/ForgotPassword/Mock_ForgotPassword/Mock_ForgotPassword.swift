//
//  Mock_ForgotPassword.swift
//  PetCare
//
//  Created by Apple on 24/09/21.
//

import Foundation
struct Mock_ForgotPassword{
    var code : Int!
    var message : String!
    var status : Bool!
    var otpCode : Int?
  
}
extension Mock_ForgotPassword:Parceable {
    static func parseObject(dictionary: [String : AnyObject]) -> Result<Mock_ForgotPassword, ErrorResult> {
        if let message = dictionary["message"] as? String{
         
            var otp:Int?
            if let data  = dictionary["data"] as? [String:Any] {
                if let otpValue  = data["otpCode"] as? Int{
                    otp = otpValue
                }
                
             }
          
         let object =   Mock_ForgotPassword(code: dictionary["code"] as? Int ?? nil, message: message, status: dictionary["status"] as? Bool ?? nil,otpCode: otp)
            
            
            return Result.success(object)
        }else{
            return Result.failure(ErrorResult.parser(string: "Unable to parse conversion rate"))
        }
    }
}
