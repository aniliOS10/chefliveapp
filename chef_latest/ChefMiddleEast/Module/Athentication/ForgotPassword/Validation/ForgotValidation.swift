//
//  ForgotValidation.swift
//  PetCare
//
//  Created by Apple on 24/09/21.
//

import Foundation
class ForgotValidation {
  static  let shared:ForgotValidation = ForgotValidation()
 
    func  validatePhoneOrEmail(_ inputString:String)throws -> String
    {
        if Int(inputString) != nil{
            return try self.validatePhone(inputString)
        }else{
            return try self.validateEmail(inputString)
        }
        return inputString
    }
    
    
    func validatePhone(_ phone:String?) throws -> String {
        guard let phoneValue = phone else { throw ValidationErrorRegisterVC_Phone.emptyPhone }
        guard phoneValue.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorRegisterVC_Phone.emptyPhone }
        guard phoneValue.isValidPhoneNumber() else {
            throw ValidationErrorRegisterVC_Phone.phoneIsNotValid
        }
        return phoneValue
      
    }
    func validateEmail(_ email:String?) throws -> String {
        guard let emailValue = email else { throw ValidationErrorRegisterVC_Email.emptyEmail }
        guard emailValue.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorRegisterVC_Email.emptyEmail }
        guard emailValue.isValidEmail() else {
            throw ValidationErrorRegisterVC_Email.emailIsNotValid
        }
        return emailValue
    }
}
