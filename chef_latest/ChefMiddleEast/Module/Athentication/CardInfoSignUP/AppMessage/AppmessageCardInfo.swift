//
//  AppmessageCardInfo.swift
//  ChefMiddleEast
//
//  Created by Apple on 27/10/21.
//

import Foundation
class AppmessageCardInfo{
    struct Placeholder {
        static let CardHolderName = NSLocalizedString("CardHolderName", comment: "")
        static let CardNumber = NSLocalizedString("CardNumber", comment: "")
    }
}
