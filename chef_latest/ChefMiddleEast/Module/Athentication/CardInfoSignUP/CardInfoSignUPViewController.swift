//
//  CardInfoSignUPViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 27/10/21.
//

import UIKit
import CocoaTextField

@available(iOS 13.0, *)
class CardInfoSignUPViewController: BaseViewController {
   
  
    @IBOutlet weak var cardHolderName: UITextField!
    @IBOutlet weak var CardNumber: UITextField!
    @IBOutlet weak var expiryDateDDTF: UITextField!
    @IBOutlet weak var expiryDateMMTF: UITextField!
    @IBOutlet weak var cvvNumberTF: UITextField!
    @IBOutlet weak var titleLabel :UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var alreadyHaveAnAccountLabel: UILabel!
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var backViewWhite: UIView!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var buttonSignIn: UIButton!
    @IBOutlet weak var viewCardHolderName: UIView!
    @IBOutlet weak var viewCardNumber: UIView!
    @IBOutlet weak var viewExpiryDDDate: UIView!
    @IBOutlet weak var viewExpiryMMDate: UIView!
    @IBOutlet weak var viewCVVNumber: UIView!
    @IBOutlet weak var VWbtnSkip: UIView!
    @IBOutlet weak var btnSkip: UIButton!
    
    var storeProfileImg : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cardHolderName.delegate = self
        CardNumber.delegate = self
        expiryDateDDTF.delegate = self
        expiryDateDDTF.delegate = self
        cvvNumberTF.delegate = self
        
        self.btnSkip.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessageAddressSignUP.Placeholder.skip, size: 22.0, fontName: FontName.Optima.Bold), for: .normal)
        
        self.NextButton.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessageAddressSignUP.Placeholder.save, size: 22.0, fontName: FontName.Optima.Bold), for: .normal)
        
        self.buttonSignIn.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessageRegisterUser.Placeholder.signin, size: 14, fontName: FontName.Optima.Bold), for: .normal)
        
        Utility.shared.makeShadowsOfView_roundCorner(view: VWbtnSkip, shadowRadius: 4.0, cornerRadius: 27.5, borderColor: UIColor.lightGray)
        
        Utility.shared.makeShadowsOfView_roundCorner(view: viewCardHolderName, shadowRadius: 4.0, cornerRadius: 10, borderWidth: 0.5, borderColor: UIColor.lightGray.withAlphaComponent(0.5))
        
        Utility.shared.makeShadowsOfView_roundCorner(view: viewCardNumber, shadowRadius: 4.0, cornerRadius: 10, borderWidth: 0.5, borderColor: UIColor.lightGray.withAlphaComponent(0.5))
        Utility.shared.makeShadowsOfView_roundCorner(view: viewExpiryDDDate, shadowRadius: 4.0, cornerRadius: 10, borderWidth: 0.5, borderColor: UIColor.lightGray.withAlphaComponent(0.5))
      
        Utility.shared.makeShadowsOfView_roundCorner(view: viewExpiryMMDate, shadowRadius: 4.0, cornerRadius: 10, borderWidth: 0.5, borderColor: UIColor.lightGray.withAlphaComponent(0.5))
       
        Utility.shared.makeShadowsOfView_roundCorner(view: viewCVVNumber, shadowRadius: 4.0, cornerRadius: 10, borderWidth: 0.5, borderColor: UIColor.lightGray.withAlphaComponent(0.5))
       self.backViewWhite .layer.masksToBounds = true
        self.backViewWhite.layoutIfNeeded()
       
        Utility.shared.roundParticular(corners: [.topLeft,.topRight], cornerRadius: 40, view1: backViewWhite)
    }
    
    //MARK:- Card Params
    func cardParams(){

        let params = CardModel()
        params.cardHolderName = cardHolderName.text?.trimmingCharacters(in: .whitespaces)
        params.cardHolderNumber = CardNumber.text?.trimmingCharacters(in: .whitespaces)
        params.cardExpiryDate = expiryDateDDTF.text?.trimmingCharacters(in: .whitespaces)
        params.custAccount = cvvNumberTF.text?.trimmingCharacters(in: .whitespaces)
        signUpParams.envelopeParams.card = params
      
       // print(signUpParams.toJSONString())

        SignupAPI(Model: signUpParams)
    }
    
    @IBAction func backButtonPressed(_ sender: Any){
        navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnContinue(_ sender: Any){
        self.cardParams()
    }
    
    @IBAction func btnSignInPressed(_ sender: Any){
        RootManager().setLoginRoot()
    }
    
    @IBAction func btnSkip(_ sender: Any){
        self.cardParams()
    }

    //MARK:- SignUp API
    func SignupAPI(Model:SignUpModel){
        self.showLoader()
        EndPoint.SignUP(params: Model, t: UserModel.self) { (result) in
            switch result {
            
            case .onSuccess(let user):
                self.hideLoader()
                if user != nil{
                    UserDefaults.standard.set(user?.email, forKey: Constants.email)
                    UserDefaults.standard.set(user?.userId, forKey: Constants.userId)
                    UserDefaults.standard.set(user?.token, forKey: Constants.token)
                    UserDefaults.standard.set(user?.firstName, forKey: Constants.firstName)
                    UserDefaults.standard.set(user?.lastName, forKey: Constants.lastName)
                    UserDefaults.standard.set(user?.gender, forKey: Constants.gender)
                    UserDefaults.standard.set(user?.dob, forKey: Constants.dob)
                    UserDefaults.standard.set(user?.custAccount, forKey: Constants.custAccount)

                    
                    UserDefaults.standard.set(true, forKey: Constants.NotificationOnOff)
                    UserDefaults.standard.synchronize()

                    if self.storeProfileImg != nil {
                        self.uploadProfileImageApi(userID: user!.userId!)
                    }
                    else{
                        NotificationAlert().NotificationAlert(titles: "User created successfully")

                        RootManager().SideRootScreen()
                    }

                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    func uploadProfileImageApi(userID: String){
        
        let compressData = self.storeProfileImg!.jpegData(compressionQuality: 0.5)
        let compressedImage = UIImage(data: compressData!)
        self.showLoader()
        
        ImageUploadManager().uploadImageaaaRemote(image:compressedImage!,UserId: userID){ data, error -> Void in
            self.hideLoader()

            if !data!.isEmpty{
                print("profile image is :- ",data!)
                UserDefaults.standard.set(data!, forKey: Constants.userImg)
                NotificationAlert().NotificationAlert(titles: "User created successfully")
                RootManager().SideRootScreen()
            }
        }
        
    }
}


//MARK:- UITextFieldDelegate
@available(iOS 13.0, *)
extension CardInfoSignUPViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
