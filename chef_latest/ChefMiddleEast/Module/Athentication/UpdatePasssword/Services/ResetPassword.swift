//
//  ResetPassword.swift
//  PetCare
//
//  Created by Apple on 28/09/21.
//

import UIKit
import Alamofire
protocol ResetPasswordServiceProtocol :class {
    func hitResetPasswordServiceProtocol( _ para:[String:AnyObject]?,completion:@escaping((Result<MockUp_ResetPassword,ErrorResult>)->Void))
}
class ResetPasswordService: RequestHandler ,ResetPasswordServiceProtocol  {
    
    
    
    static let shared = ResetPasswordService()
    let endpoint = APIUrl.baseUrl
    let resetPassword = APIUrl.ResetPassword()
 //   let registerUser = APIUrl.registerUser(parameter: nil).url
    
    var request:DataRequest?
    
    func hitResetPasswordServiceProtocol(_ para: [String : AnyObject]?, completion: @escaping ((Result<MockUp_ResetPassword, ErrorResult>) -> Void)) {
        self.cancelSendOtpService()
        //hitting service
        request =  RequestService().loadData_Alamofire(urlString: resetPassword.url, method: .post, parameters: para, completion: self.networkResult(completion: completion))
    }
    
   
    
    
    func cancelSendOtpService(){
        self.request?.cancel()
    }
    
   
    

}
