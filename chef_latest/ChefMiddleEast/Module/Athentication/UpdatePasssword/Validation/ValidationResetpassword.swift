//
//  UpatePassword_Validation.swift
//  PetCare
//
//  Created by Apple on 28/09/21.
//

import Foundation
struct ValidationResetpassword {
func validatePassword(_ password:String?) throws -> String {
    guard let upasswordValue = password else { throw ValidationErrorRegisterVC_password.emptyPassword }
    guard upasswordValue.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorRegisterVC_password.emptyPassword }
    guard upasswordValue.count >= 5 else { throw ValidationErrorRegisterVC_password.passwordTooShort }
    return upasswordValue
}
func validateConfirmPassword(_ confrimePassword:String?,password:String?) throws -> String {
    
    guard let passwordValue = password else { throw ValidationErrorRegisterVC_password.emptyPassword }
    
    guard let confrimValue = confrimePassword else { throw ValidationErrorRegisterVC_password.emptyConfirmPassword }
    
    guard confrimValue.replacingOccurrences(of: " ", with: "") != "" else { throw ValidationErrorRegisterVC_password.emptyConfirmPassword }
    
    if  confrimValue == passwordValue{
        return confrimValue
    } else { throw ValidationErrorRegisterVC_password.passwordDonotMatch }
}
}
