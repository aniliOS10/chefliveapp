//
//  UpdatePasswordViewModel.swift
//  PetCare
//
//  Created by Apple on 28/09/21.
//

import Foundation
import UIKit
@available(iOS 13.0, *)
@available(iOS 13.0, *)
class ResetpasswordViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    
    weak var service: ResetPasswordService?
    private let validation:ValidationResetpassword?
    
    init(service:ResetPasswordService,validation:ValidationResetpassword) {
        self.service = service
        self.validation =  validation
    }
    func hitResetPasswordServiceProtocol(param:[String:AnyObject]?){
        guard let service = self.service else {
            onErrorHandling?(ErrorResult.custom(string: "Missing Service"))
            return
        }
        guard let param1 = param else {
            onErrorHandling?(ErrorResult.custom(string: "Missing Parmeters"))
            return
        }
        service.hitResetPasswordServiceProtocol(param1) { results in
            switch results{
            case .success(let object):
               
                self.onCompletionHandling!(object)
              break
            
            case .failure(let error):
                self.onErrorHandling?(error)
            }
        }
        
    }
  
    func  validatePassword(_ inputString:String)throws -> String?
    {
        do{
            let validatte = try self.validation?.validatePassword(inputString) ?? nil
            return validatte
            
        }
        catch{
            onErrorHandling?(ErrorResult.custom(string: error.localizedDescription))
            throw ErrorResult.custom(string: error.localizedDescription)
        }
       
    }
    func  validateConfirmPassword(_ inputString:String,password:String)throws -> String?
    {
        do{
            let validatte = try self.validation?.validateConfirmPassword(inputString, password: password) ?? nil
            return validatte
            
        }
        catch{
            onErrorHandling?(ErrorResult.custom(string: error.localizedDescription))
            throw ErrorResult.custom(string: error.localizedDescription)
        }
     
    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    
    func addEye(textField:UITextField)-> UIButton{
       return Utility.shared.addEye(textField: textField)
    }
}
