//
//  MockUp_UpdatePassword.swift
//  PetCare
//
//  Created by Apple on 28/09/21.
//

import Foundation
struct MockUp_ResetPassword{
    var code : Int!
        var data : Data!
        var message : String!
        var status : Bool!

    init(fromDictionary dictionary: [String:Any]){
            code = dictionary["code"] as? Int
           
            message = dictionary["message"] as? String
            status = dictionary["status"] as? Bool
        }
}
extension MockUp_ResetPassword:Parceable {
    static func parseObject(dictionary: [String : AnyObject]) -> Result<MockUp_ResetPassword, ErrorResult> {
        if (dictionary["message"] as? String) != nil{
            let object =   MockUp_ResetPassword.init(fromDictionary: dictionary)
            return Result.success(object)
        }else{
            return Result.failure(ErrorResult.parser(string: "Unable to parse conversion rate"))
        }
    }
}
