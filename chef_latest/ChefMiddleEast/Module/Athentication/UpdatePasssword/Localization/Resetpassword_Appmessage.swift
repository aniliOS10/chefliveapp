//
//  UpdatePassword_Appmessage.swift
//  PetCare
//
//  Created by Apple on 28/09/21.
//

import UIKit

class Resetpassword_Appmessage: NSObject {
    struct Placeholder {
      //  static let EnterEmailOtp = NSLocalizedString("EnterEmailOtp", comment: "")
        static let EmptyNewPassword = NSLocalizedString("EmptyNewPassword", comment: "")
        static let EmptyConfirmPassword = NSLocalizedString("EmptyConfirmPassword", comment: "")
        static let TitleResetPassword = NSLocalizedString("TitleResetPassword", comment: "")
        static let Reset = NSLocalizedString("Reset", comment: "")
    }
}
