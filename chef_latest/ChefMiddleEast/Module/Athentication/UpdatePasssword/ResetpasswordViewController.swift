//
//  UpdatePasswordViewController.swift
//  PetCare
//
//  Created by Apple on 28/09/21.
//

import UIKit
import CocoaTextField
@available(iOS 13.0, *)
class ResetPasswordViewController: BaseViewController {
    var regierUser_par:RegierUser_par?
    @IBOutlet weak var titleString: UILabel!
    @IBOutlet weak var subTitleString: UILabel!
    @IBOutlet weak var backViewWhite: UIView!
    
    @IBOutlet weak var backButton1 : UIButton!
    @IBOutlet weak var passwordTextField: CocoaTextField!
    @IBOutlet weak var confirmTextField: CocoaTextField!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var labelTitleMessage: UILabel!
    var eyeClick1 = true
    var eyeClick2 = true
    var validation:ValidationResetpassword?
    var service:ResetPasswordService?
    lazy var viewModel:ResetpasswordViewModel = {
        let viewModel = ResetpasswordViewModel(service: self.service ?? ResetPasswordService(), validation: self.validation ?? ValidationResetpassword())
        return viewModel
    }()
    init(validation:ValidationResetpassword,sevice:ResetPasswordService)
    {
        self.validation = validation
        self.service = sevice
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        self.validation = ValidationResetpassword()
        self.service = ResetPasswordService()
        super.init(coder: coder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confirmTextField.delegate = self
        passwordTextField.delegate = self
        passwordTextField.isSecureTextEntry = true
        confirmTextField.isSecureTextEntry = true

        self.addEye()
        self.applyStyle(to: self.confirmTextField)
        self.applyStyle(to: self.passwordTextField)
        
        Utility.shared.makeShadowsOfView_roundCorner(view: confirmTextField, shadowRadius: 4.0, cornerRadius: 30, borderColor: UIColor.lightGray.withAlphaComponent(0.2))
        Utility.shared.makeShadowsOfView_roundCorner(view: passwordTextField, shadowRadius: 4.0, cornerRadius: 30, borderColor: UIColor.lightGray.withAlphaComponent(0.2))
        
        self.backViewWhite .layer.masksToBounds = true
        self.backViewWhite.layoutIfNeeded()
        Utility.shared.roundParticular(corners: [.topLeft,.topRight], cornerRadius: 40, view1: backViewWhite)
        
        self.viewModel.compeltionBlockLocaliable = {[weak
        self] object in
            self?.passwordTextField.placeholder = Resetpassword_Appmessage.Placeholder.EmptyNewPassword
            self?.confirmTextField.placeholder = Resetpassword_Appmessage.Placeholder.EmptyConfirmPassword
            self?.resetButton.setTitle(Resetpassword_Appmessage.Placeholder.Reset, for: .normal)
        }
        self.viewModel.onCompletionHandling = {[weak
        self] object in
            let ob = object as! MockUp_ResetPassword
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                self?.hideLoader()
                let alertVC  = AlertVC()
                alertVC.delegateAlert = self
                alertVC.withOk(message: ob.message, vc: strongSelf, okStyle: .default, controllerStyle: .alert)
            }
        }
        self.viewModel.onErrorHandling = {[weak
        self] error in
            
            let alertVC  = AlertVC()
            alertVC.type = .errorA
            alertVC.delegateAlert = self
            var errorMessage = ""
            switch error {
            case .custom(let message):
                errorMessage = message
            case .network(let message):
                errorMessage = message
            case .parser(let message):
                errorMessage = message
            default:
                errorMessage = ""
            }
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                alertVC.withOk(message: errorMessage, vc: strongSelf, okStyle: .default, controllerStyle: .alert)
            }
        }
        self.viewModel.updateLocalizable()
     /*   let buttonPassword = self.viewModel.addEye(textField: passwordTextField)
        buttonPassword.addTarget(self, action: #selector(self.reveal1), for: .touchUpInside)
        let buttonConfirmPassword = self.viewModel.addEye(textField: confirmTextField)
        buttonConfirmPassword.addTarget(self, action: #selector(self.reveal2), for: .touchUpInside)*/
    }
    
    
    func addEye(){
        let buttonNew = UIButton(type: .custom)
        buttonNew.setImage(UIImage(named: "hiddenPass"), for: .normal)
        buttonNew.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        buttonNew.frame = CGRect(x: CGFloat(passwordTextField.frame.size.width - 35), y: CGFloat(5), width: CGFloat(35), height: CGFloat(35))
        buttonNew.addTarget(self, action: #selector(self.reveal1), for: .touchUpInside)
        passwordTextField.rightView = buttonNew
        passwordTextField.rightViewMode = .always
        
        let button1 = UIButton(type: .custom)
        button1.setImage(UIImage(named: "hiddenPass"), for: .normal)
        button1.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        button1.frame = CGRect(x: CGFloat(confirmTextField.frame.size.width - 35), y: CGFloat(5), width: CGFloat(35), height: CGFloat(35))
        button1.addTarget(self, action: #selector(self.reveal2), for: .touchUpInside)
        confirmTextField.rightView = button1
        confirmTextField.rightViewMode = .always
    }
    
    //MARK:- ResetPassword Params
    func paramResetPassword(){
        
        if passwordTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please enter password")
            return
        }
        else  if (passwordTextField.text?.trimmingCharacters(in: .whitespaces).count)! < 6 {
            NotificationAlert().NotificationAlert(titles: "Password should be 6 characters or more")
            return
        }
        
        else if confirmTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please enter confirm password ")
            return
        }
        else  if (passwordTextField.text?.trimmingCharacters(in: .whitespaces)) != (confirmTextField.text?.trimmingCharacters(in: .whitespaces)){
           
            NotificationAlert().NotificationAlert(titles: "Password not match")
            return
        }
        else {
            let params = UserModel()
            
            if let email = UserDefaults.standard.string(forKey: Constants.email),!email.isEmpty {
                
                params.email = email.trimmingCharacters(in: .whitespaces)
                
                params.password = self.confirmTextField.text?.trimmingCharacters(in: .whitespaces)
                
                self.resetPasswordAPI(Model: params)
            }
            else{
                NotificationAlert().NotificationAlert(titles: "email is not register")
            }
        }
    }
    
    
    // MARK: - Button Action
    @IBAction func reveal1(_ sender: UIButton) {
        if(eyeClick1 == true) {
            sender.setImage(UIImage(named: "viewPass"), for: .normal)
            passwordTextField.isSecureTextEntry = false
        } else {
            sender.setImage(UIImage(named: "hiddenPass"), for: .normal)
            passwordTextField.isSecureTextEntry = true
        }
        eyeClick1 = !eyeClick1
    }
    
    @IBAction func reveal2(_ sender: UIButton) {
        
        if(eyeClick2 == true) {
            sender.setImage(UIImage(named: "viewPass"), for: .normal)
            confirmTextField.isSecureTextEntry = false
        } else {
            sender.setImage(UIImage(named: "hiddenPass"), for: .normal)
            confirmTextField.isSecureTextEntry = true
        }
        eyeClick2 = !eyeClick2
    }
    
    @IBAction func buttonBackButtonPressed(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func buttonResetButtonPressed(_ sender: Any) {
        
        self.paramResetPassword()
    }
    
    // MARK: - Reset Password Send OTP API
    func resetPasswordAPI(Model: UserModel){
        self.showLoader()
        EndPoint.ResetPassword(params: Model,t: UserModel.self) { result in
            switch result {
            case .onSuccessWithStringValue(let msg):
                self.hideLoader()

                NotificationAlert().NotificationAlert(titles: msg)
                let controller:PasswordSuccessViewController =  UIStoryboard(storyboard: .athentication).initVC()
                self.navigationController?.pushViewController(controller, animated: true)
            case .onFailure(let error):
                print(error)
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }

}
@available(iOS 13.0, *)
extension ResetPasswordViewController: AlertDelegate {
    func okButtonPressed(alertController: UIAlertController, alert: AlertVC) {
       if alert.type == .successA{
        self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func cancelButtonPressed(alertController: UIAlertController, alert: AlertVC) {
        
    }
}
@available(iOS 13.0, *)
extension ResetPasswordViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == ""{
            return true
        }
        if textField.text == "" && string == " "{
            return false
        }
        if textField.text?.count ?? 0 > 40{
            return false
        }
        
//        if textField == passwordTextField && textField.text?.count ?? 0 > 7{
//           return false
//        }
//        if textField == confirmTextField && textField.text?.count ?? 0 > 7{
//           return false
//        }
        return true
    }
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
         if textField == passwordTextField {
        }
        else if textField == confirmTextField {
        }
    }
    
}
