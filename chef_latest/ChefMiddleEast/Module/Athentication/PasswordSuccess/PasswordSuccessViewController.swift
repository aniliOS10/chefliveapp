//
//  PasswordSuccessViewController.swift
//  ChefMiddleEast
//
//  Created by rupinder singh on 02/11/21.
//

import UIKit

@available(iOS 13.0, *)
class PasswordSuccessViewController: BaseViewController {
    @IBOutlet weak var backViewWhite: UIView!
    @IBOutlet weak var backButton1 : UIButton!
    @IBOutlet weak var imgView : UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backViewWhite .layer.masksToBounds = true
        self.backViewWhite.layoutIfNeeded()
        Utility.shared.roundParticular(corners: [.topLeft,.topRight], cornerRadius: 40, view1: backViewWhite)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
            self.setupAnimation()
        }
    }
    
    func setupAnimation() {
        let jeremyGif = UIImage.gif(name: "reset_succes")
        imgView.animationImages = jeremyGif?.images
        imgView.animationDuration = jeremyGif!.duration
        imgView.animationRepeatCount = 1
        imgView.image = jeremyGif?.images?.last
        imgView.startAnimating()
    }
    
    
    @IBAction func buttonBackButtonPressed(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func buttonResetButtonPressed(_ sender: Any) {
    
        RootManager().setLoginRoot()
    }
}
