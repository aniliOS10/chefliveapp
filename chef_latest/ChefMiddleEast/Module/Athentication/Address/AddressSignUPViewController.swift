//
//  AddressSignUPViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 27/10/21.
//

import UIKit
import CocoaTextField
import DropDown
@available(iOS 13.0, *)


class AddressSignUPViewController: BaseViewController {
    
    @IBOutlet weak var houseAddressTF: CocoaTextField!
    @IBOutlet weak var stateProvinceTF: CocoaTextField!
    @IBOutlet weak var cityVillageTF: CocoaTextField!
    @IBOutlet weak var pinCodeTF: CocoaTextField!
    @IBOutlet weak var selectCountryTF: CocoaTextField!
    @IBOutlet weak var dobTF: CocoaTextField!

    @IBOutlet weak var VWbtnSkip: UIView!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var titleLabel :UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var alreadyHaveAnAccountLabel: UILabel!
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var backViewWhite: UIView!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var buttonSignIn: UIButton!
    @IBOutlet var countryPicker: UIPickerView!
    @IBOutlet weak var VWCountryPicker: UIView!
    @IBOutlet weak var buttonOpenPicker: UIButton!
    @IBOutlet weak var btnCountry: UIButton!

    @IBOutlet weak var imgViewMRadio: UIImageView!
    @IBOutlet weak var imgViewFRadio: UIImageView!
    @IBOutlet weak var imgViewORadio: UIImageView!


    @IBOutlet var vwHome: UIView!
    
    @IBOutlet var btnHome: UIButton!
    
    @IBOutlet var vwWork: UIView!
    
    @IBOutlet var btnWork: UIButton!
    
    @IBOutlet var vwOffice: UIView!
    
    @IBOutlet var btnOffice: UIButton!
    
    let dropCountry = DropDown()

    var arrCountryList = [CountryListModel]()
    var arrCityList = [CitiesListModel]()

    var selectedCountry = String()
    var countryRegionId = String()
    var countryId = Int()
    var imgProfile : UIImage?
    var countryToolBar = UIToolbar()
    var selectedAddressType = String()
    var taxGroup = String()
    var currentDate = Date()
    var genderStr = "Male"

    
    
    //MARK:- VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        vwHome.layer.cornerRadius = vwHome.layer.frame.height/2
        vwWork.layer.cornerRadius = vwWork.layer.frame.height/2
        vwOffice.layer.cornerRadius = vwOffice.layer.frame.height/2
        selectedAddressType = "Home"
        vwHome.backgroundColor = #colorLiteral(red: 0.6274509804, green: 0.01960784314, blue: 0.1529411765, alpha: 1)
        btnHome.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        btnWork.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnOffice.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        vwWork.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        vwOffice.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        cityVillageTF.isUserInteractionEnabled = false
        pinCodeTF.isUserInteractionEnabled = false
        houseAddressTF.delegate = self
        stateProvinceTF.delegate = self
        cityVillageTF.delegate = self
        pinCodeTF.delegate = self
        selectCountryTF.delegate = self
        self.countryPicker.delegate = self
        self.countryPicker.dataSource = self
        self.countryPicker.isHidden = false
        self.setUI()
        self.GetCountryList()
        self.GetCityList()

    }
    
   
    func pickUp(_ textField : UITextField){

        // UIPickerView
        self.countryPicker = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.countryPicker.delegate = self
        self.countryPicker.dataSource = self
        self.countryPicker.backgroundColor = UIColor.white
        textField.inputView = self.countryPicker

        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = .black
            //UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()

        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(AddressSignUPViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(AddressSignUPViewController.cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar

    }
    
    
    @objc func donePicker() {
        print("done")
        
        if !self.selectedCountry.trimmingCharacters(in: .whitespaces).isEmpty || self.selectedCountry != "" {
            selectCountryTF.text = selectedCountry
            EndPoint.userId = "\(self.countryId)"
            self.GetCityList()
        }

        VWCountryPicker.isHidden = true
        countryPicker.removeFromSuperview()
        self.selectCountryTF.resignFirstResponder()
    }
    
    @objc func cancelPicker() {
        print("cancel")
        self.selectedCountry = ""
        self.countryId = 0
        VWCountryPicker.isHidden = true
        countryPicker.removeFromSuperview()
        self.selectCountryTF.resignFirstResponder()
    }
    
    
    
    func setUI(){
        
        self.backViewWhite .layer.masksToBounds = true
        self.backViewWhite.layoutIfNeeded()
        Utility.shared.roundParticular(corners: [.topLeft,.topRight], cornerRadius: 40, view1: backViewWhite)
        self.btnSkip.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessageAddressSignUP.Placeholder.skip, size: 22.0, fontName: FontName.Optima.Bold), for: .normal)
        Utility.shared.makeShadowsOfView_roundCorner(view: VWbtnSkip, shadowRadius: 4.0, cornerRadius: 27.5, borderColor: UIColor.lightGray)
        self.buttonSignIn.setAttributedTitle(Utility.shared.provideAttributedFamily(text: AppMessageRegisterUser.Placeholder.signin, size: 17, fontName: FontName.Optima.Bold), for: .normal)
        
        Utility.shared.makeShadowsOfView_roundCorner(view: dobTF, shadowRadius: 4.0, cornerRadius: 31, borderColor: UIColor.lightGray)
        
        Utility.shared.makeShadowsOfView_roundCorner(view: houseAddressTF, shadowRadius: 4.0, cornerRadius: 31, borderColor: UIColor.lightGray)
        Utility.shared.makeShadowsOfView_roundCorner(view: stateProvinceTF, shadowRadius: 4.0, cornerRadius: 31, borderColor: UIColor.lightGray)
        Utility.shared.makeShadowsOfView_roundCorner(view: cityVillageTF, shadowRadius: 4.0, cornerRadius: 31, borderColor: UIColor.lightGray)
        Utility.shared.makeShadowsOfView_roundCorner(view: pinCodeTF, shadowRadius: 4.0, cornerRadius: 31, borderColor: UIColor.lightGray)
        Utility.shared.makeShadowsOfView_roundCorner(view: selectCountryTF, shadowRadius: 4.0, cornerRadius: 31, borderColor: UIColor.lightGray)
        
        
        applyStyle(to: dobTF)
        dobTF.placeholder = AppMessageAddressSignUP.Placeholder.DOBDate
        
        applyStyle(to: houseAddressTF) 
        houseAddressTF.placeholder = AppMessageAddressSignUP.Placeholder.HouseApartNumber
        applyStyle(to: stateProvinceTF)
        stateProvinceTF.placeholder = "Address" //AppMessageAddressSignUP.Placeholder.HouseProvince
        applyStyle(to: cityVillageTF)
        cityVillageTF.placeholder = "City" //AppMessageAddressSignUP.Placeholder.CityVillage
        applyStyle(to: pinCodeTF)
        pinCodeTF.placeholder = AppMessageAddressSignUP.Placeholder.PinCode
        applyStyle(to: selectCountryTF)
        selectCountryTF.placeholder = AppMessageAddressSignUP.Placeholder.SelectCountry
    }
    
    //MARK:- Address Params
    func addressParams(){
        
        if (dobTF.text?.trimmingCharacters(in: .whitespaces).count)! > 0
        {
        }
        else{
            NotificationAlert().NotificationAlert(titles: "Select date of birth")
            return
        }
        
        let params = AddressModel()
        params.city = cityVillageTF.text?.trimmingCharacters(in: .whitespaces)
        params.countryName = selectCountryTF.text?.trimmingCharacters(in: .whitespaces)
        params.countryRegionId = self.countryRegionId
        params.street = houseAddressTF.text?.trimmingCharacters(in: .whitespaces)
        params.address = stateProvinceTF.text?.trimmingCharacters(in: .whitespaces)
        params.isPrimary = 1
        params.logisticsLocationRoleName = "Invoice"
        params.descrptn = "test"
        params.addressType = selectedAddressType
        params.taxGroup = self.taxGroup
        signUpParams.envelopeParams.address = params
        let Cardparams = CardModel()
        Cardparams.cardHolderName = ""
        Cardparams.cardHolderNumber = ""
        Cardparams.cardExpiryDate = ""
        Cardparams.custAccount = ""
        signUpParams.envelopeParams.birthDate = dobTF.text?.trimmingCharacters(in: .whitespaces)
        signUpParams.envelopeParams.gender = genderStr
        signUpParams.envelopeParams.card = Cardparams
      
        
        if (selectCountryTF.text?.trimmingCharacters(in: .whitespaces).count)! > 0 && (cityVillageTF.text?.trimmingCharacters(in: .whitespaces).count)! > 0{
        }
        else{
            NotificationAlert().NotificationAlert(titles: "Select country and city")
        }
        if (stateProvinceTF.text?.trimmingCharacters(in: .whitespaces).count ?? 0) > 0 {
            self.SignupAPI(Model: signUpParams)
        }
        else{
            NotificationAlert().NotificationAlert(titles: "Enter address")
        }
    }
    
    
    
    //MARK:- Button Action
    
    @IBAction func btnMale(_ sender: UIButton){
        
        imgViewMRadio.image = UIImage(named: "selectedImage")
        imgViewFRadio.image = UIImage(named: "unselected")
        imgViewORadio.image = UIImage(named: "unselected")
        genderStr = "Male"

    }
    
    @IBAction func btnFemale(_ sender: UIButton){
        
        imgViewFRadio.image = UIImage(named: "selectedImage")
        imgViewMRadio.image = UIImage(named: "unselected")
        imgViewORadio.image = UIImage(named: "unselected")
        genderStr = "Female"

    }
    
    @IBAction func btnOther(_ sender: UIButton){
        
        imgViewORadio.image = UIImage(named: "selectedImage")
        imgViewMRadio.image = UIImage(named: "unselected")
        imgViewFRadio.image = UIImage(named: "unselected")
        genderStr = "Others"

    }
    
    @IBAction func btnHome(_ sender: UIButton){
        selectedAddressType = "Home"
        vwHome.backgroundColor = #colorLiteral(red: 0.6274509804, green: 0.01960784314, blue: 0.1529411765, alpha: 1)
        btnHome.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        btnWork.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnOffice.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        vwWork.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        vwOffice.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    }
    
    @IBAction func btnWork(_ sender: UIButton){
        selectedAddressType = "Work"
        vwWork.backgroundColor = #colorLiteral(red: 0.6274509804, green: 0.01960784314, blue: 0.1529411765, alpha: 1)
        btnWork.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        btnHome.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnOffice.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        vwHome.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        vwOffice.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    }
    
    @IBAction func btnOffice(_ sender: UIButton){
        selectedAddressType = "Office"
        vwOffice.backgroundColor = #colorLiteral(red: 0.6274509804, green: 0.01960784314, blue: 0.1529411765, alpha: 1)
        btnOffice.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        btnHome.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnWork.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        vwHome.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        vwWork.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    }
    
    @IBAction func backButtonPressed(_ sender: Any){
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnContinue(_ sender: Any){
        addressParams()
    }
    
    @IBAction func btnSignInPressed(_ sender: Any){
        RootManager().setLoginRoot()
    }
    
    @IBAction func btnSkip(_ sender: Any){
        
        addressParams()
    }
    
    @IBAction func countryList(_ sender: UIButton){
        
        setupCountryDropDown(sender, 0)
    }
    
    @IBAction func buttonOpenPicker(_ sender: UIButton){
        
        setupCountryDropDown(sender, 1)

    }
    
    
    
    func setupCountryDropDown(_ button: UIButton, _ index : Int) {
        
        if index == 0 {
        
            var countryArray = [String]()
            var countryArrayID = [String]()
            var selectedCountryCodeArray = [String]()

            
            for i in 0..<arrCountryList.count
            {
                let dict =  arrCountryList[i]
                countryArray.append(dict.countryName ?? "United Arab Emirates")
                countryArrayID.append(String(dict.countryId ?? 1))
                selectedCountryCodeArray.append(dict.countryCode ?? "UAE")

            }
      
            
            if countryArray.count == 0 {
                countryArray.append("United Arab Emirates")
                countryArrayID.append("1")
                selectedCountryCodeArray.append("UAE")
            }
            
            dropCountry.anchorView = button
            dropCountry.dataSource = countryArray
            dropCountry.selectionAction = { [weak self] (index, item) in
                self?.selectCountryTF.text = countryArray[index]
                self?.selectedCountry = countryArray[index]
                self?.countryRegionId = selectedCountryCodeArray[index]
                EndPoint.userId = "\(countryArrayID[index])"
            }
            
            dropCountry.show()
        }
        else if index == 1 {
            
            var selectedCityArray = [String]()
            var selectedCityCodeArray = [String]()

            for i in 0..<arrCityList.count
            {
                let dict =  arrCityList[i]
                selectedCityArray.append(dict.cityName ?? "Abu Dhabi")
                selectedCityCodeArray.append(dict.cityCode ?? "C-L-ADB-N")

            }
            
            if selectedCityArray.count == 0 {
                selectedCityArray.append("Abu Dhabi")
                selectedCityCodeArray.append("C-L-ADB-N")
            }
            
            dropCountry.anchorView = button
            dropCountry.dataSource = selectedCityArray
            dropCountry.selectionAction = { [weak self] (index, item) in
                self?.cityVillageTF.text =  item
                self?.pinCodeTF.text = selectedCityCodeArray[index]
                self?.taxGroup = selectedCityCodeArray[index]
            }
            dropCountry.show()
        }
    }

    
    //MARK:- Get CountryList API
    func GetCountryList(){
        EndPoint.GetCountryList(t: CountryListModel.self) { (result) in
            switch result {
            
            case .onSuccess(let items):
                if items.count > 0 {
                    self.arrCountryList = items
                 //   self.countryPicker.reloadAllComponents()
                    
                    self.selectCountryTF.text = self.arrCountryList[0].countryName!
                    self.selectedCountry = self.arrCountryList[0].countryName!
                    self.countryRegionId = self.arrCountryList[0].countryCode!
                    EndPoint.userId = "\(self.arrCountryList[0].countryId!)"
                        
                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- Get CityList API
    func GetCityList(){
        EndPoint.GetCityList(t: CitiesListModel.self) { (result) in
            switch result {
            
            case .onSuccess(let items):
                if items.count > 0 {
                    
                    self.arrCityList = items
                    self.cityVillageTF.text = items[0].cityName!
                    self.pinCodeTF.text = items[0].cityCode
                    self.taxGroup = items[0].cityCode!
                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    
    //MARK:- SignUp API
    func SignupAPI(Model:SignUpModel){
        self.showLoader()
        EndPoint.SignUP(params: Model, t: UserModel.self) { (result) in
            
            self.hideLoader()
            switch result {
            case .onSuccess(let user):
                print(user)
                self.hideLoader()
                if user != nil{
                    UserDefaults.standard.set(user?.email, forKey: Constants.email)
                    UserDefaults.standard.set(user?.userId, forKey: Constants.userId)
                    UserDefaults.standard.set(user?.token, forKey: Constants.token)
                    UserDefaults.standard.set(user?.firstName, forKey: Constants.firstName)
                    UserDefaults.standard.set(user?.lastName, forKey: Constants.lastName)
                    UserDefaults.standard.set(user?.gender, forKey: Constants.gender)
                    UserDefaults.standard.set(user?.dob, forKey: Constants.dob)
                    UserDefaults.standard.set(user?.phoneNumber, forKey: Constants.phone)
                    UserDefaults.standard.set(user?.custAccount, forKey: Constants.custAccount)
                    UserDefaults.standard.synchronize()

                    if self.imgProfile != nil {
                        self.uploadProfileImageApi(userID: user!.userId!)
                    }
                    else{
                        NotificationAlert().NotificationAlert(titles: "User created successfully")
                        UserDefaults.standard.set(false, forKey: Constants.guestUser)
                        UserDefaults.standard.synchronize()
                        RootManager().SideRootScreen()
                    }
                }
            case .onFailure(let error):
                print(error)
                self.hideLoader()
                NotificationAlert().NotificationAlert(titles: error)
            }
        }
    }
    
    func uploadProfileImageApi(userID: String){
        
        let compressData = self.imgProfile!.jpegData(compressionQuality: 0.8)
        let compressedImage = UIImage(data: compressData!)
        self.showLoader()
        
        ImageUploadManager().uploadImageaaaRemote(image:compressedImage!,UserId: userID){ data, error -> Void in
            if !data!.isEmpty{
                self.hideLoader()
                print("profile image is :- ",data ?? "")
                UserDefaults.standard.set(data ?? "iOS", forKey: Constants.userImg)
                UserDefaults.standard.set(false, forKey: Constants.guestUser)
                UserDefaults.standard.synchronize()
                NotificationAlert().NotificationAlert(titles: "User created successfully")
                RootManager().SideRootScreen()

            }
        }
        
    }

    
    @IBAction func DateOfBrith(_ sender: Any) {

        addPicker()
        
    }
    
    func addPicker(){
       
        let alert = UIAlertController(style: .alert, title: "Select Date of Birth")
        
        if #available(iOS 13.4, *) {
            alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = UIColor.white
            alert.setTitle(font: UIFont(name: FontName.Optima.Bold, size: 15)!, color: UIColor.black)
            alert.view.tintColor = UIColor.black
        }
        else{
            alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = .white
            alert.setTitle(font: UIFont(name: FontName.Optima.Bold, size: 15)!, color: UIColor.white)
            alert.view.tintColor = UIColor.white
        }
        
        let date = Calendar.current.date(byAdding: .year, value: -8, to: Date())
        currentDate = date ?? Date()
        
        
        if self.dobTF.text == ""
        {
            currentDate = date ?? Date()
        }
        else
        {
            
            let isoDate = dobTF.text
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "yyyy-MM-dd"
            currentDate = dateFormatter.date(from:isoDate!)!
            
        }
        
        alert.addDatePicker(mode: .date, date: currentDate, minimumDate: nil, maximumDate: date) { date in
            self.currentDate = date
            }
        alert.addAction( title: "OK", style: .default, isEnabled: true) { (action) in
            
            self.dobTF.text = "str".convertToYYYYMMDD(date:self.currentDate)
            
            }
        alert.addAction(title: "Cancel", style: .cancel){ (action) in
            
        }
        
        alert.show()
        
    }
}




//MARK:- UITextFieldDelegate
@available(iOS 13.0, *)
extension AddressSignUPViewController: UITextFieldDelegate {
 
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        VWCountryPicker.isHidden = true
        
        if textField == cityVillageTF  {
            if  selectCountryTF.text == ""{
                NotificationAlert().NotificationAlert(titles: "Select Country First")
            }
        }
        
        if textField == selectCountryTF{
         //   pickUp(selectCountryTF)
          //  VWCountryPicker.isHidden = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == ""{
            return true
        }
        if textField.text == "" && string == " "{
            return false
        }
        return true
    }
    
}

extension AddressSignUPViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCountryList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let countryName = arrCountryList[row].countryName!
        let countryCode = arrCountryList[row].countryCode!
     //   self.selectCountryTF.text = arrCountryList[row].countryName!
        self.selectedCountry = arrCountryList[row].countryName!
        self.countryRegionId = arrCountryList[row].countryCode!
        self.countryId = arrCountryList[row].countryId!

        return "\(countryCode) " + " \(countryName)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.selectCountryTF.text = arrCountryList[row].countryName!
        self.selectedCountry = arrCountryList[row].countryName!
        self.countryRegionId = arrCountryList[row].countryCode!
        EndPoint.userId = "\(arrCountryList[row].countryId!)"
        
        self.cityVillageTF.text = ""
        
    }
}
