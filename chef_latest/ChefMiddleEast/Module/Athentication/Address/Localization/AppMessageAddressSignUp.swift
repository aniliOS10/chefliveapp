//
//  AppMessageAddressSignUp.swift
//  ChefMiddleEast
//
//  Created by Apple on 27/10/21.
//

import Foundation
class AppMessageAddressSignUP{
struct Placeholder {
   
    static let DOBDate = NSLocalizedString("DOB", comment: "")
    static let HouseApartNumber = NSLocalizedString("HouseApartNumber", comment: "")
    static let HouseProvince = NSLocalizedString("HouseProvince", comment: "")
    static let CityVillage = NSLocalizedString("CityVillage", comment: "")
    static let PinCode = NSLocalizedString("PinCode", comment: "")
    static let SelectCountry = NSLocalizedString("SelectCountry", comment: "")
   
    static let skip = NSLocalizedString("skip", comment: "")
    static let next = NSLocalizedString("next", comment: "")
    static let save = NSLocalizedString("save", comment: "")
  
}
struct ValidationError {
static let EmptyfullName = NSLocalizedString("EmptyfullName", comment: "")
static let EmptyPhonenumber = NSLocalizedString("EmptyPhonenumber", comment: "")
    
static let EmptyEmail = NSLocalizedString("EmptyEmail", comment: "")
static let EmptyPassword = NSLocalizedString("EmptyPassword", comment: "")
static let EmptyConfirmPassword = NSLocalizedString("EmptyConfirmPassword", comment: "")
static let PasswordDonotMatch = NSLocalizedString("PasswordDonotMatch", comment: "")
static let continueButton = NSLocalizedString("continue", comment: "")

    
    static let InvalidEmail = NSLocalizedString("InvalidEmail", comment: "")
    static let InvalidPhone = NSLocalizedString("InvalidPhone", comment: "")
    static let passwordTooShort = NSLocalizedString("passwordTooShort", comment: "")

static let fullNameTooShort = NSLocalizedString("fullNameTooShort", comment: "")

}
}
