//
//  CountryListModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 25/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class CountryListModel: BaseResponse{
    
    var countryId : Int?
    var countryName : String?
    var countryCode : String?
    var cityName : String?
    var cityCode : String?

    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        countryId <- map["countryId"]
        countryName <- map["countryName"]
        countryCode <- map["countryCode"]
        cityName <- map["cityName"]
        cityCode <- map["cityCode"]
    }
}

class CitiesListModel: BaseResponse{
    
    var countryId : Int?
    var cityCode : String?
    var cityName : String?
    

    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        countryId <- map["countryId"]
        cityName <- map["cityName"]
        cityCode <- map["cityCode"]
    }
}
