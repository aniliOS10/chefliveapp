//
//  CategoryListDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import UIKit

extension CategoryListVC : UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView {
        case tblSearch:
            return 2
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        switch tableView {
        case tblSearch:
            return nil
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch tableView {
        case tblSearch:
            return 0
        default:
            return 0
        }
       // return section == 0 ? 0.0 :40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        switch tableView {
        case tblSearch:
            if section == 0 {
                return arrLocalCategory.count
            }
            else if section == 1 {
                return arrlocalProduct.count
            }
            else{
                return 0
            }
        default:
            return 0
        }
      //  return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        switch tableView {
        case tblSearch:
            if indexPath.section == 0 {
                let cell = tblSearch.dequeueReusableCell(withIdentifier: "CategoryListSearchTVC") as! CategoryListSearchTVC
                cell.lblTitleName.textColor = UIColor.black
                if arrLocalCategory.count > 0 {
                    let catName = arrLocalCategory[indexPath.row]
                    cell.lblTitleName.text =  "\(catName)\nIn Category"
                    cell.lblTitleName.textColor = UIColor().getThemeColor()
                }
                
                return cell
            }
            else {
                let cell = tblSearch.dequeueReusableCell(withIdentifier: "CategoryListSearchTVC") as! CategoryListSearchTVC
                
                cell.lblTitleName.textColor = UIColor.black
                if arrlocalProduct.count > 0 {
                    cell.lblTitleName.text = arrlocalProduct[indexPath.row].productDescription ?? ""
                }
                
                return cell
            }
            
        default:
            break
        }
        
        return UITableViewCell.init()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        // return 200
        switch tableView {
        case tblSearch:
            return UITableView.automaticDimension
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        switch tableView {
        case tblSearch:
            view.endEditing(true)
            if indexPath.section == 0 {
                
                let controller:CategoryListingViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                controller.shouldDisplayHome = false
                controller.selectedCatIdName = arrLocalCategory[indexPath.row]
                controller.selectedCatName = arrLocalCategory[indexPath.row]
                
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else{
                let controller:ProjectDetailVC =  UIStoryboard(storyboard: .DashBoard).initVC()
                controller.arrProductDetail = arrlocalProduct[indexPath.row]
                controller.productId = arrlocalProduct[indexPath.row].productId!
              //  controller.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(controller, animated: true)
                /*let subCategoryId = arrlocalProduct[indexPath.row].subCategoryId
                
                let arrSubProduct = arrProduct.filter {$0.subCategoryId == subCategoryId}
                        
                let controller:NewArrivalsViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                controller.arrProduct = arrSubProduct
                controller.screenCommingFrom = "SubCategory"
                controller.shouldDisplayHome = false
                self.navigationController?.pushViewController(controller, animated: true)*/
            }
           
        default:
            break
        }
        
    }
    
}
