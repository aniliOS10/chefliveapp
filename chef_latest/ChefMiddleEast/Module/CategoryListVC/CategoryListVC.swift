//
//  CategoryListVC.swift
//  ChefMiddleEast
//
//  Created by Apple on 01/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit
import ObjectMapper


class CategoryListVC: BaseViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    
    @IBOutlet weak var txtSearch : UITextField!
    @IBOutlet weak var tblSearch : UITableView!
    @IBOutlet weak var vwTopSearch : UIView!
   
    @IBOutlet weak var categoryListCV : UICollectionView!
    @IBOutlet weak var vwSearch : UIView!
    @IBOutlet weak var vwBaseSearch : UIView!
    
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    var arrAllProduct = [NewArrivalsModel]()
    var arrlocalProduct = [NewArrivalsModel]()
    var arrCategoryNames = [String]()
    var arrLocalCategory = [String]()
    var arrCategory = [CategoryListingModel]()
    var sortedCategory = [CategoryListingModel]()
    var dictDashBoard = DashBoardModel()
    var searchText = String()
    
    lazy var dispatchGroup: DispatchGroup = {
            let dispatchGroup = DispatchGroup()
            return dispatchGroup
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.vwSearch.isHidden = true
        vwBaseSearch.isHidden = true
        self.txtSearch.text = ""
        self.txtSearch.delegate = self
        self.tblSearch.keyboardDismissMode = .onDrag
        
        self.txtSearch.resignFirstResponder()

        self.navigationController?.navigationBar.isHidden = false
        
        self.arrAllProduct = fetchAllProductLocalStorge()
        let arrCategory1 = arrAllProduct.unique{$0.categoryId != nil }.map({ $0.categoryId ?? "" })
        self.arrCategoryNames = arrCategory1
        
        if arrCategory.count > 0 {
            sortedCategory = arrCategory.sorted(by: { $0.categoryId! < $1.categoryId! })
        } else {
            categoryListAPI()
        }
        vwTopSearch.addShadow()
        vwTopSearch.layer.cornerRadius = 25.0
        setUpTabelView()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        vwBaseSearch.addGestureRecognizer(tap)
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        vwSearch.isHidden = true
        vwBaseSearch.isHidden = true
    }
    
    func setUpTabelView() {
        
        self.tblSearch.delegate = self
        self.tblSearch.dataSource = self
    }
    
    //MARK:- Fetch All Product
    func fetchAllProductLocalStorge() -> [NewArrivalsModel] {
        if let syncContactsData = UserDefaults.standard.value(forKey: Constants.ProductModel) as? Data {
            if let aryProduct:[[String:Any]] = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(syncContactsData) as? [[String : Any]] {
                return Mapper<NewArrivalsModel>().mapArray(JSONArray: aryProduct)
            }
        }
        return []
    }
    
    func categoryListAPI(){
        self.showLoader()
        EndPoint.GetAllCategoryList(t: CategoryListingModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccess(let items):
                if items.count > 0 {
                    self.arrCategory = items
                    
                    var tempArrCategory = [CategoryListingModel]()
                    for item in self.arrCategory {
                        if item.categoryId != nil {
                            tempArrCategory.append(item)
                        }
                    }
                    
                    self.sortedCategory = tempArrCategory.sorted(by: { $0.categoryId! < $1.categoryId! })
                    
                    DispatchQueue.main.async {
                        self.categoryListCV.reloadData()
                    }
                }
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
                self.hideLoader()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.vwSearch.isHidden = true
        self.txtSearch.text = ""
        self.txtSearch.resignFirstResponder()
        txtSearch.autocorrectionType = .no
        txtSearch.spellCheckingType = .no
        
        self.tabBarController?.tabBar.isHidden = false
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            EndPoint.deviceType = "ios"
        }
        self.setNavigationButton()
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        if updatedString!.length > 0 {
            self.searchText = updatedString!
            NSObject.cancelPreviousPerformRequests( withTarget: self, selector: #selector(searchDasboardProducts), object: nil)
            
            self.perform(#selector(searchDasboardProducts), with: nil, afterDelay: 0.5)
            
        } else {
            self.searchText = ""
            self.vwSearch.isHidden = true
            self.vwBaseSearch.isHidden = true
        }
        return true

    }
    
    //MARK:- search Dasboard Products
    @objc private func searchDasboardProducts() {
      
        self.vwSearch.layer.cornerRadius = 15
        self.vwSearch.addShadow()
        let arrLocalProduct = self.arrAllProduct
        let arrCategory = arrAllProduct.unique{$0.categoryId}.map({ $0.categoryId ?? "" })
        
        var tempArrCategory = [String]()
        for item in arrCategory {
            if  item != ""{
                tempArrCategory.append(item)
            }
        }
        self.arrLocalCategory  = tempArrCategory.filter { $0.localizedCaseInsensitiveContains(searchText) }
        self.arrlocalProduct = (arrLocalProduct.filter({(($0.productDescription ?? "").localizedCaseInsensitiveContains(searchText))}))
        
        if self.arrLocalCategory.count > 0 ||  self.arrlocalProduct.count > 0 {
            if searchText.length > 0 {
            self.vwSearch.isHidden = false
            self.vwBaseSearch.isHidden = false
                
            if self.arrLocalCategory.count > 4 ||  self.arrlocalProduct.count > 4 {
                  searchViewHeight.constant = CGFloat(444)
                }
                else{
                    if self.arrLocalCategory.count > self.arrlocalProduct.count {
                        searchViewHeight.constant = CGFloat(self.arrLocalCategory.count * 75)
                    }
                    if self.arrlocalProduct.count > self.arrLocalCategory.count {
                        searchViewHeight.constant = CGFloat(self.arrlocalProduct.count * 75)
                    }
                }
            }
            else{
                self.vwSearch.isHidden = true
                self.vwBaseSearch.isHidden = true
            }
        }
        else{
            self.vwSearch.isHidden = true
            self.vwBaseSearch.isHidden = true
        }
        tblSearch.reloadData()
    }
    
    // MARK: - GetArtistProfileAndUpcomingEvents
        func groupAllDashboardApis() {
            dispatchGroup.enter()
            CallGetDashBoardAPIApi()
            dispatchGroup.notify(queue: .main) {
                DispatchQueue.main.async {
                    self.hideLoader()
                    self.categoryListCV.reloadData()
                }

            }
        }
    
    //MARK:- Call Dasboard API
    func CallGetDashBoardAPIApi() {
        
        self.getDashBoardAPI(){ sucess in
            if sucess {
                self.dispatchGroup.leave()
            }
        }
    }
    
    
    //MARK:- Dashboard Api
    func getDashBoardAPI(response:((_ success:Bool)->Void)!) {
        EndPoint.DashBoard(t: DashBoardModel.self) { [self] result in
            switch result {
            case .onSuccess(let data):
                if data != nil {
                    self.dictDashBoard = data!
                    //Filter Category array to avoid category without name
                    for item in self.dictDashBoard.categoriesList {
                        if item.categoryId != nil {
                            arrCategory.append(item)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        
                        self.categoryListCV.reloadData()

                    }
                    
                    
                    response(true)
                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
        
    //MARK:- Set Navigation
    func setNavigationButton() {
        self.initMenuButton()
        self.initBellButton()

        menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
        bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            
            self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [], title: Appmessage_CategoryListing.Placeholder.Categories)

        }
        else {
            self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [bellBarButton], title: Appmessage_CategoryListing.Placeholder.Categories)

        }
 
    }
    
    
    //MARK:- Notification Button
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- Menu Button
    @objc func menuButtonPressed(){
        self.sideMenuController?.revealMenu()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        sortedCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell: DashCategoryCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashCategoryCollectionCell", for: indexPath) as! DashCategoryCollectionCell
        
        cell.cellViewRoundBG.addShadowDash()
        cell.cellViewRound.layer.cornerRadius = 10
        cell.cellViewRoundBG.layer.cornerRadius = 10
        
        if let imgUrl = sortedCategory[indexPath.row].categoryImage,!imgUrl.isEmpty {
            
            let imagePath = "\(EndPoint.BASE_API_IMAGE_URL)\(imgUrl)"
            let urlString = imagePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                if (error != nil) {
                    cell.imageV.image = UIImage(named: "placeholder")
                } else {
                    cell.imageV.image = image
                }
            }
        } else {
            cell.imageV.image = UIImage(named: "placeholder")
        }
        cell.titleLabel.text = sortedCategory[indexPath.row].categoryId
            
        return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let width = (UIScreen.main.bounds.size.width/2)
       
        let size = CGSize(width: width, height: 185)
        
        return size
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            let controller:CategoryListingViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            controller.shouldDisplayHome = false
            controller.selectedCatIdName = sortedCategory[indexPath.row].categoryId ?? ""
            controller.selectedCatName = sortedCategory[indexPath.row].categoryId ?? ""
            controller.lastClass = "TabBar"
        
            self.navigationController?.pushViewController(controller, animated: true)
        
    }
    

    

}
