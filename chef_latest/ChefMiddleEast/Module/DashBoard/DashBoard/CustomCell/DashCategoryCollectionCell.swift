//
//  DashCategoryCollectionCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 28/10/21.
//

import UIKit

class DashCategoryCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imageV: UIImageView!
  //  @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cellViewRound: UIView!
    
    @IBOutlet weak var cellViewRoundBG: UIView!

}
