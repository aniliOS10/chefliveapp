//
//  DashRecipeTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 28/10/21.
//

import UIKit

@available(iOS 13.0, *)
class DashRecipeTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout   {
    
    @IBOutlet weak var collectionV: UICollectionView!
    
    var arrRecipe = [RecipeListModel]()
    
    var completion_BtnFav: ((_ button:UIButton) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrRecipe.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: DashRecipeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashRecipeCollectionViewCell", for: indexPath) as! DashRecipeCollectionViewCell
        
        cell.cellViewRound.layer.cornerRadius = 6
        cell.cellViewRoundBG.addShadowDash()
        cell.cellViewRoundBG.layer.cornerRadius = 6
        
        if arrRecipe.count > 0 {
            cell.vwMinutes.layer.cornerRadius = 15
            cell.btnFav.layer.cornerRadius = 15
            cell.vwMinutes.addShadow()
            
            var preparetime = ""
            preparetime = String(format: "%@ %@", arrRecipe[indexPath.row].preparetime  ?? "" ,arrRecipe[indexPath.row].preparetimeDesc ?? "")
        
            cell.btnFav.setTitle(preparetime, for: .normal)
            cell.desLabel.text = "Difficulty level"

            cell.imgDifficulty1.image = UIImage(named: "un_difficulty")
            cell.imgDifficulty2.image = UIImage(named: "un_difficulty")
            cell.imgDifficulty3.image = UIImage(named: "un_difficulty")
            if let difficultyLevel = arrRecipe[indexPath.row].difficultyLevel, difficultyLevel != 0 {
                if difficultyLevel == 1{
                    cell.imgDifficulty1.image = UIImage(named: "selected_difficulity")
                    cell.imgDifficulty2.image = UIImage(named: "un_difficulty")
                    cell.imgDifficulty3.image = UIImage(named: "un_difficulty")
                }
                if difficultyLevel == 2{
                    cell.imgDifficulty1.image = UIImage(named: "selected_difficulity")
                    cell.imgDifficulty2.image = UIImage(named: "selected_difficulity")
                    cell.imgDifficulty3.image = UIImage(named: "un_difficulty")
                }
                if difficultyLevel == 3{
                    cell.imgDifficulty1.image = UIImage(named: "selected_difficulity")
                    cell.imgDifficulty2.image = UIImage(named: "selected_difficulity")
                    cell.imgDifficulty3.image = UIImage(named: "selected_difficulity")
                }
            }
            
            cell.titleLabel.text = arrRecipe[indexPath.row].recipeTitle
            if let imgUrl = arrRecipe[indexPath.row].recipeImage,!imgUrl.isEmpty {
                
                let img = "\(EndPoint.BASE_API_IMAGE_URL)\(imgUrl)"
                let urlString = img.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
                cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                    if (error != nil) {
                        cell.imageV.image = UIImage(named: "placeholder")
                    } else {
                        cell.imageV.image = image
                    }
                }
            }
            else {
                cell.imageV.image = UIImage(named: "placeholder")
            }
            /*if let bookmark = arrRecipe[indexPath.row].isBookmark, bookmark == true {
                cell.btnBookMark.isSelected = true
            }
            else{
                cell.btnBookMark.isSelected = false
            }
            
            if let favourite = arrRecipe[indexPath.row].isFavourite, favourite == true {
                cell.btnFav.isSelected = true
            }
            else{
                cell.btnFav.isSelected = false
            }*/
        }
       
        cell.btnFav.tag = indexPath.row
        cell.btnFav.addTarget(self, action: #selector(btnFavouritTap(button:)), for: .touchUpInside)
        
        cell.btnBookMark.tag = indexPath.row
        cell.btnBookMark.addTarget(self, action: #selector(btnBookMarkTap(button:)), for: .touchUpInside)
        
        cell.btnEye.isHidden = true
        cell.btnBookMark.isHidden = true

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let width = (UIScreen.main.bounds.size.width/2)-10
        
        let size = CGSize(width: width, height: 231)
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let controller:RecipeDetailViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
        controller.recipeId = arrRecipe[indexPath.row].recipeId!
        self.parentViewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- Fav Button Tap
    @objc func btnFavouritTap(button:UIButton){
       /* button.isSelected = !button.isSelected
        arrRecipe[button.tag].isFavourite = button.isSelected
        
        EndPoint.recipeId = "\(arrRecipe[button.tag].recipeId!)"
        if button.isSelected == true {
            EndPoint.favouriteStatus = "true"
        }
        else {
            EndPoint.favouriteStatus = "false"
        }
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            markRecipeFavouriteAPI()
        }*/
    }
    
    //MARK:- Favourite Button Tap
    @objc func btnBookMarkTap(button:UIButton){
       /* button.isSelected = !button.isSelected
        if button.isSelected == true {
            EndPoint.bookmarkStatus = "true"
        }
        else {
            EndPoint.bookmarkStatus = "false"
        }
        EndPoint.recipeId = "\(arrRecipe[button.tag].recipeId!)"
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            markRecipeBookmarkAPI()
        }*/
    }
    
}

//MARK:- API's
extension DashRecipeTableViewCell{
    
    // MARK: - Recipe List API
    func markRecipeFavouriteAPI(){
       // self.showLoader()
       
        EndPoint.MarkRecipeFavourite(t: RecipeListModel.self) { result in
            switch result {
            case .onSuccess(_ ):
                NotificationAlert().NotificationAlert(titles: "Sucess Response")
             //   self.hideLoader()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    // MARK: - Recipe List API
    func markRecipeBookmarkAPI(){
      //  self.showLoader()
       
        EndPoint.MarkRecipeBookmark(t: RecipeListModel.self) { result in
            switch result {
            case .onSuccess(_ ):
                NotificationAlert().NotificationAlert(titles: "Sucess Response")
             //   self.hideLoader()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
}


extension UIView {
    
    func addShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 2.0
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    }
    func addLightShadow(){
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 1.0
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    }
    
    func addShadowDash(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 1.8
        self.layer.shadowOffset = CGSize(width: 0.4, height: 0.4)
    }
    
    func addShadowHomeBanner(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 1.8
        self.layer.shadowOffset = CGSize(width: 0.1, height: 0.1)
    }
    
    func addShadowFull(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 2.0
        self.layer.shadowOffset = CGSize(width: 0.4, height: 0.4)
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

