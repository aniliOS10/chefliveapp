//
//  DashCategoryTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 28/10/21.
//

import UIKit

@available(iOS 13.0, *)
class DashCategoryTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
    
    
    @IBOutlet weak var collectionV: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
   
    var arrSortedCategory = [CategoryListingModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrSortedCategory.count
}
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
    let cell: DashCategoryCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashCategoryCollectionCell", for: indexPath) as! DashCategoryCollectionCell

    cell.cellViewRoundBG.addShadowDash()
    cell.cellViewRoundBG.layer.cornerRadius = 6
    cell.cellViewRound.layer.cornerRadius = 6
    if let imgUrl = arrSortedCategory[indexPath.row].categoryImage,!imgUrl.isEmpty {
        
        let img = "\(EndPoint.BASE_API_IMAGE_URL)\(imgUrl)"
        let urlString = img.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
            if (error != nil) {
                cell.imageV.image = UIImage(named: "placeholder")
            } else {
                cell.imageV.image = image
            }
        }
    } else {
        cell.imageV.image = UIImage(named: "placeholder")
    }
    cell.titleLabel.text = arrSortedCategory[indexPath.row].categoryId
        
    return cell
}
    
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
    let width = (UIScreen.main.bounds.size.width/2) - 11
    let size = CGSize(width: width, height: 185)
    return size
}
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller:CategoryListingViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
        controller.shouldDisplayHome = false
        controller.selectedCatIdName = arrSortedCategory[indexPath.row].categoryId ?? ""
        controller.selectedCatName = arrSortedCategory[indexPath.row].categoryId ?? ""
        self.parentViewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
