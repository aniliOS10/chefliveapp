//
//  DashCollectionTCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 29/10/21.
//

import UIKit

@available(iOS 13.0, *)
class DashCollectionTCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout   {
  
    @IBOutlet weak var collectionV: UICollectionView!
    var arrProduct = [NewArrivalsModel]()
    var arrRecipe = [RecipeListModel]()

    
    var arrCollectionList = [CollectionListModel]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrCollectionList.count
}
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
    let cell: DashCollectionTcollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashCollectionTcollectionCell", for: indexPath) as! DashCollectionTcollectionCell
  
    cell.cellViewRound.addShadowDash()
    cell.cellViewRound.layer.cornerRadius = 6
    cell.imageV.layer.cornerRadius = 6
    
    cell.lblTitle.text = arrCollectionList[indexPath.row].collectionName
    
    if let imgUrl = arrCollectionList[indexPath.row].collectionImage,!imgUrl.isEmpty {
        
        let imgPath = String("\(EndPoint.BASE_API_IMAGE_URL)\(imgUrl)")
        
        let urlString = imgPath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

        
        cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
            if (error != nil) {
                cell.imageV.image = UIImage(named: "placeholder")
            } else {
                cell.imageV.image = image
            }
        }
    }

    return cell
}
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
    let width = (UIScreen.main.bounds.size.width/2)
   
    let size = CGSize(width: width, height: 200)
    
    return size
   // return UICollectionViewFlowLayout.automaticSize
}
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let productID = arrCollectionList[indexPath.row].productId, !productID.isEmpty {
            print(productID)
           
            let arrProductList = productID.components(separatedBy: ",")
            
            let controller:NewArrivalsViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            if arrProductList.count > 0 {
                let arrFiltered = arrProduct.filter( { arrProductList.contains(String("\($0.productId!)") ) == true} )
                controller.arrProduct = arrFiltered
            }
            controller.screenCommingFrom = "SubCategory"
            controller.shouldDisplayHome = false
            controller.arrSelectedProduct = arrProductList
            controller.headerTitle = arrCollectionList[indexPath.row].collectionName!
            self.parentViewController?.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            if let recipeId = arrCollectionList[indexPath.row].recipeId, !recipeId.isEmpty {
                print(recipeId)
                let controller:RecipeListViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                let arrRecipe = recipeId.components(separatedBy: ",")
                controller.arrSelectedRecipe = arrRecipe
                self.parentViewController?.navigationController?.pushViewController(controller, animated: true)
               
            }
        }
    }
}

