//
//  DashRecipeCollectionViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 28/10/21.
//

import UIKit

class DashRecipeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnBookMark: UIButton!
    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var imgDifficulty1: UIImageView!
    @IBOutlet weak var imgDifficulty2: UIImageView!
    @IBOutlet weak var imgDifficulty3: UIImageView!
    @IBOutlet weak var vwMinutes: UIView!
    
    @IBOutlet weak var cellViewRoundBG: UIView!

}
