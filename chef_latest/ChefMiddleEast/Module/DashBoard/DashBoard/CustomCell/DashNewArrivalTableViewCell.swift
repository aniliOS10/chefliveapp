//
//  DashNewArrivalTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 28/10/21.
//

import UIKit

@available(iOS 13.0, *)
class DashNewArrivalTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout   {
    
    
    @IBOutlet weak var collectionV: UICollectionView!
    
    var arrNevArrival = [NewArrivalsModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrNevArrival.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: DashNewArrivalCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashNewArrivalCollectionViewCell", for: indexPath) as! DashNewArrivalCollectionViewCell
        
        cell.cellViewRound.addShadowDash()
        cell.cellViewRound.layer.cornerRadius = 6
        cell.cellView.layer.cornerRadius = 6
        cell.titleLabel.numberOfLines = 2
        cell.titleLabel.text = arrNevArrival[indexPath.row].productDescription
        cell.imageV.image = UIImage(named: "placeholder")
        cell.imageV.contentMode = .scaleAspectFill

        
        cell.btnAdd.layer.cornerRadius = 5
        
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(btnAddTap(sender:)), for: .touchUpInside)
    
        if (arrNevArrival[indexPath.row].productImage != nil) {
            if (arrNevArrival[indexPath.row].productImage)?.count ?? 0 > 0 {
                let imageData = arrNevArrival[indexPath.row].productImage![0] as! [String : Any]
                let imagePath = String("\(EndPoint.BASE_API_PRODUCT_IMAGE_URL)\(imageData["productDetailImagePath"] as! String)")
                
                let urlString = imagePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

                cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                    if (error != nil) {
                        cell.imageV.image = UIImage(named: "placeholder")
                    } else {
                        cell.imageV.contentMode = .scaleAspectFit
                        cell.imageV.image = image
                    }
                }
            }
        }else {
            cell.imageV.image = UIImage(named: "placeholder")
        }
        
        
        
         cell.salePriceLabel.isHidden = true
        
        let availableQty = arrNevArrival[indexPath.row].availableQuantity
        if ((availableQty) != nil )  {
            if (availableQty)! > 5 {
           
        //Display agreementValue
         if let startDate = arrNevArrival[indexPath.row].fromDate,startDate != nil {
             
             let fromDate : Date = startDate.dateFromString(startDate)
             let endDate = arrNevArrival[indexPath.row].toDate
             let toDate : Date = endDate!.dateFromString(endDate!)
            
             if Date().isBetween(fromDate, and: toDate) {
                 if let agreementValue = arrNevArrival[indexPath.row].agreementValue, agreementValue != nil {
                     
                     cell.salePriceLabel.isHidden = false
                     
                     let agreementValue = arrNevArrival[indexPath.row].agreementValue
                     cell.salePriceLabel.text = String(format: "AED %.2f", agreementValue!)
                     cell.salePriceLabel.textAlignment = .right

                     if let price = arrNevArrival[indexPath.row].salesPrice,price != nil {
                         let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: String(format: "  %.2f", price))
                         attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
                         cell.desLabel.attributedText = attributeString
                         cell.desLabel.textAlignment = .left
                         cell.desLabel.isHidden = false
                     }
                 }
             } else {
                 if let price = arrNevArrival[indexPath.row].salesPrice,price != nil {
                     cell.desLabel.isHidden = true
                     cell.salePriceLabel.isHidden = false
                     cell.salePriceLabel.text = String(format: "AED %.2f", price)
                     cell.desLabel.text = ""
                 }
             }
         } else {
             if let price = arrNevArrival[indexPath.row].salesPrice,price != nil {
                 cell.desLabel.isHidden = true
                 cell.salePriceLabel.isHidden = false
                 cell.salePriceLabel.text = String(format: "AED %.2f", price)
                 cell.desLabel.text = ""

             }
         }
                cell.btnAdd.isHidden = false
                cell.comingSoonLabel.isHidden = true
                
                if let price = arrNevArrival[indexPath.row].salesPrice,price > 0 {
                }
                else{
                    cell.btnAdd.isHidden = true
                    cell.comingSoonLabel.isHidden = false
                    cell.salePriceLabel.isHidden = true
                    cell.desLabel.isHidden = true
                    cell.desLabel.text = ""
                }
                
            } else {
                 cell.btnAdd.isHidden = true
                 cell.comingSoonLabel.isHidden = false
                 cell.salePriceLabel.isHidden = true
                 cell.desLabel.isHidden = true
            }
        }else {
            cell.btnAdd.isHidden = true
            cell.comingSoonLabel.isHidden = false
            cell.salePriceLabel.isHidden = true
            cell.desLabel.isHidden = true
        }
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let width = (UIScreen.main.bounds.size.width/2 ) + 10
        
        let size = CGSize(width: width, height: 250)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller:ProjectDetailVC =  UIStoryboard(storyboard: .DashBoard).initVC()
        controller.arrProductDetail = arrNevArrival[indexPath.row]
        controller.productId = arrNevArrival[indexPath.row].productId!
        self.parentViewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- Add Button Tap
    @objc func btnAddTap(sender:UIButton){
        
      //  if screenCommingFrom == "SubCategory" {
            
            if !arrNevArrival.isEmpty {
                guard let productId = arrNevArrival[sender.tag].productId else {return}
                if productId != 0 {
                    let param = AddToCartModel()
                    if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                        param.userId = userId
                    }
                    let paramProduct = ProductModel()
                    paramProduct.productId = productId
                    paramProduct.productQuantity = 1
                    param.product.append(paramProduct)
                    self.addToCartAPI(Model: param)
                }
            }
      //  }
     /*   else{
            if !arrFavourit.isEmpty {
                guard let favouriteId = arrFavourit[sender.tag].favouriteId else {return}
                if favouriteId != 0 {
                    let param = AddToCartModel()
                    if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                        param.userId = userId
                    }
                    let paramProduct = ProductModel()
                    paramProduct.productId = favouriteId
                    paramProduct.productQuantity = 1
                    param.product.append(paramProduct)
                    self.addToCartAPI(Model: param)
                }
            }
        }*/
        
    }
    
    //MARK:- Add to Cart Api
    func addToCartAPI(Model: AddToCartModel) {
        
        EndPoint.AddToCart(params: Model,t: AddToCartModel.self) { result in
            switch result {
            case .onSuccessWithStringValue(let msg):
                NotificationAlert().NotificationAlert(titles: msg)
                self.cartCountAPI()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
     
    //MARK:- Cart Count Api
    func cartCountAPI() {
        EndPoint.CartCount(t: CartCountModel.self) { result in
            switch result {
            case .onSuccess(let data):
            if data != nil{
                dictCartData = data!
                if let cartCount = dictCartData.cartCount, cartCount != 0 {
                    if let tabItems = self.parentViewController?.tabBarController?.tabBar.items {
                        // In this case we want to modify the badge number of the third tab:
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "\(cartCount)"
                    }
                }
            }
            case .onFailure(let error):
                print(error)
            }
        }
    }
}
