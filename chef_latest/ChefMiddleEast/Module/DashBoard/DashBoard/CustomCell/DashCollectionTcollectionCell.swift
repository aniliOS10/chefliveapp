//
//  DashCollectionTcollectionCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 29/10/21.
//

import UIKit

class DashCollectionTcollectionCell: UICollectionViewCell {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var lblTitle: UILabel!
}
