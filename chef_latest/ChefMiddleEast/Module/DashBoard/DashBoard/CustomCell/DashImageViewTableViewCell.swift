//
//  DashImageViewTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 28/10/21.
//

import UIKit

@available(iOS 13.0, *)
class DashImageViewTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionV: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    
    var arrDashBoardBanner = [DashBoardBannerModel]()
    var arrProduct = [NewArrivalsModel]()
    
    var timer = Timer()
    var counter = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pageController.numberOfPages = arrDashBoardBanner.count
        pageController.currentPage = 0
        DispatchQueue.main.async {
              self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
           } 
    }
    
    @objc func changeImage() {
         if counter < arrDashBoardBanner.count {
              let index = IndexPath.init(item: counter, section: 0)
             self.collectionV.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
             self.pageController.currentPage = self.counter
             self.counter += 1
//             UIView.animate(withDuration: 2, delay: 0.0, options: .curveEaseInOut, animations: {
//                 self.collectionV.layoutIfNeeded()
//
//             })
            
         } else {
             
             if arrDashBoardBanner.count > 0 {
                 counter = 0
                 let index = IndexPath.init(item: counter, section: 0)
                 self.collectionV.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
                 pageController.currentPage = counter
                  counter = 1
             }
            
           }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in collectionV.visibleCells {
            let indexPath = collectionV.indexPath(for: cell)
            pageController.currentPage = indexPath?.row ?? counter
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrDashBoardBanner.count
    }
    
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
      let cell: DashImageViewCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashImageViewCollectionViewCell", for: indexPath) as! DashImageViewCollectionViewCell
       cell.cellViewRound.addShadowHomeBanner()
       cell.cellViewRound.layer.cornerRadius = 12
       cell.imageV.layer.cornerRadius = 12
       cell.imageV.image = UIImage(named: "placeholder")

    if let imgUrl = arrDashBoardBanner[indexPath.row].featureBannerImage,!imgUrl.isEmpty {
        let img  = "\(EndPoint.BASE_API_IMAGE_URL)\(imgUrl)"
        let urlString = img.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
//        cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
//            if (error != nil) {
//                cell.imageV.image = UIImage(named: "placeholder")
//            } else {
//                cell.imageV.image = image
//            }
//        }
        
        cell.imageV?.sd_setImage(with: URL.init(string:(urlString)),
                              placeholderImage: UIImage(named: "placeholder"),
                              options: .refreshCached,
                              completed: nil)
    }
     
    return cell
}

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let size = CGSize(width:  (UIScreen.main.bounds.size.width), height: collectionView.frame.size.height)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let productID = arrDashBoardBanner[indexPath.row].productId, !productID.isEmpty {
           
            let arrProductList = productID.components(separatedBy: ",")
            
            if arrProductList.count > 0 {
                
                let arrFiltered = arrProduct.filter( { arrProductList.contains(String("\($0.productId!)") ) == true} )

                if arrFiltered.count > 0{
                let controller:NewArrivalsViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                
                controller.screenCommingFrom = "SubCategory"
                controller.arrProduct = arrFiltered

                controller.shouldDisplayHome = false
                controller.arrSelectedProduct = arrProductList
                self.parentViewController?.navigationController?.pushViewController(controller, animated: true)
                    
            }
                else {
                    if let recipeId = arrDashBoardBanner[indexPath.row].recipeId, !recipeId.isEmpty {
                        print(recipeId)
                        let controller:RecipeListViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                        let arrRecipe = recipeId.components(separatedBy: ",")
                        controller.arrSelectedRecipe = arrRecipe
                        self.parentViewController?.navigationController?.pushViewController(controller, animated: true)
                    }
                }
                
            }
          
            
            
        } else if let recipeId = arrDashBoardBanner[indexPath.row].recipeId, !recipeId.isEmpty {
            print(recipeId)
            let controller:RecipeListViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            let arrRecipe = recipeId.components(separatedBy: ",")
            controller.arrSelectedRecipe = arrRecipe
            self.parentViewController?.navigationController?.pushViewController(controller, animated: true)
        }
        
    }

}
