//
//  DashNewArrivalCollectionViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 28/10/21.
//

import UIKit

class DashNewArrivalCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var salePriceLabel: UILabel!
    @IBOutlet weak var comingSoonLabel: UILabel!
    @IBOutlet weak var cellView: UIView!

    
}
