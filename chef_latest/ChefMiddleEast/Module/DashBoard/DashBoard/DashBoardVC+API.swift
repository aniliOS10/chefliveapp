//
//  DashBoardVC+API.swift
//  ChefMiddleEast
//
//  Created by sandeep on 14/12/21.
//

import Foundation
import UIKit

extension DashBoardViewController{
    
    typealias CompletionHandler = (() -> Bool)

    
    //MARK:- Dashboard Api
    func getDashBoardAPI(response:((_ success:Bool)->Void)!) {
        EndPoint.DashBoard(t: DashBoardModel.self) { result in
            switch result {
            case .onSuccess(let data):
                if data != nil {
                    self.dictDashBoard = data!
                    response(true)
                }
            case .onFailure(let error):
                print(error)
                if error.contains("blocked.")
                {
                    NotificationAlert().NotificationAlert(titles: error)
                    CoreDataManager.shared.deleteObject_string(name: Entity.user.rawValue)
                    UserDefaults.standard.removeObject(forKey: Constants.token)
                    UserDefaults.standard.removeObject(forKey: Constants.userId)
                    UserDefaults.standard.removeObject(forKey: Constants.userImg)
                    UserDefaults.standard.removeObject(forKey: Constants.firstName)
                    UserDefaults.standard.removeObject(forKey: Constants.lastName)
                    UserDefaults.standard.removeObject(forKey: Constants.email)
                    UserDefaults.standard.removeObject(forKey: Constants.phone)
                    UserDefaults.standard.removeObject(forKey: Constants.custAccount)
                    RootManager().setLoginRoot()
                }
                else {
                    if error != "The request timed out."{
                        NotificationAlert().NotificationAlert(titles: error)
                    }
                }
            }
        }
    }
    
    //MARK:- Cart Count Api
    func cartCountAPI() {
        EndPoint.CartCount(t: CartCountModel.self) { result in
            switch result {
            case .onSuccess(let data):
            if data != nil{
                                if let cartCount = data!.cartCount, cartCount != 0 {
                                    self.setBagCountOnCart(count: cartCount)
                                    if let tabItems = self.tabBarController?.tabBar.items {
                                        let tabItem = tabItems[2]
                                        tabItem.badgeValue = "\(cartCount)"
                                    }
                                }
                                else{
                                    if let tabItems = self.tabBarController?.tabBar.items {
                                        let tabItem = tabItems[2]
                                        tabItem.badgeValue = "0"
                                    }
                                }
                                if let notificationCount = data!.notificationCount, notificationCount >= 0 {
                                    self.setNotificationBagCount(count: notificationCount)
                                }
                
                            }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
}
