//
//  Appmessage_Dashboard.swift
//  PetCare
//
//  Created by rupinder singh on 04/10/21.
//

import UIKit

class Appmessage_Dashboard: NSObject {
    struct Placeholder {
        static let Home = NSLocalizedString("Home", comment: "")
        static let MyPets = NSLocalizedString("MyPets", comment: "")
        static let CallAVet = NSLocalizedString("CallAVet", comment: "")
        static let OnlineInterviewQuestions = NSLocalizedString("OnlineInterviewQuestions", comment: "")
        static let seeAll = NSLocalizedString("seeAll", comment: "")
    }
}
