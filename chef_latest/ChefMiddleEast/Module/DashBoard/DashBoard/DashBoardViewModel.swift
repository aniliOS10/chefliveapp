//
//  DashBoardViewModel.swift
//  PetCare
//
//  Created by rupinder singh on 04/10/21.
//

import Foundation
import UIKit
class DashBoardViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    weak var dataSource:GenericDataSource<MockUp_Dashboard>?
    
    init(dataSource : GenericDataSource<MockUp_Dashboard>?) {
        self.dataSource = dataSource    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(){
        self.dataSource?.data.value.append(MockUp_Dashboard(title: Appmessage_Dashboard.Placeholder.MyPets, description: Appmessage_Dashboard.Placeholder.MyPets, image: UIImage.init(named: "imgPetCare") ?? UIImage()))
        self.dataSource?.data.value.append(MockUp_Dashboard(title: Appmessage_Dashboard.Placeholder.CallAVet, description: Appmessage_Dashboard.Placeholder.CallAVet, image: UIImage.init(named: "imgPetCare") ?? UIImage()))
    }
}
