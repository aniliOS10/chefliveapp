//
//  DashBoardCollectionViewDataSource.swift
//  PetCare
//
//  Created by rupinder singh on 04/10/21.
//

import UIKit

extension DashBoardViewController: UITableViewDataSource,UITableViewDelegate {
    
  
    func numberOfSections(in tableView: UITableView) -> Int {
        
        switch tableView {
        case tblDashboard:
            return 5
        case tblSearch:
            return 2
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        switch tableView {
        case tblDashboard:
            let cell = tblDashboard.dequeueReusableCell(withIdentifier: "DashSectionTableViewCell") as! DashSectionTableViewCell
            cell.seeAllButton.setTitle(Appmessage_Dashboard.Placeholder.seeAll, for: .normal)
            cell.seeAllButton.tag = section
            cell.seeAllButton.addTarget(self, action: #selector(seeAllButtonPressed(button:)), for: .touchUpInside)
            switch section {
            case 1:
                cell.titleLabel.text = "Categories"
            case 2:
                if newArrivalArray.count > 0 {
                    cell.titleLabel.text = "New Arrivals"
                    cell.seeAllButton.isHidden = false
                    return cell
                }
                else{
                    return nil
                }
                
            case 3:
                cell.titleLabel.text = "Recipes"
            case 4:
                cell.titleLabel.text = "Collections"
            default:
                return nil
            }
        
            return cell
        case tblSearch:
            return nil
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch tableView {
        case tblDashboard:
            if section == 2 {
                if newArrivalArray.count > 0 {
                    return 40
                }else{
                    return 0.1
                }
            }
            
        return section == 0 ? 0.0 :40
        case tblSearch:
            return 0
        default:
            return 0
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        switch tableView {
        case tblDashboard:
            return  1.1
        case tblSearch:
            return 0.0
        default:
            return 0.0
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        switch tableView {
        case tblDashboard:
            
            if section == 2 {
                if newArrivalArray.count > 0 {
                    return 1
                }
                else{
                    return 0
                }
            }
            return 1
        case tblSearch:
            if section == 0 {
                return arrLocalCategory.count
            }
            else if section == 1 {
                return arrlocalProduct.count
            }
            else{
                return 0
            }
        default:
            return 0
        }
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        switch tableView {
        case tblDashboard:
            if indexPath.section == 0{
                let cell = tblDashboard.dequeueReusableCell(withIdentifier: "DashImageViewTableViewCell") as! DashImageViewTableViewCell
                
                cell.collectionV.dataSource = cell
                cell.collectionV.delegate = cell
                cell.arrProduct = arrProduct
                cell.arrDashBoardBanner = dictDashBoard.featureBannerList
                cell.pageController.numberOfPages = dictDashBoard.featureBannerList.count
                cell.pageController.pageIndicatorTintColor = UIColor.lightGray
                cell.pageController.currentPageIndicatorTintColor = UIColor.white
                cell.collectionV.reloadData()
                return cell
            }
            else if  indexPath.section == 1{
                let cell = tblDashboard.dequeueReusableCell(withIdentifier: "DashCategoryTableViewCell") as! DashCategoryTableViewCell
                cell.collectionV.dataSource = cell
                cell.collectionV.delegate = cell
                cell.collectionV.width = cell.width
            
                cell.arrSortedCategory.removeAll()
                for item in arrSortedCategory {
                    if item.categoryId != nil {
                        cell.arrSortedCategory.append(item)
                    }
                }
                cell.collectionV.reloadData()
                return cell
            }
            else if  indexPath.section == 2{
                let cell = tblDashboard.dequeueReusableCell(withIdentifier: "DashNewArrivalTableViewCell") as! DashNewArrivalTableViewCell
                
                cell.collectionV.dataSource = cell
                cell.collectionV.delegate = cell
                cell.collectionV.width = cell.width
                cell.arrNevArrival = newArrivalArray
                cell.collectionV.reloadData()
                
                return cell
            }
            else if  indexPath.section == 3{
                let cell = tblDashboard.dequeueReusableCell(withIdentifier: "DashRecipeTableViewCell") as! DashRecipeTableViewCell
                
                cell.collectionV.dataSource = cell
                cell.collectionV.delegate = cell
                cell.collectionV.width = cell.width
                cell.arrRecipe = arrRecipe
                cell.collectionV.reloadData()
                
                return cell
            }
            else if  indexPath.section == 4{
                let cell = tblDashboard.dequeueReusableCell(withIdentifier: "DashCollectionTCell") as! DashCollectionTCell
                cell.collectionV.dataSource = cell
                cell.collectionV.delegate = cell
                cell.collectionV.width = cell.width
                cell.arrProduct = arrProduct
                cell.arrRecipe = arrRecipe
                cell.arrCollectionList = arrCollection
                cell.collectionV.reloadData()
                return cell
            }
        case tblSearch:
            if indexPath.section == 0 {
                let cell = tblSearch.dequeueReusableCell(withIdentifier: "DashboardSearchTVC") as! DashboardSearchTVC
                cell.lblTitleName.textColor = UIColor.black
                if arrLocalCategory.count > 0 {
                    let catName = arrLocalCategory[indexPath.row]
                    cell.lblTitleName.text =  "\(catName)\nIn Category"
                    cell.lblTitleName.textColor = UIColor().getThemeColor()
                }
                
                return cell
            }
            else {
                let cell = tblSearch.dequeueReusableCell(withIdentifier: "DashboardSearchTVC") as! DashboardSearchTVC
                
                cell.lblTitleName.textColor = UIColor.black
                if arrlocalProduct.count > 0 {
                    cell.lblTitleName.text = arrlocalProduct[indexPath.row].productDescription ?? ""
                }
                
                return cell
            }
            
        default:
            
            let cell = tblDashboard.dequeueReusableCell(withIdentifier: "DashCollectionTCell") as! DashCollectionTCell
            return cell
        }
        
        return UITableViewCell.init()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        // return 200
        switch tableView {
        case tblDashboard:
            switch indexPath.section {
            case 0:
                return 200
            case 1:
                return 195
            case 2:
                return 260
            case 3:
                return 250
            case 4:
                return 203
            default:
                return UITableView.automaticDimension
            }
        case tblSearch:
            return UITableView.automaticDimension
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        switch tableView {
        case tblDashboard:
            break
        case tblSearch:
            view.endEditing(true)
            if indexPath.section == 0 {
                
                let controller:CategoryListingViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                controller.shouldDisplayHome = false
                controller.selectedCatIdName = arrLocalCategory[indexPath.row]
                controller.selectedCatName = arrLocalCategory[indexPath.row]
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else{
                let controller:ProjectDetailVC =  UIStoryboard(storyboard: .DashBoard).initVC()
                controller.arrProductDetail = arrlocalProduct[indexPath.row]
                controller.productId = arrlocalProduct[indexPath.row].productId!
                controller.homeToSearch = "Yes"
              //  controller.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(controller, animated: true)
     
            }
           
        default:
            break
        }
        
    }
    
    //MARK:- Sell Button Tap
    @objc func seeAllButtonPressed(button:UIButton){
        
        
        switch button.tag {
        case 4:
            let controller:CollectionProductViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            controller.arrCollection = arrCollection
            self.navigationController?.pushViewController(controller, animated: true)
        default:
            break
        }
        switch button.tag {
        case 2:
            let controller:NewArrivalsViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            controller.titleString = Appmessage_NewArrivals.Placeholder.NewArrivals
            controller.subtitleString = Appmessage_NewArrivals.Placeholder.NewArrivalsProducts
            let arrProductArray = newArrivalArray.filter {$0.salesPrice != 0}
            controller.arrProduct = arrProductArray
            
            controller.shouldDisplayHome = false
            self.navigationController?.pushViewController(controller, animated: true)
        case 1:
            
            var tempArrCategory = [CategoryListingModel]()
            for item in arrCategory {
                if item.categoryId != nil {
                    tempArrCategory.append(item)
                }
            }
            
            let controller:CategoryListVC =  UIStoryboard(storyboard: .DashBoard).initVC()
            controller.arrCategory = tempArrCategory
            self.navigationController?.pushViewController(controller, animated: true)
            
        case 3:
            let controller:RecipeListViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            self.navigationController?.pushViewController(controller, animated: true)
        default:
            break
        }
    }
        
}

/*
 class DashBoardCollectionViewDataSource:GenericDataSource<MockUp_Dashboard>,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
 
 func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
 return self.data.value.count
 }
 func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 
 if let cell: DashBoardCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashBoardCollectionViewCell", for: indexPath) as? DashBoardCollectionViewCell
 {
 let categoryList = self.data.value[indexPath.row]
 cell.lblTitle.text = categoryList.title
 cell.imgView.image = categoryList.image
 //cell.imageView.image =  self.imageArray[indexPath.row] as? UIImage
 return cell
 }
 return UICollectionViewCell()
 }
 func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
 let size = CGSize(width: (UIScreen.main.bounds.size.width/2)-10/2, height: (UIScreen.main.bounds.size.width/2) - 10/2)
 return size
 // return UICollectionViewFlowLayout.automaticSize
 }
 }
 */
