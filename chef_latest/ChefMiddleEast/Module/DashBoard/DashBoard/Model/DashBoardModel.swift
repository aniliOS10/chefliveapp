//
//  DashBoardModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 17/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class DashBoardModel: BaseResponse {
    
    var featureBannerList = [DashBoardBannerModel]()
    var recipesList = [RecipeListModel]()
    var collectionList = [CollectionListModel]()
    var newArrivalList = [NewArrivalsModel]()
    var categoriesList = [CategoryListingModel]()
    var categoriesFeatureBannerList = [CategoryFeatureBannerImages]()
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        featureBannerList <- map["featureBannerList"]
        recipesList <- map["recipesList"]
        collectionList <- map["collectionList"]
        newArrivalList <- map["newArrivalList"]
        categoriesList <- map["categoriesList"]
    }
}
