//
//  DashBoardBannerModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 17/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class DashBoardBannerModel: BaseResponse {
    
    var featureBannerId : String?
    var featureBannerImage : String?
    var isModifiedAndroid : Bool?
    var isModifiedIos : Bool?
    var recipeId : String?
    var productId : String?
    
    override init() {
         super.init()
    }
    convenience required init?(map: Map) {
        self.init()
    }
    override func mapping(map: Map) {
        featureBannerId <- map["featureBannerId"]
        featureBannerImage <- map["featureBannerImage"]
        isModifiedAndroid <- map["isModifiedAndroid"]
        isModifiedIos <- map["isModifiedIos"]
        recipeId <- map["recipeId"]
        productId <- map["productId"]
    }
}


