//
//  ItemAddedToCartOptionVC.swift
//  ChefMiddleEast
//
//  Created by Apple on 21/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class ItemAddedToCartOptionVC: UIViewController {

    @IBOutlet weak var baseView : UIView!
    @IBOutlet weak var btnGoToCart : UIButton!
    @IBOutlet weak var btnShopping : UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnGoToCart.addShadow()
        btnGoToCart.layer.cornerRadius = 22.5
        btnShopping.addShadow()
        btnShopping.layer.cornerRadius = 22.5
        round(corners: [.topLeft, .topRight], cornerRadius: 20)
    }
    
    func round(corners: UIRectCorner, cornerRadius: Double) {
            
            let size = CGSize(width: cornerRadius, height: cornerRadius)
            let bezierPath = UIBezierPath(roundedRect: baseView.bounds, byRoundingCorners: corners, cornerRadii: size)
            let shapeLayer = CAShapeLayer()
            shapeLayer.frame = baseView.bounds
            shapeLayer.path = bezierPath.cgPath
            baseView.layer.mask = shapeLayer
    }
    
    
    @IBAction func onClickContinueShoppingAcn() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickGoToCartAcn() {
        weak var pvc = self.presentingViewController
        self.dismiss(animated: true, completion: {
            
            let controller:CartSummaryViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            controller.screenCommingFrom = "ProductDetails"
            pvc?.navigationController!.pushViewController(controller, animated: true)

        })
    }
}
