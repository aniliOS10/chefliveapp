//
//  DashBoardViewController.swift
//  PetCare
//
//  Created by rupinder singh on 04/10/21.
//

import UIKit
import SideMenuSwift
import ObjectMapper
import SDWebImage

class DashBoardViewController: BaseViewController, UITextFieldDelegate {
    
    var dictAppInfo = AppInfoModel()
    @IBOutlet weak var vwWhatsApp : UIView!
    @IBOutlet weak var vwAnnouncement : UIView!
    @IBOutlet weak var lblAnnouncement : UILabel!
    @IBOutlet weak var vwAnnouncementHeight : NSLayoutConstraint!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!

    @IBOutlet weak var btnWhatsApp : UIButton!
    @IBOutlet weak var tblDashboard : UITableView!
    @IBOutlet weak var search : UISearchBar!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet weak var tblSearch : UITableView!
    @IBOutlet weak var vwSearch : UIView!
    @IBOutlet weak var txtSearch : UITextField!
    @IBOutlet weak var vwBoxSearch : UIView!
    var isSearchOn : Bool = false
    
    var dictDashBoard = DashBoardModel()
    var newArrivalArray = [NewArrivalsModel]()
    var arrProduct = [NewArrivalsModel]()
    var arrlocalProduct = [NewArrivalsModel]()
    var arrCategoryNames = [String]()
    var searchText = String()
    var arrLocalCategory = [String]()
    var arrRecipe = [RecipeListModel]()
    var arrCategory = [CategoryListingModel]()
    var arrSortedCategory = [CategoryListingModel]()
    var arrCollection = [CollectionListModel]()
    @IBOutlet weak var vwBaseSearch : UIView!
    
    lazy var dispatchGroup: DispatchGroup = {
            let dispatchGroup = DispatchGroup()
            return dispatchGroup
        }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    //MARK:- VC Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        
        self.vwBaseSearch.isHidden = true
        self.vwSearch.isHidden = true
        self.txtSearch.text = ""
        self.txtSearch.resignFirstResponder()

        txtSearch.delegate = self
        txtSearch.autocorrectionType = .no
        txtSearch.spellCheckingType = .no
        self.tblSearch.keyboardDismissMode = .onDrag

        self.tabBarController?.tabBar.isHidden = false
       
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            EndPoint.deviceType = "ios"
            self.cartCountAPI()
        }
        
        self.arrProduct = fetchAllProductLocalStorge()
        if self.arrProduct.count == 0 {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.setParamForAllProductsListApi()
        }
        
        self.groupAllDashboardApis()
        self.setParamOfCollectionListApi()
        self.setParamForRecipeListApi()
        self.getAppInfoAPI()
        
        if self.arrSortedCategory.count == 0 {
            self.categoryListAPI()
        }
        
        self.setNavigationButton()
        vwWhatsApp.layer.cornerRadius = 25.0
        
        self.arrProduct = fetchAllProductLocalStorge()
        
        let arrCategory = arrProduct.unique{$0.categoryId}.map({ $0.categoryId ?? "" })
        var tempArrCategory = [String]()
        for item in arrCategory {
            if  item != ""{
                tempArrCategory.append(item)
            }
        }
        self.arrCategoryNames = tempArrCategory
        self.arrCategoryNames = self.arrCategoryNames.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
                
        let filterNewArray = self.arrProduct.filter{$0.salesPrice != 0 && $0.availableQuantity ?? 0 > 0 && $0.productDescription != "" && $0.productDescription != nil && $0.productImage != nil && $0.isProductActive == true}
        
        if filterNewArray.count > 0  {
            self.newArrivalArray.removeAll()
            if filterNewArray.count > 10 {
                for index in 0..<10 {
                    self.newArrivalArray.append(filterNewArray[index])
                }
            }else {
                for index in 0..<filterNewArray.count {
                    self.newArrivalArray.append(filterNewArray[index])
                }
            }
        }
        
        SDImageCache.shared.clearMemory()
        SDImageCache.shared.clearDisk()
        self.tblDashboard.reloadData()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        let statusBarUpdate =  UIView()
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let rect = CGRect(x: 0, y: 0, width:  window?.size.width ?? 300, height:44)
        let height = window?.windowScene?.statusBarManager?.statusBarFrame ?? rect
        statusBarUpdate.frame = height
        statusBarUpdate.backgroundColor = UIColor (red: 158.0/255.0, green: 11.0/255.0, blue: 42/255.0, alpha: 1.0)
        window?.addSubview(statusBarUpdate)
    
        self.setUpTabelView()
        self.setFilter()
        self.categoryListAPI()
        
        vwBoxSearch.addShadow()
        vwBoxSearch.layer.cornerRadius = 25.0
        
        if let controller  = self.tabBarController as? DashTabBarViewController{
            DashTabBarViewController.sharedTabBar =  controller
            
        }
    
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        vwBaseSearch.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        vwSearch.isHidden = true
        vwBaseSearch.isHidden = true
        
    }
    
    //MARK:- Fetch All Product
    func fetchAllProductLocalStorge() -> [NewArrivalsModel] {
        if let syncContactsData = UserDefaults.standard.value(forKey: Constants.ProductModel) as? Data {
            if let aryProduct:[[String:Any]] = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(syncContactsData) as? [[String : Any]] {
                return Mapper<NewArrivalsModel>().mapArray(JSONArray: aryProduct)
            }
        }
        return []
    }
    
    func setParamForRecipeListApi() {
        let params = CollectionListModel()
        params.pageNumber = 1
        params.pageSize = 10
        EndPoint.searchText = ""
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            params.userId = userId
        }
        self.recipeListAPI(Model: params)
    }
    
    func setParamOfCollectionListApi() {
        let params = CollectionListModel()
        params.pageNumber = 1
        params.pageSize = 10
        params.collectionType = "Feature"
        self.collectionListAPI(Model: params)
    }
    
    func collectionListAPI(Model: CollectionListModel){
        EndPoint.CollectionList(params: Model, t: CollectionListModel.self) { result in
            switch result {
            case .onSuccess(let items):
                if items.count > 0 {
                    self.arrCollection = items
                    self.tblDashboard.reloadData()
                }
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    // MARK: - Category List API
    func categoryListAPI(){
        self.arrSortedCategory.removeAll()
        EndPoint.GetAllCategoryList(t: CategoryListingModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccess(let items):
                if items.count > 0 {
                    self.arrSortedCategory.removeAll()
                    var tempArrCategory = [CategoryListingModel]()
                    for item in items {
                        if item.categoryId != nil {
                            tempArrCategory.append(item)
                        }
                    }
                    
                    self.arrSortedCategory = tempArrCategory.sorted(by: { $0.categoryId! < $1.categoryId! })
                    self.arrSortedCategory = self.arrSortedCategory.unique{$0.categoryId}
                    
                    DispatchQueue.main.async {
                        self.tblDashboard.reloadData()
                    }
                }
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    // MARK: - Recipe List API
    func recipeListAPI(Model: CollectionListModel){
       // self.showLoader()
       
        EndPoint.RecipeList(params: Model, t: RecipeListModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccess(let items):
                if items.count > 0 {
                    print(items.toJSON())
                    self.arrRecipe = items
                    self.tblDashboard.reloadData()
                }
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    @IBAction func onClickWhatsAppAcn(){
        if dictAppInfo.phone != nil || dictAppInfo.phone != "" {
            
            let phone = dictAppInfo.phone
            let whatsAppString = String(format: "https://api.whatsapp.com/send?phone=%@&text=Hello", phone ?? "")
            let whatsappURL = URL(string: whatsAppString)
            if UIApplication.shared.canOpenURL(whatsappURL!) {
                    UIApplication.shared.openURL(whatsappURL!)
            }
        }
    }
    
    func setupAnnouncementView() {
        if dictAppInfo.announcement != nil || dictAppInfo.announcement != "" {
            let title = dictAppInfo.announcement ?? ""
            if title == "" {
                vwAnnouncement.isHidden = true
                vwAnnouncementHeight.constant = 0
            } else {
                lblAnnouncement.text = String(format: "%@     ", title)
                vwAnnouncement.isHidden = false
                vwAnnouncementHeight.constant = 40

            }
        } else {
            vwAnnouncement.isHidden = true
            vwAnnouncementHeight.constant = 0
        }
        
        if EndPoint.fcmToken != "" {
            updateFCMTokenOnServer()
        }
        
       
    }
    
    func getAppInfoAPI() {
        EndPoint.GetAppInfo(t: AppInfoModel.self) { result in
               switch result {
               case .onSuccess(let data):
                   if data != nil {
                       self.dictAppInfo = data!
                       self.setupAnnouncementView()
                   }
                   
               case .onFailure(let error):
                   print(error)
                   if error != "The request timed out."{
                       NotificationAlert().NotificationAlert(titles: error)
                   }
                   print(error)
               }
           }
       }
    
    func updateFCMTokenOnServer(){
        EndPoint.UpdateFCMToken(t: NotificationsModel.self) { result in
            switch result {
            case .onSuccessWithStringValue(let strData):
                print(strData)
                break
            case .onFailure(let error):
                print(error)
            }
        }
    }
    
    // MARK: - GetArtistProfileAndUpcomingEvents
        func groupAllDashboardApis() {
            self.showLoader()
            dispatchGroup.enter()
            CallGetDashBoardAPIApi()
            dispatchGroup.notify(queue: .main) {
                self.hideLoader()
                self.tblDashboard.reloadData()
            }
        }
    
    //MARK:- Call Dasboard API
    func CallGetDashBoardAPIApi() {
        
        self.getDashBoardAPI(){ sucess in
            if sucess {
                self.dispatchGroup.leave()
            }
        }
    }
    
    //MARK:- Set Filter
    func setFilter(){
        
    }
    
    //MARK:- Set Tableview
    func setUpTabelView() {
        
        self.tblDashboard.delegate = self
        self.tblDashboard.dataSource = self
        self.tblSearch.delegate = self
        self.tblSearch.dataSource = self
        
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tblDashboard.tableHeaderView = UIView(frame: frame)
        tblDashboard.tableFooterView = UIView(frame: frame)

        if #available(iOS 11.0, *) {
            tblDashboard.contentInsetAdjustmentBehavior = .never
            if #available(iOS 15.0, *) {
                tblDashboard.sectionHeaderTopPadding = 0.0
            }
        }
    }
    
    //MARK:- Set Search
    func setUpSearch() {
        
        search.searchBarStyle = .minimal
        search.backgroundColor = UIColor.white
        search.tintColor = UIColor.white
        search.delegate = self
        
        if #available(iOS 13.0, *) {
            search.searchTextField.backgroundColor = UIColor.white
            search.searchTextField.textColor =  UIColor.black
            search.searchTextField.tintColor =  UIColor().getThemeColor()
            
            Utility.shared.makeShadowsOfView_blow_roundCorner(view: search, shadowRadius: 4.0, shadowOffset: CGSize(width: 3.0, height: -3.0), cornerRadius: 15, borderColor: UIColor.white.withAlphaComponent(0.5))
            
            
        }
        else {
            // Fallback on earlier versions
        }
        
        
        search.compatibleSearchTextField.textColor = UIColor.white
        search.compatibleSearchTextField.backgroundColor = UIColor.white
        
        
    }
    
    //MARK:- Set Navigation
    func setNavigationButton() {
        self.initMenuButton()
        menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [], title: Appmessage_Dashboard.Placeholder.Home)
        } else {
            self.initBellButton()
            bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
            self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [bellBarButton], title: Appmessage_Dashboard.Placeholder.Home)
        }
       // UIColor().getThemeColor()
    }
    
    //MARK:- Menu Button
    @objc func menuButtonPressed(){
        self.sideMenuController?.revealMenu()
    }
    
    //MARK:- Notification Button
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @IBAction func filterButtonPressed(_ sender: Any) {
       
        /*let controller:SortAndFilterViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.present(controller, animated: true, completion: nil)*/
    }
    
}

//MARK:- SearchBar Extension
extension DashBoardViewController: UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)  {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText \(searchText)")
        

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        if updatedString!.length > 0 {
            self.isSearchOn = true
            self.searchText = updatedString!
            NSObject.cancelPreviousPerformRequests( withTarget: self, selector: #selector(searchDasboardProducts), object: nil)
            self.perform(#selector(searchDasboardProducts), with: nil, afterDelay: 0.5)
            
        } else {
            self.searchText = ""
            self.isSearchOn = false
            self.vwSearch.isHidden = true
            self.vwBaseSearch.isHidden = true
        }
        return true
    }
    
    
    //MARK:- search Dasboard Products
    @objc private func searchDasboardProducts() {
        
        let arrCategory = arrProduct.unique{$0.categoryId}.map({ $0.categoryId ?? "" })
        
        var tempArrCategory = [String]()
        for item in arrCategory {
            if  item != ""{
                tempArrCategory.append(item)
            }
        }
        
        self.vwSearch.layer.cornerRadius = 15
        self.vwSearch.addShadow()
        let arrLocalProduct = self.arrProduct
        self.arrLocalCategory  = tempArrCategory.filter { $0.localizedCaseInsensitiveContains(searchText) }
        self.arrlocalProduct = (arrLocalProduct.filter({(($0.productDescription ?? "").localizedCaseInsensitiveContains(searchText))}))
        
        if self.arrLocalCategory.count > 0 ||  self.arrlocalProduct.count > 0 {
            if searchText.length > 0 {
            self.vwSearch.isHidden = false
            self.vwBaseSearch.isHidden = false
                if self.arrLocalCategory.count > 4 ||  self.arrlocalProduct.count > 4 {
                      searchViewHeight.constant = CGFloat(444)
                    }
                    else{
                        if self.arrLocalCategory.count > self.arrlocalProduct.count {
                            searchViewHeight.constant = CGFloat(self.arrLocalCategory.count * 75)
                        }
                        if self.arrlocalProduct.count > self.arrLocalCategory.count {
                            searchViewHeight.constant = CGFloat(self.arrlocalProduct.count * 75)
                        }
                    }
            }
            else{
                self.vwSearch.isHidden = true
                self.vwBaseSearch.isHidden = true
            }
        }
        else{
            self.vwSearch.isHidden = true
            self.vwBaseSearch.isHidden = true
        }
        tblSearch.reloadData()
    }
    
}


extension UISearchBar {

    // Due to searchTextField property who available iOS 13 only, extend this property for iOS 13 previous version compatibility
    var compatibleSearchTextField: UITextField {
        guard #available(iOS 13.0, *) else { return legacySearchField }
        return self.searchTextField
    }

    private var legacySearchField: UITextField {
        if let textField = self.subviews.first?.subviews.last as? UITextField {
            // Xcode 11 previous environment
            return textField
        } else if let textField = self.value(forKey: "searchField") as? UITextField {
            // Xcode 11 run in iOS 13 previous devices
            return textField
        } else {
            // exception condition or error handler in here
            return UITextField()
        }
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

