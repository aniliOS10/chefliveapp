//
//  SideCartListTVC.swift
//  ChefMiddleEast
//
//  Created by Apple on 24/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class SideCartListTVC: UITableViewCell {

    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var lblCount : UILabel!
    @IBOutlet weak var baseView : UIView!
    @IBOutlet var lblQty: UILabel!
    @IBOutlet var vwSelectQty: UIView!
    @IBOutlet weak var lblCountTotal : UILabel!
    
    @IBOutlet var btnRemoveProduct: UIButton!
    @IBOutlet var btnAddProduct: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
