//
//  SeaFoodCollectionViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 06/11/21.
//

import UIKit

class SeaFoodCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var increaseButton: UIButton!
    @IBOutlet weak var decreaseButton: UIButton!
}
