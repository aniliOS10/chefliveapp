//
//  SeaFoodRowTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 06/11/21.
//

import UIKit

@available(iOS 13.0, *)
class SeaFoodRowTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout   {
    @IBOutlet weak var collectionV: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    
    
    
    var tagRow:Int = 0
    var tagSection:Int = 0
  
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
    return 5
}
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
    let cell: SeaFoodCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeaFoodCollectionViewCell", for: indexPath) as! SeaFoodCollectionViewCell
    Utility.shared.makeRoundCorner(layer: cell.cellViewRound.layer, color: UIColor.lightGray.withAlphaComponent(0.4), radius: 5.0)
    
   // Utility.shared.makeRoundCorner(layer:  cell.imageV.layer, color: UIColor.lightGray, radius: 12)
        //let categoryList = self.data.value[indexPath.row]
         let width = (UIScreen.main.bounds.size.width/3)-10
       // cell.imageViewHeight.constant = width
   // cell.imageV.backgroundColor = UIColor.red
        if indexPath.row%2 == 0{
           // cell.imageV.backgroundColor = UIColor.red
            cell.imageV.image = UIImage.init(named: "Category1")
            //cell.imageV.backgroundColor = UIColor.red
          
            
        }else{
          
            cell.imageV.image = UIImage.init(named: "Category1")
         
        }
     cell.titleLabel.text = "Bakery"
     cell.desLabel.text = "AED 102.2"
     
        //cell.imageView.image =  self.imageArray[indexPath.row] as? UIImage
    return cell
}
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
    
   let width = (collectionView.bounds.size.width/2)
    let size = CGSize(width: width, height: self.collectionHeight.constant)
   
    return size
    
   
   // return UICollectionViewFlowLayout.automaticSize
}


}
