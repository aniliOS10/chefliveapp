//
//  SeaFoodCartTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 06/11/21.
//

import UIKit

class SeaFoodCartTableViewCell: UITableViewCell {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
