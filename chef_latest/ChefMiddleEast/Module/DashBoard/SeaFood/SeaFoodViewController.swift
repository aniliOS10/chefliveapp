//
//  SeaFoodViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import UIKit

@available(iOS 13.0, *)
class SeaFoodViewController:BaseViewController {
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var carttableView : UITableView!
    @IBOutlet weak var search : UISearchBar!
    
    @IBOutlet weak var buttonPayment: UIButton!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var backGroundView2: UIView!
    
   private let dataSource:SeaFoodDataSource?
    private let cartdataSource:SeaFoodCartDataSource?
    var titleString:String = AppMessage_SeaFood.Placeholder.Seafood
    var subtitleString:String = AppMessage_SeaFood.Placeholder.Product
    
    
    
    lazy var viewModel:SeaFoodViewModel = {
        let viewModel = SeaFoodViewModel(dataSource: self.dataSource,cartdataSource:self.cartdataSource)
        return viewModel
    }()
    init(dashBoardViewDataSource:SeaFoodDataSource,seaFoodCartDataSource:SeaFoodCartDataSource)
    {
        self.dataSource = dashBoardViewDataSource
        self.cartdataSource = seaFoodCartDataSource
        super.init(nibName: nil, bundle: nil)
    }
  
    convenience init(dataSourceCollectionView1:SeaFoodDataSource,seaFoodCartDataSource:SeaFoodCartDataSource) {
        self.init(dashBoardViewDataSource: dataSourceCollectionView1,seaFoodCartDataSource:seaFoodCartDataSource)
    }
    required init?(coder: NSCoder) {
        self.dataSource = SeaFoodDataSource()
        self.cartdataSource = SeaFoodCartDataSource()
         super.init(coder: coder)
     }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        search.searchBarStyle = .minimal
        search.backgroundColor = UIColor.white
        search.tintColor = UIColor.white
      
      if #available(iOS 13.0, *) {
          search.searchTextField.backgroundColor = UIColor().getSearchColor()
          search.searchTextField.textColor =  UIColor().getThemeColor()
          search.searchTextField.tintColor =  UIColor().getThemeColor()
          Utility.shared.makeRoundCorner(layer: search.searchTextField.layer, color:UIColor.white.withAlphaComponent(0.5), radius: 5)
       //   Utility.shared.makeShadowsOfView_roundCorner(view: search, shadowRadius: 4.0, cornerRadius: 30, borderWidth:0.0, borderColor: UIColor.lightGray)
          let white = UIColor.red
          search.searchTextField.background = UIImage.init(named: "imageV1") //white.imageWithColor(width: Int(self.view.bounds.width), height: Int(self.search.searchTextField.frame.height))
          
          
         
    } else {
          // Fallback on earlier versions
      }
        
        
        Utility.shared.makeShadowsOfView_roundCorner(view: backGroundView, shadowRadius: 4.0, cornerRadius: 30, borderColor: UIColor.lightGray.withAlphaComponent(0.5))
        Utility.shared.makeShadowsOfView_roundCorner(view: backGroundView2, shadowRadius: 4.0, cornerRadius: 30, borderColor: UIColor.lightGray.withAlphaComponent(0.5))
        
        self.dataSource?.subtitleString = self.subtitleString
        self.tableView.delegate = self.dataSource
        self.tableView.dataSource = self.dataSource
        
        
    
        self.carttableView.delegate = self.cartdataSource
        self.carttableView.dataSource = self.cartdataSource
        self.viewModel.dataSource?.data.addAndNotify(observer: self, completionHandler: { mockUp_Dashboard in
            self.tableView.reloadData()
        })
       
        
        self.viewModel.cartdataSource?.data.addAndNotify(observer: self, completionHandler: { mockUp_Dashboard in
            self.tableView.reloadData()
        })
        
        
        
        
        self.viewModel.makeDashboardList()
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
                            
        menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
        
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: titleString)
        }else {
            self.initBellButton()
            bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
            self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [bellBarButton], title: titleString)
        }
        
        
        
        
        
      
        // Do any additional setup after loading the view.
    }
    @objc func menuButtonPressed(){
        self.sideMenuController?.revealMenu()
    }
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
    
    @IBAction func buttonSaveAndMakePaymentPressed(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
