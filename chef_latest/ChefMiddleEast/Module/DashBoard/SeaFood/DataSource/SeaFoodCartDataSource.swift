//
//  SeaFoodCartDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 06/11/21.
//

import Foundation
import UIKit
class SeaFoodCartDataSource:GenericDataSource<MockUp_Seafood>,UITableViewDataSource,UITableViewDelegate{
   
    var subtitleString:String = AppMessage_SeaFood.Placeholder.Cart
    var didSelectRow: ((_ cell:UITableViewCell, _ indexPath:IndexPath) -> Void)?
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SeaFoodSectionTableViewCell") as! SeaFoodSectionTableViewCell
            cell.titleLabel.text = subtitleString
            return cell
        }
       
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 40.0 :0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "SeaFoodCartTableViewCell") as! SeaFoodCartTableViewCell
        cell.imageV.image = UIImage.init(named: "Category1")
          cell.titleLabel.text = "Bakery"
        return cell
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       // return 200
        return UITableView.automaticDimension
    }
    
}

/*
class DashBoardCollectionViewDataSource:GenericDataSource<MockUp_Dashboard>,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
    return self.data.value.count
}
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
    if let cell: DashBoardCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashBoardCollectionViewCell", for: indexPath) as? DashBoardCollectionViewCell
    {
        let categoryList = self.data.value[indexPath.row]
        cell.lblTitle.text = categoryList.title
        cell.imgView.image = categoryList.image
        //cell.imageView.image =  self.imageArray[indexPath.row] as? UIImage
    return cell
}
        return UICollectionViewCell()
}
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
    let size = CGSize(width: (UIScreen.main.bounds.size.width/2)-10/2, height: (UIScreen.main.bounds.size.width/2) - 10/2)
    return size
   // return UICollectionViewFlowLayout.automaticSize
}
}
*/
