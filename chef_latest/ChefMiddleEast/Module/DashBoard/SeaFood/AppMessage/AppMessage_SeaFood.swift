//
//  AppMessage_SeaFood.swift
//  ChefMiddleEast
//
//  Created by Apple on 06/11/21.
//

import Foundation
class AppMessage_SeaFood: NSObject {
    struct Placeholder {
        static let Product = NSLocalizedString("Product", comment: "")
        static let Cart = NSLocalizedString("Cart", comment: "")
        static let Seafood = NSLocalizedString("Seafood", comment: "")
    }
}
