//
//  CollectionListModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 13/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class CollectionListModel: BaseResponse {
    
    var collectionId: Int?
    var collectionImage: String?
    var collectionType: String?
    var userId: String?
    var pageNumber: Int?
    var pageSize: Int?
    var collectionTitle: String?
    var collectionName: String?
    var productId: String?
    var recipeId: String?
    
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        collectionId <- map["collectionId"]
        collectionImage <- map["collectionImage"]
        userId <- map["userId"]
        collectionType <- map["collectionType"]
        pageNumber <- map["pageNumber"]
        pageSize <- map["pageSize"]
        collectionTitle <- map["collectionTitle"]
        collectionName <- map["collectionName"]
        productId <- map["productId"]
        recipeId <- map["recipeId"]
    }
}


class SettingModel: BaseResponse {
    
    var notificationStatus: Bool?
    var userId: String?
    var subscriptionStatus: Bool?
   
    
    
    override init() {
         super.init()
     }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        notificationStatus <- map["notificationStatus"]
        subscriptionStatus <- map["subscriptionStatus"]
        userId <- map["userId"]
     
    }
}
