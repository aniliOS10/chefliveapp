//
//  DashBoardCollectionViewDataSource.swift
//  PetCare
//
//  Created by rupinder singh on 04/10/21.
//

import UIKit
@available(iOS 13.0, *)
extension CollectionProductViewController: UICollectionViewDelegateFlowLayout,UICollectionViewDelegate, UICollectionViewDataSource{
   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell: CollectionListCVCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionListCVCell", for: indexPath) as! CollectionListCVCell
        
        cell.cellViewRound.addShadowFull()
        cell.cellViewRound.layer.cornerRadius = 6
        cell.imgCollection.layer.cornerRadius = 6
        
        if let imgUrl = arrCollection[indexPath.row].collectionImage,!imgUrl.isEmpty {
            
            let imagePath = "\(EndPoint.BASE_API_IMAGE_URL)\(imgUrl)"
            let urlString = imagePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

            
            cell.imgCollection?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                if (error != nil) {
                    cell.imgCollection.image = UIImage(named: "placeholder")
                } else {
                    cell.imgCollection.image = image
                }
            }
        }
        cell.lblTitle.text = arrCollection[indexPath.row].collectionName
        return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let width = (UIScreen.main.bounds.size.width/2) - 16
       
        let size = CGSize(width: width, height: 200 )
        return size
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let productID = arrCollection[indexPath.row].productId, !productID.isEmpty {
            print(productID)
           
            let arrProductList = productID.components(separatedBy: ",")
            
            let controller:NewArrivalsViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            if arrProductList.count > 0 {
                let arrFiltered = arrProduct.filter( { arrProductList.contains(String("\($0.productId!)") ) == true} )
                controller.arrProduct = arrFiltered
            }
            controller.screenCommingFrom = "SubCategory"
            controller.shouldDisplayHome = false
            controller.arrSelectedProduct = arrProductList
            controller.headerTitle = arrCollection[indexPath.row].collectionName!
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    
    /*
    var didSelectRow: ((_ cell:UITableViewCell, _ indexPath:IndexPath) -> Void)?
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.0 :0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionProductTableViewCell") as! CollectionProductTableViewCell
        cell.tagRow = indexPath.row
        cell.tagSection = indexPath.section
       
        if indexPath.section == 0{
           
            let width = (UIScreen.main.bounds.size.width)-50
            cell.collectionHeight.constant = width
            
           
        }else if indexPath.section == 1{
            let width = (UIScreen.main.bounds.size.width/2)-5
            cell.collectionHeight.constant = width
        }else{
            let width = (UIScreen.main.bounds.size.width/2)
            cell.collectionHeight.constant = width
        }
        
        cell.collectionV.dataSource = cell
        cell.collectionV.delegate = cell
        cell.collectionV.reloadData()
    
        return cell
        
   }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       // return 200
        return UITableView.automaticDimension
    }
     */
    
}

/*
class DashBoardCollectionViewDataSource:GenericDataSource<MockUp_Dashboard>,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
    return self.data.value.count
}
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
    if let cell: DashBoardCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashBoardCollectionViewCell", for: indexPath) as? DashBoardCollectionViewCell
    {
        let categoryList = self.data.value[indexPath.row]
        cell.lblTitle.text = categoryList.title
        cell.imgView.image = categoryList.image
        //cell.imageView.image =  self.imageArray[indexPath.row] as? UIImage
    return cell
}
        return UICollectionViewCell()
}
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
    let size = CGSize(width: (UIScreen.main.bounds.size.width/2)-10/2, height: (UIScreen.main.bounds.size.width/2) - 10/2)
    return size
   // return UICollectionViewFlowLayout.automaticSize
}
}
*/
