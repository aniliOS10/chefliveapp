//
//  CollectionProductCollectionViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 31/10/21.
//

import UIKit

class CollectionProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var lblCollectionName: UILabel!
}
