//
//  DashRecipeTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 28/10/21.
//

import UIKit
import SDWebImage

@available(iOS 13.0, *)
class CollectionProductTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout   {
    @IBOutlet weak var collectionV: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    
    var tagRow:Int = 0
    var tagSection:Int = 0
    
    var arrData = [CollectionListModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrData.count
       // return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CollectionProductCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionProductCollectionViewCell", for: indexPath) as! CollectionProductCollectionViewCell
        Utility.shared.makeRoundCorner(layer: cell.cellViewRound.layer, color: UIColor.lightGray.withAlphaComponent(0.1), radius: 5.0)
        
        if let imgUrl = arrData[indexPath.row].collectionImage,!imgUrl.isEmpty {
            
            let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            
            
            cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                if (error != nil) {
                    cell.imageV.image = UIImage(named: "placeholder")
                } else {
                    cell.imageV.image = image
                }
            }
        }
        // Utility.shared.makeRoundCorner(layer:  cell.imageV.layer, color: UIColor.lightGray, radius: 12)
        //let categoryList = self.data.value[indexPath.row]
        let width = (UIScreen.main.bounds.size.width/3)-10
        // cell.imageViewHeight.constant = width
        // cell.imageV.backgroundColor = UIColor.red
       /* if indexPath.row%2 == 0{
            // cell.imageV.backgroundColor = UIColor.red
            cell.imageV.image = UIImage.init(named: "Category1")
            //cell.imageV.backgroundColor = UIColor.red
        }
        else{
            cell.imageV.image = UIImage.init(named: "Category1")
        }*/
        //cell.imageView.image =  self.imageArray[indexPath.row] as? UIImage
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        var width = (UIScreen.main.bounds.size.width)-5
        if tagSection == 1{
            width = (UIScreen.main.bounds.size.width/2)-5
        }
        let size = CGSize(width: width, height: self.collectionHeight.constant)
        return size
   }
    
    
}
