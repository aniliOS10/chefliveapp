//
//  CollectionListCVCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 18/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class CollectionListCVCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var imgCollection : UIImageView!
    @IBOutlet weak var cellViewRound : UIView!
    
}
