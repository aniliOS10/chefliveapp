//
//  CollectionProductViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 31/10/21.
//

import UIKit
import ObjectMapper

@available(iOS 13.0, *)
class CollectionProductViewController:BaseViewController {
    
    @IBOutlet weak var listCollectionView : UICollectionView!
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var search : UISearchBar!
    @IBOutlet weak var segmentController: UISegmentedControl!
    
    var arrCollection = [CollectionListModel]()
    var arrProduct = [NewArrivalsModel]()
    

    
    //MARK:- VC Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
        self.setNavigation()
        self.setSegmentControllerUI()
      //  self.setUpSearch()
      //  self.segmentController.selectedSegmentIndex = 2

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        listCollectionView.delegate = self
        listCollectionView.dataSource = self
        listCollectionView.reloadData()
        self.arrProduct = fetchAllProductLocalStorge()
      //  self.viewModel.makeDashboardList()
        self.setUpTableview()
      //  if segmentController.selectedSegmentIndex == 2 {
            setParamOfCollectionListApi()
       // }
    }
    
    //MARK:- Fetch All Product
    func fetchAllProductLocalStorge() -> [NewArrivalsModel] {
        if let syncContactsData = UserDefaults.standard.value(forKey: Constants.ProductModel) as? Data {
            if let aryProduct:[[String:Any]] = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(syncContactsData) as? [[String : Any]] {
                return Mapper<NewArrivalsModel>().mapArray(JSONArray: aryProduct)
            }
        }
        return []
    }
    
    //MARK:- Param
    func setParamOfCollectionListApi() {
        
        let params = CollectionListModel()
        params.pageNumber = 1
        params.pageSize = 10
        params.collectionType = "Feature"
        self.collectionListAPI(Model: params)
    }
    
   
    
    
    //MARK:- Set Search
    func setUpSearch() {
        
        search.searchBarStyle = .minimal
        search.backgroundColor = UIColor.white
        search.tintColor = UIColor.white
        if #available(iOS 13.0, *) {
            search.searchTextField.backgroundColor = UIColor().getSearchColor()
            search.searchTextField.textColor =  UIColor().getThemeColor()
            search.searchTextField.tintColor =  UIColor().getThemeColor()
            Utility.shared.makeRoundCorner(layer: search.searchTextField.layer, color:UIColor.white.withAlphaComponent(0.5), radius: 15)
            //   Utility.shared.makeShadowsOfView_roundCorner(view: search, shadowRadius: 4.0, cornerRadius: 30, borderWidth:0.0, borderColor: UIColor.lightGray)
            let white = UIColor.red
            search.searchTextField.background = UIImage.init(named: "imageV1") //white.imageWithColor(width: Int(self.view.bounds.width), height: Int(self.search.searchTextField.frame.height))
        }
        else {
            // Fallback on earlier versions
        }
       /* self.viewModel.dataSource?.data.addAndNotify(observer: self, completionHandler: { mockUp_Dashboard in
            self.tableView.reloadData()
        })*/
    }
    
    //MARK:- Set Tableview
    func setUpTableview() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    //MARK:- Set Navigation Button
    func setNavigation() {
        
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: Appmessage_CollectionProduct.Placeholder.Collection)
        } else {
            self.initBellButton()
            bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
            self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [bellBarButton], title: Appmessage_CollectionProduct.Placeholder.Collection)
        }
    }
    
    //MARK:- Menu Button
    @objc func menuButtonPressed(){
        
        self.sideMenuController?.revealMenu()
    }
    
    //MARK:- Notification Button
    @objc func bellButtonPressed(){
        
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
    
    //MARK:- Set Segment Controller UI
    func setSegmentControllerUI(){
        
        if #available(iOS 13.0, *) {
            let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)]
            segmentController.setTitleTextAttributes(titleTextAttributes, for:.normal)
            let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)]
            segmentController.setTitleTextAttributes(titleTextAttributes1, for:.selected)
        }
        else {
            // Fallback on earlier versions
        }
        segmentController.tintColor = UIColor.white
    }
    
    //MARK:- Button Action
    
    @IBAction func segmentControllActn(_ sender: UISegmentedControl) {
        
        let params = CollectionListModel()
        params.pageNumber = 1
        params.pageSize = 10
        
        if sender.selectedSegmentIndex == 0 {
            params.collectionType = "Popular"
            self.collectionListAPI(Model: params)
        }
        else if sender.selectedSegmentIndex == 1{
            params.collectionType = "Hot deal"
            self.collectionListAPI(Model: params)
        }
        else{
            params.collectionType = "Feature"
            self.collectionListAPI(Model: params)
        }
    }
    
    
    // MARK: - Collection List API
    func collectionListAPI(Model: CollectionListModel){
        self.showLoader()
        EndPoint.CollectionList(params: Model, t: CollectionListModel.self) { result in
            switch result {
            case .onSuccess(let items):
                if items.count > 0 {
                    print(items.toJSON())
                    self.arrCollection = items
                    self.listCollectionView.reloadData()
                    self.tableView.reloadData()
                }
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
}


extension CollectionProductViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrCollection.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.0 :0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrCollection.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionProductTableViewCell") as! CollectionProductTableViewCell
        cell.tagRow = indexPath.row
        cell.tagSection = indexPath.section
        cell.arrData = arrCollection
        if indexPath.section == 0{
           
            let width = (UIScreen.main.bounds.size.width)-50
            cell.collectionHeight.constant = width
            
           
        }else if indexPath.section == 1{
            let width = (UIScreen.main.bounds.size.width/2)-5
            cell.collectionHeight.constant = width
        }else{
            let width = (UIScreen.main.bounds.size.width/2)
            cell.collectionHeight.constant = width
        }
        
        cell.collectionV.dataSource = cell
        cell.collectionV.delegate = cell
        cell.collectionV.reloadData()
    
        return cell
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       // return 200
        return UITableView.automaticDimension
    }
}
