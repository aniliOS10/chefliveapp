//
//  DashBoard_MockUp.swift
//  PetCare
//
//  Created by rupinder singh on 04/10/21.
//

import Foundation
import UIKit
struct MockUp_CollectionProduct{
    var description : String!
    var title : String!
    var image : UIImage!

    init(title:String,description:String,image:UIImage){
        self.description = description
        self.title = title
        self.image = image
        }
}
