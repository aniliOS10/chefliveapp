//
//  CategoryListTitleDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import Foundation
import UIKit
extension CategoryListingViewController: UITableViewDataSource,UITableViewDelegate{
   
   
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView {
        case tblCategoryName:
        return 1
        case tblSubCategory:
            return 2
        case tblSearch:
            return 2
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        switch tableView {
        case tblCategoryName:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTitleSectionTableViewCell") as! CategoryTitleSectionTableViewCell
            
            return cell
            
        case tblSubCategory:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTitleSectionTableViewCell") as! CategoryTitleSectionTableViewCell
            cell.titleLabel.text = selectedCatName
            
            if UserDefaults.standard.string(forKey: "Sub_Sub_Key") == "Yes"{
                cell.imgView.isHidden = false
                cell.x_titleLabelConstraint.constant = 25

            }
            else{
                cell.imgView.isHidden = true
                cell.x_titleLabelConstraint.constant = 10
            }

            
            return cell
        case tblSearch:
            return nil
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTitleSectionTableViewCell") as! CategoryTitleSectionTableViewCell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == tblSearch {
            return 0
        }
        
        if tblSubCategory == tableView {
            return section == 0 ? 35.0 :0.0
        }
        return section == 0 ? 35.0 :0.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if  tblCategoryName == tableView {
            return 12.0
        }
        if tblSubCategory == tableView {
            return 0.1
        }
        return 0.0
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case tblCategoryName:
        return arrCategoryNames.count
        case tblSubCategory:
            return 1
        case tblSearch:
            if section == 0 {
                return arrLocalCategory.count
            }
            else if section == 1 {
                return arrlocalProduct.count
            }
            else{
                return 0
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case tblSearch:
            if indexPath.section == 0 {
                let cell = tblSearch.dequeueReusableCell(withIdentifier: "CategoryListSearchTVC") as! CategoryListSearchTVC
                cell.lblTitleName.textColor = UIColor.black
                if arrLocalCategory.count > 0 {
                    let catName = arrLocalCategory[indexPath.row]
                    cell.lblTitleName.text =  "\(catName)\nIn Category"
                    cell.lblTitleName.textColor = UIColor().getThemeColor()
                }
                
                return cell
            }
            else {
                let cell = tblSearch.dequeueReusableCell(withIdentifier: "CategoryListSearchTVC") as! CategoryListSearchTVC
                
                cell.lblTitleName.textColor = UIColor.black
                if arrlocalProduct.count > 0 {
                    cell.lblTitleName.text = arrlocalProduct[indexPath.row].productDescription ?? ""
                }
                
                return cell
            }
        case tblCategoryName:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTitleTableViewCell") as! CategoryTitleTableViewCell
            cell.titleLabel.text = arrCategoryNames[indexPath.row]
            cell.titleLabel.textColor = .black
            cell.backgroundColor = .clear
            
            if selectedCatName == arrCategoryNames[indexPath.row]{
                cell.titleLabel.textColor = .white
                cell.backgroundColor = UIColor().getThemeColor()
            }
            return cell
        case tblSubCategory:
            if indexPath.section == 0 {
                let cell = tblSubCategory.dequeueReusableCell(withIdentifier: "CategoryLisitingTableViewCell") as! CategoryLisitingTableViewCell
                cell.tagRow = indexPath.row
                cell.tagSection = indexPath.section
                cell.arrFeatureBannerList = arrFeatureBannerList
                if arrFeatureBannerList.count == 1 {
                    cell.pageNoControl.numberOfPages = 0
                }
                else{
                    cell.pageNoControl.numberOfPages = arrFeatureBannerList.count
                }
                cell.CVBannerSubCategory.dataSource = cell
                cell.CVBannerSubCategory.delegate = cell
                cell.CVBannerSubCategory.reloadData()
                return cell
            }
            else {
                let cell = tblSubCategory.dequeueReusableCell(withIdentifier: "SubCategoryListingTVC") as! SubCategoryListingTVC
                
                cell.tagRow = indexPath.row
                cell.CVSubCategory.dataSource = cell
                cell.CVSubCategory.delegate = cell
                cell.categoryID = selectedCatName
                cell.ClassNameAnimation = lastClass
                cell.arrAllProduct = self.arrProduct
                if UserDefaults.standard.string(forKey: "Sub_Sub_Key") == "Yes"{

                }
                else{
                    cell.isSubSubCategory = false
                    cell.arrSubCategory = arrSubCategory

                    DispatchQueue.main.async {
                        cell.CVSubCategory.reloadData()
                    }
                }
               
                return cell
            }
        default:
            return UITableViewCell.init()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView {
        case tblCategoryName:
            return 42
        case tblSubCategory:
            if indexPath.section == 0 {
                return 170
            }
            else {
                return tblSubCategory.frame.size.height - (170 + 42)
            }
        case tblSearch:
            return UITableView.automaticDimension
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case tblCategoryName:
            selectedCatName = arrCategoryNames[indexPath.row]
            self.arrSubCategory = arrProduct.filter {$0.categoryId == selectedCatName}
            self.arrSubCategory = self.arrSubCategory.filter {$0.subCategoryId != nil}

            var findImageArray = [NewArrivalsModel]()
            findImageArray = self.arrSubCategory
            self.arrSubCategory = self.arrSubCategory.filter {$0.subCategoryImage != nil}
            var addImageArray = [NewArrivalsModel]()

            for i in 0..<findImageArray.count
            {
                if arrSubCategory.contains(where: {$0.subCategoryId == findImageArray[i].subCategoryId}) {
                } else {
                    addImageArray.append(findImageArray[i])
                }
            }
            self.arrSubCategory = self.arrSubCategory.unique{$0.subCategoryId}
            self.arrSubCategory = self.arrSubCategory + addImageArray
            self.arrSubCategory = self.arrSubCategory.unique{$0.subCategoryId}
            self.arrSubCategory = self.arrSubCategory.filter {$0.subCategoryId != nil}
            
            var nonZero = [NewArrivalsModel]()
            
            for i in 0..<arrSubCategory.count
            {
                let subCategoryId = arrSubCategory[i].subCategoryId
                let arrSubProduct = arrProduct.filter {$0.subCategoryId == subCategoryId}
                let arrSubProduct1 = arrSubProduct.filter {$0.salesPrice ?? 0.0 > 0.0}
                if arrSubProduct1.count > 0 {
                    nonZero.append(arrSubCategory[i])
                }
            }
            
            self.arrSubCategory = nonZero
            self.arrSubCategory = self.arrSubCategory.sorted { $0.subCategoryId?.compare($1.subCategoryId ?? "") == .orderedAscending }
            self.selectedCatId = indexPath.row
            UserDefaults.standard.set("No", forKey: "Sub_Sub_Key")
            tblCategoryName.reloadData()
            tblSubCategory.reloadData()
            
        case tblSubCategory:
            break
        case tblSearch:
            if indexPath.section == 0 {
                selectedCatName = arrLocalCategory[indexPath.row]
                self.arrSubCategory = arrProduct.filter {$0.categoryId == selectedCatName}
                self.arrSubCategory = self.arrSubCategory.unique{$0.subCategoryId}
            }else {
                let controller:ProjectDetailVC =  UIStoryboard(storyboard: .DashBoard).initVC()
                controller.arrProductDetail = arrlocalProduct[indexPath.row]
                controller.productId = arrlocalProduct[indexPath.row].productId!
                self.navigationController?.pushViewController(controller, animated: true)
            }
            view.endEditing(true)
            vwSearch.isHidden = true
            break
        default:
            break
        }
        tblCategoryName.reloadData()
        tblSubCategory.reloadData()
    }
}
