//
//  CategorySubListDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import Foundation
import UIKit
@available(iOS 13.0, *)
class CategorySubListDataSource:GenericDataSource<MockUpCategoryList>,UITableViewDataSource,UITableViewDelegate{
   
    var didSelectRow: ((_ cell:UITableViewCell, _ indexPath:IndexPath) -> Void)?
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTitleSectionTableViewCell") as! CategoryTitleSectionTableViewCell
            cell.titleLabel.text = "Sub Categories"
        return cell
        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 40.0 :0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryLisitingTableViewCell") as! CategoryLisitingTableViewCell
        cell.tagRow = indexPath.row
        cell.tagSection = indexPath.section
        var width = cell.bounds.size.width
        if indexPath.section > 1{
             width = (width/2) + 40
        }
        cell.CVBannerSubCategory.dataSource = cell
        cell.CVBannerSubCategory.delegate = cell
        cell.CVBannerSubCategory.reloadData()
    
        return cell

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

