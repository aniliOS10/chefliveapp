//
//  CategoryListingViewModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import Foundation
class CategoryListingViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
   
    weak var dataSource:GenericDataSource<MockUpCategoryTitle>?
   
    weak var dataSourceCategoryList:GenericDataSource<MockUpCategoryList>?
    
    init(dataSource : GenericDataSource<MockUpCategoryTitle>?,dataSourceCategoryList : GenericDataSource<MockUpCategoryList>?) {
        self.dataSource = dataSource
        self.dataSourceCategoryList = dataSourceCategoryList
    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(){
        
    }
}
