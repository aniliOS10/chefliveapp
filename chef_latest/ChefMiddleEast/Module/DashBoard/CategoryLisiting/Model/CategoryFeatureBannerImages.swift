//
//  CategoryFeatureBannerImages.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class CategoryFeatureBannerImages: BaseResponse {
    
    var featureBannerId: Int?
    var featureBannerImage: String?
    var featureBannerType: String?
    var recipeId: String?
    var productId: String?
    var categoryId: String?
    var createDate: String?
    var modifyDate: String?
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        featureBannerId <- map["featureBannerId"]
        featureBannerImage <- map["featureBannerImage"]
        featureBannerType <- map["featureBannerType"]
        recipeId <- map["recipeId"]
        productId <- map["productId"]
        categoryId <- map["categoryId"]
        createDate <- map["createDate"]
        modifyDate <- map["modifyDate"]
    }
    
}
