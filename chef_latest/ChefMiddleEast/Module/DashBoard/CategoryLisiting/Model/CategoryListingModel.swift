//
//  CategoryListingModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 17/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class CategoryListingModel: BaseResponse {
    
    var categoryId: String?
    var categoryImage: String?

    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        categoryId <- map["categoryId"]
        categoryImage <- map["categoryImage"]
    }
}
