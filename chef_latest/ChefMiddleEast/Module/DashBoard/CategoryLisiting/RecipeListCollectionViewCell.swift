//
//  RecipeListCollectionViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import UIKit

class RecipeListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var ribonButton: UIButton!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var cellViewRound_top: NSLayoutConstraint!
    @IBOutlet weak var cellViewRound_Bottom: NSLayoutConstraint!
    
    @IBOutlet weak var imgDifficulty1: UIImageView!
    @IBOutlet weak var imgDifficulty2: UIImageView!
    @IBOutlet weak var imgDifficulty3: UIImageView!
    @IBOutlet weak var vwMinutes: UIView!
    @IBOutlet weak var cellViewRoundBG: UIView!

}
