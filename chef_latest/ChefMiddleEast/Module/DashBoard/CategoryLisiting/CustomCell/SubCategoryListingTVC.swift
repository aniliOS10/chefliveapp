//
//  SubCategoryListingTVC.swift
//  ChefMiddleEast
//
//  Created by sandeep on 07/01/22.
//

import UIKit

class SubCategoryListingTVC: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout    {
    
    
    @IBOutlet var CVSubCategory: UICollectionView!
    
    @IBOutlet var CVHeightSubCategory: NSLayoutConstraint!
    
    
    var tagRow:Int = 0
    var tagSection:Int = 0
    var arrSubCategory = [NewArrivalsModel]()
    var arrAllProduct = [NewArrivalsModel]()
    var categoryID = String()
    var ClassNameAnimation = ""
    var isSubSubCategory = false
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrSubCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: SubCategoryListingCVC = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryListingCVC", for: indexPath) as! SubCategoryListingCVC
        
        cell.VWCellBG.addShadow()
        cell.VWCell.layer.cornerRadius = 5
        cell.VWCellBG.layer.cornerRadius = 5
        if isSubSubCategory {
            cell.lblProductName.text = arrSubCategory[indexPath.row].subSubCategoryId
            cell.imgProduct.image = UIImage(named: "placeholder")

            
            DispatchQueue.main.async {
                if let imgUrl = self.arrSubCategory[indexPath.row].subSubCategoryImage, !imgUrl.isEmpty {
                let originalString  = "\(EndPoint.BASE_API_IMAGE_URL)\(imgUrl)"
                let urlString = originalString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
                cell.imgProduct.sd_setImage(with:URL(string: urlString), placeholderImage: UIImage(named: "placeholder"))
            }
            else {
                cell.imgProduct.image = UIImage(named: "placeholder")
                print("====NO")

            }
            }
        }
        else{
            cell.lblProductName.text = arrSubCategory[indexPath.row].subCategoryId
            cell.imgProduct.image = UIImage(named: "placeholder")

            
            DispatchQueue.main.async {
                if let imgUrl = self.arrSubCategory[indexPath.row].subCategoryImage, !imgUrl.isEmpty {
                let originalString  = "\(EndPoint.BASE_API_IMAGE_URL)\(imgUrl)"
                let urlString = originalString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
               
                    
                cell.imgProduct.sd_setImage(with:URL(string: urlString), placeholderImage: UIImage(named: "placeholder"))
            }
            else {
                cell.imgProduct.image = UIImage(named: "placeholder")
                print("====NO")

            }
            }
        }
  
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let size = CGSize(width: CVSubCategory.frame.size.width/2, height: CVSubCategory.frame.size.width/2 + 32)
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //top, left, bottom, right
        return UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var ssA = [NewArrivalsModel]()
        let id = arrSubCategory[indexPath.row].subCategoryId

        ssA = arrAllProduct.filter {$0.subCategoryId == id}
        ssA = ssA.filter {$0.subCategoryId != nil}
        ssA = ssA.filter {$0.subSubCategoryId != nil}
        var subSCategoryId = ""
        if ssA.count > 0 {
            if ssA.count > indexPath.row{
                subSCategoryId = ssA[indexPath.row].subSubCategoryId ?? ""
                ssA = arrAllProduct.filter {$0.subSubCategoryId == subSCategoryId}
                if isSubSubCategory {
                    let controller:NewArrivalsViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                    controller.arrProduct = ssA
                    controller.screenCommingFrom = "SubCategory"
                    controller.shouldDisplayHome = false
                    controller.headerTitle = subSCategoryId ?? ""
                    controller.selectedCatIdName = categoryID
                    controller.selectedSubCatIdName = subSCategoryId ?? ""
                    controller.lastClass = ClassNameAnimation
                    self.parentViewController?.navigationController?.pushViewController(controller, animated: true)
                }
                else{
                    arrSubCategory = ssA
                    isSubSubCategory = true
                    CVSubCategory.reloadData()
                    UserDefaults.standard.set("Yes", forKey: "Sub_Sub_Key")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SubSubCategory"), object: nil)

                }
            }
        }
        else{
            let subCategoryId = arrSubCategory[indexPath.row].subCategoryId
            let arrSubProduct = arrAllProduct.filter {$0.subCategoryId == subCategoryId}
            let arrSubProduct1 = arrSubProduct.filter {$0.salesPrice ?? 0.0 > 0.0}
            let controller:NewArrivalsViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            controller.arrProduct = arrSubProduct1
            controller.screenCommingFrom = "SubCategory"
            controller.shouldDisplayHome = false
            controller.headerTitle = subCategoryId ?? ""
            controller.selectedCatIdName = categoryID
            controller.selectedSubCatIdName = subCategoryId ?? ""
            controller.lastClass = ClassNameAnimation
            self.parentViewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
}
