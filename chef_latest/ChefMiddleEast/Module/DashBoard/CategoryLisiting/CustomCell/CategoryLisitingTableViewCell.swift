//
//  CategoryLisitingTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import UIKit

@available(iOS 13.0, *)
class CategoryLisitingTableViewCell:UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout   {
    
    
    @IBOutlet weak var CVBannerSubCategory: UICollectionView!
    @IBOutlet var pageNoControl: UIPageControl!
    
    var tagRow:Int = 0
    var tagSection:Int = 0
    var arrFeatureBannerList = [CategoryFeatureBannerImages]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in CVBannerSubCategory.visibleCells {
            self.pageNoControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return  arrFeatureBannerList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CategoryLisitingCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryLisitingCollectionViewCell", for: indexPath) as! CategoryLisitingCollectionViewCell
        
        
        Utility.shared.makeRoundCorner(layer: cell.cellViewRound.layer, color: UIColor.lightGray.withAlphaComponent(0.1), radius: 8.0)
        Utility.shared.makeRoundCorner(layer: cell.imageV.layer, color: UIColor.lightGray.withAlphaComponent(0.1), radius: 8.0)
        cell.imageV.image = UIImage(named: "placeholder")

        if let imgUrl =
            arrFeatureBannerList[indexPath.row].featureBannerImage,!imgUrl.isEmpty {
            let stringURL = "\(EndPoint.BASE_API_IMAGE_URL)\(imgUrl)"
            let urlString = stringURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                if (error != nil) {
                    cell.imageV.image = UIImage(named: "placeholder")
                } else {
                    cell.imageV.image = image
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //top, left, bottom, right
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        
        let size = CGSize(width: self.CVBannerSubCategory.frame.size.width, height: self.CVBannerSubCategory.frame.size.height)
        
        return size
        
    }
}
