//
//  CategoryListingViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 01/11/21.
//

import UIKit
import ObjectMapper

@available(iOS 13.0, *)
class CategoryListingViewController:BaseViewController , UITextFieldDelegate {
    
    @IBOutlet weak var tblCategoryName : UITableView!
    @IBOutlet weak var tblSubCategory : UITableView!
    @IBOutlet weak var search : UISearchBar!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet weak var txtSearch : UITextField!
    @IBOutlet weak var vwBoxSearch : UIView!
    
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!

    
    var arrProduct = [NewArrivalsModel]()
    var arrCategoryNames = [String]()

    var arrSubCategory = [NewArrivalsModel]()
    var arrFeatureBannerList = [CategoryFeatureBannerImages]()
    var arrSortedCategory = [CategoryListingModel]()

    var selectedCatName = ""
    var selectedCatId = 0
    var selectedCatIdName = ""
    var selectedSubCatIdName = ""

    var lastClass = ""
    
    var shouldDisplayHome:Bool = true
    
    @IBOutlet weak var vwSearch : UIView!
    var searchText = String()
    @IBOutlet weak var tblSearch : UITableView!
    var arrlocalProduct = [NewArrivalsModel]()
    var arrLocalCategory = [String]()
    @IBOutlet weak var vwBaseSearch : UIView!
    
    
    //MARK:- VC Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
        
        self.vwBaseSearch.isHidden = true
        self.vwSearch.isHidden = true
        self.txtSearch.text = ""
        self.txtSearch.resignFirstResponder()
        
        txtSearch.autocorrectionType = .no
        txtSearch.spellCheckingType = .no

        vwBoxSearch.addShadow()
        vwBoxSearch.layer.cornerRadius = 25.0
        
        self.tabBarController?.tabBar.isHidden = false
        
        txtSearch.delegate = self
        txtSearch.autocorrectionType = .no
        txtSearch.spellCheckingType = .no
        
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            self.cartCountAPI()
        }
        UserDefaults.standard.set("No", forKey: "Sub_Sub_Key")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblSearch.keyboardDismissMode = .onDrag
        NotificationCenter.default.addObserver(self, selector: #selector(self.setToSubSubCategory(notification:)), name: Notification.Name("SubSubCategory"), object: nil)
        

        
        categoryListAPI()
        self.setNavigationButton()
        self.setUpTabelView()
        self.setFilter()
        
        self.arrProduct = fetchAllProductLocalStorge()
        let arrCategory = arrProduct.unique{$0.categoryId}.map({ $0.categoryId ?? "" })
    
        var tempArrCategory = [String]()
        for item in arrCategory {
            if  item != ""{
                tempArrCategory.append(item)
            }
        }
        
        self.arrCategoryNames = tempArrCategory
        self.arrCategoryNames = self.arrCategoryNames.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        
        if !selectedCatIdName.isEmpty {
            let index = self.arrCategoryNames.firstIndex { $0 == selectedCatIdName }
            selectedCatId = index ?? 0
        }
        self.arrSubCategory = arrProduct.filter {$0.categoryId == self.arrCategoryNames[selectedCatId]}
        self.arrSubCategory = self.arrSubCategory.filter {$0.subCategoryId != nil}

        var findImageArray = [NewArrivalsModel]()
        findImageArray = self.arrSubCategory
        self.arrSubCategory = self.arrSubCategory.filter {$0.subCategoryImage != nil}
        var addImageArray = [NewArrivalsModel]()

        for i in 0..<findImageArray.count
        {
            if arrSubCategory.contains(where: {$0.subCategoryId == findImageArray[i].subCategoryId}) {
               // it exists, do something
            } else {
               //item could not be found
                addImageArray.append(findImageArray[i])
            }
        }
        
        self.arrSubCategory = self.arrSubCategory.unique{$0.subCategoryId}
        self.arrSubCategory = self.arrSubCategory + addImageArray
        self.arrSubCategory = self.arrSubCategory.unique{$0.subCategoryId}
        self.arrSubCategory = self.arrSubCategory.filter {$0.subCategoryId != nil}
        
        
        var nonZero = [NewArrivalsModel]()
        
        for i in 0..<arrSubCategory.count
        {
            let subCategoryId = arrSubCategory[i].subCategoryId
            let arrSubProduct = arrProduct.filter {$0.subCategoryId == subCategoryId}
            let arrSubProduct1 = arrSubProduct.filter {$0.salesPrice ?? 0.0 > 0.0}
            if arrSubProduct1.count > 0 {
                nonZero.append(arrSubCategory[i])
            }
        }
        
     
        self.arrSubCategory = nonZero
        
        
        self.arrSubCategory = self.arrSubCategory.sorted { $0.subCategoryId?.compare($1.subCategoryId ?? "") == .orderedAscending }
        
        DispatchQueue.main.async {
            self.tblSubCategory.reloadData()
            self.tblCategoryName.reloadData()
        }
      
        
        self.fetchCategoryBannerImages()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        vwBaseSearch.addGestureRecognizer(tap)
    }
    
    @objc func setToSubSubCategory(notification: Notification) {
        
        self.tblSubCategory.reloadData()
    }
    
    
    // MARK: - Category List API
    func categoryListAPI(){
        self.arrSortedCategory.removeAll()
        EndPoint.GetAllCategoryList(t: CategoryListingModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccess(let items):
                if items.count > 0 {
                    self.arrSortedCategory.removeAll()
                    var tempArrCategory = [CategoryListingModel]()
                    
                    var stringArray = [String]()

                    for item in items {
                        if item.categoryId != nil {
                            tempArrCategory.append(item)
                            
                            if item.categoryId != "" {
                                stringArray.append(item.categoryId ?? "")

                            }
                        }
                    }
                    
                    self.arrSortedCategory = tempArrCategory.sorted(by: { $0.categoryId! < $1.categoryId! })
                    self.arrSortedCategory = self.arrSortedCategory.unique{$0.categoryId}
                    
                    self.arrCategoryNames = stringArray
                    self.arrCategoryNames = self.arrCategoryNames.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
                    DispatchQueue.main.async {
                        self.tblCategoryName.reloadData()
                    }

                }
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                self.hideLoader()
            }
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        vwSearch.isHidden = true
        vwBaseSearch.isHidden = true
        
    }
    
    func fetchAllProductLocalStorge() -> [NewArrivalsModel] {
        if let syncContactsData = UserDefaults.standard.value(forKey: Constants.ProductModel) as? Data {
            if let aryProduct:[[String:Any]] = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(syncContactsData) as? [[String : Any]] {
                return Mapper<NewArrivalsModel>().mapArray(JSONArray: aryProduct)
            }
        }
        return []
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        if updatedString!.length > 0 {
            self.searchText = updatedString!
            NSObject.cancelPreviousPerformRequests( withTarget: self, selector: #selector(searchDasboardProducts), object: nil)
            self.perform(#selector(searchDasboardProducts), with: nil, afterDelay: 0.5)
            
        } else {
            self.searchText = ""
            self.vwSearch.isHidden = true
            self.vwBaseSearch.isHidden = true
        }
        
        return true
    }
    
    //MARK:- search Dasboard Products
    @objc private func searchDasboardProducts() {
    
        self.vwSearch.layer.cornerRadius = 15
        self.vwSearch.addShadow()
        let arrlocal = self.arrCategoryNames
        let arrLocalProduct = self.arrProduct
        self.arrLocalCategory  = arrlocal.filter { $0.localizedCaseInsensitiveContains(searchText) }
        self.arrlocalProduct = (arrLocalProduct.filter({(($0.productDescription ?? "").localizedCaseInsensitiveContains(searchText))}))
        
        if self.arrLocalCategory.count > 0 ||  self.arrlocalProduct.count > 0 {
            if searchText.length > 0 {
            self.vwSearch.isHidden = false
            self.vwBaseSearch.isHidden = false
                if self.arrLocalCategory.count > 4 ||  self.arrlocalProduct.count > 4 {
                      searchViewHeight.constant = CGFloat(444)
                    }
                    else{
                        if self.arrLocalCategory.count > self.arrlocalProduct.count {
                            searchViewHeight.constant = CGFloat(self.arrLocalCategory.count * 75)
                        }
                        if self.arrlocalProduct.count > self.arrLocalCategory.count {
                            searchViewHeight.constant = CGFloat(self.arrlocalProduct.count * 75)
                        }
                    }
            }
            else{
                self.vwSearch.isHidden = true
                self.vwBaseSearch.isHidden = true
            }
        }
        else{
            self.vwSearch.isHidden = true
            self.vwBaseSearch.isHidden = true
         
        }
        tblSearch.reloadData()
    }
    
    
    //MARK:- Terms And Condition API
    func fetchCategoryBannerImages(){
        self.showLoader()
        
        EndPoint.GetCategoryFeatureImages(t: CategoryFeatureBannerImages.self) {
            (result) in
            self.hideLoader()
            switch result {
            case .onSuccess(let items):
                self.arrFeatureBannerList = items
                self.tblSubCategory.reloadData()
                
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- Set Tableview
    func setUpTabelView() {
        
        if #available(iOS 11.0, *) {
            tblCategoryName.contentInsetAdjustmentBehavior = .never
            tblSubCategory.contentInsetAdjustmentBehavior = .never
            
            if #available(iOS 15.0, *) {
                tblCategoryName.sectionHeaderTopPadding = 3
                tblSubCategory.sectionHeaderTopPadding = 3
            }
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        self.tblCategoryName.delegate = self
        self.tblCategoryName.dataSource = self
        self.tblSubCategory.delegate = self
        self.tblSubCategory.dataSource = self
        
        self.tblCategoryName.addCorner(cornerRadius: 11)
        self.tblSubCategory.addCorner(cornerRadius: 1)
        
    }
    
    //MARK:- Set Search
    func setUpSearch() {
        
        search.searchBarStyle = .minimal
        search.backgroundColor = UIColor.white
        search.tintColor = UIColor.white
        search.delegate = self

        if #available(iOS 13.0, *) {
            search.searchTextField.backgroundColor = UIColor().getSearchColor()
            search.searchTextField.textColor =  UIColor().getThemeColor()
            search.searchTextField.tintColor =  UIColor().getThemeColor()
            Utility.shared.makeRoundCorner(layer: search.searchTextField.layer, color:UIColor.white.withAlphaComponent(0.5), radius: 15)
            //   Utility.shared.makeShadowsOfView_roundCorner(view: search, shadowRadius: 4.0, cornerRadius: 30, borderWidth:0.0, borderColor: UIColor.lightGray)
            let white = UIColor.red
            search.searchTextField.background = UIImage.init(named: "imageV1") //white.imageWithColor(width: Int(self.view.bounds.width), height: Int(self.search.searchTextField.frame.height))
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    //MARK:- Set Navigation
    func setNavigationButton() {
        
        self.initMenuButton()
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
        bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
        
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            if self.shouldDisplayHome{
                self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [], title: Appmessage_CategoryListing.Placeholder.Categories)
            }else{
                self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: Appmessage_CategoryListing.Placeholder.Categories)
            }
        }else {
            if self.shouldDisplayHome{
                self.initBellButton()
                self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [bellBarButton], title: Appmessage_CategoryListing.Placeholder.Categories)
            }else{
                self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [bellBarButton], title: Appmessage_CategoryListing.Placeholder.Categories)
            }
        }
        
        
        
    }
    
    //MARK:- Menu Button
    @objc func menuButtonPressed(){
        self.sideMenuController?.revealMenu()
    }
    
    //MARK:- Notification Button
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
    
    //MARK:- Set Filter
    func setFilter(){
        
       // Utility.shared.makeShadowsOfView_roundCorner(view: btnFilter, shadowRadius: 1.0, cornerRadius: 22, borderColor: UIColor.lightGray)
    }
    
    
    
    //MARK:- Button Action
    @IBAction func filterButtonPressed(_ sender: Any) {
        
        let controller:SortAndFilterViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func seeAllSubCategoryPressed(_ sender: Any) {
       
        var arrProductList = [NewArrivalsModel]()
        if arrProduct.count > 0 {
            arrProductList = arrProduct.filter{$0.categoryId != nil && $0.categoryId == selectedCatName}
        }
        
        
        let controller:NewArrivalsViewController =  UIStoryboard(storyboard: .DashBoard).initVC()

        controller.screenCommingFrom = "SubCategory"
        controller.shouldDisplayHome = false
        controller.headerTitle = selectedCatName
        controller.selectedCatIdName = selectedCatName
        controller.lastClass = lastClass
        controller.arrProduct = arrProductList
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func SubBackPressed(_ sender: Any) {
      
        UserDefaults.standard.set("No", forKey: "Sub_Sub_Key")
        self.tblSubCategory.reloadData()

    }
    
    //MARK:- Cart Count Api
    func cartCountAPI() {
        EndPoint.CartCount(t: CartCountModel.self) { result in
            switch result {
            case .onSuccess(let data):
            if data != nil{
                dictCartData = data!
                if let cartCount = dictCartData.cartCount, cartCount != 0 {
                    self.setBagCountOnCart(count: cartCount)
                    if let tabItems = self.tabBarController?.tabBar.items {
                        // In this case we want to modify the badge number of the third tab:
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "\(cartCount)"
                    }
                }
                if let notificationCount = dictCartData.notificationCount, notificationCount >= 0 {
                    self.setNotificationBagCount(count: notificationCount)
                    
                }
                
            }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }

    
}


extension Array {
    func unique<T:Hashable>(by: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(by(value)) {
                set.insert(by(value))
                arrayOrdered.append(value)
            }
        }
        return arrayOrdered
    }
}

extension CategoryListingViewController: UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)  {
        searchBar.resignFirstResponder()
    }
}
extension UITableView {
    func addCorner(cornerRadius:CGFloat){
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
    }
}
