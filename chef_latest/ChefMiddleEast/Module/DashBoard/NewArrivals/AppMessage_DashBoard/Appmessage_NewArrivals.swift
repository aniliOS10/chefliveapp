//
//  Appmessage_Dashboard.swift
//  PetCare
//
//  Created by rupinder singh on 04/10/21.
//

import UIKit

class Appmessage_NewArrivals: NSObject {
    struct Placeholder {
        static let NewArrivalsProducts = NSLocalizedString("NewArrivalsProducts", comment: "")
        static let NewArrivals = NSLocalizedString("NewArrivals", comment: "")
        static let MyFavourites = NSLocalizedString("MyFavourites", comment: "")
        static let MyFavouritesProducts = NSLocalizedString("MyFavouritesProducts", comment: "")
    
        static let Favourites = NSLocalizedString("Favourites", comment: "")
    }
}
