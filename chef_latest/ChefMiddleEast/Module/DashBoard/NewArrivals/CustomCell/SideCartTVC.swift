//
//  SideCartTVC.swift
//  ChefMiddleEast
//
//  Created by Apple on 25/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class SideCartTVC: UITableViewCell {

    @IBOutlet weak var imgItem : UIImageView!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var lblCount : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
