//
//  CollectionProductCollectionViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 31/10/21.
//

import UIKit

class NewArrivalsCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var salePriceLabel: UILabel!
    @IBOutlet weak var increaseButton: UIButton!
    @IBOutlet weak var decreaseButton: UIButton!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var comingSoonLabel: UILabel!
    @IBOutlet weak var cellViewRoundMore: UIView!

}
