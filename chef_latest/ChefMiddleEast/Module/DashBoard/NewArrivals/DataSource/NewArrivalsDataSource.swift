//
//  DashBoardCollectionViewDataSource.swift
//  PetCare
//
//  Created by rupinder singh on 04/10/21.
//

import UIKit
@available(iOS 13.0, *)

extension NewArrivalsViewController: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UITableViewDelegate, UITableViewDataSource {
    

    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView {
        case tblSearch:
            return 2
        case sideCartTblView:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch tableView {
        case tblSearch:
            return nil
        case sideCartTblView:
            let headerView = UIView()
            headerView.backgroundColor = .white
            return nil
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch tableView {
        case tblSearch:
            return 0
        case sideCartTblView:
            return 0.1
        default:
            return 0
        }
       // return section == 0 ? 0.0 :40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        switch tableView {
        case tblSearch:
            if section == 0 {
                return arrLocalCategory.count
            }
            else if section == 1 {
                return arrlocalProduct.count
            }
            else{
                return 0
            }
        case sideCartTblView:
            return arrCartProduct.count
        default:
            return 0
        }
      //  return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        switch tableView {
        case tblSearch:
            if indexPath.section == 0 {
                let cell = tblSearch.dequeueReusableCell(withIdentifier: "NewArrivalSearchTVC") as! NewArrivalSearchTVC
                cell.lblTitleName.textColor = UIColor.black
                if arrLocalCategory.count > 0 {
                    let catName = arrLocalCategory[indexPath.row]
                    cell.lblTitleName.text =  "\(catName)\nIn Category"
                    cell.lblTitleName.textColor = UIColor().getThemeColor()
                }
                return cell
            }
            else {
                let cell = tblSearch.dequeueReusableCell(withIdentifier: "NewArrivalSearchTVC") as! NewArrivalSearchTVC
                
                cell.lblTitleName.textColor = UIColor.black
                if arrlocalProduct.count > 0 {
                    cell.lblTitleName.text = arrlocalProduct[indexPath.row].productDescription ?? ""
                }
                
                return cell
            }
        case sideCartTblView:
            let cell = sideCartTblView.dequeueReusableCell(withIdentifier: "SideCartListTVC") as! SideCartListTVC
            cell.selectionStyle = .none
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(showDeleteItemAlert(button:)), for: .touchUpInside)
            cell.baseView.layer.cornerRadius = 4.0
            cell.baseView.addShadow()
            cell.lblCountTotal.text = String("\(arrCartProduct[indexPath.row].productQuantity!)")
            cell.btnAddProduct.tag = indexPath.row
            cell.btnRemoveProduct.tag = indexPath.row
            cell.vwSelectQty.layer.cornerRadius = 4.0

            cell.btnAddProduct.addTarget(self, action: #selector(btnAddProductTap(sender:)), for: .touchUpInside)
            
            cell.btnRemoveProduct.addTarget(self, action: #selector(btnRemoveProductTap(sender:)), for: .touchUpInside)
            
            if let imgUrl = arrCartProduct[indexPath.row].productImage, !imgUrl.isEmpty {
                
                let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

                
                cell.imgView?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                    if (error != nil) {
                        cell.imgView.image = UIImage(named: "placeholder")
                    } else {
                        cell.imgView.image = image
                    }
                }
            }
            else {
                cell.imgView.image = UIImage(named: "placeholder")
            }
            
            return cell
           
            
        default:
            break
        }
        return UITableViewCell.init()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        // return 200
        switch tableView {
        case tblSearch:
            return UITableView.automaticDimension
        case sideCartTblView:
            return 109
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        switch tableView {
        case tblSearch:
            
            if indexPath.section == 0 {
                
                let controller:CategoryListingViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                controller.shouldDisplayHome = false
                controller.selectedCatIdName = arrLocalCategory[indexPath.row]
                controller.selectedCatName = arrLocalCategory[indexPath.row]
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else{
                let controller:ProjectDetailVC =  UIStoryboard(storyboard: .DashBoard).initVC()
                controller.arrProductDetail = arrlocalProduct[indexPath.row]
                controller.productId = arrlocalProduct[indexPath.row].productId!
                
              //  controller.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(controller, animated: true)
                /*let subCategoryId = arrlocalProduct[indexPath.row].subCategoryId
                
                let arrSubProduct = arrProduct.filter {$0.subCategoryId == subCategoryId}
                        
                let controller:NewArrivalsViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                controller.arrProduct = arrSubProduct
                controller.screenCommingFrom = "SubCategory"
                controller.shouldDisplayHome = false
                self.navigationController?.pushViewController(controller, animated: true)*/
            }
            view.endEditing(true)
        default:
            break
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        
        if arrProduct.count > 0 {
            self.lbe_NoData.isHidden = true
            self.img_NoData.isHidden = true
        }
        else {
            self.lbe_NoData.isHidden = false
            self.img_NoData.isHidden = false
        }
        return arrProduct.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: NewArrivalsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewArrivalsCollectionViewCell", for: indexPath) as! NewArrivalsCollectionViewCell
        
        cell.cellViewRoundMore.addShadowDash()
        cell.cellViewRound.layer.cornerRadius = 6
        cell.cellViewRoundMore.layer.cornerRadius = 6
        cell.btnAdd.layer.cornerRadius = 5
        
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(btnAddTap(sender:)), for: .touchUpInside)
                
        cell.titleLabel.text = arrProduct[indexPath.row].productDescription
        
        
        cell.imageV.image = UIImage(named: "placeholder")
        
        
        if (arrProduct[indexPath.row].productImage != nil) {
            
            if (arrProduct[indexPath.row].productImage)?.count ?? 0 > 0 {
                let imageData = arrProduct[indexPath.row].productImage![0] as! [String : Any]
                
                let imagePath = String("\(EndPoint.BASE_API_PRODUCT_IMAGE_URL)\(imageData["productDetailImagePath"] as! String)")
                
                let urlString = imagePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

                
                cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                    if (error != nil) {
                        cell.imageV.image = UIImage(named: "placeholder")
                    } else {
                        cell.imageV.image = image
                    }
                }
            }
        }else {
            cell.imageV.image = UIImage(named: "placeholder")
        }
  
            cell.salePriceLabel.isHidden = true
            cell.btnFav.isHidden = true
           
        //Display Coming soon
        let availableQty = arrProduct[indexPath.row].availableQuantity
        if ((availableQty) != nil )  {
            if (availableQty)! > 5 {
           
       //Display agreementValue
        if let startDate = arrProduct[indexPath.row].fromDate,startDate != nil {
            
            let fromDate : Date = startDate.dateFromString(startDate)
            let endDate = arrProduct[indexPath.row].toDate
            let toDate : Date = endDate!.dateFromString(endDate!)
            
            if Date().isBetween(fromDate, and: toDate) {
                if let agreementValue = arrProduct[indexPath.row].agreementValue, agreementValue != nil {

                    cell.salePriceLabel.isHidden = false
                    let agreementValue = arrProduct[indexPath.row].agreementValue
                    cell.salePriceLabel.text = String(format: "AED %.2f", agreementValue!)
                    
                    if let price = arrProduct[indexPath.row].salesPrice,price != nil {
                        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: String(format: " %.2f", price))
                        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
                        cell.desLabel.isHidden = false
                        cell.desLabel.attributedText = attributeString
                    }
                }
            } else {
                if let price = arrProduct[indexPath.row].salesPrice,price != nil {
                    cell.desLabel.isHidden = true
                    cell.desLabel.text = ""
                    cell.salePriceLabel.isHidden = false
                    cell.salePriceLabel.text = String(format: "AED %.2f", price)
                }
            }
        } else {
            if let price = arrProduct[indexPath.row].salesPrice,price != nil {
                cell.desLabel.isHidden = true
                cell.desLabel.text = ""
                cell.salePriceLabel.isHidden = false
                cell.salePriceLabel.text = String(format: "AED %.2f", price)
            }
        }
            cell.btnAdd.isHidden = false
            cell.comingSoonLabel.isHidden = true
            if let price = arrProduct[indexPath.row].salesPrice,price > 0 {
            }
            else{
                cell.btnAdd.isHidden = true
                cell.comingSoonLabel.isHidden = false
                cell.salePriceLabel.isHidden = true
                cell.desLabel.isHidden = true
                cell.desLabel.text = ""
            }
        
        } else {
                 cell.btnAdd.isHidden = true
                 cell.comingSoonLabel.isHidden = false
                 cell.salePriceLabel.isHidden = true
                 cell.desLabel.isHidden = true
                 cell.desLabel.text = ""

            }
        }else {
            cell.btnAdd.isHidden = true
            cell.comingSoonLabel.isHidden = false
            cell.salePriceLabel.isHidden = true
            cell.desLabel.isHidden = true
            cell.desLabel.text = ""

        }

     
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        if isSideCartON {
            let width = (collectionView.bounds.size.width/2)
            let size = CGSize(width: width, height: width + 100)
            
            return size
        } else {
          
          //  let width = (UIScreen.main.bounds.size.width/2) - 15
            let width = (collectionView.bounds.size.width/2) - 2

            let size = CGSize(width: width, height: width + 100)
            
            return size
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            self.view.endEditing(true)
        
            let controller:ProjectDetailVC =  UIStoryboard(storyboard: .DashBoard).initVC()
            controller.arrProductDetail = arrProduct[indexPath.row]
            controller.productId = arrProduct[indexPath.row].productId!
            controller.homeToSearch = "Yes"
            self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- Add Button Tap
    @objc func btnAddTap(sender:UIButton){
        
            
            if !arrProduct.isEmpty {
                guard let productId = arrProduct[sender.tag].productId else {return}
                let arrFilter = arrCartProduct.filter{$0.productId == productId}
                
                if arrFilter.count > 0 {
                    
                    let alert = UIAlertController(title: nil, message:"Are you sure to add same item to cart?", preferredStyle: .alert)
                    let No = UIAlertAction(title:"No", style: .default, handler: { action in
                    })
                    alert.addAction(No)
                    let Yes = UIAlertAction(title:"Yes", style: .default, handler: { [self] action in
                        
                        let productData = arrFilter[0]
                        var addQty = productData.productQuantity ?? 0
                        addQty = addQty + 1

                        let param = AddToCartModel()
                        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                            param.userId = userId
                        }
                        param.productId = productData.productId
                        param.productQuantity = addQty
                        self.updateQuantity(Model: param)
                        
                        let cell : NewArrivalsCollectionViewCell = self.collectionV.cellForItem(at: IndexPath(item: sender.tag, section: 0)) as! NewArrivalsCollectionViewCell
                        self.addItemToCartAnimation(cell)
                        
                    })
                    alert.addAction(Yes)
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true)
                    })
                } else {
                    if productId != 0 {
                        let param = AddToCartModel()
                        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                            param.userId = userId
                        }
                        
                        
                        let paramProduct = ProductModel()
                        paramProduct.productId = productId
                        paramProduct.productQuantity = 1
                        param.product.append(paramProduct)
                        self.addToCartAPI(Model: param, tag: sender.tag)
                        
                        let cell : NewArrivalsCollectionViewCell = self.collectionV.cellForItem(at: IndexPath(item: sender.tag, section: 0)) as! NewArrivalsCollectionViewCell
                        self.addItemToCartAnimation(cell)
                        
                    }
                }
            }
    }
    
    
    
    //MARK:- Delete Button Tap
     func btnDeleteItemTap(button:UIButton){
        
        if !arrCartProduct.isEmpty {
            
            if arrCartProduct.count == button.tag {
               return
            }
            var addQty = arrCartProduct[button.tag].productQuantity ?? 0
            addQty = 0

            let param = AddToCartModel()
            if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                param.userId = userId
            }
            param.productId = arrCartProduct[button.tag].productId
            param.productQuantity = addQty
            self.updateQuantity(Model: param)
        }
    }
    
    @objc func showDeleteItemAlert(button:UIButton){
        let alert = UIAlertController(title: nil, message:"Are you sure you want to remove this item?", preferredStyle: .alert)
        
        let No = UIAlertAction(title:"No", style: .default, handler: { action in
        })
        alert.addAction(No)
        
        let Yes = UIAlertAction(title:"Yes", style: .default, handler: { [self] action in
            btnDeleteItemTap(button: button)
        })
        alert.addAction(Yes)
        
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    
    //MARK:- Add Product Tap
    @objc func btnAddProductTap(sender: UIButton) {
        
        if !arrCartProduct.isEmpty {
            
            if arrCartProduct.count == sender.tag
             {
                return
             }
            addQty = arrCartProduct[sender.tag].productQuantity ?? 0
            if addQty < 15 {

            }else{
                NotificationAlert().NotificationAlert(titles: "You have reached maximum order quantity")
                return
            }
            addQty = addQty + 1

            let param = AddToCartModel()
            if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                param.userId = userId
            }
            param.productId = arrCartProduct[sender.tag].productId
            param.productQuantity = addQty
            self.showLoader()

            self.updateQuantity(Model: param)
        }
    }
    
    //MARK:- Remove Product Tap
    @objc func btnRemoveProductTap(sender: UIButton) {
        
        if !arrCartProduct.isEmpty {
           if arrCartProduct.count == sender.tag
            {
               return
            }
            addQty = arrCartProduct[sender.tag].productQuantity ?? 0
            addQty = addQty - 1

            let param = AddToCartModel()
            if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                param.userId = userId
            }
            param.productId = arrCartProduct[sender.tag].productId
            param.productQuantity = addQty
            self.showLoader()

            self.updateQuantity(Model: param)
        }
    }
    
    
    //MARK:- Update Quantity Api
    func updateQuantity(Model: AddToCartModel) {
        EndPoint.UpdateQuantity(params: Model,t: AddToCartModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccessWithStringValue(let msg):
               // NotificationAlert().NotificationAlert(titles: msg)
                if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        let param = AddToCartModel()
                        param.userId = userId
                        self.getCartListAPI(Model: param)
                        self.cartCountAPI()
                    }
                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    
    //MARK:- Fav Button Tap
    @objc func btnFavouritTap(button:UIButton){
        button.isSelected = !button.isSelected
        arrFavourit[button.tag].isFavourite = button.isSelected
        EndPoint.recipeId = "\(arrFavourit[button.tag].recipeId!)"
        if button.isSelected == true {
            EndPoint.favouriteStatus = "true"
        }
        else {
            EndPoint.favouriteStatus = "false"
        }
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            markRecipeFavouriteAPI()
        }
    }
        // MARK: - Recipe List API
        func markRecipeFavouriteAPI(){
            EndPoint.MarkRecipeFavourite(t: RecipeListModel.self) { result in
                switch result {
                case .onSuccess(_ ):
                    NotificationAlert().NotificationAlert(titles: "Sucess Response")
                case .onFailure(let error):
                    print(error)
                    if error != "The request timed out."{
                        NotificationAlert().NotificationAlert(titles: error)
                    }
                }
            }
        }
    
    //MARK:- Add to Cart Api
    func addToCartAPI(Model: AddToCartModel, tag : Int) {
        
        EndPoint.AddToCart(params: Model,t: AddToCartModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccessWithStringValue(let msg):
                if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
                    EndPoint.userId = userId
                    self.cartCountAPI()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.8) {
                        let param = AddToCartModel()
                        param.userId = userId
                        self.getCartListAPI(Model: param)
                    }
                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
      
}

