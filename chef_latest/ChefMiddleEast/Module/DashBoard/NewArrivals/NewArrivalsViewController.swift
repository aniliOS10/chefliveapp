//
//  CollectionProductViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 31/10/21.
//

import UIKit
import ObjectMapper



class NewArrivalsViewController:BaseViewController, UITextFieldDelegate, FilterSelectionDelegate, CAAnimationDelegate {
    
    @IBOutlet weak var search : UISearchBar!
    @IBOutlet weak var buttonPayment: UIButton!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var sideCartView: UIView!
    @IBOutlet weak var sideCartViewWidth: NSLayoutConstraint!
    @IBOutlet weak var collectionVidth: NSLayoutConstraint!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!

    @IBOutlet weak var filterView: UIView!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet weak var collectionV: UICollectionView!
    
    @IBOutlet weak var lbe_NoData : UILabel!
    @IBOutlet weak var img_NoData : UIImageView!
    
    var filterSelection = [String:Any]()

    var arrFavourit = [FavouriteModel]()
    var titleString:String = Appmessage_NewArrivals.Placeholder.MyFavourites
    var subtitleString:String = Appmessage_NewArrivals.Placeholder.MyFavouritesProducts
    var shouldDisplayHome:Bool = true
    var headerTitle = String()
    
    var arrProduct = [NewArrivalsModel]()
    var arrBackupProduct = [NewArrivalsModel]()
    var screenCommingFrom = String()
    
    @IBOutlet weak var txtSearch : UITextField!
    @IBOutlet weak var vwBoxSearch : UIView!
    @IBOutlet weak var tblSearch : UITableView!
    @IBOutlet weak var vwSearch : UIView!
    @IBOutlet weak var vwBaseSearch : UIView!
    
    var arrSelectedProduct = [String]()
    var searchText = String()
    
    @IBOutlet weak var vwTopSearch : UIView!
    var arrAllProduct = [NewArrivalsModel]()
    var arrlocalProduct = [NewArrivalsModel]()
    var arrCategoryNames = [String]()
    var arrLocalCategory = [String]()
    
    var selectedCatIdName = ""
    var selectedSubCatIdName = ""
    var arrCartProduct = [ProductModel]()
    var addQty = Int()
    var lastClass = ""

    lazy var dispatchGroup: DispatchGroup = {
            let dispatchGroup = DispatchGroup()
            return dispatchGroup
    }()
 
    var isSideCartON : Bool = false
    @IBOutlet weak var cartImage : UIImageView!
    @IBOutlet weak var sideCartTblView : UITableView!
    @IBOutlet weak var btnBuy : UIButton!
    
    
    //MARK:- VC Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        
        txtSearch.autocorrectionType = .no
        txtSearch.spellCheckingType = .no
        txtSearch.text = ""
        vwSearch.isHidden = true
        vwBaseSearch.isHidden = true
        
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
                EndPoint.userId = userId
                EndPoint.favouriteType = "recipe"
            }

        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            self.cartCountAPI()
        }
        

        
        self.sideCartView.layer.cornerRadius = 5.0
        self.sideCartView.addShadow()
        
        self.sideCartView.frame.origin.x = self.view.frame.size.width
        self.sideCartView.isHidden = true
        self.sideCartViewWidth.constant = 0
        
        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            let param = AddToCartModel()
            param.userId = userId
            self.getCartListAPI(Model: param)
            self.cartCountAPI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtSearch.delegate = self
        
        self.vwBaseSearch.isHidden = true
        self.vwSearch.isHidden = true
        self.txtSearch.text = ""
        self.txtSearch.resignFirstResponder()
        self.tblSearch.keyboardDismissMode = .onDrag

        self.arrAllProduct = fetchAllProductLocalStorge()
        
        let arrCategory = self.arrAllProduct.unique{$0.categoryId}.map({ $0.categoryId ?? "" })
        
        var tempArrCategory = [String]()
        for item in arrCategory {
            if  item != ""{
                tempArrCategory.append(item)
            }
        }
        self.arrCategoryNames = tempArrCategory
        self.arrCategoryNames = self.arrCategoryNames.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        
        vwBoxSearch.addShadow()
        vwBoxSearch.layer.cornerRadius = 25.0
        
        vwTopSearch.addShadow()
        vwTopSearch.layer.cornerRadius = 25.0
        
        self.SetNavigation()

        setUpTabelView()
        filterView.layer.cornerRadius = 22.5
        
        print("selectedCatIdName")
        print(selectedSubCatIdName)
        print(selectedCatIdName)
        
        arrProduct = arrProduct.filter {$0.salesPrice ?? 0.0 > 0.0}
        let temp = self.arrProduct
        self.arrProduct.removeAll()
    
        self.arrProduct = temp.sorted(by: { $0.availableQuantity ?? 0.0  > $1.availableQuantity ?? 0.0 })

        arrBackupProduct = arrProduct
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        vwBaseSearch.addGestureRecognizer(tap)
        
        btnBuy.layer.cornerRadius = 5.0
        btnBuy.addShadow()
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        vwSearch.isHidden = true
        vwBaseSearch.isHidden = true
        
    }
    
    func setUpTabelView() {
        self.tblSearch.delegate = self
        self.tblSearch.dataSource = self
        self.tblSearch.reloadData()
        
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        sideCartTblView.tableHeaderView = UIView(frame: frame)
        sideCartTblView.tableFooterView = UIView(frame: frame)

        if #available(iOS 11.0, *) {
            sideCartTblView.contentInsetAdjustmentBehavior = .never
            if #available(iOS 15.0, *) {
                sideCartTblView.sectionHeaderTopPadding = 0.0
            }
        }
    }
    
    func filterResetAcn() {
        self.arrProduct = arrBackupProduct
        
        let temp = self.arrProduct
        self.arrProduct.removeAll()
        self.arrProduct = temp.sorted(by: { $0.availableQuantity ?? 0.0  > $1.availableQuantity ?? 0.0 })
        
        collectionV.reloadData()
        filterSelection.removeAll()
    }
    func filterCancelAcn() {
        self.arrProduct = arrBackupProduct
        
        let temp = self.arrProduct
        self.arrProduct.removeAll()
        self.arrProduct = temp.sorted(by: { $0.availableQuantity ?? 0.0  > $1.availableQuantity ?? 0.0 })
        
        collectionV.reloadData()
        filterSelection.removeAll()

    }
    
    //MARK:- Get Cart List Api
    func getCartListAPI(Model: AddToCartModel) {
        EndPoint.GetCartList(params: Model,t: AddToCartModel.self) { [self] result in
            self.hideLoader()
            switch result {
            case .onSuccess(let data):
                if data != nil {
                    self.arrCartProduct =  data!.productList.reversed()
                    self.sideCartTblView.reloadData()
                }
                else{
                    self.arrCartProduct.removeAll()
                    self.sideCartTblView.reloadData()
                }
            case .onFailure(let error):
                print(error)
                self.arrCartProduct.removeAll()
                self.sideCartTblView.reloadData()
            }
        }
    }
  
    
    func userSelectedFilter(info: [String:Any]) {
        print(info)
        filterSelection = info
        let arrlocal = self.arrCategoryNames
        self.arrLocalCategory  = arrlocal.filter { $0.contains(searchText) }
        
        let tempArr = filterSelection.filter { $0.value as? Bool == true }
        print("tempArr")
        print(tempArr)
        
        let brandList = filterSelection["Brand"] as? NSArray ?? NSArray()
        let statusList = filterSelection["Status"] as? NSArray ?? NSArray()
        let originList = filterSelection["Origin"] as? NSArray ?? NSArray()
        let price = filterSelection["Price"] as! String
        
        self.arrProduct = arrBackupProduct

        
        if brandList.count > 0 {
            self.arrProduct.removeAll()
            for item in brandList {
                let arrCategoryTemp = arrBackupProduct.filter{$0.brand == (item as! String)}
                self.arrProduct.append(contentsOf: arrCategoryTemp)
            }
        }
        
        if statusList.count > 0 {
            self.arrProduct.removeAll()
            for item in statusList {
                let arrCategoryTemp = arrBackupProduct.filter{$0.status == (item as! String)}
                self.arrProduct.append(contentsOf: arrCategoryTemp)
            }
        }
        
        if originList.count > 0 {
            self.arrProduct.removeAll()
            for item in originList {
                let arrCategoryTemp = arrBackupProduct.filter{$0.origin == (item as! String)}
                self.arrProduct.append(contentsOf: arrCategoryTemp)
            }
        }

        
        if price == "Price High To Low" {

            let temp = self.arrProduct
            self.arrProduct.removeAll()
            self.arrProduct = temp.filter{ $0.availableQuantity ?? 0.0 > 5 }
            
            let temp2 = self.arrProduct
            self.arrProduct.removeAll()
            self.arrProduct = temp2.sorted(by: { $0.salesPrice! > $1.salesPrice!})
        } else  if price == "Price Low To High" {

            let temp = self.arrProduct
            self.arrProduct.removeAll()
            self.arrProduct = temp.filter{ $0.availableQuantity ?? 0.0 > 5 }
            
            let temp2 = self.arrProduct
            self.arrProduct.removeAll()
            self.arrProduct = temp2.sorted(by: { $0.salesPrice! < $1.salesPrice!})
        }
        else{
            let temp = self.arrProduct
            self.arrProduct.removeAll()
            self.arrProduct = temp.sorted(by: { $0.availableQuantity ?? 0.0  > $1.availableQuantity ?? 0.0 })
        }
        
        collectionV.reloadData()
    }
    
    //MARK:- Fetch All Product
    func fetchAllProductLocalStorge() -> [NewArrivalsModel] {
        if let syncContactsData = UserDefaults.standard.value(forKey: Constants.ProductModel) as? Data {
            if let aryProduct:[[String:Any]] = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(syncContactsData) as? [[String : Any]] {
                return Mapper<NewArrivalsModel>().mapArray(JSONArray: aryProduct)
            }
        }
        return []
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
    

        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        if updatedString!.length > 0 {
            self.searchText = updatedString!
            NSObject.cancelPreviousPerformRequests( withTarget: self, selector: #selector(searchDasboardProducts), object: nil)
            self.perform(#selector(searchDasboardProducts), with: nil, afterDelay: 0.5)
            
        } else {
            self.searchText = ""
            self.vwSearch.isHidden = true
            self.vwBaseSearch.isHidden = true
        }
        return true
         
    }
    
    //MARK:- search Dasboard Products
    @objc private func searchDasboardProducts() {        
        self.vwSearch.layer.cornerRadius = 15
        self.vwSearch.addShadow()
        let arrLocalProduct = self.arrAllProduct
        let arrlocal = self.arrCategoryNames

        self.arrLocalCategory  = arrlocal.filter { $0.localizedCaseInsensitiveContains(searchText) }
        
        self.arrlocalProduct = (arrLocalProduct.filter({(($0.productDescription ?? "").localizedCaseInsensitiveContains(searchText))}))
        
        if self.arrLocalCategory.count > 0 ||  self.arrlocalProduct.count > 0 {
            if searchText.length > 0 {
            self.vwSearch.isHidden = false
            self.vwBaseSearch.isHidden = false
                
                if self.arrLocalCategory.count > 4 ||  self.arrlocalProduct.count > 4 {
                      searchViewHeight.constant = CGFloat(444)
                    }
                    else{
                        if self.arrLocalCategory.count > self.arrlocalProduct.count {
                            searchViewHeight.constant = CGFloat(self.arrLocalCategory.count * 75)
                        }
                        if self.arrlocalProduct.count > self.arrLocalCategory.count {
                            searchViewHeight.constant = CGFloat(self.arrlocalProduct.count * 75)
                        }
                    }

            }
            else{
                self.vwSearch.isHidden = true
                self.vwBaseSearch.isHidden = true
            }
        }
        else{
            self.vwSearch.isHidden = true
            self.vwBaseSearch.isHidden = true
        }
        tblSearch.reloadData()
    }
    
    
    
    
    //MARK:- Set Navigation
    func SetNavigation(){
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        self.initMenuButton()
        menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
       /* self.initBellButton()
       bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
      
        self.initCartButton()
        cartButton.addTarget(self, action:#selector(cartButtonPressed), for: .touchUpInside)*/
        
        if self.shouldDisplayHome {
            
            if titleString == "" {
                self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [bellBarButton], title: "Product List")
            } else {
                self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [bellBarButton], title: titleString)
            }
            
            
            //self.lblTopTitle.text = Appmessage_NewArrivals.Placeholder.Favourites
        }
        else {
           
            if screenCommingFrom == "SubCategory" {
                if headerTitle == "" {
                    self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [bellBarButton], title: "Product List")
                }else {
                    self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [bellBarButton], title: headerTitle)
                }
               
                //self.lblTopTitle.text = "Product List"
            }
            else {
                if titleString == "" {
                    self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [bellBarButton], title: "Product List")
                } else {
                    self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [bellBarButton], title: titleString)
                }
                
                //self.lblTopTitle.text = Appmessage_NewArrivals.Placeholder.NewArrivalsProducts
            }
        }
    }
    
    //MARK:- Set Search
    func setSearchBar() {
        
        search.searchBarStyle = .minimal
        search.backgroundColor = UIColor.white
        search.tintColor = UIColor.white
        search.delegate = self
        if #available(iOS 13.0, *) {
            search.searchTextField.backgroundColor = UIColor().getSearchColor()
            search.searchTextField.textColor =  UIColor().getThemeColor()
            search.searchTextField.tintColor =  UIColor().getThemeColor()
            Utility.shared.makeRoundCorner(layer: search.searchTextField.layer, color:UIColor.white.withAlphaComponent(0.5), radius: 5)
            search.searchTextField.background = UIImage.init(named: "imageV1")
        }
        else {
        }
    }
    
    //MARK:- Menu Button
    @IBAction func filterButtonPressed(){
        let controller:SortAndFilterViewController =  UIStoryboard(storyboard: .Setting).initVC()
        controller.delegate = self
        controller.selectedCategoryID = selectedCatIdName
        controller.selectedSubCategoryID = selectedSubCatIdName
        controller.filterSelectionOpen = filterSelection
        self.navigationController?.pushViewController(controller, animated: false)
    }
    @objc func menuButtonPressed(){
        self.sideMenuController?.revealMenu()
    }
    
    //MARK:- Notification Button
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
    
    //MARK:- Cart Button
    @objc func cartButtonPressed(){
        
        let controller:CartSummaryViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
        controller.hidesBottomBarWhenPushed = true
        controller.screenCommingFrom = "ProductDetails"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func showCart(){
        isSideCartON = true
        UIView.animate(withDuration: 0.5, delay: 0.5, options: UIView.AnimationOptions(), animations: { () -> Void in
            self.sideCartView.frame.origin.x = self.view.frame.size.width - 80
            self.sideCartView.isHidden = false
            self.sideCartViewWidth.constant = 80
            self.viewDidLayoutSubviews()
                }, completion: { (finished: Bool) -> Void in
            })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.collectionV.reloadData()
        }
    }
    
    func addItemToCartAnimation(_ cell : NewArrivalsCollectionViewCell)  {
       /*
        print(cartImage.center)
        let bezierPath = UIBezierPath()
        bezierPath.move(to: cell.imageV.center)
        bezierPath.addQuadCurve(to: filterView.center, controlPoint: CGPoint(x:menuButton.center.y , y:menuButton.center.x ))
        
        let moveAnim = CAKeyframeAnimation(keyPath: "position")
        moveAnim.path = bezierPath.cgPath
        moveAnim.isRemovedOnCompletion = true
        
        let scaleAnim = CABasicAnimation(keyPath: "transform")
        scaleAnim.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
        scaleAnim.toValue = NSValue(caTransform3D: CATransform3DMakeScale(0.1, 0.1, 1.0))
        scaleAnim.isRemovedOnCompletion = true
        
        let opacityAnim = CABasicAnimation(keyPath: "alpha")
        opacityAnim.fromValue = NSNumber(value: 1.0)
        opacityAnim.toValue = NSNumber(value: 0.5)
        opacityAnim.isRemovedOnCompletion = true
        
        let animGroup = CAAnimationGroup()
        animGroup.delegate = self
        animGroup.setValue("curvedAnim", forKey: "animationName")
        animGroup.animations = [moveAnim,scaleAnim,opacityAnim]
        animGroup.duration = 0.9
        cell.imageV.layer.add(animGroup, forKey: "curvedAnim")
        */
        
       
        
        let imageViewPosition : CGPoint = cell.imageV.convert(cell.imageV.frame.origin, to: self.view)
        let imgViewTemp = UIImageView(frame: CGRect(x: imageViewPosition.x, y: imageViewPosition.y, width: cell.imageV.frame.size.width, height: cell.imageV.frame.size.height))
        
        imgViewTemp.image = cell.imageV.image
        animation(tempView: imgViewTemp)
  
    }
    
    
    func animation(tempView : UIView)  {
        

        self.view.addSubview(tempView)
        //1
        tempView.animationZoom(scaleX: 0.0, y: 0.0)

        UIView.animate(withDuration: 0.6,
                       animations: {
          //  1.5
            tempView.animationZoom(scaleX: 0.2, y: 0.2)
            tempView.animationRoted(angle: CGFloat(Double.pi))

        }, completion: { _ in
            
            UIView.animate(withDuration: 0.6, animations: {
                tempView.animationRoted(angle: CGFloat(Double.pi))
                tempView.animationZoom(scaleX: 0.2, y: 0.2)
                
                tempView.frame.origin.x = self.sideCartView.frame.origin.x + 20
                tempView.frame.origin.y = self.sideCartView.frame.origin.y + 40
                
            }, completion: { _ in
                
                tempView.removeFromSuperview()
                
                UIView.animate(withDuration: 1.0, animations: {
                    
                   // self.counterItem += 1
                   // self.lableNoOfCartItem.text = "\(self.counterItem)"
                    self.cartImage.animationZoom(scaleX: 1.4, y: 1.4)
                }, completion: {_ in
                    self.cartImage.animationZoom(scaleX: 1.0, y: 1.0)
                })
            })
        })
    }

    
    func hideCart(){
        self.isSideCartON = false
        UIView.animate(withDuration: 0.5, delay: 0.25, options: UIView.AnimationOptions(), animations: { () -> Void in
            self.sideCartView.frame.origin.x = self.view.frame.size.width + 80
            self.sideCartView.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.collectionV.reloadData()
            }
            self.sideCartViewWidth.constant = 0
            self.viewDidLayoutSubviews()
                }, completion: { (finished: Bool) -> Void in
            })
    }
    
    //MARK:- Button Action
    @IBAction func buttonSaveAndMakePaymentPressed(_ sender: Any) {
        
    }
    @IBAction func buttonBuyPaymentPressed(_ sender: Any) {
        
        let controller:CartConfirmationVC =  UIStoryboard(storyboard: .DashBoard).initVC()
        controller.ClassName = lastClass
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    // MARK: - GET Favourit List  API
    func getFavouritListAPI(){
        self.showLoader()
        
        EndPoint.GetFavouritList(t: FavouriteModel.self) { result in
            switch result {
            case .onSuccess(let items):
                if items.count > 0 {
                    self.arrFavourit = items
                    self.collectionV.reloadData()
                }
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- Cart Count Api
    func cartCountAPI() {
        EndPoint.CartCount(t: CartCountModel.self) { result in
            switch result {
            case .onSuccess(let data):
            if data != nil{
                dictCartData = data!
                if let cartCount = dictCartData.cartCount, cartCount != 0 {
                    self.setBagCountOnCart(count: cartCount)
                    if let tabItems = self.tabBarController?.tabBar.items {
                        // In this case we want to modify the badge number of the third tab:
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "\(cartCount)"
                        self.showCart()
                    }
                }else {
                    self.setBagCountOnCart(count: 0)
                    if let tabItems = self.tabBarController?.tabBar.items {
                        // In this case we want to modify the badge number of the third tab:
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "0"
                    }
                    self.hideCart()
                }
                if let notificationCount = dictCartData.notificationCount, notificationCount >= 0 {
                    self.setNotificationBagCount(count: notificationCount)
                }
            }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
}

extension NewArrivalsViewController: UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)  {
        searchBar.resignFirstResponder()
    }
}

extension UIView{
    func animationZoom(scaleX: CGFloat, y: CGFloat) {
        self.transform = CGAffineTransform(scaleX: scaleX, y: y)
    }
    
    func animationRoted(angle : CGFloat) {
        self.transform = self.transform.rotated(by: angle)
    }
}
