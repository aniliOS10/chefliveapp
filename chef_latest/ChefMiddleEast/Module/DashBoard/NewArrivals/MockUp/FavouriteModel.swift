//
//  FavouriteModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 23/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class FavouriteModel: BaseResponse {
    
    var favouriteId: Int?
    var userId: String?
    var isFavourite: Bool?
    var favourtieType: String?
    var favouriteMarkDate: String?
    var recipeId: Int?
    var recipeTitle: String?
    var recipeDate: String?
    var preparetime: String?
    var recipeImage: String?
    
 
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        favouriteId <- map["favouriteId"]
        userId <- map["userId"]
        isFavourite <- map["isFavourite"]
        favourtieType <- map["favourtieType"]
        favouriteMarkDate <- map["favouriteMarkDate"]
        recipeId <- map["recipeId"]
        recipeTitle <- map["recipeTitle"]
        recipeDate <- map["recipeDate"]
        preparetime <- map["preparetime"]
        recipeImage <- map["recipeImage"]
        
    }
}
