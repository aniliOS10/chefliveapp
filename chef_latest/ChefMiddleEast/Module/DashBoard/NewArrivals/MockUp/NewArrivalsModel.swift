//
//  NewArrivalsModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 22/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class NewArrivalsModel: BaseResponse {
    
    var productId: Int?
    var productDescription: String?
    var categoryId: String?
    var categoryImage: String?
    var subCategoryId: String?
    var subCategoryImage: String?
    var subSubCategoryId: String?
    var subSubCategoryImage: String?
    var itemGroupId: String?
    var dataOrigin: String?
    var itemRemarks: String?
    var itemResult: String?
    var salesPrice: Float?
    var salesUnit: String?
    var availableQuantity: Float?
    var productImage: NSArray?
    var accountNum: String?
    var agreementType: String?
    var agreementValue: Float?
    var fromDate: String?
    var toDate: String?
    var inventSiteId: String?
    var quantityFrom: String?
    var subProducts: NSArray?
    var subsituteProducts: NSArray?

    var variantType: String?
    var containerType: String?
    var productState: String?
    var brand: String?
    var feature1: String?
    var feature2: String?
    var feature3: String?
    var weight: String?
    var groupAttribute: String?
    var referenceCode: String?
    var ingredients: String?
    var origin: String?
    var packing: String?
    var status: String?
    var productImageStr: String?
    var isProductActive: Bool?

    var productDetailImageList =  [ProductDetailModel]()
 
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        productId <- map["productId"] 
        productDescription <- map["productDescription"]
        categoryId <- map["categoryId"]
        subCategoryId <- map["subCategoryId"]
        subSubCategoryId <- map["subSubCategoryId"]

        subCategoryId <- map["subCategoryId"]
        subSubCategoryImage <- map["subSubCategoryImage"]
        
        categoryImage <- map["categoryImage"]
        subCategoryImage <- map["subCategoryImage"]
        
      //  print(subCategoryImage ?? "NO")


        itemGroupId <- map["itemGroupId"]
        dataOrigin <- map["dataOrigin"]
        itemRemarks <- map["itemRemarks"]
        itemResult <- map["itemResult"]
        salesPrice <- map["salesPrice"]
        salesUnit <- map["salesUnit"]
        availableQuantity <- map["availableQuantity"]
        productImage <- map["productImages"] 
        accountNum <- map["accountNum"]
        agreementType <- map["agreementType"]
        agreementValue <- map["agreementValue"]
        fromDate <- map["fromDate"]
        toDate <- map["toDate"]
        inventSiteId <- map["inventSiteId"]
        quantityFrom <- map["quantityFrom"]
        
        variantType <- map["variantType"]
        containerType <- map["containerType"]
        productState <- map["productState"]
        brand <- map["brand"]
        feature1 <- map["feature1"]
        feature2 <- map["feature2"]
        feature3 <- map["feature3"]
        weight <- map["weight"]
        groupAttribute <- map["groupAttribute"]
        referenceCode <- map["referenceCode"]
        ingredients <- map["ingredients"]
        origin <- map["origin"]
        packing <- map["packing"]
        status <- map["status"]
        
        productDetailImageList <- map["productDetailImageList"]
        subProducts <- map["subProducts"]
        productImageStr <- map["productImage"]
        isProductActive <- map["isProductActive"]
        
        subsituteProducts  <- map["subsituteProducts"]

    }
}
