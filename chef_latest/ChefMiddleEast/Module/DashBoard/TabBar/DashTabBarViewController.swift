//
//  DashTabBarViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 01/11/21.
//

import UIKit

class DashTabBarViewController: UITabBarController,UITabBarControllerDelegate {
    
    static var sharedTabBar:DashTabBarViewController = DashTabBarViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.delegate = self
         self.tabBar.tintColor = UIColor.white
         self.tabBar.unselectedItemTintColor = UIColor.white
        
        
      //  UIColor().getThemeColor()
        self.tabBar.barTintColor = UIColor (red: 158.0/255.0, green: 11.0/255.0, blue: 42/255.0, alpha: 1.0)
        self.tabBar.isTranslucent = false
        self.tabBar.backgroundColor = UIColor (red: 158.0/255.0, green: 11.0/255.0, blue: 42/255.0, alpha: 1.0)
       
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
            
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)

        NotificationCenter.default.addObserver(self, selector: #selector(self.HomeViewOpen(notification:)), name: Notification.Name("HomeViewShow"), object: nil)
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            viewControllers?.remove(at: 3)
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
//        for controller in self.viewControllers ?? []{
//            if  controller != self.selectedViewController
//            {
//                if  controller.isKind(of: UINavigationController.self){
//                    if  let  navi  = controller as? UINavigationController{
//                        navi.popToRootViewController(animated: true)
//                    }
//                }
//            }
//        }
        
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {

        let selectedIndex = tabBarController.selectedIndex
        if selectedIndex == 3{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GetUserProfileData"), object: nil)
        }
        
        let rootView = self.viewControllers?[self.selectedIndex] as? UINavigationController
        if rootView != nil {
            rootView?.popToRootViewController(animated: false)
        }
    }
    

    @objc func HomeViewOpen(notification: Notification) {
                
        let rootView = self.viewControllers?[self.selectedIndex] as? UINavigationController
        if rootView != nil {
            rootView?.popToRootViewController(animated: false)
        }
        self.selectedIndex = 0
    }

}
