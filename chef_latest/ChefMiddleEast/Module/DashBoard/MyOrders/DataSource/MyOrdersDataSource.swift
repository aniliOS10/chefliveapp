//
//  MyOrdersDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 01/11/21.
//

import Foundation
import UIKit

@available(iOS 13.0, *)
extension MyOrdersViewController:UITableViewDataSource,UITableViewDelegate{
   
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.0 :0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        /*if  isHistory{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderHistoryTableViewCell") as! MyOrderHistoryTableViewCell
            Utility.shared.makeShadowsOfView_roundCorner(view: cell.cellViewRound, shadowRadius: 4.0, cornerRadius: 15, borderColor: UIColor.lightGray.withAlphaComponent(0.5))
            Utility.shared.makeRoundCorner(layer: cell.cellViewRound.layer, color: UIColor.lightGray.withAlphaComponent(0.1), radius: 5.0)
            return cell
        }*/
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderTableViewCell") as! MyOrderTableViewCell
        cell.lblOrderNo.text = self.arrMyOrders[indexPath.row].salesOrderId
                
        let dateStr = self.arrMyOrders[indexPath.row].orderDate
        cell.lblDate.text = dateStr?.changeDateFormat_String(dateStr ?? "")
        
        let deliveryDateStr = self.arrMyOrders[indexPath.row].deliveryDate
        cell.lblDeliveryDate.text = deliveryDateStr?.changeDateFormat_String(deliveryDateStr ?? "")
        
        cell.lblTotalAmount.text = String(format: "AED %.2f", self.arrMyOrders[indexPath.row].totalAmount ?? "")
        cell.lblProductItemsCount.text = "\(self.arrMyOrders[indexPath.row].products?.count ?? 0) Items"
        cell.btnOrderStatus.setTitle(self.arrMyOrders[indexPath.row].status, for: .normal)
        cell.arrProducts = self.arrMyOrders[indexPath.row].products ?? []
        cell.tagRow = indexPath.row
        cell.tagSection = indexPath.section
        cell.cellViewRound.addShadow()
        cell.cellViewRound.layer.cornerRadius = 5
        cell.cellViewRound2.addShadow()
        cell.cellViewRound2.layer.cornerRadius = 5
        let width = (UIScreen.main.bounds.size.width/4)
        cell.collectionHeight.constant = width + 5
        cell.collectionV.dataSource = cell
        cell.collectionV.delegate = cell
        cell.collectionV.reloadData()
        
        cell.btnViewDetails.tag = indexPath.row
        cell.btnViewDetails.addTarget(self, action: #selector(btnViewDetailsTap(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let cell = tableView.cellForRow(at: indexPath){
            //self.didSelectRow!(cell,indexPath)
        }
        
    }
    
    //MARK:- View Details Tap
    @objc func btnViewDetailsTap(sender: UIButton){
       
        if let salesOrderId = self.arrMyOrders[sender.tag].salesOrderId, !salesOrderId.isEmpty {
            let controller:OrderDetailViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            controller.salesOrderId = salesOrderId
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

/*
class DashBoardCollectionViewDataSource:GenericDataSource<MockUp_Dashboard>,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
    return self.data.value.count
}
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
    if let cell: DashBoardCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashBoardCollectionViewCell", for: indexPath) as? DashBoardCollectionViewCell
    {
        let categoryList = self.data.value[indexPath.row]
        cell.lblTitle.text = categoryList.title
        cell.imgView.image = categoryList.image
        //cell.imageView.image =  self.imageArray[indexPath.row] as? UIImage
    return cell
}
        return UICollectionViewCell()
}
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
    let size = CGSize(width: (UIScreen.main.bounds.size.width/2)-10/2, height: (UIScreen.main.bounds.size.width/2) - 10/2)
    return size
   // return UICollectionViewFlowLayout.automaticSize
}
}
*/
