//
//  CardModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 25/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class MyOrdersModel: BaseResponse {
    
    var salesOrderId : String?
    var orderDate : String?
    var totalAmount : Float?
    var status : String?
    var deliveryDate : String?
    var products : [NewArrivalsModel]?
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        salesOrderId <- map["salesOrderId"]
        orderDate <- map["orderDate"]
        totalAmount <- map["totalAmount"]
        status <- map["status"]
        deliveryDate <- map["deliveryDate"]
        products <- map["products"]
    }
}
