//
//  MyOrdersViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 01/11/21.
//

import UIKit

@available(iOS 13.0, *)
class MyOrdersViewController:BaseViewController {
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var search : UISearchBar!
    @IBOutlet weak var segmentController: UISegmentedControl!
    
    @IBOutlet weak var topTableViewConst: NSLayoutConstraint!
    @IBOutlet weak var lbeNoData : UILabel!
    @IBOutlet weak var imgViewNoData : UIImageView!

    
    var arrMyOrders = [MyOrdersModel]()
    var screenCommingFrom = String()
    
    //MARK:- VC Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        EndPoint.filterWith = ""
        if let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            self.orderListAPI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbeNoData.isHidden = false
        imgViewNoData.isHidden = false
        self.setUpNavigation()
        self.setSearchBar()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
            if #available(iOS 15.0, *) {
                tableView.sectionHeaderTopPadding = 3
            }
        }
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            segmentController.isHidden = true
            topTableViewConst.constant = 5
        }
        else{
            self.setSegmentControllerUI()
        }
    }
    
    //MARK:- Setup Navigation
    func setUpNavigation(){
        
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        backButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
        initMenuButton()
        menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            if screenCommingFrom == "ViewOrderButtonTap" {
                self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [], title:Appmessage_MyOrders.Placeholder.MyOrders )
            }
            else{
                self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: Appmessage_MyOrders.Placeholder.MyOrders)
            }
        }else{
           self.initBellButton()
           bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
            if screenCommingFrom == "ViewOrderButtonTap" {
                self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [bellBarButton], title:Appmessage_MyOrders.Placeholder.MyOrders )
            }
            else{
                self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: Appmessage_MyOrders.Placeholder.MyOrders)
            }
        }
        
        
        
    }
    
    //MARK:- Set Search Bar
    func setSearchBar(){
        
        search.searchBarStyle = .minimal
        search.backgroundColor = UIColor.white
        search.tintColor = UIColor.white
        
        if #available(iOS 13.0, *) {
            search.searchTextField.backgroundColor = UIColor().getSearchColor()
            search.searchTextField.textColor =  UIColor().getThemeColor()
            search.searchTextField.tintColor =  UIColor().getThemeColor()
            Utility.shared.makeRoundCorner(layer: search.searchTextField.layer, color:UIColor.white.withAlphaComponent(0.5), radius: 5)
           
            let white = UIColor.red
            search.searchTextField.background = UIImage.init(named: "imageV1")
        }
        else {
            // Fallback on earlier versions
        }
    }
    
    
    //MARK:- Menu Button
    @objc func menuButtonPressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Notification Button
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
    
    //MARK:- Set Segment Controller UI
    func setSegmentControllerUI() {
        if #available(iOS 13.0, *) {
            let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)]
            segmentController.setTitleTextAttributes(titleTextAttributes, for:.normal)
            
            let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)]
            segmentController.setTitleTextAttributes(titleTextAttributes1, for:.selected)
        }
        else {
            // Fallback on earlier versions
        }
        segmentController.tintColor = UIColor.white
    }
    
    //MARK:- Button Action
    @IBAction func buttonSaveAndMakePaymentPressed(_ sender: Any) {
        
    }
   
    @IBAction func segmentControllerValueChanged(_ sender: Any) {
        EndPoint.filterWith = ""
        if segmentController.selectedSegmentIndex == 1 {
            EndPoint.filterWith = "past"
        }
        if let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            self.orderListAPI()
        }
    }
    
    // MARK: - Recipe List API
    func orderListAPI(){
        self.showLoader()
        EndPoint.getOrderList(t: MyOrdersModel.self) { result in
            self.arrMyOrders = []
            switch result {
            case .onSuccess(let items):
                self.hideLoader()
                if items.count > 0 {
                    self.lbeNoData.isHidden = true
                    self.imgViewNoData.isHidden = true
                    self.arrMyOrders = items
                }
                else{
                    self.lbeNoData.isHidden = false
                    self.imgViewNoData.isHidden = false
                }
                self.tableView.reloadData()
            case .onFailure(let error):
                print(error)
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    
    
}
