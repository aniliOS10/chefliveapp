//
//  MyOrderHistoryTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 08/11/21.
//

import UIKit

class MyOrderHistoryTableViewCell: UITableViewCell {
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var detailButton: UIButton!
    @IBOutlet weak var reOrderButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
