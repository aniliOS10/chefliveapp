//
//  MyOrderTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 01/11/21.
//

import UIKit
import FloatRatingView
@available(iOS 13.0, *)
class MyOrderTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout   {
    @IBOutlet weak var collectionV: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var cellViewRound2: UIView!
    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDeliveryDate: UILabel!
    @IBOutlet weak var lblProductItemsCount: UILabel!
    @IBOutlet weak var btnOrderStatus: UIButton!
    @IBOutlet weak var btnViewDetails: UIButton!

    
    var tagRow:Int = 0
    var tagSection:Int = 0
    var arrProducts = [NewArrivalsModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MYOrderCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MYOrderCollectionViewCell", for: indexPath) as! MYOrderCollectionViewCell
        cell.cellViewRound.addShadow()
        cell.cellViewRound.layer.cornerRadius = 5
        cell.imageV.layer.cornerRadius = 5
        
        
        if (arrProducts[indexPath.row].productImageStr != nil) {
            let imageData = arrProducts[indexPath.row].productImageStr
            
            let urlString = imageData?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

            
            if let url = URL(string: urlString ?? ""){
                
                
            cell.imageV?.sd_setImage(with: url) { (image, error, cache, urls) in
                if (error != nil) {
                    cell.imageV.image = UIImage(named: "placeholder")
                } else {
                    cell.imageV.image = image
                }
            }
        }else {
            cell.imageV.image = UIImage(named: "placeholder")
        }
        }
        else{
            cell.imageV.image = UIImage(named: "placeholder")
        }
        
        
        /*
        if let imgUrl = arrProducts[indexPath.row].productImage, !imgUrl.isEmpty {
            cell.imageV?.sd_setImage(with: URL.init(string:("\(imgUrl)"))) { (image, error, cache, urls) in
                if (error != nil) {
                    cell.imageV.image = UIImage(named: "placeholder")
                } else {
                    cell.imageV.image = image
                }
            }
        }
        else {
            cell.imageV.image = UIImage(named: "placeholder")
        }
         */
        
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let width = (UIScreen.main.bounds.size.width/4)-15
        let size = CGSize(width: width, height: width - 5)
        return size
    }
}
