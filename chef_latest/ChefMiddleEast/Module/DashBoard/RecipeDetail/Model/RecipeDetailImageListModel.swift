//
//  RecipeDetailImageList.swift
//  ChefMiddleEast
//
//  Created by sandeep on 23/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class RecipeDetailImageListModel: BaseResponse {
    
    
    var recipeDetailImageId: String?
    var recipeId: Int?
    var recipeDetailImagePath: String?
    
    
    override init() {
        super.init()
    }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        recipeDetailImageId <- map["recipeDetailImageId"]
        recipeId <- map["recipeId"]
        recipeDetailImagePath <- map["recipeDetailImagePath"]
    }
    
}
