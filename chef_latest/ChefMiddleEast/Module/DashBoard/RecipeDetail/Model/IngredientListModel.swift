//
//  IngredientListModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 23/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class IngredientListModel: BaseResponse {
    
    
    var productId: Int?
    var ingredientName: String?
    var quantity: Int?
    var minimumQrder: Int?
    var unit: String?
    var availableQuantity: Int?
    
    override init() {
        super.init()
    }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        productId <- map["productId"]
        ingredientName <- map["ingredientName"]
        quantity <- map["quantity"]
        minimumQrder <- map["minimumOrder"]
        unit <- map["unit"]
        availableQuantity <- map["availableQuantity"]
    }
    
}

