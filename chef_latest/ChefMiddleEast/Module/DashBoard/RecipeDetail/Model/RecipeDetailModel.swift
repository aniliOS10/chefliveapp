//
//  RecipeDetailModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 19/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class RecipeDetailModel: BaseResponse{
   
    var recipeId: String?
    var recipeTitle: String?
    var recipeDate: String?
    var preparetime: String?
    var recipeImage: String?
    var recipeDescription: String?
    var keyIngredients: String?
    var topTips: String?
    var category: String?
    var serving: String?
    var cookingTime: String?
    var calories: String?
    var videoUrl: String?
    var recipeNote: String?
    var difficultyLevel: Int?
    var directions: String?
    var preparetimeDesc: String?
    var cookingTimeDesc: String?

    
    var nutritionList = [NutritionListModel]()
    var ingredientList = [IngredientListModel]()
    var ingredientToShow = [IngredientListModel]()
    var recipeDetailImageList = [RecipeDetailImageListModel]()
    var ingredientShowList = [IngredientShowListModel]()
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        cookingTimeDesc <- map["cookingTimeDesc"]
        preparetimeDesc <- map["preparetimeDesc"]
        recipeId <- map["recipeId"]
        recipeTitle <- map["recipeTitle"]
        recipeDate <- map["recipeDate"]
        preparetime <- map["preparetime"]
        recipeImage <- map["recipeImage"]
        recipeDescription <- map["recipeDescription"]
        keyIngredients <- map["keyIngredients"]
        topTips <- map["topTips"]
        category <- map["category"]
        serving <- map["serving"]
        cookingTime <- map["cookingTime"]
        calories <- map["calories"]
        videoUrl <- map["videoUrl"]
        recipeNote <- map["recipeNote"]
        difficultyLevel <- map["difficultyLevel"]
        directions <- map["directions"]
        
        
        nutritionList <- map["nutritionList"]
      //  ingredientList <- map["ingredientList"]
        ingredientList <- map["ingredientToSale"]
        ingredientToShow <- map["ingredientToShow"]
        recipeDetailImageList <- map["recipeDetailImageList"]
        ingredientShowList <- map["ingredientToShow"]
    }
}
