//
//  IngredientShowListModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class IngredientShowListModel : BaseResponse {
    
    var ingredientName: String?
    var unit: String?
    var quantity: String?
    var ingredientToShowId: Int?
    var productId: Int?
    
    override init() {
        super.init()
    }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        ingredientToShowId <- map["ingredientToShowId"]
        productId <- map["productId"]
        ingredientName <- map["ingredientName"]
        quantity <- map["quantity"]
        unit <- map["unit"]
    }
    
}
