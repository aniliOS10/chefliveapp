//
//  NutritionListModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 23/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class NutritionListModel: BaseResponse {
    
    
     var nutritionType: String?
     var quantity: Int?
     var unit: String?
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        nutritionType <- map["nutritionType"]
        quantity <- map["quantity"]
        unit <- map["unit"]
        
    }
    
}
