//
//  RecipeDetailViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/11/21.
//

import UIKit
import Alamofire

@available(iOS 13.0, *)
class RecipeDetailViewController:BaseViewController, RecipeDetailAddToCartTableViewCellDelegate {
    
    
    @IBOutlet weak var btnGoToCheckout : UIButton!
    @IBOutlet weak var btnContinueShopping : UIButton!
    @IBOutlet weak var addToCartBaseView : UIView!
    @IBOutlet weak var addToCartOptionView : UIView!
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var buttonPayment: UIButton!
    

    var dictRecipeDetail = RecipeDetailModel()
    var arrIngredientList = [IngredientListModel]()
    var recipeId = Int()
    var addQty = Int()
    var productId = Int()
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        EndPoint.recipeId = "\(recipeId)"
        EndPoint.deviceType = "ios"
        self.recipeDetailAPI()
        self.tableView.delegate = self
        self.tableView.dataSource = self

        self.tableView.contentInsetAdjustmentBehavior = .never
        if #available(iOS 15.0, *) {
            self.tableView.sectionHeaderTopPadding = 0
        }
        addToCartBaseView.isHidden = true
        addToCartOptionView.layer.cornerRadius = 10.0
        btnContinueShopping.layer.cornerRadius = 20
        btnContinueShopping.addShadow()
        btnGoToCheckout.layer.cornerRadius = 20
        btnGoToCheckout.addShadow()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initBackButton()
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: "Recipe Details")
    }
    
    func addDefaultQty() {
        
        if !arrIngredientList.isEmpty {
            for index in 0..<arrIngredientList.count {
                let productId = arrIngredientList[index].productId ?? 0
                var qty = arrIngredientList[index].minimumQrder
                if qty == nil {
                    qty = 0
                }
                addQty = qty!                
                self.addAndRemoveIngredientsQtyAndIndex(qty: addQty, productId: productId)
            }
        }
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func onClickContinueShoppingAcn() {
        addToCartBaseView.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickGoToCartAcn() {
        
        addToCartBaseView.isHidden = true
        let controller:CartSummaryViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
        controller.screenCommingFrom = "ProductDetails"
        self.navigationController!.pushViewController(controller, animated: true)
        
    }
    
    // MARK: - Menu Button
    @objc func menuButtonPressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Notification Button
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
    
    // MARK: - Button Action
    @IBAction func buttonBackButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonAddToCartPressed(_ sender: Any) {
   
        var isFalse = Bool()
        if dictRecipeDetail.ingredientList.count > 0 {
            for item in self.arrIngredientList {
                if item.availableQuantity ?? 0 > 5 {
                    isFalse = true
                }
            }
        }
    
        var flag : Bool = false
        let param = AddToCartModel()
        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            param.userId = userId
        }
        for info in dictRecipeDetail.ingredientList{
            if info.quantity ?? 0 > 0 {
                flag = true
                let paramProduct = ProductModel()
                paramProduct.productId = info.productId
                paramProduct.productQuantity = info.quantity
                param.product.append(paramProduct)
            }
        }
            if flag {
                if isFalse {
                    self.addToCartAPI(Model: param)
                }
            }
        else {
            
        }
    }
    
    @IBAction func segmentControllerValueChanged(_ sender: Any) {
        
        /*
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { 
            let indexPath = IndexPath(row: 3, section: 0)
            self.tableView.beginUpdates()
            self.tableView.reloadRows(at: [indexPath], with: .fade)
            self.tableView.endUpdates()
        }
        */
        
        self.tableView.reloadData()
    }
    
    //MARK:- Custome Delegate
    func addAndRemoveIngredientsQtyAndIndex(qty: Int, productId: Int) {
        self.addQty = qty
        self.productId = productId
        let arrIngred = dictRecipeDetail.ingredientList
        let index = arrIngred.firstIndex{ $0.productId == self.productId}
        if arrIngred.count > 0 {
            self.dictRecipeDetail.ingredientList[index!].quantity = self.addQty
        }
    }
    
    // MARK: - Recipe Detail  API
    func recipeDetailAPI(){
        self.tableView.isHidden = true
        self.showLoader()
        EndPoint.RecipeDetail(t: RecipeDetailModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccess(let data):
                if data != nil {
                    self.dictRecipeDetail = data!
                    self.arrIngredientList = self.dictRecipeDetail.ingredientList.isEmpty == true ? self.dictRecipeDetail.ingredientToShow : self.dictRecipeDetail.ingredientList
                    
                    self.addDefaultQty()
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                }
                else{
                    self.tableView.isHidden = true
                    self.tableView.reloadData()
                }
            case .onFailure(let error):
                print(error)
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- Add to Cart Api
    func addToCartAPI(Model: AddToCartModel) {
        
        EndPoint.AddToCart(params: Model,t: AddToCartModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccessWithStringValue(let msg):
                if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                    EndPoint.userId = userId
                    self.cartCountAPI()
                }
                NotificationAlert().NotificationAlert(titles: msg)
                self.tabBarController?.tabBar.isHidden = true
                self.addToCartBaseView.isHidden = false
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- Cart Count Api
    func cartCountAPI() {
        
        EndPoint.CartCount(t: CartCountModel.self) { result in
            switch result {
            case .onSuccess(let data):
            if data != nil{
                dictCartData = data!
                if let cartCount = dictCartData.cartCount, cartCount != 0 {
                    self.setBagCountOnCart(count: cartCount)
                }
            }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
}






