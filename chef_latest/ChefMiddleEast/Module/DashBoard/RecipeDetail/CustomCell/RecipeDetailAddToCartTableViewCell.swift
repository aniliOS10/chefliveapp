//
//  RecipeDetailRatingVedioTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/11/21.
//

import UIKit

protocol RecipeDetailAddToCartTableViewCellDelegate {
    func addAndRemoveIngredientsQtyAndIndex(qty: Int,productId:Int)
}

class RecipeDetailAddToCartTableViewCell: UITableViewCell {
    
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var cellViewRound1: UIView!
    @IBOutlet weak var cellViewRound2: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var ingredientSaleViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var lblRecipeNotes: UILabel!
    
    @IBOutlet weak var ingredientToShowTblHeight: NSLayoutConstraint!
    @IBOutlet weak var ingredientToShowViewHeight: NSLayoutConstraint!
    @IBOutlet weak var ingredientToShowTblView: UITableView!
    @IBOutlet weak var ingredientToShowBaseView: UIView!
    
    @IBOutlet weak var imageVLineR: UIImageView!
    @IBOutlet weak var lblR: UILabel!

    @IBOutlet weak var lblRHeight: NSLayoutConstraint!

    @IBOutlet weak var cellAddToCarts: UIView!
    @IBOutlet weak var recipesTop: NSLayoutConstraint!
    @IBOutlet weak var recipesBottom: NSLayoutConstraint!
    @IBOutlet weak var btnAddToCartHeight: NSLayoutConstraint!


    
    var arrrRecipeDetailImageList = [RecipeDetailImageListModel]()
    var arrrNutritionList = [NutritionListModel]()
    var arrIngredientList = [IngredientListModel]()
    var arrIngredientShowList = [IngredientShowListModel]()
    var dictRecipeDetail = RecipeDetailModel()

    var addQty = Int()
    var delegate : RecipeDetailAddToCartTableViewCellDelegate!
    var isFalse = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.tableFooterView = UIView()
        if #available(iOS 11.0, *) {
            ingredientToShowTblView.contentInsetAdjustmentBehavior = .never
            if #available(iOS 15.0, *) {
                ingredientToShowTblView.sectionHeaderTopPadding = 13
            } else {
              //  self.ingredientToShowTblView.contentInset = UIEdgeInsets(top: -13, left: 0, bottom: 0, right: 0)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension RecipeDetailAddToCartTableViewCell : UITableViewDataSource,UITableViewDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return nil
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if tableView == ingredientToShowTblView {
            if self.segmentController.selectedSegmentIndex == 0{
                return arrIngredientShowList.count
            } else if self.segmentController.selectedSegmentIndex == 1 {
                return 1
            } else if self.segmentController.selectedSegmentIndex == 2 {
                return 1
            } else if self.segmentController.selectedSegmentIndex == 3 {
                return arrrNutritionList.count
            }
        }else {
            
            if dictRecipeDetail.ingredientList.count > 0 {
                
                for item in self.arrIngredientList {
                    if item.availableQuantity ?? 0 > 5 {
                        isFalse = true
                    }
                }
                
                if !isFalse {
                    self.btnAddToCartHeight.constant = 0
                }
                else{
                    self.btnAddToCartHeight.constant = 50
                }
                 return arrIngredientList.count

            }
            self.ingredientSaleViewHeight.constant = 0
            self.lblRHeight.constant = 0
            self.recipesTop.constant = 0
            self.recipesBottom.constant = 0
            self.btnAddToCartHeight.constant = 0

            self.imageVLineR.isHidden = true
            self.lblR.isHidden = true
            cellAddToCarts.isHidden = true

            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        
        if tableView == ingredientToShowTblView {
            if self.segmentController.selectedSegmentIndex == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeDetailIngredientsShowTblCell") as! RecipeDetailIngredientsShowTblCell
                    cell.selectionStyle = .none
                    if arrIngredientShowList.count > 0{
                        cell.lblIngredientName.text = arrIngredientShowList[indexPath.row].ingredientName
                        cell.lblQuantity.text = String("\(arrIngredientShowList[indexPath.row].quantity!)\(" ")\(arrIngredientShowList[indexPath.row].unit!)")
                    }
                    return cell
            }
            else if self.segmentController.selectedSegmentIndex == 1{
                    let cell = ingredientToShowTblView.dequeueReusableCell(withIdentifier: "RecipeDetailDirectionTVC") as! RecipeDetailDirectionTVC
                cell.selectionStyle = .none
                cell.lblDescription.text = dictRecipeDetail.directions
                    return cell
            }
            else if self.segmentController.selectedSegmentIndex == 2{
                    let cell = ingredientToShowTblView.dequeueReusableCell(withIdentifier: "RecipeDetailTipsTableViewCell") as! RecipeDetailTipsTableViewCell
                    cell.selectionStyle = .none
                    cell.lblDescription.text = dictRecipeDetail.topTips
                    return cell
            } else {
                    let cell = ingredientToShowTblView.dequeueReusableCell(withIdentifier: "RecipeDetailNutritionTableViewCell") as! RecipeDetailNutritionTableViewCell
                    cell.selectionStyle = .none

                    if arrrNutritionList.count > 0{
                        cell.lblNutritionName.text = arrrNutritionList[indexPath.row].nutritionType
                        let unit = arrrNutritionList[indexPath.row].unit ?? ""
                        let qty = arrrNutritionList[indexPath.row].quantity ?? 1
                        cell.lblQuantityAndUnit.text = "\(qty) " + unit
                    }
                
                if arrrNutritionList.count == indexPath.row + 1{
                    cell.lblLine.isHidden = true
                }
                else{
                    cell.lblLine.isHidden = false
                }
                
                    return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeDetailIngredientsTableViewCell") as! RecipeDetailIngredientsTableViewCell
            cell.selectionStyle = .none
            cell.btnImgIngredient.imageView?.contentMode = .scaleAspectFit
            
            if dictRecipeDetail.ingredientList.count > 0 {
                if arrIngredientList.count > 0{
                    cell.lblIngredientName.text = arrIngredientList[indexPath.row].ingredientName
                    cell.lblQuantity.text = "\(arrIngredientList[indexPath.row].quantity ?? 1)"
                    cell.btnAdd.tag = indexPath.row
                    cell.btnSub.tag = indexPath.row
                    cell.btnAdd.addTarget(self, action: #selector(addQty(sender:)), for: .touchUpInside)
                    cell.btnSub.addTarget(self, action: #selector(addSubtract(sender:)), for: .touchUpInside)
                    
                    if arrIngredientList[indexPath.row].availableQuantity ?? 0 > 5 {
                        cell.lblQuantity.isHidden = false
                        cell.btnAdd.isHidden = false
                        cell.btnSub.isHidden = false
                        cell.lblComingSoon.isHidden = true
                    }
                    else{
                        cell.lblQuantity.isHidden = true
                        cell.btnAdd.isHidden = true
                        cell.btnSub.isHidden = true
                        cell.lblComingSoon.isHidden = false
                    }
                    
                    if arrIngredientList.count == indexPath.row + 1{
                        cell.lblLine.isHidden = true
                    }
                    else{
                        cell.lblLine.isHidden = false
                    }
                }
                
                
            }
            else{
                self.ingredientSaleViewHeight.constant = 0
                self.lblRHeight.constant = 0
                self.imageVLineR.isHidden = true
                self.lblR.isHidden = true
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView ==  ingredientToShowTblView  {
            if self.segmentController.selectedSegmentIndex == 0 {
                return 30
            }
            if self.segmentController.selectedSegmentIndex == 2 {
                                
                return UITableView.automaticDimension
            }
            else {
                return UITableView.automaticDimension
            }
        } else if tableView == tableView {
            return UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
    
    //MARK:- Add Qty
    @objc func addQty(sender: UIButton){
        
        if !arrIngredientList.isEmpty {
            let productId = arrIngredientList[sender.tag].productId ?? 0
            var qty = arrIngredientList[sender.tag].quantity
            
            if qty == nil {
                qty = 0
            }
            
            if qty ?? 0 < 15 {
                addQty = qty! + 1
                arrIngredientList[sender.tag].quantity = addQty
                self.delegate.addAndRemoveIngredientsQtyAndIndex(qty: addQty, productId: productId)
            }
            else{
                NotificationAlert().NotificationAlert(titles: "You have reached maximum order quantity")
            }
        }
        self.tableView.reloadData()
    }
    
    //MARK:- Remove Qty
    @objc func addSubtract(sender: UIButton){
        if !arrIngredientList.isEmpty {
            let productId = arrIngredientList[sender.tag].productId ?? 0
            if let qty = arrIngredientList[sender.tag].quantity{
                
                if qty != 0 {
                    addQty = qty - 1
                }
                else{
                    addQty = 0
                }
                arrIngredientList[sender.tag].quantity = addQty
            }
            self.delegate.addAndRemoveIngredientsQtyAndIndex(qty: addQty, productId: productId)
        }
        self.tableView.reloadData()
    }
}
