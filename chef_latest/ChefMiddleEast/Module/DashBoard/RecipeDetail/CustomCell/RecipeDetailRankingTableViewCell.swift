//
//  RecipeDetailRankingTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/11/21.
//

import UIKit

class RecipeDetailRankingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var btnCategory1Title: UIButton!
    @IBOutlet weak var btnCategory2Title: UIButton!
    @IBOutlet weak var btnCategory3Title: UIButton!
    @IBOutlet weak var btnCategory4Title: UIButton!
    @IBOutlet weak var btnCategory5Title: UIButton!
    
    @IBOutlet weak var viewCategory1: UIView!
    
    @IBOutlet weak var viewCategory2: UIView!
    
    @IBOutlet weak var viewCategory3: UIView!
    
    @IBOutlet weak var viewCategory4: UIView!
    @IBOutlet weak var viewCategory5: UIView!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
