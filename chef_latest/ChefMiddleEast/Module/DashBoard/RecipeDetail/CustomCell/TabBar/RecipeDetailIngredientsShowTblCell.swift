//
//  RecipeDetailIngredientsShowTblCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class RecipeDetailIngredientsShowTblCell: UITableViewCell {

    @IBOutlet weak var lblIngredientName : UILabel!
    @IBOutlet weak var lblQuantity : UILabel!
    @IBOutlet weak var imgIngredient : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
