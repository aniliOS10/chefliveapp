//
//  CategoryListIngredientsTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 09/11/21.
//

import UIKit

class RecipeDetailIngredientsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblIngredientName : UILabel!
    @IBOutlet weak var lblQuantity : UILabel!
    @IBOutlet weak var lblUnit : UILabel!
    @IBOutlet weak var btnAdd : UIButton!
    @IBOutlet weak var btnSub : UIButton!
    @IBOutlet weak var btnImgIngredient : UIButton!
    @IBOutlet weak var lblComingSoon : UILabel!

    @IBOutlet weak var lblLine : UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
