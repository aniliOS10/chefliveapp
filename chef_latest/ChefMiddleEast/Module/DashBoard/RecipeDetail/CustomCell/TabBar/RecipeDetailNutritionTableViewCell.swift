//
//  CategoryListNutritionTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 09/11/21.
//

import UIKit

class RecipeDetailNutritionTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNutritionName : UILabel!
    @IBOutlet weak var lblQuantityAndUnit : UILabel!
    @IBOutlet weak var lblLine : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
