//
//  RecipeDetailVedioTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/11/21.
//

import UIKit
import youtube_ios_player_helper

class RecipeDetailVedioTableViewCell: UITableViewCell, YTPlayerViewDelegate {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var playVedioButton: UIButton!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var ytPlayerView: YTPlayerView!
    var isLoaded : Bool = false
    
    
    var url:URL? {
        didSet {
            
            if let youTubeVideoId = url?.absoluteString {
                self.ytPlayerView.load(withVideoId: youTubeVideoId)
                self.ytPlayerView.delegate = self
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: YT PlayerView Delegate
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        //playerView.playVideo()
    }
    
    func playerViewPreferredWebViewBackgroundColor(_ playerView: YTPlayerView) -> UIColor {
        return .clear
    }
    
    func playerViewPreferredInitialLoading(_ playerView: YTPlayerView) -> UIView? {
        let indictor = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        indictor.startAnimating()
        indictor.color = UIColor().getThemeColor()
        return indictor
    }

}
extension String {
    var youTubeVideoID: String? {
        if self.contains("youtube") || self.contains("youtu.be") {
            let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
            let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            let range = NSRange(location: 0, length: count)
            guard let result = regex?.firstMatch(in: self, range: range) else {
                return nil
            }
            return (self as NSString).substring(with: result.range)
        }
        return nil
    }
}
