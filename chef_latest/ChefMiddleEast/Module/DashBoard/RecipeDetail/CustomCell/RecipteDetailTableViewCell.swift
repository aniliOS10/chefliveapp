//
//  RecipteDetailTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/11/21.
//

import UIKit

class RecipteDetailTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lblRecipeDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTopTip: UILabel!
    @IBOutlet weak var lblKeyIngredint: UILabel!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var cellViewRoundHeight: NSLayoutConstraint!
    @IBOutlet weak var cellViewRound2Height: NSLayoutConstraint!
    @IBOutlet weak var cellViewRound2: UIView!
    @IBOutlet weak var AddLittelView: UIView!
    @IBOutlet weak var KeyIngredientView: UIView!
    @IBOutlet weak var TopTipView: UIView!
    @IBOutlet weak var imgDifficulty1: UIImageView!
    @IBOutlet weak var imgDifficulty2: UIImageView!
    @IBOutlet weak var imgDifficulty3: UIImageView!
    
    
    
    @IBOutlet weak var collectionV: UICollectionView!
    
    
    var timer = Timer()
    var counter = 0
    var arrDashBoardBanner = [RecipeDetailImageListModel]()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backButton.setImage(UIImage(named: "BlackArrowWithBG"), for: .normal)
        
        
    
        DispatchQueue.main.async {
              self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
           }
    }

    @objc func changeImage() {
         if counter < arrDashBoardBanner.count {
              let index = IndexPath.init(item: counter, section: 0)
             self.collectionV.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
             self.counter += 1
            
         } else {
             
             if arrDashBoardBanner.count > 0 {
                 counter = 0
                 let index = IndexPath.init(item: counter, section: 0)
                 self.collectionV.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
                  counter = 1
             }
           }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrDashBoardBanner.count
}
    
    
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
    let cell: RecipeImageTopViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecipeImageTopViewCell", for: indexPath) as! RecipeImageTopViewCell
   
        
    if let imgUrl = arrDashBoardBanner[indexPath.row].recipeDetailImagePath,!imgUrl.isEmpty {
        
        let img  = "\(EndPoint.BASE_API_IMAGE_URL)\(imgUrl)"
        let urlString = img.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

        cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
            if (error != nil) {
                cell.imageV.image = UIImage(named: "placeholder")
            } else {
                cell.imageV.image = image
            }
        }
    }
    else{
        cell.imageV.image = UIImage(named: "placeholder")
    }
     
    return cell
}

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let size = CGSize(width:  (UIScreen.main.bounds.size.width), height: 300)
        return size
    }
}
