//
//  RecipeDetailDirectionTVC.swift
//  ChefMiddleEast
//
//  Created by Apple on 19/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class RecipeDetailDirectionTVC: UITableViewCell {

    @IBOutlet weak var lblDescription : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
