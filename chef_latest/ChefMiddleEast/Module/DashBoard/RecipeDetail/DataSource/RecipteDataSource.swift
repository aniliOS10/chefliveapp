//
//  RecipteDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/11/21.
//

import Foundation
import UIKit
@available(iOS 13.0, *)

extension RecipeDetailViewController: UITableViewDataSource,UITableViewDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.0 :0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecipteDetailTableViewCell") as! RecipteDetailTableViewCell
            
            cell.imageV.layer.cornerRadius = 20
            cell.imageV.addShadow()
    
            cell.cellViewRound2.layer.cornerRadius = 20
            cell.cellViewRound.layer.cornerRadius = 20
            cell.cellViewRound2.addShadow()
            cell.cellViewRound.addShadow()

            if dictRecipeDetail != nil{
                
                cell.arrDashBoardBanner = dictRecipeDetail.recipeDetailImageList
                cell.collectionV.reloadData()
                
                cell.titleLabel.text = dictRecipeDetail.recipeTitle
                cell.lblRecipeDescription.text = dictRecipeDetail.recipeDescription
                cell.lblTopTip.text = dictRecipeDetail.topTips
                
                
                let size = dictRecipeDetail.recipeDescription?.height(withConstrainedWidth: self.view.frame.width - 50, font: UIFont(name: "Optima-Regular", size: 13.0) ?? UIFont.systemFont(ofSize: 20.0))
                
                if size != nil {
                    cell.cellViewRoundHeight.constant = size! + 100
                }
                
                let topTipsSize = dictRecipeDetail.topTips?.height(withConstrainedWidth: self.view.frame.width - 70, font: UIFont(name: "Optima-Regular", size: 13.0) ?? UIFont.systemFont(ofSize: 22.0))
                let keyIngredientsSize = dictRecipeDetail.keyIngredients?.height(withConstrainedWidth: self.view.frame.width - 70, font: UIFont(name: "Optima-Regular", size: 13.0) ?? UIFont.systemFont(ofSize: 22.0))
                
               
                
                if topTipsSize != nil {
                    cell.cellViewRound2Height.constant = topTipsSize! + 50
                }
                if keyIngredientsSize != nil {
                    cell.cellViewRound2Height.constant = cell.cellViewRound2Height.constant + keyIngredientsSize!
                }
                
                
                cell.cellViewRound2Height.constant = cell.cellViewRound2Height.constant + 60
          
                cell.lblKeyIngredint.text = dictRecipeDetail.keyIngredients
                let arrImages = dictRecipeDetail.recipeDetailImageList
                if let imgUrl = dictRecipeDetail.recipeImage,!imgUrl.isEmpty {
                    
                   let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
                    
                    cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                        if (error != nil) {
                            cell.imageV.image = UIImage(named: "placeholder")
                        } else {
                            cell.imageV.image = image
                        }
                    }
                }
                else if arrImages.count > 0 {
                    
                    let imgUrl = "\(EndPoint.BASE_API_IMAGE_URL)\(arrImages[0].recipeDetailImagePath ?? "")"
                    
                    let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

                    cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                        if (error != nil) {
                            cell.imageV.image = UIImage(named: "placeholder")
                        } else {
                            cell.imageV.image = image
                        }
                    }
                }
                else {
                    cell.imageV.image = UIImage(named: "placeholder")
                }
                
                cell.imgDifficulty1.image = UIImage(named: "un_difficulty")
                cell.imgDifficulty2.image = UIImage(named: "un_difficulty")
                cell.imgDifficulty3.image = UIImage(named: "un_difficulty")
                if let difficultyLevel = dictRecipeDetail.difficultyLevel, difficultyLevel != 0 {
                    if difficultyLevel == 1{
                        cell.imgDifficulty1.image = UIImage(named: "selected_difficulity")
                        cell.imgDifficulty2.image = UIImage(named: "un_difficulty")
                        cell.imgDifficulty3.image = UIImage(named: "un_difficulty")
                    }
                    if difficultyLevel == 2{
                        cell.imgDifficulty1.image = UIImage(named: "selected_difficulity")
                        cell.imgDifficulty2.image = UIImage(named: "selected_difficulity")
                        cell.imgDifficulty3.image = UIImage(named: "un_difficulty")
                    }
                    if difficultyLevel == 3{
                        cell.imgDifficulty1.image = UIImage(named: "selected_difficulity")
                        cell.imgDifficulty2.image = UIImage(named: "selected_difficulity")
                        cell.imgDifficulty3.image = UIImage(named: "selected_difficulity")
                    }
                }
                
            }
            return cell
        }
        else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeDetailRankingTableViewCell") as! RecipeDetailRankingTableViewCell
            
            cell.cellViewRound.layer.cornerRadius = 20
            cell.cellViewRound.addShadow()
            
            if dictRecipeDetail != nil{
                cell.btnCategory1Title.setTitle(dictRecipeDetail.category, for: .normal)
                cell.btnCategory2Title.setTitle(dictRecipeDetail.serving, for: .normal)
                
                
                var cookingTimeDesc = ""
                cookingTimeDesc = String(format: "%@ %@", dictRecipeDetail.cookingTime  ?? "" ,dictRecipeDetail.cookingTimeDesc ?? "")
                cell.btnCategory3Title.setTitle(cookingTimeDesc, for: .normal)
                
                
              //  cell.btnCategory3Title.setTitle(dictRecipeDetail.cookingTime, for: .normal)
                
                var preparetime = ""
                preparetime = String(format: "%@ %@", dictRecipeDetail.preparetime  ?? "" ,dictRecipeDetail.preparetimeDesc ?? "")
                cell.btnCategory4Title.setTitle(preparetime, for: .normal)
                
                
             //   cell.btnCategory4Title.setTitle(dictRecipeDetail.preparetime, for: .normal)
                
                
                cell.btnCategory5Title.setTitle(dictRecipeDetail.calories, for: .normal)
                
                if dictRecipeDetail.category == "" {
                    cell.viewCategory1.isHidden = true
                }
                if dictRecipeDetail.serving == "" {
                    cell.viewCategory2.isHidden = true
                }
                if dictRecipeDetail.cookingTime == "" {
                    cell.viewCategory5.isHidden = true
                }
                if dictRecipeDetail.preparetime == "" {
                    cell.viewCategory3.isHidden = true
                }
                if dictRecipeDetail.calories == "" {
                    cell.viewCategory4.isHidden = true
                }
            }
            return cell
        }
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeDetailVedioTableViewCell") as! RecipeDetailVedioTableViewCell
            cell.roundView.layer.cornerRadius = 10
            cell.roundView.addShadow()
            cell.ytPlayerView.layer.cornerRadius = 10
            cell.ytPlayerView.addShadow()
            
            if !cell.isLoaded {
                cell.isLoaded = true
                guard let urlString = dictRecipeDetail.videoUrl,
                let url = URL(string: urlString)
                else {
                   // cell.url = URL(string: "https://www.youtube.com/watch?v=njAWJHGqsPs")
                    return cell
                }
                cell.url = url
            }
            return cell
        }
        else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeDetailAddToCartTableViewCell") as! RecipeDetailAddToCartTableViewCell
  
            cell.dictRecipeDetail = dictRecipeDetail
            cell.delegate = self
            cell.tableView.dataSource = cell
            cell.tableView.delegate = cell
            
            
            cell.ingredientToShowTblView.delegate = cell
            cell.ingredientToShowTblView.dataSource = cell
            
            cell.tableView.layer.cornerRadius = 20
            cell.tableView.layer.borderWidth = 0.5
            cell.tableView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            
            cell.ingredientToShowTblView.layer.cornerRadius = 20
            cell.ingredientToShowTblView.layer.borderWidth = 0.5
            cell.ingredientToShowTblView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            
            
            cell.cellViewRound1.layer.cornerRadius = 20
            cell.cellViewRound1.addShadow()
            cell.cellViewRound2.layer.cornerRadius = 20
            cell.cellViewRound2.addShadow()
            cell.ingredientToShowBaseView.layer.cornerRadius = 20
            cell.ingredientToShowBaseView.addShadow()
            cell.ingredientToShowTblView.reloadData()
            cell.arrIngredientList = dictRecipeDetail.ingredientList.isEmpty == true ? dictRecipeDetail.ingredientToShow : dictRecipeDetail.ingredientList
          //  cell.arrIngredientList = dictRecipeDetail.ingredientList
            
            cell.arrrNutritionList = dictRecipeDetail.nutritionList
            cell.arrrRecipeDetailImageList = dictRecipeDetail.recipeDetailImageList
            cell.lblRecipeNotes.text = dictRecipeDetail.recipeNote
            cell.arrIngredientShowList = dictRecipeDetail.ingredientShowList
            
            cell.tableViewHeight.constant = CGFloat(cell.arrIngredientList.count * 60)
            cell.tableView.reloadData()
            
            if cell.segmentController.selectedSegmentIndex == 0 {
                
                cell.ingredientSaleViewHeight.constant = CGFloat(cell.arrIngredientList.count * 60) + 20
                
                if cell.arrIngredientShowList.count == 1 {
                    cell.ingredientToShowViewHeight.constant = CGFloat(cell.arrIngredientShowList.count * 35) + 20
                }
                else{
                    cell.ingredientToShowViewHeight.constant = CGFloat(cell.arrIngredientShowList.count * 30) + 30
                }
                
            } else if cell.segmentController.selectedSegmentIndex == 1 {
                let heightSize = dictRecipeDetail.directions?.height(withConstrainedWidth: self.view.frame.width - 70, font: UIFont(name: "Optima", size: 14.0) ?? UIFont.systemFont(ofSize: 15.0))
                if heightSize != nil {
                    if heightSize ?? 0.0 < 50 {
                        cell.ingredientToShowViewHeight.constant = CGFloat(70)
                    }
                    else{
                        cell.ingredientToShowViewHeight.constant = CGFloat(heightSize! + 25)
                    }
                }
                
            } else if cell.segmentController.selectedSegmentIndex == 2 {
                
                let heightSizeLine = dictRecipeDetail.topTips?.heightForView(text: "", font: UIFont(name: "Optima", size: 14.0) ?? UIFont.systemFont(ofSize: 15.0), width: self.view.frame.width - 70)
                if heightSizeLine ?? 0.0 < 50 {
                    cell.ingredientToShowViewHeight.constant = 55
                }
                else{
                    cell.ingredientToShowViewHeight.constant = CGFloat(heightSizeLine! + 12)
                    
                }
            
            } else if cell.segmentController.selectedSegmentIndex == 3 {
                
            
                if cell.arrrNutritionList.count == 1 {
                    cell.ingredientToShowViewHeight.constant = CGFloat(cell.arrrNutritionList.count * 37) + 20
                }
                else{
                    cell.ingredientToShowViewHeight.constant = CGFloat(cell.arrrNutritionList.count * 37) + 10
                }
                
                if cell.arrrNutritionList.count == 0 {
                    cell.ingredientToShowViewHeight.constant = 0
                }
            }
            
            if #available(iOS 13.0, *) {
                
                let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 13)]
                cell.segmentController.setTitleTextAttributes(titleTextAttributes, for:.normal)
                
                let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 13)]
                cell.segmentController.setTitleTextAttributes(titleTextAttributes1, for:.selected)
            }
            else {
                // Fallback on earlier versions
            }
            cell.segmentController.tintColor = UIColor.white
            return cell
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
        if indexPath.row == 2 {
             let urlString = dictRecipeDetail.videoUrl
             if urlString != ""{
                return 300
             }
             else {
                return 0
             }
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
    
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let  view =  UIView.init()
        view.backgroundColor = UIColor.clear
        return view
    }
}

