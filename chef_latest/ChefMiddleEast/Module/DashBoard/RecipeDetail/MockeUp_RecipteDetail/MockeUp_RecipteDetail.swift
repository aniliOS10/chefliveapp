//
//  MockeUp_RecipteDetail.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/11/21.
//

import Foundation
import UIKit
struct MockeUp_RecipteDetail{
    var description : String!
    var title : String!
    var image : UIImage!

    init(title:String,description:String,image:UIImage){
        self.description = description
        self.title = title
        self.image = image
        }
}
