//
//  OrderDetailViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 09/11/21.
//

import UIKit

class OrderDetailViewController:BaseViewController {
    
    @IBOutlet weak var tableView : UITableView!

    var salesOrderId = String()
    var dictOrderDetail = OrderDetailModel()
    
    let documentInteractionController = UIDocumentInteractionController()

    //MARK:- VC Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            EndPoint.salesOrderId = salesOrderId
            self.getOrderDetailAPI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.tableView.delegate = self
        self.tableView.dataSource = self
        SetUpNavigation()
   }

    func shareData(str: String) {
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: str){
                        let documento = NSData(contentsOfFile: str)
                        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [documento!], applicationActivities: nil)
                        activityViewController.popoverPresentationController?.sourceView = self.view
                       self.present(activityViewController, animated: true, completion: nil)
                    }
                    else {
                        print("document was not found")
                    }
    }
    
 
    func storeAndShare(withURLString: String) {
        
        guard let url = URL(string: withURLString) else { return }
        let fileName = salesOrderId + "Invoice"

        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            let tmpURL = FileManager.default.temporaryDirectory
                .appendingPathComponent("\(fileName).pdf")
            do {
                try data.write(to: tmpURL)
                DispatchQueue.main.async {
                    self.shareData(str: tmpURL.path)
                }
                
            } catch {
                print(error)
            }
           
            }.resume()
    
    }
    
    // MARK: - Navigation
    func SetUpNavigation(){
        
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
                            
        menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: AppMessage_OrderDetail.Placeholder.OrderDetail)
        }else {
            self.initBellButton()
            bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
            self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: AppMessage_OrderDetail.Placeholder.OrderDetail)
        }
        
        
    }

    
    //MARK:- Menu Button
    @objc func menuButtonPressed(){
        self.sideMenuController?.revealMenu()
    }
    
    //MARK:- Notification Button
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
    
    //MARK:- Button Action
    @IBAction func buttonSaveAndMakePaymentPressed(_ sender: Any) {
        
    }
    
    // MARK: - Get Order detail  API
    func getInvioceAPI(){
        self.showLoader()
        EndPoint.pdfSalesOrderId(t: BaseResponse.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccessWithStringValue(let strData):
                if strData != ""{
                    self.storeAndShare(withURLString: EndPoint.BASE_API_IMAGE_URL + strData)
                }
                else{
                    NotificationAlert().NotificationAlert(titles: "Invoice file not found")
                }
            case .onFailure(let error):
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
                self.hideLoader()
            }
        }
    }
    
    
    // MARK: - Get Order detail  API
    func getOrderDetailAPI(){
        self.showLoader()
        EndPoint.GetOrderDetail(t: OrderDetailModel.self) { result in
            switch result {
            case .onSuccess(let data):
                self.hideLoader()
                if data != nil{
                    self.dictOrderDetail = data!
                }
                self.tableView.reloadData()
            case .onFailure(let error):
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
                self.hideLoader()
            }
        }
    }
    
    // MARK: - Cancel Order  API
    func cancelOrderAPI(){
        self.showLoader()
        EndPoint.CancelOrder(t: OrderDetailModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccessWithStringValue(let msg):
                
            NotificationAlert().NotificationAlert(titles: msg)
            self.navigationController?.popViewController(animated: true)
                self.tableView.reloadData()

            case .onFailure(let error):
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
                self.hideLoader()
            }
        }
    }
    
    
}
extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}
