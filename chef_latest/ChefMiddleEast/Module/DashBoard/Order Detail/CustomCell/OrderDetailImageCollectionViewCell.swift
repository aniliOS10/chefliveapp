//
//  OrderDetailImageCollectionViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 09/11/21.
//

import UIKit

class OrderDetailImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var cellViewRound1: UIView!
    @IBOutlet weak var cellViewRound2: UIView!
    @IBOutlet weak var cellViewRound3: UIView!
    
    @IBOutlet  var lblProductName: UILabel!
    @IBOutlet  var lblQty: UILabel!
    @IBOutlet  var lblAmount: UILabel!
    @IBOutlet  var btnAddItem: UIButton!
}
