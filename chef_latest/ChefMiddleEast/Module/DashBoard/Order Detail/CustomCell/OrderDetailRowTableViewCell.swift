//
//  OrderDetailRowTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 12/11/21.
//

import UIKit

@available(iOS 13.0, *)

protocol AddItemToCartDelegate {
    func addProductInCart(_productData : OrderProductListModel)
}

class OrderDetailRowTableViewCell:UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout   {
    
    
    var delegate: AddItemToCartDelegate?
    @IBOutlet weak var collectionV: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var cellViewRound2: UIView!
    @IBOutlet  var lblOrderNumber: UILabel!
    @IBOutlet  var lblOrderDate: UILabel!
    @IBOutlet  var lblFinalTotalAmount: UILabel!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet  var lblDeliveryDate: UILabel!
    @IBOutlet  var lblProductCount: UILabel!
    @IBOutlet weak var btnCancelOrder: UIButton!
    @IBOutlet weak var btnInvoice: UIButton!


    
    var tagRow:Int = 0
    var tagSection:Int = 0
    var arrOrderProduct = [OrderProductListModel]()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrOrderProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: OrderDetailImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderDetailImageCollectionViewCell", for: indexPath) as! OrderDetailImageCollectionViewCell
//        cell.cellViewRound1.layer.cornerRadius = 5
//        cell.cellViewRound1.addShadow()
//
        cell.cellViewRound2.layer.cornerRadius = 5
        cell.cellViewRound2.addShadow()
        
        cell.imageV.layer.cornerRadius = 5
        cell.btnAddItem.layer.cornerRadius = 5
        cell.btnAddItem.addShadow()
        cell.imageV.addShadow()
      //  Utility.shared.makeRoundCorner(layer: cell.cellViewRound1.layer, color: UIColor.lightGray.withAlphaComponent(0.5), radius: 5.0)
     //   Utility.shared.makeRoundCorner(layer: cell.cellViewRound2.layer, color: UIColor.lightGray.withAlphaComponent(0.5), radius: 5.0)
     //   Utility.shared.makeRoundCorner(layer: cell.imageV.layer, color: UIColor.lightGray.withAlphaComponent(0.5), radius: 5.0)
        
      //  let width = (UIScreen.main.bounds.size.width/3)-10
        
        if arrOrderProduct.count > 0 {
            cell.lblProductName.text = arrOrderProduct[indexPath.row].productTitle ?? ""
            cell.lblQty.text = "Quantity: \(arrOrderProduct[indexPath.row].salesQty ?? 0)"
            
            if let totalAmount = arrOrderProduct[indexPath.row].salesPrice,totalAmount != 0.0 {
                cell.lblAmount.text = String(format: "AED %.2f", totalAmount)
            }
                        
            if (arrOrderProduct[indexPath.row].productImageStr != nil) {
                
                let imageData = arrOrderProduct[indexPath.row].productImageStr
                
                let urlString = imageData?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

                if let url = URL(string: urlString ?? ""){
                
               // let imagePath = String("\(EndPoint.BASE_API_PRODUCT_IMAGE_URL)\(imageData["productDetailImagePath"] as! String)")
                
                cell.imageV?.sd_setImage(with: url) { (image, error, cache, urls) in
                    if (error != nil) {
                        cell.imageV.image = UIImage(named: "placeholder")
                    } else {
                        cell.imageV.image = image
                    }
                }
                
                //imagePath["productDetailImagePath"]
            }else {
                cell.imageV.image = UIImage(named: "placeholder")
            }
            }
            
            /*
            if let imgUrl = arrOrderProduct[indexPath.row].productImage, !imgUrl.isEmpty {
                cell.imageV?.sd_setImage(with: URL.init(string:("\(imgUrl)"))) { (image, error, cache, urls) in
                    if (error != nil) {
                        cell.imageV.image = UIImage(named: "placeholder")
                    } else {
                        cell.imageV.image = image
                    }
                }
            }
            else {
                cell.imageV.image = UIImage(named: "placeholder")
            }*/
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let width = collectionView.bounds.size.width
        let size = CGSize(width: width, height: 90)
        
        return size
    }
    
    @IBAction func addItemAction(_ sender : UIButton) {
        if (self.delegate != nil) {
            
            self.delegate?.addProductInCart(_productData: arrOrderProduct[sender.tag])
        }
    }
    
    
    
}
