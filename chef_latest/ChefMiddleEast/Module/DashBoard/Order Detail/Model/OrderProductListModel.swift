//
//  OrderProductListModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 22/01/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class OrderProductListModel: BaseResponse{
    
    var discAmountPerQty: String?
    var eComInventTransId: String?
    var eComSalesId: String?
    var fOCItem: Int?
    var inventTransId: String?
    var itemId: Int?
    var productTitle: String?
    var productImage: NSArray?
    var https: String?
    var salesPrice: Float?
    var salesQty: Int?
    var taxRate: Int?
    var productImageStr: String?

    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        
        discAmountPerQty <- map["discAmountPerQty"]
        eComInventTransId <- map["eComInventTransId"]
        eComSalesId <- map["eComSalesId"]
        fOCItem <- map["fOCItem"]
        inventTransId <- map["inventTransId"]
        itemId <- map["itemId"]
        productTitle <- map["productTitle"]
        productImage <- map["productImages"]
        https <- map["https"]
        salesPrice <- map["salesPrice"]
        salesQty <- map["salesQty"]
        taxRate <- map["taxRate"]
        productImageStr <- map["productImage"]

        
       
    }
}
