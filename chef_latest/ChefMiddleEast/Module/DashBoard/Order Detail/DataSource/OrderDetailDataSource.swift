//
//  OrderDetailDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 09/11/21.
//

import Foundation
import UIKit

extension OrderDetailViewController: UITableViewDataSource, UITableViewDelegate, AddItemToCartDelegate {
   
    
    func addProductInCart(_productData: OrderProductListModel) {
        let param = AddToCartModel()
        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            param.userId = userId
        }
            let paramProduct = ProductModel()
            paramProduct.productId = _productData.itemId
            paramProduct.productQuantity = _productData.salesQty
            param.product.append(paramProduct)
        
        self.showLoader()
        EndPoint.AddToCart(params: param, t: AddToCartModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccessWithStringValue(let msg):
                NotificationAlert().NotificationAlert(titles: msg)
                self.cartCountAPI()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    
    
    
    //MARK:- Add to Cart Api
    func addToCartAPI() {
        let param = AddToCartModel()
        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            param.userId = userId
        }
        for info in dictOrderDetail.orderProductList {
            let paramProduct = ProductModel()
            paramProduct.productId = info.itemId
            paramProduct.productQuantity = info.salesQty
            param.product.append(paramProduct)
        }
        self.showLoader()
        EndPoint.AddToCart(params: param, t: AddToCartModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccessWithStringValue(let msg):
                NotificationAlert().NotificationAlert(titles: msg)
                self.cartCountAPI()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    // MARK: - Cart Count Api
    func cartCountAPI() {
        
        EndPoint.CartCount(t: CartCountModel.self) { result in
            switch result {
            case .onSuccess(let data):
            if data != nil{
                dictCartData = data!
                if let cartCount = dictCartData.cartCount, cartCount != 0 {
                    self.setBagCountOnCart(count: cartCount)
                    if let tabItems = self.tabBarController?.tabBar.items {
                        // In this case we want to modify the badge number of the third tab:
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "\(cartCount)"
                    }
                }
                if let notificationCount = dictCartData.notificationCount, notificationCount >= 0 {
                    self.setNotificationBagCount(count: notificationCount)
                    
                }
                
            }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.0 :0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailRowTableViewCell") as! OrderDetailRowTableViewCell
            cell.delegate = self
            cell.cellViewRound.addShadow()
            cell.cellViewRound.layer.cornerRadius = 5
            cell.cellViewRound2.addShadow()
            cell.cellViewRound2.layer.cornerRadius = 5
            
            let orderDateStr = dictOrderDetail.requestedDate
            cell.lblOrderDate.text = orderDateStr?.changeDateFormat_String(orderDateStr ?? "")

            let dateStr = dictOrderDetail.deliveryDate
            cell.lblDeliveryDate.text = dateStr?.changeDateFormat_String(dateStr ?? "")

           
            cell.lblOrderNumber.text = salesOrderId
            if let totalAmount = dictOrderDetail.totalAmount,totalAmount != 0.0 {
                cell.lblFinalTotalAmount.text = String(format: "AED %.2f", totalAmount)
            }
            cell.btnStatus.setTitle(dictOrderDetail.status ?? "", for: .normal)
            cell.lblProductCount.text = "\(dictOrderDetail.orderProductList.count) Items"
            cell.arrOrderProduct = dictOrderDetail.orderProductList
            cell.tagRow = indexPath.row
            cell.tagSection = indexPath.section
            cell.collectionHeight.constant = CGFloat(dictOrderDetail.orderProductList.count * 95)
            cell.collectionV.dataSource = cell
            cell.collectionV.delegate = cell
            cell.collectionV.reloadData()
            cell.btnCancelOrder.tag = indexPath.row
            cell.btnCancelOrder.addTarget(self, action: #selector(btnCancelOrderTap(sender:)), for: .touchUpInside)
            cell.btnInvoice.addTarget(self, action: #selector(btnInvoiceTap(sender:)), for: .touchUpInside)
            
            if dictOrderDetail.status == "Dispatched"{
                cell.btnInvoice.isHidden = false
            }
            else{
                cell.btnInvoice.isHidden = true
            }

            
            return cell
        }
        if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReOrderRefreshTableViewCell") as! ReOrderRefreshTableViewCell
            if let totalAmount = dictOrderDetail.totalAmount,totalAmount != 0.0 {
               // cell.btnShowTotal.setTitle(String(format: "AED %.2f", totalAmount), for: .normal)
                
                cell.btnShowTotal.addTarget(self, action: #selector(btnReOrderTap(sender:)), for: .touchUpInside)

            }
            

            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailTotalTableViewCell") as! OrderDetailTotalTableViewCell
        cell.cellViewRound.layer.cornerRadius = 5
        cell.cellViewRound.addShadow()
        if let subTotal = dictOrderDetail.subTotal,subTotal != 0.0 {
            cell.lblSubTotalAmnt.text =
                String(format: "AED %.2f", subTotal)
        }
        if let totalAmount = dictOrderDetail.totalAmount,totalAmount != 0.0 {
            cell.lblTotalAmnt.text = String(format: "AED %.2f", totalAmount)
        }
        let vat = dictAppInfo.vatPercent
        cell.lblVat.text = "VAT (\(vat!)%)"
        if let vatTotal = dictOrderDetail.vat,vatTotal != 0.0 {
            cell.lblVatAmnt.text =  String(format: "AED %.2f", vatTotal)
        }
        
        if let discount = dictOrderDetail.discount,discount != 0.0 {
            cell.lblDiscountAmnt.text = String(format: "AED %.2f", discount)
            cell.lblDiscount.text = "Discount"
            cell.viewDiscountTop.constant = 8
        }
        else{
            cell.lblDiscountAmnt.text = ""
            cell.lblDiscount.text = ""
            cell.viewDiscountTop.constant = 0
        }
        if let shipCharges = dictOrderDetail.shipCharges,!shipCharges.isEmpty {
            cell.lblDeliveryFeeAmnt.text = "AED \(shipCharges)"
            
            if shipCharges == "0"{
                cell.lblDeliveryFeeAmnt.text = "Free"

            }
        }
        else{
            cell.lblDeliveryFeeAmnt.text = "Free"
        }
        cell.lblPaymentMode.text = dictOrderDetail.paymentMode ?? ""
     
        
        cell.viewInvoice.isHidden = true
        cell.viewInvoiceHeight.constant = 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    //MARK:- Download Order
    @objc func btnInvoiceTap(sender: UIButton){
        if let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            EndPoint.salesOrderId = salesOrderId
            getInvioceAPI()
        }
    }
    
    //MARK:- Cancel Order
    @objc func btnCancelOrderTap(sender: UIButton){
        self.showAlert()
    }
    
    @objc func btnReOrderTap(sender: UIButton) {
        self.addToCartAPI()
    }
    
    func showAlert(){
        let alert = UIAlertController(title: nil, message:"Are you sure you want to Cancel?", preferredStyle: .alert)
        
        let No = UIAlertAction(title:"No", style: .default, handler: { action in
        })
        alert.addAction(No)
        
        let Yes = UIAlertAction(title:"Yes", style: .default, handler: { [self] action in
            
            if !salesOrderId.isEmpty {
                EndPoint.salesOrderId = salesOrderId
                EndPoint.dataOrigin = "Mobile"
                cancelOrderAPI()
            }
        })
        alert.addAction(Yes)
        
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    //MARK:- Add to Cart Api
    func addToCartAPI(Model: AddToCartModel) {
        
        EndPoint.AddToCart(params: Model,t: AddToCartModel.self) { result in
            switch result {
            case .onSuccessWithStringValue(let msg):
                if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
                    EndPoint.userId = userId
                    self.cartCountAPI()
                }
                NotificationAlert().NotificationAlert(titles: msg)
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    
   
     
    
}
