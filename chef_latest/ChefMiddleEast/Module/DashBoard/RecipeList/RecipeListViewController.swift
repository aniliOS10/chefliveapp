//
//  RecipeListViewController.swift
//  ChefMiddleEast
//
//  Created by rupinder singh on 01/11/21.
//

import UIKit

@available(iOS 13.0, *)
class RecipeListViewController:BaseViewController {
    
  //  @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var search : UISearchBar!
    @IBOutlet weak var buttonPayment: UIButton!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var collectionV: UICollectionView!

    var arrRecipe = [RecipeListModel]()
    var arrSelectedRecipe = [String]()
 
    
    //MARK:- VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setParamForRecipeListApi()
        self.SetupSearch()
        self.setSegmentControllerUI()
        self.collectionV.delegate = self
        self.collectionV.dataSource = self
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
       
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: AppMessage_RecipeList.Placeholder.Recipes)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func setParamForRecipeListApi() {
        
        let params = CollectionListModel()
        params.pageNumber = 1
        params.pageSize = 2000
        EndPoint.searchText = search.text ?? ""
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            params.userId = userId
        }
        self.recipeListAPI(Model: params)
    }
    
    //MARK:-  Menu Button
    @objc func menuButtonPressed(){
        self.sideMenuController?.revealMenu()
    }
    
    //MARK:- Notification Button
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
    
    //MARK:- Set Search
    func SetupSearch() {
        
        search.searchBarStyle = .minimal
        search.backgroundColor = UIColor.white
        search.tintColor = UIColor.white
        if #available(iOS 13.0, *) {
            search.searchTextField.backgroundColor = UIColor().getSearchColor()
            search.searchTextField.textColor =  UIColor().getThemeColor()
            search.searchTextField.tintColor =  UIColor().getThemeColor()
            Utility.shared.makeRoundCorner(layer: search.searchTextField.layer, color:UIColor.white.withAlphaComponent(0.5), radius: 5)
            let white = UIColor.red
            search.searchTextField.background = UIImage.init(named: "imageV1")
        }
        else {
            // Fallback on earlier versions
        }
    }
    
    //MARK:- Segmant controll UI
    func setSegmentControllerUI(){
        
        if #available(iOS 13.0, *) {
            let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)]
            segmentController.setTitleTextAttributes(titleTextAttributes, for:.normal)
            
            let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)]
            segmentController.setTitleTextAttributes(titleTextAttributes1, for:.selected)
        }
        else {
            
        }
        segmentController.tintColor = UIColor.white
    }
    
    //MARK:- Button Action
    @IBAction func buttonSaveAndMakePaymentPressed(_ sender: Any) {
        
    }
    
    @IBAction func segmentControllerValueChanged(_ sender: Any) {
       
    }
    
   /* func didSelectRow(){
        self.dataSource?.didSelectRow = { cell,indexPath in
            
            let controller:RecipeDetailViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }*/
    
    // MARK: - Recipe List API
    func recipeListAPI(Model: CollectionListModel){
        self.showLoader()
       
        EndPoint.RecipeList(params: Model, t: RecipeListModel.self) { result in
            switch result {
            case .onSuccess(let items):
                if items.count > 0 {
                    print(items.toJSON())
                    if self.arrSelectedRecipe.count > 0 {
                        let arrFiltered = items.filter( {self.arrSelectedRecipe.contains(String("\($0.recipeId!)") ) == true} )
                        self.arrRecipe = arrFiltered
                    } else {
                        self.arrRecipe = items
                    }
                    self.collectionV.reloadData()
                }
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    // MARK: - Recipe List API
    func markRecipeFavouriteAPI(){
      //  self.showLoader()
       
        EndPoint.MarkRecipeFavourite(t: RecipeListModel.self) { result in
            switch result {
            case .onSuccess(_ ):
                NotificationAlert().NotificationAlert(titles: "Sucess Response")
          //      self.hideLoader()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
       
            }
        }
    }
    
    // MARK: - Recipe List API
    func markRecipeBookmarkAPI(){
      //  self.showLoader()
       
        EndPoint.MarkRecipeBookmark(t: RecipeListModel.self) { result in
            switch result {
            case .onSuccess(_ ):
                NotificationAlert().NotificationAlert(titles: "Sucess Response")
            //    self.hideLoader()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
             //   self.hideLoader()
            }
        }
    }
}


