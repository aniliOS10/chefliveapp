//
//  RecipteListDataSource.swift
//  ChefMiddleEast
//
//  Created by rupinder singh on 01/11/21.
//

import Foundation
import UIKit
@available(iOS 13.0, *)

extension RecipeListViewController: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrRecipe.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: RecipeListCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecipeListCollectionViewCell", for: indexPath) as! RecipeListCollectionViewCell
                
        let width = (UIScreen.main.bounds.size.width/3)-10
        
        cell.cellViewRound.layer.cornerRadius = 6
        cell.cellViewRoundBG.addShadowDash()
        cell.cellViewRoundBG.layer.cornerRadius = 6
    
        if arrRecipe.count > 0 {
            cell.vwMinutes.layer.cornerRadius = 15
            cell.vwMinutes.addShadow()
            cell.btnFav.layer.cornerRadius = 15
            
            var preparetime = ""
            preparetime = String(format: "%@ %@", arrRecipe[indexPath.row].preparetime  ?? "" ,arrRecipe[indexPath.row].preparetimeDesc ?? "")
        
            cell.btnFav.setTitle(preparetime, for: .normal)
            cell.desLabel.text = "Difficulty level"

            cell.imgDifficulty1.image = UIImage(named: "un_difficulty")
            cell.imgDifficulty2.image = UIImage(named: "un_difficulty")
            cell.imgDifficulty3.image = UIImage(named: "un_difficulty")
            if let difficultyLevel = arrRecipe[indexPath.row].difficultyLevel, difficultyLevel != 0 {
                if difficultyLevel == 1{
                    cell.imgDifficulty1.image = UIImage(named: "selected_difficulity")
                    cell.imgDifficulty2.image = UIImage(named: "un_difficulty")
                    cell.imgDifficulty3.image = UIImage(named: "un_difficulty")
                }
                if difficultyLevel == 2{
                    cell.imgDifficulty1.image = UIImage(named: "selected_difficulity")
                    cell.imgDifficulty2.image = UIImage(named: "selected_difficulity")
                    cell.imgDifficulty3.image = UIImage(named: "un_difficulty")
                }
                if difficultyLevel == 3{
                    cell.imgDifficulty1.image = UIImage(named: "selected_difficulity")
                    cell.imgDifficulty2.image = UIImage(named: "selected_difficulity")
                    cell.imgDifficulty3.image = UIImage(named: "selected_difficulity")
                }
            }
            cell.titleLabel.text = arrRecipe[indexPath.row].recipeTitle
            if let imgUrl = arrRecipe[indexPath.row].recipeImage,!imgUrl.isEmpty {
                
                let strURL = "\(EndPoint.BASE_API_IMAGE_URL)\(imgUrl)"
                
                let urlString = strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

                
                cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                    if (error != nil) {
                        cell.imageV.image = UIImage(named: "placeholder")
                    } else {
                        cell.imageV.image = image
                    }
                }
            }
            if let bookmark = arrRecipe[indexPath.row].isBookmark, bookmark == true {
                cell.ribonButton.isSelected = true
            }
            else{
                cell.ribonButton.isSelected = false
            }
            if let favourite = arrRecipe[indexPath.row].isFavourite, favourite == true {
                cell.btnFav.isSelected = true
            }
            else{
                cell.btnFav.isSelected = false
            }
             cell.btnFav.tag = indexPath.row
             cell.btnFav.addTarget(self, action: #selector(btnFavouritTap(button:)), for: .touchUpInside)
            cell.ribonButton.isHidden = true
            cell.ribonButton.tag = indexPath.row
            cell.ribonButton.addTarget(self, action: #selector(btnRibonButtonTap(button:)), for: .touchUpInside)
       }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let width = (UIScreen.main.bounds.size.width/2)-5
        
        let size = CGSize(width: width, height: 233)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let controller:RecipeDetailViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
        controller.recipeId = arrRecipe[indexPath.row].recipeId!
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- Fav Button Tap
    @objc func btnFavouritTap(button:UIButton){
        
      /*  button.isSelected = !button.isSelected
        arrRecipe[button.tag].isFavourite = button.isSelected
        
        EndPoint.recipeId = "\(arrRecipe[button.tag].recipeId!)"
        if button.isSelected == true {
            EndPoint.favouriteStatus = "true"
        }
        else {
            EndPoint.favouriteStatus = "false"
        }
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            markRecipeFavouriteAPI()
        }*/
    }
    
    //MARK:- Ribon Button Tap
    @objc func btnRibonButtonTap(button:UIButton){
        button.isSelected = !button.isSelected
        if button.isSelected == true {
            EndPoint.bookmarkStatus = "true"
        }
        else {
            EndPoint.bookmarkStatus = "false"
        }
        EndPoint.recipeId = "\(arrRecipe[button.tag].recipeId!)"
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            markRecipeBookmarkAPI()
        }
    }
}


