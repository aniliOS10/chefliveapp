//
//  MockUp_RecipeList.swift
//  ChefMiddleEast
//
//  Created by rupinder singh on 01/11/21.
//

import Foundation
import UIKit
struct MockUp_RecipeList{
    var description : String!
    var title : String!
    var image : UIImage!

    init(title:String,description:String,image:UIImage){
        self.description = description
        self.title = title
        self.image = image
        }
}
