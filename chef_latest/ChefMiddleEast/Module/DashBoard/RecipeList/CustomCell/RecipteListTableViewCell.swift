//
//  RecipteListTableViewCell.swift
//  ChefMiddleEast
//
//  Created by rupinder singh on 01/11/21.
//

import UIKit

@available(iOS 13.0, *)
class RecipteListTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout   {
    
    @IBOutlet weak var collectionV: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    
    var didSelectRow: ((_ indexPath:IndexPath) -> Void)?
    
    var tagRow:Int = 0
    var tagSection:Int = 0
    var arrRecipe = [RecipeListModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrRecipe.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: RecipeListCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecipeListCollectionViewCell", for: indexPath) as! RecipeListCollectionViewCell
        
        Utility.shared.makeRoundCorner(layer: cell.cellViewRound.layer, color: UIColor.lightGray.withAlphaComponent(0.1), radius: 5.0)
       
        let width = (UIScreen.main.bounds.size.width/3)-10
        
        if arrRecipe.count > 0 {
            
            cell.titleLabel.text = arrRecipe[indexPath.row].recipeTitle
            cell.desLabel.text = arrRecipe[indexPath.row].recipeDate
            
            if let imgUrl = arrRecipe[indexPath.row].recipeImage,!imgUrl.isEmpty {
                
                
                let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
                
                cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                    if (error != nil) {
                        cell.imageV.image = UIImage(named: "recipe-img3")
                    } else {
                        cell.imageV.image = image
                    }
                }
            }
            else {
                cell.imageV.image = UIImage(named: "placeholder")
            }
            
            if let bookmark = arrRecipe[indexPath.row].isBookmark, bookmark == true {
                
                cell.ribonButton.setImage(UIImage(named: "heart"), for: .normal)
            }
            else{
                cell.ribonButton.setImage(UIImage(named: "ribbonDashboard"), for: .normal)
            }
            
            if let favourite = arrRecipe[indexPath.row].isFavourite, favourite == true {
                cell.btnFav.isSelected = true
               // cell.btnFav.setImage(UIImage(named: "heart"), for: .normal)
            }
            else{
                //cell.btnFav.setImage(UIImage(named: "Uheart"), for: .normal)
                cell.btnFav.isSelected = false
            }
 
             cell.btnFav.tag = indexPath.row
             cell.btnFav.addTarget(self, action: #selector(btnFavouritTap(button:)), for: .touchUpInside)
            
            cell.ribonButton.tag = indexPath.row
            cell.ribonButton.addTarget(self, action: #selector(btnRibonButtonTap(button:)), for: .touchUpInside)
 
       }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        var width = (UIScreen.main.bounds.size.width/2)-5
        let size = CGSize(width: width, height: self.collectionHeight.constant)
        return size
        // return UICollectionViewFlowLayout.automaticSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.didSelectRow!(indexPath)
    }
    
    //MARK:- Fav Button Tap
    @objc func btnFavouritTap(button:UIButton){
        
        button.isSelected = !button.isSelected
        arrRecipe[button.tag].isFavourite = button.isSelected
        
        EndPoint.recipeId = "\(arrRecipe[button.tag].recipeId!)"
        if button.isSelected == true {
            EndPoint.favouriteStatus = "true"
        }
        else {
            EndPoint.favouriteStatus = "false"
        }
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            markRecipeFavouriteAPI()
        }
        
    }
    
    //MARK:- Ribon Button Tap
    @objc func btnRibonButtonTap(button:UIButton){
        button.isSelected = !button.isSelected
        if button.isSelected == true {
            EndPoint.bookmarkStatus = "1"
        }
        else {
            EndPoint.bookmarkStatus = "0"
        }
        EndPoint.recipeId = "\(arrRecipe[button.tag].recipeId)"
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), userId.isEmpty {
            EndPoint.userId = userId
            markRecipeBookmarkAPI()
        }
    }
    
}


//MARK:- API's
extension RecipteListTableViewCell{
    
    // MARK: - Recipe List API
    func markRecipeFavouriteAPI(){
      //  self.showLoader()
       
        EndPoint.MarkRecipeFavourite(t: RecipeListModel.self) { result in
            switch result {
            case .onSuccess(_ ):
                NotificationAlert().NotificationAlert(titles: "Sucess Response")
          //      self.hideLoader()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    // MARK: - Recipe List API
    func markRecipeBookmarkAPI(){
      //  self.showLoader()
       
        EndPoint.MarkRecipeBookmark(t: RecipeListModel.self) { result in
            switch result {
            case .onSuccess(_ ):
                NotificationAlert().NotificationAlert(titles: "Sucess Response")
            //    self.hideLoader()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
}
