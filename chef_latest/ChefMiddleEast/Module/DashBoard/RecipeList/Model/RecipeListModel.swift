//
//  RecipeListModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 13/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class RecipeListModel: BaseResponse {
    
    var recipeId: Int?
    var recipeTitle: String?
    var recipeDate: String?
    var preparetime: String?
    var userId: String?
    var isFavourite: Bool?
    var isBookmark: Bool?
    var recipeImage: String?
    var isModifiedAndroid: Bool?
    var isModifiedIos: Bool?
    var difficultyLevel : Int?
    var preparetimeDesc: String?
    // New items add =======
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        
        preparetimeDesc <- map["preparetimeDesc"]
        recipeId <- map["recipeId"]
        recipeTitle <- map["recipeTitle"]
        recipeDate <- map["recipeDate"]
        userId <- map["userId"]
        preparetime <- map["preparetime"]
        isFavourite <- map["isFavourite"]
        isBookmark <- map["isBookmark"]
        recipeImage <- map["recipeImage"]
        isModifiedAndroid <- map["isModifiedAndroid"]
        isModifiedIos <- map["isModifiedIos"]
        difficultyLevel <- map["difficultyLevel"]
}

}
