//
//  ProductDetailModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 09/01/22.
//

import Foundation
import Alamofire
import ObjectMapper

class ProductDetailModel : BaseResponse {
    
    var productDetailImageId: Int?
    var productId: String?
    var productDetailImagePath: String?
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        productDetailImageId <- map["productDetailImageId"]
        productId <- map["productId"]
        productDetailImagePath <- map["productDetailImagePath"]
        
    }
}
