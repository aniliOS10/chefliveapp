//
//  RelatedProductsModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 16/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class RelatedProductsModel : BaseResponse {
    
    
    var subProductId: String?
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        subProductId <- map["subProductId"]
    }
    
}
