//
//  ProductDetailTVC.swift
//  ChefMiddleEast
//
//  Created by sandeep on 09/01/22.
//

import UIKit

class ProductDetailTVC: UITableViewCell {

    @IBOutlet var lblProducTitle: UILabel!
    @IBOutlet var lblProductDescriptn: UILabel!
    @IBOutlet var vwRound: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
