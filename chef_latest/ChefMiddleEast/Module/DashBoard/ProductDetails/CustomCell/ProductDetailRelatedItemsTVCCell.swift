//
//  ProductDetailRelatedItemsTVCCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 18/04/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class ProductDetailRelatedItemsTVCCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var relatedProductCV : UICollectionView!
    var arrRelatedProducts =  [NewArrivalsModel]()
    var lastView = UIViewController()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrRelatedProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ProductDetailRelatedCVCCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductDetailRelated", for: indexPath) as! ProductDetailRelatedCVCCell
        
        cell.cellViewRound.addShadow()
        cell.cellViewRound.layer.cornerRadius = 10
        cell.imageV.layer.cornerRadius = 10
        cell.btnAdd.layer.cornerRadius = 5
        
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(btnAddTap(sender:)), for: .touchUpInside)
        
        cell.titleLabel.text = arrRelatedProducts[indexPath.row].productDescription
        
        cell.imageV.image = UIImage(named: "placeholder")
        
        
        if (arrRelatedProducts[indexPath.row].productImage != nil) {
            if (arrRelatedProducts[indexPath.row].productImage)?.count ?? 0 > 0 {

            let imageData = arrRelatedProducts[indexPath.row].productImage![0] as! [String : Any]

            let imagePath = String("\(EndPoint.BASE_API_PRODUCT_IMAGE_URL)\(imageData["productDetailImagePath"] as! String)")
                
                let urlString = imagePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""



            cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                if (error != nil) {
                    cell.imageV.image = UIImage(named: "placeholder")
                } else {
                    cell.imageV.image = image
                }
            }
            }
            else{
                cell.imageV.image = UIImage(named: "placeholder")

            }
        }else {
            cell.imageV.image = UIImage(named: "placeholder")
        }
        
            cell.salePriceLabel.isHidden = true
            cell.btnFav.isHidden = true
       
       //Display agreementValue
        if let startDate = arrRelatedProducts[indexPath.row].fromDate,startDate != nil {
            
            let fromDate : Date = startDate.dateFromString(startDate)
            let endDate = arrRelatedProducts[indexPath.row].toDate
            let toDate : Date = endDate!.dateFromString(endDate!)
            
            if Date().isBetween(fromDate, and: toDate) {
                if let agreementValue = arrRelatedProducts[indexPath.row].agreementValue, agreementValue != nil {
                   
                    cell.salePriceLabel.isHidden = false
                    let agreementValue = arrRelatedProducts[indexPath.row].agreementValue
                    
                    cell.salePriceLabel.text = String(format: "AED %.2f", agreementValue!)
                    
                    if let price = arrRelatedProducts[indexPath.row].salesPrice,price != nil {
                        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: String(format: " %.2f", price))
                        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
                        cell.desLabel.isHidden = false
                        cell.desLabel.attributedText = attributeString
                    }
                }
            } else {
                cell.salePriceLabel.isHidden = true
            }
        } else {
            if let price = arrRelatedProducts[indexPath.row].salesPrice,price != nil {
                cell.desLabel.text = String(format: "AED %.2f", price)
            }
        }
        
        
        
        //Display Coming soon
        let availableQty = arrRelatedProducts[indexPath.row].availableQuantity
        if ((availableQty) != nil )  {
            if (availableQty)! > 5 {
                if let price = arrRelatedProducts[indexPath.row].salesPrice,price > 0 {
                    cell.btnAdd.isHidden = false
                    cell.desLabel.isHidden = false
                    cell.salePriceLabel.isHidden = false
                    cell.lbeComingSoon.isHidden = true
                }
                else{
                    cell.btnAdd.isHidden = true
                    cell.lbeComingSoon.isHidden = false
                    cell.desLabel.isHidden = true
                    cell.salePriceLabel.isHidden = true
                }
            }
            else{
                cell.btnAdd.isHidden = true
                cell.lbeComingSoon.isHidden = false
                cell.desLabel.isHidden = true
                cell.salePriceLabel.isHidden = true
            }
        }
        else{
            cell.btnAdd.isHidden = true
            cell.lbeComingSoon.isHidden = false
            cell.desLabel.isHidden = true
            cell.salePriceLabel.isHidden = true
        }
       
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let width = (UIScreen.main.bounds.size.width/2) - 15
        let size = CGSize(width: width, height: width + 100)
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let controller:ProjectDetailVC =  UIStoryboard(storyboard: .DashBoard).initVC()
        controller.arrProductDetail = arrRelatedProducts[indexPath.row]
        controller.productId = arrRelatedProducts[indexPath.row].productId!
        lastView.navigationController?.pushViewController(controller, animated: true)
    
    }
    
    //MARK:- Add Button Tap
    @objc func btnAddTap(sender:UIButton){
        
            if !arrRelatedProducts.isEmpty {
                guard let productId = arrRelatedProducts[sender.tag].productId else {return}
                if productId != 0 {
                    let param = AddToCartModel()
                    if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                        param.userId = userId
                    }
                    let paramProduct = ProductModel()
                    paramProduct.productId = productId
                    paramProduct.productQuantity = 1
                    param.product.append(paramProduct)
                    self.addToCartAPI(Model: param)
                }
            }
    }
    
    //MARK:- Add to Cart Api
    func addToCartAPI(Model: AddToCartModel) {
        
        EndPoint.AddToCart(params: Model,t: AddToCartModel.self) { result in
            switch result {
            case .onSuccessWithStringValue(let msg):
                if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
                    EndPoint.userId = userId
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CartCountUpdate"), object: nil)
                }
                NotificationAlert().NotificationAlert(titles: msg)
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    


}

