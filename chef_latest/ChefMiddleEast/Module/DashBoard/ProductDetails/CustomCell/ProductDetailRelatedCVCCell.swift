//
//  ProductDetailRelatedCVCCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 18/04/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class ProductDetailRelatedCVCCell:  UICollectionViewCell {
    
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var salePriceLabel: UILabel!
    @IBOutlet weak var increaseButton: UIButton!
    @IBOutlet weak var decreaseButton: UIButton!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    
    @IBOutlet weak var lbeComingSoon: UILabel!
}
