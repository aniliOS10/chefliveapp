//
//  ProjectDetailVC.swift
//  ChefMiddleEast
//
//  Created by sandeep on 09/01/22.
//

import UIKit
import ObjectMapper

var dictCartData = CartCountModel()

class ProjectDetailVC: BaseViewController {

    @IBOutlet weak var btnGoToCheckout : UIButton!
    @IBOutlet weak var btnContinueShopping : UIButton!
    @IBOutlet weak var addToCartBaseView : UIView!
    @IBOutlet weak var addToCartOptionView : UIView!
    
    @IBOutlet var tblProduct: UITableView!
    @IBOutlet var vwHeader: UIView!
    @IBOutlet var cvHeader: UICollectionView!
    @IBOutlet var myPageCntrl: UIPageControl!
    @IBOutlet var vwRoundHeader: UIView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblProductSubtitle: UILabel!
    @IBOutlet var lblProductPrice: UILabel!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var lblComingSoon: UILabel!
    @IBOutlet var vwFooter: UIView!
    @IBOutlet var vwRoundFooter: UIView!
    @IBOutlet var tblDescription: UITableView!
    
    @IBOutlet var vwBottemButton: UIViewX!
    @IBOutlet var btnShowItemCount: UIButton!
    @IBOutlet var btnGoToCart: UIButton!
    
    @IBOutlet var vwQty: UIView!
    @IBOutlet var lblQty: UILabel!
    
    var arrProductDetail = NewArrivalsModel()
    var arrProductDescription = NSMutableArray()
    var arrRelatedProductIDs = NSArray()
    var arrProduct = [NewArrivalsModel]()
    var arrRelatedProducts = [NewArrivalsModel]()
    var arrsubSituteProductIDs = NSArray()
    var arrSituteProducts = [NewArrivalsModel]()
    var productId = Int()
    var addQty = Int()
    var homeToSearch = ""

    // MARK: - VC Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            EndPoint.userId = userId
            self.cartCountAPI()
        }
        
        self.view.bringSubviewToFront(vwBottemButton)
        if addQty != 0{
            self.vwBottemButton.isHidden = false
         }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.CartCountUpdateNoti(notification:)), name: Notification.Name("CartCountUpdate"), object: nil)
    
        setUpTableview()
        self.arrProduct = fetchAllProductLocalStorge()
        EndPoint.userId = "\(productId)"
        EndPoint.deviceType = "ios"
        self.filterProductDescriptionData()
        
        setHeaderData()
        SetNavigation()
        
        self.arrRelatedProductIDs = arrProductDetail.subProducts ?? []
        
        for index in 0..<self.arrRelatedProductIDs.count {
            let data = self.arrRelatedProductIDs.object(at: index) as! NSDictionary
            let subProductID = data.value(forKey: "subProductId") as! Int
            let tempArray = arrProduct.filter{ $0.productId == subProductID }
                    
            if tempArray.count > 0 {
                self.arrRelatedProducts.append(tempArray.first!)
            }
        }
        
        self.arrsubSituteProductIDs = arrProductDetail.subsituteProducts ?? []
        
        for index in 0..<self.arrsubSituteProductIDs.count {
            let data = self.arrsubSituteProductIDs.object(at: index) as! NSDictionary
            let subProductID = data.value(forKey: "subsituteProductId") as! Int
            let tempArray = arrProduct.filter{ $0.productId == subProductID }
            
            print(tempArray.count)
            
            if tempArray.count > 0 {
                self.arrSituteProducts.append(tempArray.first!)
            }
        }
        
        addToCartBaseView.isHidden = true
        addToCartOptionView.layer.cornerRadius = 10.0
        
        btnContinueShopping.layer.cornerRadius = 20
        btnContinueShopping.addShadow()
        btnGoToCart.layer.cornerRadius = 20
        btnGoToCart.addShadow()
        self.tblProduct.reloadData()
    }
    
    @objc func CartCountUpdateNoti(notification: Notification) {
        cartCountAPI()
    }
    
    
    @IBAction func onClickContinueShoppingAcn() {
        addToCartBaseView.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickGoToCartAcn() {
        self.tabBarController?.tabBar.isHidden = false
        addToCartBaseView.isHidden = true
        let controller:CartConfirmationVC =  UIStoryboard(storyboard: .DashBoard).initVC()
            controller.ClassName = "ProductDetails"
        self.navigationController!.pushViewController(controller, animated: true)
        
        
    }
    
    
    //MARK:- Set Navigation
    func SetNavigation(){
        self.initBackButton()
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        /*self.initCartButton()
        cartButton.addTarget(self, action:#selector(cartButtonPressed), for: .touchUpInside)*/
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [cartBarButton], title: "Product Details")
    }
    
    //MARK:- Fetch All Product
    func fetchAllProductLocalStorge() -> [NewArrivalsModel] {
        if let syncContactsData = UserDefaults.standard.value(forKey: Constants.ProductModel) as? Data {
            if let aryProduct:[[String:Any]] = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(syncContactsData) as? [[String : Any]] {
                return Mapper<NewArrivalsModel>().mapArray(JSONArray: aryProduct)
            }
        }
        return []
    }
    
    
    // MARK: - Cart Button
    @objc func cartButtonPressed(){
        
        let controller:CartSummaryViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
     //   controller.hidesBottomBarWhenPushed = true
        controller.screenCommingFrom = "ProductDetails"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Filter Table Description Data
    func filterProductDescriptionData() {
        
        checkIfValueExist(arrProductDetail.itemGroupId, key: "itemGroupId")
        checkIfValueExist(arrProductDetail.dataOrigin, key: "dataOrigin")
        checkIfValueExist(arrProductDetail.itemRemarks, key: "itemRemarks")
        checkIfValueExist(arrProductDetail.itemResult, key: "itemResult")
        checkIfValueExist(arrProductDetail.salesPrice, key: "salesPrice")
      //  checkIfValueExist(arrProductDetail.salesUnit, key: "salesUnit")
        checkIfValueExist(arrProductDetail.availableQuantity, key: "availableQuantity")
        checkIfValueExist(arrProductDetail.accountNum, key: "accountNum")
     //   checkIfValueExist(arrProductDetail.agreementType, key: "agreementType")
        checkIfValueExist(arrProductDetail.agreementValue, key: "agreementValue")
        checkIfValueExist(arrProductDetail.quantityFrom, key: "quantityFrom")
        checkIfValueExist(arrProductDetail.variantType, key: "variantType")
        checkIfValueExist(arrProductDetail.containerType, key: "containerType")
        checkIfValueExist(arrProductDetail.productState, key: "productState")
        checkIfValueExist(arrProductDetail.brand, key: "brand")
       // checkIfValueExist(arrProductDetail.feature1, key: "feature1")
       // checkIfValueExist(arrProductDetail.feature2, key: "feature2")
       // checkIfValueExist(arrProductDetail.feature3, key: "feature3")
        checkIfValueExist(arrProductDetail.weight, key: "weight")
        checkIfValueExist(arrProductDetail.groupAttribute, key: "groupAttribute")
      //  checkIfValueExist(arrProductDetail.referenceCode, key: "referenceCode")
        checkIfValueExist(arrProductDetail.ingredients, key: "ingredients")
        checkIfValueExist(arrProductDetail.packing, key: "packing")
        checkIfValueExist(arrProductDetail.status, key: "status")
        checkIfValueExist(arrProductDetail.origin, key: "origin")
        checkIfValueExist(arrProductDetail.productId, key: "productId")
        print(arrProductDescription)
    }
    
    func checkIfValueExist(_ value : Any?, key : String)  {
        if value is String {
            if value as? String != " " && value as? String != "n/a" && value as? String != "" && value as? String != "NA"
            {
                if value as! String != "" {
                    let data = ["key" : key, "value" : value]
                    arrProductDescription.add(data)
                }
            }
        } else if value is Int {
            if value != nil {
                let data = ["key" : key, "value" : value]
                arrProductDescription.add(data)
            }
        }
    }
    
    // MARK: - Setup TableView
    func setUpTableview(){
        tblProduct.delegate = self
        tblProduct.dataSource = self
       // tblDescription.delegate = self
       // tblDescription.dataSource = self
        cvHeader.delegate = self
        cvHeader.dataSource = self
        cvHeader.bounces = false
        cvHeader.isPagingEnabled = true
        cvHeader.showsHorizontalScrollIndicator = false
        tblProduct.tableHeaderView = vwHeader
        //tblProduct.tableFooterView = vwFooter
        
        
        cvHeader.layer.cornerRadius = 10
        cvHeader.addShadowDash()
        vwRoundHeader.layer.cornerRadius = 10
        vwRoundHeader.addShadowDash()
        
        btnAdd.layer.cornerRadius = 5
        btnAdd.addShadowDash()
        
        vwQty.layer.cornerRadius = 8
        vwQty.addShadowDash()
        
        
    }
    
    // MARK: - Set Header Data
    func setHeaderData() {
        self.lblProductName.text = arrProductDetail.productDescription
        
        
        //Display Coming soon
        let availableQty = arrProductDetail.availableQuantity
        if ((availableQty) != nil )  {
            if (availableQty)! > 5 {
                
         if let startDate = arrProductDetail.fromDate,startDate != nil {
             
             let fromDate : Date = startDate.dateFromString(startDate)
             let endDate = arrProductDetail.toDate
             let toDate : Date = endDate!.dateFromString(endDate!)
             
             if Date().isBetween(fromDate, and: toDate) {
                 if let agreementValue = arrProductDetail.agreementValue, agreementValue != nil {
                    
                     lblProductSubtitle.isHidden = false
                     let agreementValue = arrProductDetail.agreementValue
                     lblProductSubtitle.text = String(format: "AED %.2f", agreementValue!)
                     
                     if let price = arrProductDetail.salesPrice,price != nil {
                         let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: String(format: "AED %.2f", price))
                         attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
                         lblProductPrice.isHidden = false
                         lblProductPrice.attributedText = attributeString
                     }
                 }
             } else {
                 if let price = arrProductDetail.salesPrice,price != nil {
                     lblProductPrice.isHidden = true
                     lblProductSubtitle.isHidden = false
                     lblProductSubtitle.text = String(format: "AED %.2f", price)
                 }
             }
         } else {
             if let price = arrProductDetail.salesPrice,price != nil {
                 lblProductPrice.isHidden = true
                 lblProductSubtitle.isHidden = false
                 lblProductSubtitle.text = String(format: "AED %.2f", price)
             }
         }
        
  
                 btnAdd.isHidden = false
                 lblComingSoon.isHidden = true
                if let price = arrProductDetail.salesPrice,price > 0 {
                }
                else{
                    btnAdd.isHidden = true
                    lblComingSoon.isHidden = false
                    lblProductPrice.isHidden = true
                    lblProductSubtitle.isHidden = true
                }
                
            } else {
                 btnAdd.isHidden = true
                 lblComingSoon.isHidden = false
                 lblProductPrice.isHidden = true
                 lblProductSubtitle.isHidden = true
            }
        }else {
            btnAdd.isHidden = true
            lblComingSoon.isHidden = false
            lblProductPrice.isHidden = true
            lblProductSubtitle.isHidden = true
        }
        
    }
    
    // MARK: - Calculate Total Price
    func calculateTotalPrice(qty: Int){
        
        //New Changes
        if let startDate = arrProductDetail.fromDate,startDate != nil {
            
            let fromDate : Date = startDate.dateFromString(startDate)
            let endDate = arrProductDetail.toDate
            let toDate : Date = endDate!.dateFromString(endDate!)
            
            if Date().isBetween(fromDate, and: toDate) {
                if let agreementValue = arrProductDetail.agreementValue, agreementValue != nil {
                    let agreementValue = arrProductDetail.agreementValue
                    
                    let totalPrice = agreementValue! * Float(qty)
                    btnShowItemCount.setTitle("\(qty) " + "Items  | AED " + String(format: "%.2f", totalPrice), for: .normal)
                    self.lblQty.text = "\(qty)"
                    
                } else {
                    if let price = arrProductDetail.salesPrice,price != 0 {
                        let totalPrice = price * Float(qty)
                        btnShowItemCount.setTitle("\(qty) " + "Items  | AED " + String(format: "%.2f", totalPrice), for: .normal)
                        self.lblQty.text = "\(qty)"
                    }
                }
            } else {
                if let price = arrProductDetail.salesPrice,price != 0 {
                    let totalPrice = price * Float(qty)
                    btnShowItemCount.setTitle("\(qty) " + "Items  | AED " + String(format: "%.2f", totalPrice), for: .normal)
                    self.lblQty.text = "\(qty)"
                }
            }
        } else {
            if let price = arrProductDetail.salesPrice,price != 0 {
                let totalPrice = price * Float(qty)
                btnShowItemCount.setTitle("\(qty) " + "Items  | AED " + String(format: "%.2f", totalPrice), for: .normal)
                self.lblQty.text = "\(qty)"
            }
        }
        
        
        
        /*
        if let price = arrProductDetail.salesPrice,price != 0.0 {
           
            let totalPrice = price * Float(qty)
            btnShowItemCount.setTitle("\(qty) " + "Items  | AED " + String(format: "%.2f", totalPrice), for: .normal)
            self.lblQty.text = "\(qty)"
        }
        */
    }
    

    // MARK: - Button Action
    @IBAction func btnRemoveQty(_ sender: UIButton) {
        if addQty != 0 {
            addQty = addQty - 1
            
            if addQty == 0{
                self.btnAdd.isHidden = false
                self.vwQty.isHidden = true
                self.vwBottemButton.isHidden = true
            } else {
                calculateTotalPrice(qty: addQty)
            }
            
           
        } else if addQty == 0{
            self.btnAdd.isHidden = false
            self.vwQty.isHidden = true
            self.vwBottemButton.isHidden = true
        }
        else{
            self.btnAdd.isHidden = false
            self.vwQty.isHidden = true
            self.vwBottemButton.isHidden = true
        }
        self.tblProduct.reloadData()
    }
    
    @IBAction func btnAddQty(_ sender: UIButton) {
        
        if addQty < 15 {
        }else{
            NotificationAlert().NotificationAlert(titles: "You have reached maximum order quantity")
            return
        }
        
        addQty = addQty + 1
        calculateTotalPrice(qty: addQty)
        self.tblProduct.reloadData()
    }
    
  
    
    @IBAction func btnAdd(_ sender: UIButton) {
        vwBottemButton.isHidden = false
        self.view.bringSubviewToFront(vwBottemButton)

        self.btnAdd.isHidden = true
        self.vwQty.isHidden = false
        if addQty == 0 {
            addQty = 1
            self.calculateTotalPrice(qty: addQty)
        }
    }
    
    @IBAction func btnAddToCart(_ sender: UIButton) {
        if addQty != 0 {
            let param = AddToCartModel()
            if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                param.userId = userId
            }
            let paramProduct = ProductModel()
            paramProduct.productId = arrProductDetail.productId
            paramProduct.productQuantity = addQty
            param.product.append(paramProduct)
            vwBottemButton.isHidden = true
            self.addToCartAPI(Model: param)
        }
    }
    
    //MARK:- Get Product detail Api
    func getProductDetailAPI() {
        self.showLoader()
        EndPoint.ProductDetail(t: NewArrivalsModel.self) { result in
            switch result {
            case .onSuccess(let data):
                self.hideLoader()
                if data != nil {
                    self.arrProductDetail = data!
                    self.filterProductDescriptionData()
                    self.tblProduct.reloadData()
                    //self.tblDescription.reloadData()
                    self.cvHeader.reloadData()
                }
            case .onFailure(let error):
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- Add to Cart Api
    func addToCartAPI(Model: AddToCartModel) {
        
        EndPoint.AddToCart(params: Model,t: AddToCartModel.self) { result in
            switch result {
            case .onSuccessWithStringValue(let msg):
                self.hideLoader()
                if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                    EndPoint.userId = userId
                    self.cartCountAPI()
                }
                NotificationAlert().NotificationAlert(titles: msg)
                
                self.addToCartBaseView.isHidden = false
                self.tabBarController?.tabBar.isHidden = true
                /*
                let controller:CartSummaryViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                //controller.hidesBottomBarWhenPushed = true
                controller.screenCommingFrom = "ProductDetails"
                self.navigationController?.pushViewController(controller, animated: true)
                */
                
                
            case .onFailure(let error):
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- Cart Count Api
    func cartCountAPI() {
        
        EndPoint.CartCount(t: CartCountModel.self) { result in
            switch result {
            case .onSuccess(let data):
            if data != nil{
                dictCartData = data!
                if let cartCount = dictCartData.cartCount, cartCount != 0 {
                    self.setBagCountOnCart(count: cartCount)
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "\(cartCount)"
                    }
                }
            }
            case .onFailure(let error):
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }

}
