//
//  ProductDetailDataSource.swift
//  ChefMiddleEast
//
//  Created by sandeep on 09/01/22.
//

import Foundation
import UIKit


extension ProjectDetailVC : UITableViewDataSource,UITableViewDelegate{
   
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if section == 0 {
            return 1
        } else if section == 1 {
            return arrProductDescription.count
        } else if section == 2 {
            if arrSituteProducts.count >  0{
                return 1
            }
            else{
                return 0
            }
        }
        else if section == 3 {
            if arrRelatedProducts.count > 0 {
                return 1
            }
            return 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 2 {
            if arrSituteProducts.count > 0 {
                return 35
            } else {
                return 0
            }
        }
        if section == 3 {
            if arrRelatedProducts.count > 0 {
                return 35
            }
            else{
                return 0
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 30, height: 40))
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 30, height: 40))
        label.font = UIFont(name: "Optima-Bold", size: 20)
        if section == 2 {
            label.text = "Subsitute Products"

        }
        else{
            label.text = "Related Products"
        }
        headerView.backgroundColor = UIColor.white
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblProduct {
        
            if indexPath.section == 0  {
                let cell = tblProduct.dequeueReusableCell(withIdentifier: "ProductDetailTVC") as! ProductDetailTVC
                
                cell.lblProductDescriptn.text = String("\(arrProductDetail.feature1 ?? "")\(" ")\(arrProductDetail.feature2 ?? "")\(" ")\(arrProductDetail.feature3 ?? "")")
             
                cell.vwRound.layer.cornerRadius = 10.0
                cell.vwRound.addShadowDash()
                return cell
            }
            else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDescriptionTVC") as! ProductDescriptionTVC
                let data = arrProductDescription.object(at: indexPath.row) as! NSDictionary
                cell.baseView.addShadowDash()
                cell.baseView.layer.cornerRadius = 10.0
                cell.lblProducTitle.text = (data["key"] as! String)
                         
                //Display Key
                let key = (data["key"] as! String)
                
                switch key {
                case "itemGroupId":
                    cell.lblProducTitle.text = "Status"
                    break
                case "productId":
                    cell.lblProducTitle.text = "Product ID"
                    break
                case "dataOrigin":
                    cell.lblProducTitle.text = "Origin"
                    break
                case "origin":
                    cell.lblProducTitle.text = "Origin"
                    break
                case "itemRemarks":
                    cell.lblProducTitle.text = "Remarks"
                    break
                case "itemResult":
                    cell.lblProducTitle.text = "Result"
                    break
                case "salesPrice":
                    cell.lblProducTitle.text = "Sales Price"
                    break
                case "salesUnit":
                    cell.lblProducTitle.text = "Sales Unit"
                    break
                case "availableQuantity":
                    cell.lblProducTitle.text = "Quantity"
                    break
                case "accountNum":
                    cell.lblProducTitle.text = "Account Number"
                    break
                case "agreementType":
                    cell.lblProducTitle.text = "Agreement Type"
                    break
                case "agreementValue":
                    cell.lblProducTitle.text = "Agreement Value"
                    break
                case "quantityFrom":
                    cell.lblProducTitle.text = "Quantity From"
                    break
                case "variantType":
                    cell.lblProducTitle.text = "Variant Type"
                    break
                case "containerType":
                    cell.lblProducTitle.text = "Container Type"
                    break
                case "productState":
                    cell.lblProducTitle.text = "Product State"
                    break
                case "brand":
                    cell.lblProducTitle.text = "Brand"
                    break
                case "weight":
                    cell.lblProducTitle.text = "Weight"
                    break
                case "groupAttribute":
                    cell.lblProducTitle.text = "Group Attribute"
                    break
                case "referenceCode":
                    cell.lblProducTitle.text = "Reference Code"
                    break
                case "ingredients":
                    cell.lblProducTitle.text = "Ingredients"
                    break
                case "packing":
                    cell.lblProducTitle.text = "Packaging"
                    break
                case "status":
                    cell.lblProducTitle.text = "Status"
                    break
                default:
                    break
                }
                
                let value = data["value"]!
                if value is String{
                    cell.lblProductDescriptn.text = (value as! String)
                } else {
                    cell.lblProductDescriptn.text = String("\(value)")
                }
                return cell
            }
            else if indexPath.section == 2  {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailRelatedItemsTVC") as! ProductDetailRelatedItemsTVC
                cell.arrRelatedProducts = arrSituteProducts
                cell.relatedProductCV.delegate = cell
                cell.relatedProductCV.dataSource = cell
                cell.lastView = self
               // cell.relatedProductCV.reloadData()
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailRelatedItems") as! ProductDetailRelatedItemsTVCCell
                cell.arrRelatedProducts = arrRelatedProducts
                cell.relatedProductCV.delegate = cell
                cell.relatedProductCV.dataSource = cell
                cell.lastView = self

               // cell.relatedProductCV.reloadData()
                return cell
            }
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDescriptionTVC") as! ProductDescriptionTVC
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0  {
            if arrProductDetail.feature1 == nil && arrProductDetail.feature2 == nil && arrProductDetail.feature3 == nil
            {
                return 0
            }
            return UITableView.automaticDimension

        } else if indexPath.section == 1 {
            return 50
        } else {
            let width = (UIScreen.main.bounds.size.width/2) + 100
            return width
        }
    }
}


extension ProjectDetailVC : UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in cvHeader.visibleCells {
            self.myPageCntrl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                
        if (arrProductDetail.productImage != nil) {
            if (arrProductDetail.productImage)?.count ?? 0 > 0 {
                if (arrProductDetail.productImage)?.count ?? 0 == 1 {
                    myPageCntrl.numberOfPages  = 0
                    return 1
                }
                else{
                    myPageCntrl.numberOfPages  = (arrProductDetail.productImage)?.count ?? 0
                    myPageCntrl.currentPage = 0
                    return (arrProductDetail.productImage)?.count ?? 1
                }
            }
        }
        else{
            myPageCntrl.numberOfPages  = 0
            return 1
        }
        myPageCntrl.numberOfPages  = 0
        return 1
                
                
     
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductDescriptionCVC", for: indexPath) as! ProductDescriptionCVC
        
        if (arrProductDetail.productImage != nil) {
            if (arrProductDetail.productImage)?.count ?? 0 > indexPath.row {
                let imageData = arrProductDetail.productImage![indexPath.row] as! [String : Any]
                let imagePath = String("\(EndPoint.BASE_API_PRODUCT_IMAGE_URL)\(imageData["productDetailImagePath"] as! String)")
                let urlString = imagePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
                cell.imgProduct?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                    if (error != nil) {
                        cell.imgProduct.image = UIImage(named: "placeholder")
                    } else {
                        cell.imgProduct.image = image
                    }
                }
            }
            else {
                cell.imgProduct.image = UIImage(named: "placeholder")
            }
          
        }else {
            cell.imgProduct.image = UIImage(named: "placeholder")
        }
                
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: cvHeader.frame.size.width, height: cvHeader.frame.size.height)
    }
}
