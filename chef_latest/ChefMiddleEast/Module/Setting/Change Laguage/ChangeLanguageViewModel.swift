//
//  ChangeLanguageViewModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
class ChangeLanguageViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    weak var dataSource:GenericDataSource<MockUp_ChangeLanguage>?
    
    init(dataSource : GenericDataSource<MockUp_ChangeLanguage>?) {
        self.dataSource = dataSource    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(){
        self.dataSource?.data.value.append(
            MockUp_ChangeLanguage.init(description: "", title: "", PlaceHolder: AppMessage_ChangePasswordSetting.Placeholder.CurrentPassword))
    
        
    }
}
