//
//  ChangeLanguageDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
import UIKit
@available(iOS 13.0, *)
class ChangeLanguageDataSource:GenericDataSource<MockUp_ChangeLanguage>,UITableViewDataSource,UITableViewDelegate{
    var tableView:UITableView?
    var didSelectRow: ((_ cell:UITableViewCell, _ indexPath:IndexPath) -> Void)?
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeLanguageSectionTableViewCell") as! ChangeLanguageSectionTableViewCell
        cell.titleLabel.text = AppMessage_ChangeLanguage.Placeholder.ChangeLanguage
        return cell
      
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.data.value.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let object = self.data.value[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeLanguageRowTableViewCell") as! ChangeLanguageRowTableViewCell
        cell.textField.text = self.data.value[indexPath.row].title
        cell.textField.placeholder = self.data.value[indexPath.row].PlaceHolder
        cell.textField.isUserInteractionEnabled = false
        Utility.shared.makeRoundCorner(layer: cell.cellRoundView.layer, color: UIColor.lightGray.withAlphaComponent(0.5), radius: 30.0)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
   
}
