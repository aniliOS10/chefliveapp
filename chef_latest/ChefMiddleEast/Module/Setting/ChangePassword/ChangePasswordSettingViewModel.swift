//
//  ChangePasswordSettingViewModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
class ChangePasswordSettingViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    weak var dataSource:GenericDataSource<MockUp_ChangePasswordSetting>?
    
    init(dataSource : GenericDataSource<MockUp_ChangePasswordSetting>?) {
        self.dataSource = dataSource    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(){
        self.dataSource?.data.value.append(
            MockUp_ChangePasswordSetting.init(description: "", title: "", PlaceHolder: AppMessage_ChangePasswordSetting.Placeholder.CurrentPassword))
        self.dataSource?.data.value.append(
            MockUp_ChangePasswordSetting.init(description: "", title: "", PlaceHolder: AppMessage_ChangePasswordSetting.Placeholder.SetNewPassword))
        self.dataSource?.data.value.append(
            MockUp_ChangePasswordSetting.init(description: "", title: "", PlaceHolder: AppMessage_ChangePasswordSetting.Placeholder.ConfirmNewPassword))

        
    }
}
