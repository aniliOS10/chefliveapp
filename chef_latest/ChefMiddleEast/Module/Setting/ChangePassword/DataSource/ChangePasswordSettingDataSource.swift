//
//  ChangePasswordSettingDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
import UIKit

extension ChangePasswordSettingViewController: UITableViewDataSource,UITableViewDelegate{
  
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChangePasswordSectionTableViewCell") as! ChangePasswordSectionTableViewCell
        cell.titleLabel.text = AppMessage_ChangePasswordSetting.Placeholder.ChangePassword
        return cell
      
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrPlaceHolder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChangePasswordRowTableViewCell") as! ChangePasswordRowTableViewCell
        
        cell.textField.delegate = self
        cell.textField.tag = indexPath.row
        cell.textField.placeholder = self.arrPlaceHolder[indexPath.row]
        cell.selectionStyle = .none
        cell.textField.isSecureTextEntry = false
        cell.textField.returnKeyType = .done

        Utility.shared.makeRoundCorner(layer: cell.cellRoundView.layer, color: UIColor.lightGray.withAlphaComponent(0.5), radius: 30.0)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
   
}

