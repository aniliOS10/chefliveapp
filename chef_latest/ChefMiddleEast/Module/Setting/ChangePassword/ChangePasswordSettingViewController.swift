//
//  ChangePasswordSettingViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import UIKit


class ChangePasswordSettingViewController:BaseViewController {
    
    @IBOutlet weak var tableView : UITableView!
    
    var arrPlaceHolder = [AppMessage_ChangePasswordSetting.Placeholder.CurrentPassword,AppMessage_ChangePasswordSetting.Placeholder.SetNewPassword,AppMessage_ChangePasswordSetting.Placeholder.ConfirmNewPassword]
    
    let params = UserModel()
    
    var dictPasswordInfo = [String:String]()
    
    
    //MARK:- VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if dictPasswordInfo.isEmpty {
            self.setUpIntialData()
        }
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: AppMessage_Setting.Placeholder.Settings)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordSettingViewController.keyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordSettingViewController.keyboardWillHide), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - KeyBoard Functions
    @objc func keyboardWillShow(_ notification:Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }
    
    
    @objc func keyboardWillHide(_ notification:Notification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func setUpIntialData() ->Void {
        
        dictPasswordInfo["old_password"] = ""
        
        dictPasswordInfo["new_password"] = ""
        
        dictPasswordInfo["confirm_password"] = ""
    }
    
    //MARK:- Change Password Params
    func paramResetPassword(){
        
        if !dictPasswordInfo.isEmpty {
                        
            if let olPass = dictPasswordInfo["old_password"],olPass.isEmpty || olPass == "" || olPass.count == 0{
                NotificationAlert().NotificationAlert(titles: "Please Enter Current Password")
                return
            }
            else if let newPass = dictPasswordInfo["new_password"], newPass.isEmpty || newPass == "" || newPass.count == 0{
                NotificationAlert().NotificationAlert(titles: "Please Enter new Password ")
                return
            }
            else if let confPassword = dictPasswordInfo["confirm_password"], confPassword.isEmpty || confPassword == "" || confPassword.count == 0{
                NotificationAlert().NotificationAlert(titles: "Please Enter Confirm Password ")
                return
            }
            else {
                
                if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                    let newPass = dictPasswordInfo["new_password"]
                    let confPassword = dictPasswordInfo["confirm_password"]
                    let currentPass = dictPasswordInfo["old_password"]
                    if newPass != confPassword {
                        NotificationAlert().NotificationAlert(titles: "Password not match")
                        return
                    }
                    else{
                        params.userId = String(userId)
                        params.newPassword = newPass
                        params.currentPassword = currentPass
                        self.changePasswordAPI(Model: params)
                    }
                }
            }
        }
        else {
            NotificationAlert().NotificationAlert(titles: "Please fill information")
        }
        
    }
    
    
    //MARK:- Button Action
    @IBAction func buttonChangePressed(_ sender: Any) {
        self.paramResetPassword()
    }
    
    // MARK: - Change Password API
    func changePasswordAPI(Model: UserModel){
        self.showLoader()
        EndPoint.ChangePassword(params: Model,t: UserModel.self) { result in
            switch result {
            case .onSuccessWithStringValue(let msg):
                NotificationAlert().NotificationAlert(titles: msg)
                self.navigationController?.popViewController(animated: true)
                self.hideLoader()
            case .onFailure(let errorMsg):
                print(errorMsg)
                NotificationAlert().NotificationAlert(titles: errorMsg)
                self.hideLoader()
            }
        }
    }
    
}


extension ChangePasswordSettingViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .done {
            
            textField.resignFirstResponder()
        }
        else {
            
            let textTag = textField.tag + 1
            
            if let nextResponder = textField.superview?.superview?.superview?.superview?.superview?.viewWithTag(textTag) {
                
                nextResponder.becomeFirstResponder()
            }
            else {
                
                textField.resignFirstResponder()
            }
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 0 {
            
            dictPasswordInfo["old_password"] = textField.text!.trimmingCharacters(in: .whitespaces)
        }
        else if textField.tag == 1 {
            
            if textField.text!.trimmingCharacters(in: .whitespaces).count < 6 {
                
                NotificationAlert().NotificationAlert(titles: "New Password should be 6 characters or more")
                return
            }
            else {
                dictPasswordInfo["new_password"] = textField.text!.trimmingCharacters(in: .whitespaces)
            }
        }
        else {
            
            if textField.text!.trimmingCharacters(in: .whitespaces).count < 6 {
                
                NotificationAlert().NotificationAlert(titles: "Confirm Password should be 6 characters or more")
                return
            }
            else {
            dictPasswordInfo["confirm_password"] = textField.text!.trimmingCharacters(in: .whitespaces)
            }
        }
    }
}
