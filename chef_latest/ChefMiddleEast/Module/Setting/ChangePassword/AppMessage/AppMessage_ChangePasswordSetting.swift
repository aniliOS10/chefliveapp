//
//  AppMessage_ChangePasswordSetting.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
class AppMessage_ChangePasswordSetting: NSObject {
    struct Placeholder {
        static let ChangePassword = NSLocalizedString("ChangePassword", comment: "")
        static let CurrentPassword = NSLocalizedString("CurrentPassword", comment: "")
        static let SetNewPassword = NSLocalizedString("SetNewPassword", comment: "")
        static let ConfirmNewPassword = NSLocalizedString("ConfirmNewPassword", comment: "")
    }
}
