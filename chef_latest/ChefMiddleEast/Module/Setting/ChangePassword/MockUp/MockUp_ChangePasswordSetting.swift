//
//  MockUp_ChangePasswordSetting.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
class MockUp_ChangePasswordSetting{
    var description : String!
    var title : String!
    var PlaceHolder : String!
    init(description: String, title: String,PlaceHolder : String!)
    {
        self.description = description
        self.title = title
        self.PlaceHolder = PlaceHolder
    }
}
