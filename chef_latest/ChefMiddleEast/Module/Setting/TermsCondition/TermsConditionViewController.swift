//
//  TermsConditionViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/11/21.
//

import UIKit

class TermsConditionViewController:BaseViewController {
    
    @IBOutlet weak var tableView : UITableView!
   
   var strTerms = String()
    
    //MARK:- VC Life Cycle
    override func viewWillAppear(_ animated: Bool) {
      //  self.tabBarController?.tabBar.isHidden = true
        self.setNavigation()
        self.termAndConditionAPI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    //MARK:- Navigation
    func setNavigation(){
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
      
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: "Terms & Conditions")
        
    }
   
   
    //MARK:- Button Action
    @IBAction func buttonCancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Terms And Condition API
    func termAndConditionAPI(){
        self.showLoader()
        EndPoint.GetTermsAndConditions(t: CountryListModel.self) {
            (result) in
            self.hideLoader()
            switch result {
            
            case .onSuccessWithStringValue(let strData):
                print(strData.html)
                self.strTerms = strData.html2String
                self.tableView.reloadData()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }

}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}

extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}
