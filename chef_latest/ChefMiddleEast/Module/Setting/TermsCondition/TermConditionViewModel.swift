//
//  TermConditionViewModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/11/21.
//

import Foundation
class TermConditionViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    weak var dataSource:GenericDataSource<Mockup_TermsandConditions>?
    
    init(dataSource : GenericDataSource<Mockup_TermsandConditions>?) {
        self.dataSource = dataSource    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(){
        self.dataSource?.data.value.append(
            Mockup_TermsandConditions.init(description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", title: "Terms and Conditions"))
        
    }
}
