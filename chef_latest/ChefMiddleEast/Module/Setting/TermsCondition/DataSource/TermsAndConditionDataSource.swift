//
//  TermsAndConditionDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/11/21.
//

import Foundation
import UIKit


extension TermsConditionViewController: UITableViewDataSource,UITableViewDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TermsConditionSectionTableViewCell") as! TermsConditionSectionTableViewCell
        cell.titleLabel.text = AppMessage_TermCondition.Placeholder.TermsandConditions
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "TermsConditionRowTableViewCell") as! TermsConditionRowTableViewCell
        cell.titleLabel.text = self.strTerms
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
}

