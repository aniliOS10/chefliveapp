//
//  DataSourceSortAndFilter.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/11/21.
//

/*
import Foundation
//
import UIKit
@available(iOS 13.0, *)
class DataSourceSortAndFilter:GenericDataSource<MockUp_SortFilter>,UITableViewDataSource,UITableViewDelegate{
    var tableView:UITableView?
    var didSelectRow: ((_ cell:UITableViewCell, _ indexPath:IndexPath) -> Void)?
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.data.value.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortAndFilterSectionTableViewCell") as! SortAndFilterSectionTableViewCell
        
       // Utility.shared.makeShadowsOfView_roundCorner(view: cell.cellViewRound, shadowRadius: 4.0, cornerRadius: 15, borderColor: UIColor.lightGray.withAlphaComponent(0.5))
       // Utility.shared.makeRoundCorner(layer: cell.cellViewRound.layer, color: UIColor.lightGray.withAlphaComponent(0.1), radius: 5.0)
       
        let object = self.data.value[section]
        cell.arrowButton.setImage(UIImage.init(named: "PlusRed"), for: .normal)
        Utility.shared.makeShadowsOfView_roundCorner(view: cell.cellViewRound, shadowRadius: 1.0, cornerRadius: 2, borderWidth: 0.2,borderColor: UIColor.lightGray)
        cell.titleLabel.text = object.title
        if object.isSelect{
            cell.lineView.isHidden = true
            cell.titleLabel.textColor = UIColor().getThemeColor()
            
        }else{
            cell.titleLabel.textColor = UIColor.black
            cell.lineView.isHidden = true
           
        }
        cell.titleLabel.text = object.title
        cell.selectionButton.tag = section
        cell.selectionButton.addTarget(self, action: #selector(self.selectionSectionButtonPressed), for: .touchUpInside)
           
        return cell
      
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.data.value[section].row.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let object = self.data.value[indexPath.section].row[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortAndFilterINNerTableViewCell") as! SortAndFilterINNerTableViewCell
        cell.row = self.data.value[indexPath.section].row
        cell.tableView.dataSource = cell
        cell.tableView.delegate = cell
        cell.supertTableView = self.tableView
        cell.tableView.reloadData()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let object = self.data.value[indexPath.section]
        if object.isSelect{
            var height :CGFloat = CGFloat(self.data.value[indexPath.section].row.count * 70)
            
            for Rowobject in  self.data.value[indexPath.section].row{
                if Rowobject.isSelect{
                   
                        let height1 :CGFloat = CGFloat(Rowobject.row.count * 20)
                    height = height + height1
                    }
            }
            return height
        }
        return 0.0
        
    }
    @objc func selectionSectionButtonPressed(sender:UIButton){
        var object = self.data.value[sender.tag]
        object.isSelect = !object.isSelect
        self.tableView?.reloadData()
    }
}

*/
