//
//  SortAndFilterVCExtension.swift
//  ChefMiddleEast
//
//  Created by sandeep on 18/01/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import UIKit


extension SortAndFilterViewController : UITableViewDelegate, UITableViewDataSource {
   
    func numberOfSections(in tableView: UITableView) -> Int {

        if selectedCategoryID == "" {
            return 2
        } else {
            return filterDataArray.count + 2
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedCategoryID == "" {
            return 1
        } else {
            if section < filterDataArray.count {
                if self.hiddenSections.contains(section) {
                        return 0
                }
                let data : NSDictionary =  filterDataArray.object(at: section) as! NSDictionary
                let key = allKeysArray.object(at: section) as! String
                return (data.value(forKey: key) as? NSArray)?.count ?? 0
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if selectedCategoryID == "" {
            
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SortAndFilterPriceTVC") as! SortAndFilterPriceTVC
                cell.btnFirstSelect.tag = indexPath.section
                cell.btnSecondSelect.tag = indexPath.section
                
                cell.btnFirstSelect.addTarget(self, action: #selector(self.radioFirstBtnTapped), for: .touchUpInside)
                cell.btnSecondSelect.addTarget(self, action: #selector(self.radioSecondBtnTapped), for: .touchUpInside)
                
                cell.lblTitle.text = "Price"
                let savedValue = filterSelection["Price"] as! String
               
                if savedValue == "Price Low To High" {
                    cell.imgFirstRadio.image = UIImage(named: "selectedImage")
                    cell.imgSecondRadio.image = UIImage(named: "unselected")
                } else if savedValue == "Price High To Low" {
                    cell.imgFirstRadio.image = UIImage(named: "unselected")
                    cell.imgSecondRadio.image = UIImage(named: "selectedImage")
                }
                cell.lblFirstTitle.text = "Price Low To High"
                cell.lblSecondTitle.text = "Price High To Low"
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SortAndFiltterApplyTVC") as! SortAndFiltterApplyTVC
                return cell
            }
        }
        else {
            
        if indexPath.section < filterDataArray.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SortAndFilterRowTableViewCell") as! SortAndFilterRowTableViewCell
            cell.contentView.backgroundColor = .white
            
            let data : NSDictionary =  filterDataArray.object(at: indexPath.section) as! NSDictionary
            let key = allKeysArray.object(at: indexPath.section) as! String
            let dataList = data.value(forKey: key) as! NSArray
            let title = (dataList.object(at: indexPath.row) as! String)
            
            let savedArray = filterSelection[key] as! NSArray
            
            if savedArray.contains(title) {
                cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                cell.checkButton.tintColor = UIColor().getThemeColor()
            } else {
                cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                cell.checkButton.tintColor = UIColor.black

            }
            cell.titleLabel.text = title
            cell.checkButton.isUserInteractionEnabled = false
            return cell
        } else if indexPath.section == filterDataArray.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SortAndFilterPriceTVC") as! SortAndFilterPriceTVC
            cell.btnFirstSelect.tag = indexPath.section
            cell.btnSecondSelect.tag = indexPath.section
            
            cell.btnFirstSelect.addTarget(self, action: #selector(self.radioFirstBtnTapped), for: .touchUpInside)
            cell.btnSecondSelect.addTarget(self, action: #selector(self.radioSecondBtnTapped), for: .touchUpInside)
            
            cell.lblTitle.text = "Price"
            let savedValue = filterSelection["Price"] as! String
           
            if savedValue == "Price Low To High" {
                cell.imgFirstRadio.image = UIImage(named: "selectedImage")
                cell.imgSecondRadio.image = UIImage(named: "unselected")
            } else if savedValue == "Price High To Low"{
                cell.imgFirstRadio.image = UIImage(named: "unselected")
                cell.imgSecondRadio.image = UIImage(named: "selectedImage")
            }
            cell.lblFirstTitle.text = "Price Low To High"
            cell.lblSecondTitle.text = "Price High To Low"
            return cell
        } else if indexPath.section == filterDataArray.count + 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SortAndFiltterApplyTVC") as! SortAndFiltterApplyTVC
            return cell
        }
        else {
            
            let dataCount = filterDataArray.count
            let count =  indexPath.section
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SortAndFilterRowTableViewCell") as! SortAndFilterRowTableViewCell
            cell.selectionStyle = .none
            cell.checkButton.isUserInteractionEnabled = true
            cell.checkButton.addTarget(self, action: #selector(self.checkBoxBtnTapped), for: .touchUpInside)
            switch count {
            case dataCount + 1:
                cell.titleLabel.text = "Caffeine"
                if self.checkSavedFilterValue("Caffeine") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            case dataCount + 2:
                cell.titleLabel.text = "Sugar Free"
                if self.checkSavedFilterValue("Sugar Free") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            case dataCount + 3:
                cell.titleLabel.text = "Lactose"
                if self.checkSavedFilterValue("Lactose") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            case dataCount + 4:
                cell.titleLabel.text = "Nuts"
                if self.checkSavedFilterValue("Nuts") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            case dataCount + 5:
                cell.titleLabel.text = "Low Fat"
                if self.checkSavedFilterValue("Low Fat") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            case dataCount + 6:
                cell.titleLabel.text = "Low Sugar"
                if self.checkSavedFilterValue("Low Sugar") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            case dataCount + 7:
                cell.titleLabel.text = "Organic"
                if self.checkSavedFilterValue("Organic") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            case dataCount + 8:
                cell.titleLabel.text = "Reduced Sugar"
                if self.checkSavedFilterValue("Reduced Sugar") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            case dataCount + 9:
                cell.titleLabel.text = "Vegan"
                if self.checkSavedFilterValue("Vegan") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            case dataCount + 10:
                cell.titleLabel.text = "Vegetarian"
                
                if self.checkSavedFilterValue("Vegetarian") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            case dataCount + 11:
                cell.titleLabel.text = "Halal Suitable"
                if self.checkSavedFilterValue("Halal Suitable") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            case dataCount + 12:
                cell.titleLabel.text = "Kosher Suitable"
                
                if self.checkSavedFilterValue("Kosher Suitable") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            case dataCount + 13:
                cell.titleLabel.text = "Gulten Free"
                if self.checkSavedFilterValue("Gulten Free") {
                    cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                    cell.checkButton.tintColor = UIColor().getThemeColor()

                } else {
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black

                }
                cell.checkButton.tag = indexPath.section
                break
            default:
                break
            }
            return cell
        }
        }
    }
    
    func checkSavedFilterValue(_ key : String) -> Bool {
        let savedValue = filterSelection[key] as! Bool
        return savedValue
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedCategoryID == "" {
            return 100
        } else {
            if indexPath.section < filterDataArray.count {
                return 50
            } else if indexPath.section == filterDataArray.count {
                return 100
            }
            else if indexPath.section == filterDataArray.count + 1 {
                return 70
            }
            else {
                return 50
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        /*
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortAndFilterSectionTableViewCell") as! SortAndFilterSectionTableViewCell
        
        
        let object = self.arrCategory[section]
        cell.arrowButton.setImage(UIImage.init(named: "PlusRed"), for: .normal)
        Utility.shared.makeShadowsOfView_roundCorner(view: cell.cellViewRound, shadowRadius: 1.0, cornerRadius: 2, borderWidth: 0.2,borderColor: UIColor.lightGray)
       /* if object.isSelect{
            cell.lineView.isHidden = true
            cell.titleLabel.textColor = UIColor().getThemeColor()
            
        }
        else{
            cell.titleLabel.textColor = UIColor.black
            cell.lineView.isHidden = true
        }*/
        cell.titleLabel.text = object
        cell.selectionButton.tag = section
        cell.selectionButton.addTarget(self, action: #selector(self.selectionSectionButtonPressed), for: .touchUpInside)
        
        return cell
        */
        
        if selectedCategoryID == "" {
            let view = UIView()
            return view
        } else {
            if section < filterDataArray.count {
                let data : NSDictionary = self.filterDataArray.object(at: section) as! NSDictionary
                
                let object = data.allKeys
                let sectionWidth = tblCategory.size.width - 20
                //Customize base View
                let headerView = UIView.init(frame: CGRect(x: 0.0, y: 0.0, width: sectionWidth, height: 40.0))
                headerView.backgroundColor = .white
                //headerView.layer.cornerRadius = 5.0
                headerView.tag = section
               // headerView.addShadow()
                
                //Initiate Plus Image
                let plusImg = UIImageView.init(frame: CGRect(x: sectionWidth - 20, y: 20, width: 15, height: 15))
                plusImg.contentMode = .scaleAspectFit
                plusImg.tag = section
                
                if self.hiddenSections.contains(section) {
                plusImg.image = UIImage(named: "PlusRed")
                    
                }
                else{
                    plusImg.image = UIImage(named: "minus")

                }
                
                //Initiate a label to show title
                let titleLabel = UILabel.init(frame: CGRect(x: 10, y: 10, width: sectionWidth - 60, height: 30))
                titleLabel.text = (object.first as! String)
                
                titleLabel.font = UIFont(name: "Optima-Bold", size: 15.0)
                titleLabel.textColor =  UIColor().getThemeColor()
                //Initiate a button to make header view clikable
                let sectionButton = UIButton.init(frame: CGRect(x: 0, y: 0, width: sectionWidth, height: 40))
                sectionButton.backgroundColor = .clear
                sectionButton.tag = section
                sectionButton.addTarget(self,
                                        action: #selector(self.selectionSectionButtonPressed(sender:)),
                                        for: .touchUpInside)
                
                headerView.addSubview(plusImg)
                headerView.addSubview(sectionButton)
                headerView.addSubview(titleLabel)
                return headerView
            }
            else {
                let view = UIView()
                return view
            }
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if selectedCategoryID == "" {
            return 0
        } else {
            if section < filterDataArray.count  {
                return 40
            }
            return 0
        }
        
        
        //return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if selectedCategoryID == "" {
            
        } else {
            if indexPath.section < filterDataArray.count {
                let cell : SortAndFilterRowTableViewCell = tblCategory.cellForRow(at: indexPath) as! SortAndFilterRowTableViewCell
                cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                cell.checkButton.tintColor = UIColor().getThemeColor()

                let key = allKeysArray.object(at: indexPath.section) as! String
               
                let savedArray = filterSelection[key] as! NSArray
                
                var arrayMu = NSMutableArray()
                if savedArray.count > 0 {
                    arrayMu = savedArray as! NSMutableArray
                }
                
                if arrayMu.contains(cell.titleLabel.text!) {
                     let index = arrayMu.index(of: cell.titleLabel.text!)
                    arrayMu.removeObject(at: index)
                    filterSelection[key] = arrayMu
                    cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.checkButton.tintColor = UIColor.black


                }
                else{
                    let temp = savedArray.mutableCopy() as! NSMutableArray
                    
                    temp.add(cell.titleLabel.text!)
                    
                    (savedArray.mutableCopy() as! NSMutableArray).add(cell.titleLabel.text!)
                    filterSelection[key] = temp

                }
                
    
                
                
            } else {
               // let cell : SortAndFilterPriceTVC = tblCategory.cellForRow(at: indexPath) as! SortAndFilterPriceTVC
            }
        }
    }
    
    @objc func checkBoxBtnTapped(sender:UIButton){
        print(sender.tag)
        if sender.tag > filterDataArray.count {
            let cell : SortAndFilterRowTableViewCell = tblCategory.cellForRow(at: IndexPath(row: 0, section: sender.tag)) as! SortAndFilterRowTableViewCell
            
            let key = allKeysArray.object(at: sender.tag) as! String
            
            if self.checkSavedFilterValue(key) {
                filterSelection[key] = false
                cell.checkButton.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                cell.checkButton.tintColor = UIColor.black

            } else {
                filterSelection[key] = true
                cell.checkButton.setImage(UIImage(named: "checkBoxRed"), for: .normal)
                cell.checkButton.tintColor = UIColor().getThemeColor()

            }
        }
    }
    
    @objc func radioFirstBtnTapped(sender:UIButton){
        
        let cell : SortAndFilterPriceTVC = tblCategory.cellForRow(at: IndexPath(row: 0, section: sender.tag)) as! SortAndFilterPriceTVC
        
        if  selectedCategoryID == ""
            {
            cell.imgFirstRadio.image = UIImage(named: "selectedImage")
            cell.imgSecondRadio.image = UIImage(named: "unselected")
            
            var key = allKeysArray.object(at: sender.tag + 3) as! String
            
            if filterDataArray.count == 0 {
                 key = allKeysArray.object(at: sender.tag) as! String
            }
            
            if key != "Price" {
                filterSelection[key] = true
            } else {
                filterSelection[key] = "Price Low To High"
            }
            }
        else{
            cell.imgFirstRadio.image = UIImage(named: "selectedImage")
            cell.imgSecondRadio.image = UIImage(named: "unselected")
            
            var key = allKeysArray.object(at: sender.tag) as! String
            
          
            
            if key != "Price" {
                filterSelection[key] = true
            } else {
                filterSelection[key] = "Price Low To High"
            }
        }
        
      
        
    }
    @objc func radioSecondBtnTapped(sender:UIButton){
        
        let cell : SortAndFilterPriceTVC = tblCategory.cellForRow(at: IndexPath(row: 0, section: sender.tag)) as! SortAndFilterPriceTVC

        if  selectedCategoryID == "" {
            
            cell.imgFirstRadio.image = UIImage(named: "unselected")
            cell.imgSecondRadio.image = UIImage(named: "selectedImage")
            
            var key = allKeysArray.object(at: sender.tag + 3) as! String
            
            if filterDataArray.count == 0 {
                 key = allKeysArray.object(at: sender.tag) as! String
            }
            
            if key != "Price" {
                filterSelection[key] = false
            } else {
                filterSelection[key] = "Price High To Low"
            }
        }
        else{
            let cell : SortAndFilterPriceTVC = tblCategory.cellForRow(at: IndexPath(row: 0, section: sender.tag)) as! SortAndFilterPriceTVC
            
            cell.imgFirstRadio.image = UIImage(named: "unselected")
            cell.imgSecondRadio.image = UIImage(named: "selectedImage")
            
            let key = allKeysArray.object(at: sender.tag) as! String
            
            if key != "Price" {
                filterSelection[key] = false
            } else {
                filterSelection[key] = "Price High To Low"
            }
        }
        
        
        

        
     
        
    }
    
    
    func  openWithIndex(_ index: Int){
        
        /*
          var object = self.arrCategory[sender.tag]
       // object.isSelect = !object.isSelect
        
        let categoryId = self.arrCategory[sender.tag]
        self.arrSubCategory = self.arrAllProducts.filter {$0.categoryId == categoryId}
        self.arrSubCategory = self.arrSubCategory.unique{$0.subCategoryId}
        self.tblCategory.reloadData()
        */
        
        
        
        let section = index
        
        
        
        func indexPathsForSection() -> [IndexPath] {
            var indexPaths = [IndexPath]()
            
//            for row in 0..<self.arrCategory[section].count {
//                indexPaths.append(IndexPath(row: row,
//                                            section: section))
//            }
            let data : NSDictionary = filterDataArray.object(at: section) as! NSDictionary
            let key = allKeysArray.object(at: section) as! String
            for row in 0..<(data.object(forKey: key) as! NSArray).count {
                indexPaths.append(IndexPath(row: row,
                                            section: section))
            }
            return indexPaths
        }
        
      //  let headerView : UIView = tblCategory.viewWithTag(section)!

        
        if self.hiddenSections.contains(section) {
//            for subView in headerView.subviews {
//                if subView is UIImageView {
//                    let sub : UIImageView = (subView as? UIImageView)!
//                    sub.contentMode = .scaleAspectFit
//                    sub.image = UIImage(named: "minus")
//                }
//            }
            self.hiddenSections.remove(section)
            self.tblCategory.insertRows(at: indexPathsForSection(),
                                      with: .fade)
        } else {
//            for subView in headerView.subviews {
//                if subView is UIImageView {
//                    let sub : UIImageView = (subView as? UIImageView)!
//                    sub.contentMode = .scaleAspectFit
//                    sub.image = UIImage(named: "PlusRed")
//                }
//            }
            self.hiddenSections.insert(section)
            self.tblCategory.deleteRows(at: indexPathsForSection(),
                                      with: .fade)
        }
    }
    
    
    //MARK:- Button Show Sub Category
    @objc func selectionSectionButtonPressed(sender:UIButton){
       
        /*
          var object = self.arrCategory[sender.tag]
       // object.isSelect = !object.isSelect
        
        let categoryId = self.arrCategory[sender.tag]
        self.arrSubCategory = self.arrAllProducts.filter {$0.categoryId == categoryId}
        self.arrSubCategory = self.arrSubCategory.unique{$0.subCategoryId}
        self.tblCategory.reloadData()
        */
        
        
        let section = sender.tag
        
        let headerView : UIView = tblCategory.viewWithTag(section)!
        
        
        func indexPathsForSection() -> [IndexPath] {
            var indexPaths = [IndexPath]()
            
//            for row in 0..<self.arrCategory[section].count {
//                indexPaths.append(IndexPath(row: row,
//                                            section: section))
//            }
            let data : NSDictionary = filterDataArray.object(at: section) as! NSDictionary
            let key = allKeysArray.object(at: section) as! String
            for row in 0..<(data.object(forKey: key) as! NSArray).count {
                indexPaths.append(IndexPath(row: row,
                                            section: section))
            }
            return indexPaths
        }
        
        if self.hiddenSections.contains(section) {
            for subView in headerView.subviews {
                if subView is UIImageView {
                    let sub : UIImageView = (subView as? UIImageView)!
                    sub.contentMode = .scaleAspectFit
                    sub.image = UIImage(named: "minus")
                }
            }
            self.hiddenSections.remove(section)
            self.tblCategory.insertRows(at: indexPathsForSection(),
                                      with: .fade)
        } else {
            for subView in headerView.subviews {
                if subView is UIImageView {
                    let sub : UIImageView = (subView as? UIImageView)!
                    sub.contentMode = .scaleAspectFit
                    sub.image = UIImage(named: "PlusRed")
                }
            }
            self.hiddenSections.insert(section)
            self.tblCategory.deleteRows(at: indexPathsForSection(),
                                      with: .fade)
        }
        
        self.tblCategory.reloadData()
    }
    
}
