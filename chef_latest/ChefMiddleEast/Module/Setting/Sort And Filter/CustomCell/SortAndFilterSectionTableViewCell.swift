//
//  SortAndFilterSectionTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/11/21.
//

import UIKit

class SortAndFilterSectionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectionButton: UIButton!
    @IBOutlet weak var arrowButton: UIButton!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var cellViewRound: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
