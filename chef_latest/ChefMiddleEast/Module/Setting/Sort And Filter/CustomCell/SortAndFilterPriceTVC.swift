//
//  SortAndFilterPriceTVC.swift
//  ChefMiddleEast
//
//  Created by Apple on 08/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class SortAndFilterPriceTVC: UITableViewCell {

    @IBOutlet weak var imgFirstRadio : UIImageView!
    @IBOutlet weak var imgSecondRadio : UIImageView!
    @IBOutlet weak var btnFirstSelect : UIButton!
    @IBOutlet weak var btnSecondSelect : UIButton!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var headerView : UIView!
    @IBOutlet weak var lblFirstTitle : UILabel!
    @IBOutlet weak var lblSecondTitle : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
