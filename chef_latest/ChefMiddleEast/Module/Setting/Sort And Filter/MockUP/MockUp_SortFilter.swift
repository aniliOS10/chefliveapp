//
//  MockUp_SortFilter.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/11/21.
//

import Foundation
import UIKit
class MockUp_SortFilter{
    var description : String!
    var title : String!
    var isSelect : Bool = false
    var row : [MockUp_SortFilterRow] = []
    init(description: String, title: String, isSelect: Bool, row: [MockUp_SortFilterRow])
    {
        self.description = description
        self.title = title
        self.isSelect = isSelect
        self.row = row
    }
}
class MockUp_SortFilterRow{
    var description : String!
    var title : String!
    var image : UIImage!
    var isSelect : Bool = false
    var row : [MockUp_SortFilter_SubRow] = []
    init(description: String, title: String, isSelect: Bool, image: UIImage, row: [MockUp_SortFilter_SubRow])
    {
        self.description = description
        self.title = title
        self.image = image
        self.isSelect = isSelect
        self.row = row
    }
}
class MockUp_SortFilter_SubRow{
    var description : String!
    var title : String!
    var image : UIImage!
    var isSelect : Bool = false
    init(description: String, title: String, isSelect: Bool, image: UIImage)
    {
        self.description = description
        self.title = title
        self.image = image
        self.isSelect = isSelect
    }
}
