//
//  MockUp_Setting.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
//MockUp_Setting
class MockUp_Setting{
    var description : String!
    var title : String!
    var isSelected : Bool!
    var isToggle : Bool!
    init(description: String, title: String,isSelected : Bool!,isToggle : Bool!)
    {
        self.description = description
        self.title = title
        self.isSelected = isSelected
        self.isToggle = isToggle
    }
}
