//
//  SettingDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
import UIKit
class SettingDataSource:GenericDataSource<MockUp_Setting>,UITableViewDataSource,UITableViewDelegate{
    var tableView:UITableView?
    var didSelectRow: ((_ cell:UITableViewCell, _ indexPath:IndexPath) -> Void)?
    
    var isBool = false
    var isSub = false
    
    var isBoolNew = false
    var isSubNew = false
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingSectionTableViewCell") as! SettingSectionTableViewCell
        cell.titleLabel.text = AppMessage_Setting.Placeholder.Settings
        return nil
      
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.data.value.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let object = self.data.value[indexPath.row]
        if object.isToggle{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingRowSwitchTableViewCell") as! SettingRowSwitchTableViewCell
            cell.titleLabel.text = self.data.value[indexPath.row].title
            
            cell.switchButton.tag = indexPath.row // for detect which row switch Changed
            cell.switchButton.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
            
            if indexPath.row == 1 {
                cell.switchButton.isOn = UserDefaults.standard.bool(forKey: Constants.NotificationOnOff)
                isBool = cell.switchButton.isOn
            }
            else{
                cell.switchButton.isOn = UserDefaults.standard.bool(forKey: Constants.NotificationOnOffSub)
                isSub = cell.switchButton.isOn
            }
            cell.tag = indexPath.row

            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingRowForwardTableViewCell") as! SettingRowForwardTableViewCell
        cell.titleLabel.text = self.data.value[indexPath.row].title
        cell.tag = indexPath.row
       
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath){
            self.didSelectRow!(cell,indexPath)
        }
       
    }
    
     @objc func switchChanged(_ sender : UISwitch!){

        print("table row switch Changed \(sender.tag)")
        print("The switch is \(sender.isOn ? "ON" : "OFF")")

        if sender.tag == 1 {
            
            
            let cell : SettingRowSwitchTableViewCell = tableView?.cellForRow(at: IndexPath(row: 2, section: 0)) as! SettingRowSwitchTableViewCell
            isBool = sender.isOn
            isSub = cell.switchButton.isOn
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "settingAPI"), object: nil)

        }
        else{
            isSub = sender.isOn
            let cell : SettingRowSwitchTableViewCell = tableView?.cellForRow(at: IndexPath(row: 1, section: 0)) as! SettingRowSwitchTableViewCell
            isBool = cell.switchButton.isOn
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "settingAPI"), object: nil)

        }
    }
}
