//
//  SettingViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import UIKit

@available(iOS 13.0, *)
class SettingViewController:BaseViewController {
    @IBOutlet weak var tableView : UITableView!
   
     private let dataSource:SettingDataSource?
    
    lazy var viewModel:SettingViewModel = {
        let viewModel = SettingViewModel(dataSource: self.dataSource)
        return viewModel
    }()
    init(dashBoardViewDataSource:SettingDataSource)
    {
        self.dataSource = dashBoardViewDataSource
        super.init(nibName: nil, bundle: nil)
    }
  
    convenience init(dataSourceCollectionView1:SettingDataSource) {
        self.init(dashBoardViewDataSource: dataSourceCollectionView1)
    }
    required init?(coder: NSCoder) {
         self.dataSource = SettingDataSource()
         super.init(coder: coder)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource?.tableView = self.tableView
        self.tableView.delegate = self.dataSource
        self.tableView.dataSource = self.dataSource
        self.viewModel.dataSource?.data.addAndNotify(observer: self, completionHandler: { mockUp_Dashboard in
            self.tableView.reloadData()
        })
        self.viewModel.makeDashboardList()
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
      
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: AppMessage_Setting.Placeholder.Settings)
        self.didSelectRow()
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.settingAPI(notification:)), name: Notification.Name("settingAPI"), object: nil)

        // Do any additional setup after loading the view.
    }
   
    func didSelectRow(){
        self.dataSource?.didSelectRow = { cell,indexPath in
            
            if !UserDefaults.standard.bool(forKey: Constants.guestUser) {

                if self.dataSource?.data.value[cell.tag].title == AppMessage_ChangePasswordSetting.Placeholder.ChangePassword{
                    let controller:ChangePasswordSettingViewController =  UIStoryboard(storyboard: .Setting).initVC()
                    self.navigationController?.pushViewController(controller, animated: true)
                
              
            }else if   self.dataSource?.data.value[cell.tag].title == AppMessage_ChangeLanguage.Placeholder.ChangeLanguage {
                let controller:ChangeLanguageViewController =  UIStoryboard(storyboard: .Setting).initVC()
                self.navigationController?.pushViewController(controller, animated: true)
        }
            }
            
        }
    }
    @IBAction func buttonCancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func settingAPI(notification: Notification) {
             callAPI()
    }
    
    func callAPI(){
        let param = SettingModel()
        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            param.userId = userId
        }
        param.notificationStatus = dataSource?.isBool
        param.subscriptionStatus = dataSource?.isSub

        EndPoint.setNotificationStatus(params: param, t: BaseResponse.self) { [weak self] result in
            switch result {
            case .onSuccessWithStringValue(let msg):
                if msg != "" {
                    NotificationAlert().NotificationAlert(titles: msg)
                    UserDefaults.standard.set(param.notificationStatus, forKey: Constants.NotificationOnOff)
                    
                    UserDefaults.standard.set(param.subscriptionStatus, forKey: Constants.NotificationOnOffSub)
                }
            case .onFailure(let error):
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
    }
}
    
   

}
