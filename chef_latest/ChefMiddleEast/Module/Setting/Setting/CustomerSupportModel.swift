//
//  CustomerSupportModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/03/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class CustomerSupportModel : BaseResponse {
    
    var name : String?
    var email : String?
    var phone : String?
    var txtMessage : String?
    var emailTo : String?
    
    override init() {
         super.init()
    }
    convenience required init?(map: Map) {
        self.init()
    }
    override func mapping(map: Map) {
        name <- map["name"]
        email <- map["email"]
        phone <- map["phone"]
        txtMessage <- map["message"]
        emailTo <- map["emailTo"]
    }
    
}
