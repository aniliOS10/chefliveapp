//
//  AppMessage_Setting.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
//
class AppMessage_Setting: NSObject {
    struct Placeholder {
        static let Settings = NSLocalizedString("Settings", comment: "")
        
        static let ContactUs = NSLocalizedString("Contact Us", comment: "")

    }
}
