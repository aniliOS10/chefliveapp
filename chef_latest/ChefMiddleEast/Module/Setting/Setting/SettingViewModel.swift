//
//  SettingViewModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
class SettingViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    weak var dataSource:GenericDataSource<MockUp_Setting>?
    
    init(dataSource : GenericDataSource<MockUp_Setting>?) {
        self.dataSource = dataSource    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(){
       /* self.dataSource?.data.value.append(
            MockUp_Setting.init(description: "", title: "Profile Settings",isSelected: false,isToggle: false))
        */
        
            if !UserDefaults.standard.bool(forKey: Constants.guestUser) {
                self.dataSource?.data.value.append(
                    MockUp_Setting.init(description: "", title: AppMessage_ChangePasswordSetting.Placeholder.ChangePassword,isSelected: false,isToggle: false))
               /* self.dataSource?.data.value.append(
                    MockUp_Setting.init(description: "", title: "Change Language",isSelected: false,isToggle: false))
                self.dataSource?.data.value.append(
                    MockUp_Setting.init(description: "", title: "Multiple Accounts",isSelected: false,isToggle: false))
                self.dataSource?.data.value.append(
                    MockUp_Setting.init(description: "", title: "Dark Account",isSelected: true,isToggle: true))*/
                self.dataSource?.data.value.append(
                    MockUp_Setting.init(description: "", title: "Notification",isSelected: true,isToggle: true))
                
                self.dataSource?.data.value.append(
                    MockUp_Setting.init(description: "", title: "Subscribe to Newsletter",isSelected: true,isToggle: true))
            
        }
        else{
            self.dataSource?.data.value.append(
                MockUp_Setting.init(description: "", title: "Notification",isSelected: true,isToggle: true))
        }
    
        
    }
}
