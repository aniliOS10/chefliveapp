//
//  ContactUsDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

extension ContactUsViewController: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
      //  return self.data.value.count
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
         if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsSectionLineTableViewCell") as! ContactUsSectionLineTableViewCell
            
            return cell
        }
       /* if self.data.value[section].isLine{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsSectionLineTableViewCell") as! ContactUsSectionLineTableViewCell
            
            return cell
        }*/
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsSectionTableViewCell") as! ContactUsSectionTableViewCell
        cell.titleLabel.text = AppMessage_ContactUs.Placeholder.ContactUs
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 1 {
          //  return UITableView.automaticDimension
            return 30

        }
       /* if self.data.value[section].isLine{
            return UITableView.automaticDimension
        }*/
        return 0.0
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsFooterTableViewCell") as! ContactUsFooterTableViewCell
            return cell
        }
       /* if self.data.value[section].isLine{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsFooterTableViewCell") as! ContactUsFooterTableViewCell
            return cell
        }*/
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1 {
            return UITableView.automaticDimension
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0 {
            return 5
        }
        else if section == 1 {
            return 4
        }
        return 0
       // return self.data.value[section].row.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactusRowIConTableViewCell") as! ContactusRowIConTableViewCell
            
            Utility.shared.makeRoundCorner(layer: cell.cellRoundView.layer, color: UIColor.lightGray.withAlphaComponent(0.5), radius: 30.0)
           
            cell.textField.isUserInteractionEnabled = false
            cell.iconImage.image = arrImg[indexPath.row]

            if indexPath.row == 0 {
                if let phone = dictAppInfo.phone, !phone.isEmpty {
                    cell.textField.text = "Call Us " + phone
                }
                else{
                    cell.textField.placeholder = "Call Us"
                }
                
            }
            else if indexPath.row == 1{
                if let whatsAppNumber = dictAppInfo.whatsAppNumber, !whatsAppNumber.isEmpty {
                 //   cell.textField.text = whatsAppNumber
                }
                else{
                    cell.textField.placeholder = "Contact with us on Whatsapp"
                }
                
                cell.textField.text = "Contact with us on Whatsapp"

            }
            else if indexPath.row == 2{
                if let instaId = dictAppInfo.instaId, !instaId.isEmpty {
                    cell.textField.text = instaId
                }
                else{
                    cell.textField.placeholder = "Connect us on Instagram"
                }
                cell.textField.text = "Follow us on Instagram"

            }
            else if indexPath.row == 3{
                if let email = dictAppInfo.email, !email.isEmpty {
                    cell.textField.text = email
                }
                else{
                    cell.textField.placeholder = "Connect with Email"
                }
                
                cell.textField.text = "Contact us via Email"

            }
            else {
                if let address = dictAppInfo.address, !address.isEmpty {
                    cell.textField.text = address
                }
                else{
                    cell.textField.placeholder = "Address"
                }
                
                cell.textField.text = "Find us on Google Maps"

            }
           
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsRowTableViewCell") as! ContactUsRowTableViewCell
            Utility.shared.makeRoundCorner(layer: cell.cellRoundView.layer, color: UIColor.lightGray.withAlphaComponent(0.5), radius: 30.0)
            
            if indexPath.row == 0 {
        
                cell.textField.placeholder = "Name"
                
            }
            else if indexPath.row == 1 {
                cell.textField.placeholder = "Email"
            }
            else if indexPath.row == 2 {
                cell.textField.placeholder = "Phone"
            }
            else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsRowTextViewTableViewCell") as! ContactUsRowTextViewTableViewCell
                
                Utility.shared.makeRoundCorner(layer: cell.cellRoundView.layer, color: UIColor.lightGray.withAlphaComponent(0.5), radius: 10.0)
                
                return cell
            }
            
            return cell
        }
   
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsRowTextViewTableViewCell") as! ContactUsRowTextViewTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
         if indexPath.section == 1 {
            if indexPath.row == 3 {
                return 120
            }
        }
        return UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {

            if indexPath.row == 0 {
                
                if let phone = dictAppInfo.phone, !phone.isEmpty {
                    
                    guard let url = URL(string: "telprompt://\(phone)"), UIApplication.shared.canOpenURL(url) else {
                        return
                    }
                    UIApplication.shared.open(url, options: [:]
                                              , completionHandler: nil)
                }
            }
            else if indexPath.row == 1 {
                
                if let whatsAppNumber = dictAppInfo.whatsAppNumber, !whatsAppNumber.isEmpty {
                    openWhatsApp(number: whatsAppNumber)
                }
            }
            else if indexPath.row == 2 {
                
                var instagramHooks = String()
                
                if let instaId = dictAppInfo.instaId, !instaId.isEmpty {
                   //  instagramHooks = "instagram://user?username=\(instaId)"
                    
                    instagramHooks = instaId
                }
                
                let instagramUrl = NSURL(string: instagramHooks)
                if UIApplication.shared.canOpenURL(instagramUrl! as URL) {
                    UIApplication.shared.open(instagramUrl! as URL)
                } else {
                  //redirect to safari because the user doesn't have Instagram
                    UIApplication.shared.open(NSURL(string: "http://instagram.com/")! as URL)
                }
                
                
                
                
            }
            else if indexPath.row == 3 {
                if let email = dictAppInfo.email, !email.isEmpty {
                    self.sendEmail(email: email)
                }
            }
            else{
                
               var destinationLat =  Double()
               var destinationLong =  Double()
                destinationLat = 24.9661026
                destinationLong = 55.202301
                
                
                if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {  let urlbase = URL(string: "comgooglemaps://?saddr=&daddr=\(destinationLat),\(destinationLong)")
                   
                   UIApplication.shared.open(urlbase!, options: [:], completionHandler: nil)
                   
                } else {
//                   let webUrl = URL(string: "https://www.google.co.in/maps/dir/?saddr=\(destinationLat),\(destinationLong)&daddr=\(destinationLat),\(destinationLong)")
                    
                    let webURL = "https://www.google.com/maps/place/Chef+Middle+East+LLC/@24.9661026,55.202301,15z/data=!4m5!3m4!1s0x0:0x9e44eaef2fc0122!8m2!3d24.9661026!4d55.202301"
                    
                    let webUrl = URL(string:webURL)
                  
                   UIApplication.shared.open(webUrl!, options: [:], completionHandler: nil)
                   
           }
                
//
//                self.openMapForPlace(lat: 24.967784, long: 55.202672)
//
//
//                if let add = dictAppInfo.address, !add.isEmpty {
//                let address = "Dubai Investment Park Second, Dubai, United Arab Emirates"
//                let geocoder = CLGeocoder()
//                geocoder.geocodeAddressString(address) { (placemarks, error) in
//
//                  if error != nil {
//                    //Deal with error here
//                  } else if let placemarks = placemarks {
//                    if let coordinate = placemarks.first?.location?.coordinate {
//                    }
//                  }
//                }
//
//            }
        }
        }
    }
    
    public func openMapForPlace(lat:Double = 0, long:Double = 0) {
        let latitude: CLLocationDegrees = lat
        let longitude: CLLocationDegrees = long

        let regionDistance:CLLocationDistance = 100
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Chef Middle East LLC"
        mapItem.openInMaps(launchOptions: options)
    }
    
}
