//
//  ContactUsViewModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
import UIKit
class ContactUsViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    weak var dataSource:GenericDataSource<MockUP_ContactUs>?
    
    init(dataSource : GenericDataSource<MockUP_ContactUs>?) {
        self.dataSource = dataSource    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(){
        
        var arrRow1:[MockUP_ContactUs_Row] = []
        arrRow1.append(
            MockUP_ContactUs_Row.init(description: "", title: "What is Lorem IPsum", PlaceHolder: "What is Lorem IPsum", imageIcon: UIImage.init(named: "phoneCall") ?? UIImage.init(), isTextView: false, isImageIcon: true))
        arrRow1.append(
            MockUP_ContactUs_Row.init(description: "", title: "What is Lorem IPsum", PlaceHolder: "What is Lorem IPsum", imageIcon: UIImage.init(named: "EmailIcon") ?? UIImage.init(), isTextView: false, isImageIcon: true))
        arrRow1.append(
            MockUP_ContactUs_Row.init(description: "", title: "What is Lorem IPsum", PlaceHolder: "What is Lorem IPsum", imageIcon: UIImage.init(named: "cursor") ?? UIImage.init(), isTextView: false, isImageIcon: true))
      
        self.dataSource?.data.value.append(  MockUP_ContactUs.init(description: "", title: "What is Leorem", isLine: false, row: arrRow1))
        
        
        var arrRow2:[MockUP_ContactUs_Row] = []
       
        arrRow2.append(
            MockUP_ContactUs_Row.init(description: "", title: "What is Lorem IPsum", PlaceHolder: "Name", imageIcon: UIImage.init(named: "EmailIcon") ?? UIImage.init(), isTextView: false, isImageIcon: false))
        arrRow2.append(
            MockUP_ContactUs_Row.init(description: "", title: "What is Lorem IPsum", PlaceHolder: "Email", imageIcon: UIImage.init(named: "cursor") ?? UIImage.init(), isTextView: false, isImageIcon: false))
        arrRow2.append(
            MockUP_ContactUs_Row.init(description: "", title: "What is Lorem IPsum this is Lorem IPsum", PlaceHolder: "Message", imageIcon: UIImage.init(named: "phoneCall") ?? UIImage.init(), isTextView: true, isImageIcon: false))
        self.dataSource?.data.value.append(  MockUP_ContactUs.init(description: "", title: "What is Leorem", isLine: true, row: arrRow2))
        
        
    
        
    }
}
