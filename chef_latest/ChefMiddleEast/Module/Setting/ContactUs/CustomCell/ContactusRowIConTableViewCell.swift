//
//  ContactusRowIConTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import UIKit

class ContactusRowIConTableViewCell: UITableViewCell {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var cellRoundView: UIView!
    @IBOutlet weak var arrowButton: UIButton!
    @IBOutlet weak var iconImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
