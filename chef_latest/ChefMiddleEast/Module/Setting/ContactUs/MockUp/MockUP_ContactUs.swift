//
//  MockUP_ContactUs.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
import UIKit
class MockUP_ContactUs{
    var description : String!
    var title : String!
    var isLine : Bool!
    var row : [MockUP_ContactUs_Row]!
    init(description: String, title: String,isLine : Bool,row : [MockUP_ContactUs_Row])
    {
        self.description = description
        self.title = title
        self.isLine = isLine
        self.row = row
    }
}
class MockUP_ContactUs_Row{
    var description : String!
    var title : String!
    var PlaceHolder : String!
    var imageIcon : UIImage!
    var isTextView : Bool!
    var isImageIcon : Bool!
    init(description: String, title: String,PlaceHolder : String!,imageIcon : UIImage,isTextView : Bool,isImageIcon : Bool)
    {
        self.description = description
        self.title = title
        self.PlaceHolder = PlaceHolder
        self.imageIcon = imageIcon

        self.isTextView = isTextView
        self.isImageIcon = isImageIcon

    }
}
