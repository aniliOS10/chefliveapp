//
//  ContactUsViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import UIKit
import MessageUI

class ContactUsViewController:BaseViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var tableView : UITableView!
   
    var arrImg = [UIImage(named: "phoneCall"),UIImage(named: "whatsapp"),UIImage(named: "instagram"),UIImage(named: "EmailIcon"),UIImage(named: "cursor")]
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
            if #available(iOS 15.0, *) {
                tableView.sectionHeaderTopPadding = 0.0
            }
        }

        
        self.setNavigation()
    }
   
   
    // MARK: - Button Action
    @IBAction func buttonChangePressed(_ sender: Any) {
        
        let cellName : ContactUsRowTableViewCell = tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as! ContactUsRowTableViewCell
        let cellEmail : ContactUsRowTableViewCell = tableView.cellForRow(at: IndexPath(row: 1, section: 1)) as! ContactUsRowTableViewCell
        let cellPhone : ContactUsRowTableViewCell = tableView.cellForRow(at: IndexPath(row: 2, section: 1)) as! ContactUsRowTableViewCell
        let cellText : ContactUsRowTextViewTableViewCell = tableView.cellForRow(at: IndexPath(row: 3, section: 1)) as! ContactUsRowTextViewTableViewCell
        
        if cellName.textField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please Enter Name")
            return
        } else if cellEmail.textField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please Enter Email")
            return
        } else if cellPhone.textField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            NotificationAlert().NotificationAlert(titles: "Please Enter Phone")
            return
        } else if cellText.textView.text?.trimmingCharacters(in: .whitespaces).count == 0 || cellText.textView.text == "enter message" {
            NotificationAlert().NotificationAlert(titles: "Please Enter Message")
            return
        } else {
            
            
            if let validEmail = cellEmail.textField.text?.trimmingCharacters(in: .whitespaces).isValidEmail(){
                if validEmail == true {
                    
                    
                    let param = CustomerSupportModel()
                    
                    param.emailTo = dictAppInfo.email
                    param.phone = cellPhone.textField.text!
                    param.email = cellEmail.textField.text!
                    param.txtMessage = cellText.textView.text!
                    
                   
                    
                    showLoader()
                    EndPoint.SendEmailToSupport(params: param, t: CustomerSupportModel.self) { [weak self] result in
                        switch result {
                        case .onSuccess(let data):
                            self?.hideLoader()
                            NotificationAlert().NotificationAlert(titles: "Email sent successfully")
                            //Remove Data
                            cellName.textField.text = ""
                            cellEmail.textField.text = ""
                            cellPhone.textField.text = ""
                            cellText.textView.text = ""
                            self?.tableView.reloadData()
                            if data != nil {
                                
                            }
                        case .onFailure(let error):
                            self?.hideLoader()
                            print(error)
                            self?.tableView.reloadData()
                        }
                    }
                    
                    
                }
                else {
                    NotificationAlert().NotificationAlert(titles: "Please Enter valid Email")
                    return
                }
            }
            
            
            
            
        }
        
        //self.navigationController?.popViewController(animated: true)
    }
  
    // MARK: - Navigation
    func setNavigation() {
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
      
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: AppMessage_Setting.Placeholder.ContactUs)
    }
    
    
    func sendMessageToEmail(param : CustomerSupportModel) {
        
       
        
    }
    
    func sendEmail(email: String) {
        
        let recipientEmail = email
        let subject = "Email support"
        let body = ""
        
        // Show default mail composer
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([recipientEmail])
            mail.setSubject(subject)
            mail.setMessageBody(body, isHTML: false)
            
            present(mail, animated: true)
            
            // Show third party email composer if default Mail app is not present
        } else if let emailUrl = createEmailUrl(to: recipientEmail, subject: subject, body: body) {
            UIApplication.shared.open(emailUrl)
        }
    }

    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
               let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
               let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    
               let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
               let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
               let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
               let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
               let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")
    
               if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
                   return gmailUrl
               } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
                   return outlookUrl
               } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
                   return yahooMail
               } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
                   return sparkUrl
               }
    
               return defaultUrl
           }
    
    internal func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
           switch result {
           case .cancelled:
               print("Mail cancelled")
           case .saved:
               print("Mail saved")
           case .sent:
               print("Mail sent")
           case .failed:
               print("Mail sent failure")
           default:
               break
           }
           self.dismiss(animated: true, completion: nil)
            
       }
    
    //MARK :- Open Phone // Currently Not in use
    func didPressCall(number: Any) {
            guard let url = URL(string: "tel://\(number)") else {
                return //be safe
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    
    //MARK :- Open Email // Currently Not in use
    func openEmail(emailID : String) {
            if let url = URL(string: "mailto:\(emailID)") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    
    //MARK:- Open Whatsapp
    func openWhatsApp(number : String){
            var fullMob = number
            fullMob = fullMob.replacingOccurrences(of: " ", with: "")
            fullMob = fullMob.replacingOccurrences(of: "+", with: "")
            fullMob = fullMob.replacingOccurrences(of: "-", with: "")
            let urlWhats = "https://wa.me/\(fullMob)"
            
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                if let whatsappURL = NSURL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                        UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: { (Bool) in
                        })
                    }
                    else {
                        
                        UIApplication.shared.openURL(NSURL(string: "http://whatsapp.com/")! as URL)
                        NotificationAlert().NotificationAlert(titles: "WhatsApp Not Found on your device")
                    }
                }
            }
        }
    
    func openInstagramApp(number : String){
            var fullMob = number
            fullMob = fullMob.replacingOccurrences(of: " ", with: "")
            fullMob = fullMob.replacingOccurrences(of: "+", with: "")
            fullMob = fullMob.replacingOccurrences(of: "-", with: "")
            let urlWhats = "whatsapp://send?phone=\(fullMob)"
            
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                if let whatsappURL = NSURL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                        UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: { (Bool) in
                        })
                    }
                    else {
                        
                        UIApplication.shared.openURL(NSURL(string: "http://whatsapp.com/")! as URL)
                        NotificationAlert().NotificationAlert(titles: "WhatsApp Not Found on your device")
                    }
                }
            }
        }
}
