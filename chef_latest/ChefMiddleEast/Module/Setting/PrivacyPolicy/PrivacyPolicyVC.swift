//
//  PrivacyPolicyVC.swift
//  ChefMiddleEast
//
//  Created by sandeep on 28/01/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class PrivacyPolicyVC: BaseViewController {

    @IBOutlet weak var tableView : UITableView!

    var strTerms = String()
   
    
    // MARK: - VC Life Cycle
    override func viewWillAppear(_ animated: Bool) {
       // self.tabBarController?.tabBar.isHidden = true
        self.setNavigation()
        self.privacyPolicyAPI()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    

    // MARK: - Navigation
    func setNavigation(){
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
      
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: "Privacy Policy")
        
    }
    

    //MARK:- PrivacyPolicy API
    func privacyPolicyAPI(){
        self.showLoader()
        EndPoint.GetPrivacyPolicy(t: CountryListModel.self) { (result) in
            self.hideLoader()
            switch result {
            
            case .onSuccessWithStringValue(let strData):
                print(strData.html)
                self.strTerms = strData.html2String
                self.tableView.reloadData()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    

}
