//
//  MockUp_MyProfile.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
import UIKit
class MockUp_MyProfileSection{
    var description : String!
    var title : String!
    var image : UIImage!
    var row : [MockUp_MyProfileRow]!
    init(description: String, title: String,image : UIImage,row : [MockUp_MyProfileRow]!)
    {
        self.description = description
        self.title = title
        self.image = image
        self.row = row
    }
}
class MockUp_MyProfileRow{
    var description : String!
    var title : String!
    var PlaceHolder : String!
    var shouldHideArrow : Bool!
    
    init(description: String, title: String,PlaceHolder : String!,shouldHideArrow : Bool)
    {
        self.description = description
        self.title = title
        self.PlaceHolder = PlaceHolder
        self.shouldHideArrow = shouldHideArrow
    }
}
