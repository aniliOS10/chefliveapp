//
//  AppMessage_MyProfile.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
class AppMessage_MyProfile: NSObject {
    struct Placeholder {
        static let PersonalInfomation = NSLocalizedString("PersonalInfomation", comment: "")
        static let Dob = NSLocalizedString("Dob", comment: "")
        static let MYProfile = NSLocalizedString("MYProfile", comment: "")
    
    }
}
