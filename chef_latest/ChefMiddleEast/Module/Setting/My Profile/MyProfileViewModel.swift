//
//  MyProfileViewModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
import UIKit
class MyProfileViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    weak var dataSource:GenericDataSource<MockUp_MyProfileSection>?
    
    init(dataSource : GenericDataSource<MockUp_MyProfileSection>?) {
        self.dataSource = dataSource    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(){
        var row : [MockUp_MyProfileRow] = []
        row.append( MockUp_MyProfileRow.init(description: "", title: "", PlaceHolder: AppMessageRegisterUser.Placeholder.FirstName, shouldHideArrow: true)
        
        
        )
        
        row.append(
            MockUp_MyProfileRow.init(description: "", title: "", PlaceHolder: AppMessageRegisterUser.Placeholder.LastName, shouldHideArrow: true))
        
        row.append(
            MockUp_MyProfileRow.init(description: "", title: "", PlaceHolder: AppMessageRegisterUser.Placeholder.Email, shouldHideArrow: true))
       
        row.append(
            MockUp_MyProfileRow.init(description: "", title: "", PlaceHolder: AppMessage_MyProfile.Placeholder.Dob, shouldHideArrow: true)
            )
       
        
        self.dataSource?.data.value.append(MockUp_MyProfileSection.init(description: "", title: AppMessage_MyProfile.Placeholder.PersonalInfomation, image: UIImage.init(), row: row)
          )
    }
}
