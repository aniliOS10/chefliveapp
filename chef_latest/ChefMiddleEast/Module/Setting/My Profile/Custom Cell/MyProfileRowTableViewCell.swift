//
//  MyProfileRowTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import UIKit

class MyProfileRowTableViewCell: UITableViewCell {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var cellRoundView: UIView!
    @IBOutlet weak var arrowButton: UIButton!
    
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var imgViewMRadio: UIImageView!
    @IBOutlet weak var imgViewFRadio: UIImageView!
    @IBOutlet weak var imgViewORadio: UIImageView!
    
    @IBOutlet weak var btnViewM: UIButton!
    @IBOutlet weak var btnViewF: UIButton!
    @IBOutlet weak var btnViewO: UIButton!
    
    @IBOutlet weak var btnDob: UIButton!
    @IBOutlet weak var textFieldDOB: UITextField!
    @IBOutlet weak var dobView: UIView!
    @IBOutlet weak var lblGender: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
