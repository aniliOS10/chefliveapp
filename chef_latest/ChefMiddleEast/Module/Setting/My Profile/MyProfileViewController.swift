//
//  MyProfileViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import UIKit
import AVFoundation
import Photos
import SDWebImage
@available(iOS 13.0, *)
class MyProfileViewController:BaseViewController {
    
    @IBOutlet weak var tableView : UITableView!
   
    private let dataSource:MyProfileDataSource?
    var Profileimg : UIImage?
    var userName = ""
    var userLastName = ""
    var gender = ""

    
    lazy var viewModel:MyProfileViewModel = {
        let viewModel = MyProfileViewModel(dataSource: self.dataSource)
        return viewModel
    }()
    init(dashBoardViewDataSource:MyProfileDataSource)
    {
        self.dataSource = dashBoardViewDataSource
        super.init(nibName: nil, bundle: nil)
    }
  
    convenience init(dataSourceCollectionView1:MyProfileDataSource) {
        self.init(dashBoardViewDataSource: dataSourceCollectionView1)
    }
    required init?(coder: NSCoder) {
         self.dataSource = MyProfileDataSource()
         super.init(coder: coder)
     }
    
    override func viewWillAppear(_ animated: Bool) {
        // self.tabBarController?.tabBar.isHidden = false
        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            cartCountAPI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProfileAPI()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setToProfile(notification:)), name: Notification.Name("UserImageGet"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getUserProfileData(notification:)), name: Notification.Name("GetUserProfileData"), object: nil)
        
        self.dataSource?.tableView = self.tableView
        self.tableView.delegate = self.dataSource
        self.tableView.dataSource = self.dataSource
        
        self.viewModel.dataSource?.data.addAndNotify(observer: self, completionHandler: { mockUp_Dashboard in
            self.tableView.reloadData()
        })
        self.viewModel.makeDashboardList()

        self.initMenuButton()
        menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [], title: AppMessage_MyProfile.Placeholder.MYProfile)
        }else {
            self.initBellButton()
            bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
            self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [bellBarButton], title: AppMessage_MyProfile.Placeholder.MYProfile)
        }
    }
    
    
    //MARK:- Add Profile Params
    func paramProfile(){

            if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                let param = UpdateProfileModel()
                
                if dataSource?.userFirstName == ""
                {
                    param.firstName =  UserDefaults.standard.string(forKey: Constants.firstName)
                }
                else{
                    param.firstName =  dataSource?.userFirstName
                }
                if dataSource?.userlastName == ""{
                    param.lastName =  UserDefaults.standard.string(forKey: Constants.lastName)
                }
                else{
                    param.lastName =  dataSource?.userlastName
                }
                if dataSource?.genderStr == ""{
                    param.gender =  UserDefaults.standard.string(forKey: Constants.gender)
                }
                else{
                    param.gender =  dataSource?.genderStr
                }
                
                if dataSource?.userDOBDate == ""{
                    if let dateSave = UserDefaults.standard.string(forKey: Constants.dob),!dateSave.isEmpty{

                    if dateSave != nil{
                        let localDateString = dateSave.dobDateToDateString
                        param.birthDate  = localDateString
                    }
                    else{
                        param.birthDate  = dateSave
                    }
                }
                else{
                        param.birthDate = "2021-12-12"
                    }
                }
                else{
                    param.birthDate = dataSource?.userDOBDate
                }
                param.userId = userId
                param.email = UserDefaults.standard.string(forKey: Constants.email)
                param.phoneNumber = UserDefaults.standard.string(forKey: Constants.phone)
                updateUserProfileAPI(Model: param)
        }
    }
    
   
    @objc func menuButtonPressed(){
        self.sideMenuController?.revealMenu()
    }
    
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
    
    @IBAction func buttonUpdatePressed(_ sender: Any) {
        self.view.endEditing(true)

        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            if dataSource?.isUpdateImage  == true
            {
               self.uploadProfileImageApi(userID: userId)
            }
            
            if  dataSource?.isEmptyFirstName == "No"{
                NotificationAlert().NotificationAlert(titles: "Enter First Name")
                return
            }
            
            if   dataSource?.isEmptyLastName == "No"{
                 NotificationAlert().NotificationAlert(titles: "Enter Last Name")
                 return
            }
            paramProfile()
        }
    }
    
    @IBAction func buttonNextPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Cart Count Api
    func cartCountAPI() {
        
        EndPoint.CartCount(t: CartCountModel.self) { result in
            switch result {
            case .onSuccess(let data):
            if data != nil{
                if let cartCount = data!.cartCount, cartCount != 0 {
                    self.setBagCountOnCart(count: cartCount)
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "\(cartCount)"
                    }
                }
                if let notificationCount = data!.notificationCount, notificationCount >= 0 {
                    self.setNotificationBagCount(count: notificationCount)
                }
                
            }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    
    // MARK: - get Profile API
    func getProfileAPI(){
       
        EndPoint.GetProfile(t: GetProfileModel.self) { result in
            switch result {
            case .onSuccess(let user):
                
                if user != nil{
                    
                    self.dataSource?.userFirstName = ""
                    self.dataSource?.userlastName = ""
                    self.dataSource?.userDOBDate = ""
                    self.dataSource?.genderStr = ""
                    self.dataSource?.isUpdateImage = false

                    UserDefaults.standard.set(user?.firstName, forKey: Constants.firstName)
                    UserDefaults.standard.set(user?.lastName, forKey: Constants.lastName)
                    UserDefaults.standard.set(user?.gender, forKey: Constants.gender)
                    UserDefaults.standard.set(user?.birthDate, forKey: Constants.dob)
                    UserDefaults.standard.synchronize()

                    if ((user?.profilePicture?.contains("http:")) != nil) {
                        UserDefaults.standard.set(user?.profilePicture, forKey: Constants.userImg)
                        UserDefaults.standard.synchronize()

                    }
                    else{
                        var string = ""
                        if user?.profilePicture != nil {
                            string =  EndPoint.BASE_API_IMAGE_URL + (user?.profilePicture!)!
                            UserDefaults.standard.set(string, forKey: Constants.userImg)
                            UserDefaults.standard.synchronize()

                        }
                    }
                
                    self.tableView.reloadData()
                }
            case .onFailure(let errorMsg):
                print(errorMsg)
               // NotificationAlert().NotificationAlert(titles: errorMsg)
            }
        }
    }
    
    
    // MARK: - Update Profile API
    func updateUserProfileAPI(Model: UpdateProfileModel){
        self.showLoader()
        EndPoint.UpdateProfile(params: Model,t: UpdateProfileModel.self) { result in
            switch result {
            case .onSuccessWithStringValue(let msg):
                self.hideLoader()
                NotificationAlert().NotificationAlert(titles: msg)
                
                UserDefaults.standard.set(Model.firstName, forKey: Constants.firstName)
                UserDefaults.standard.set(Model.lastName, forKey: Constants.lastName)
                UserDefaults.standard.set(Model.gender, forKey: Constants.gender)
                UserDefaults.standard.set(Model.birthDate, forKey: Constants.dob)
                UserDefaults.standard.synchronize()

               
            case .onFailure(let errorMsg):
                print(errorMsg)
                NotificationAlert().NotificationAlert(titles: errorMsg)
                self.hideLoader()
            }
        }
    }
    
    func uploadProfileImageApi(userID: String){
        
        if self.Profileimg != nil{
            let compressData = self.Profileimg!.jpegData(compressionQuality: 0.8)
            let compressedImage = UIImage(data: compressData!)
            
            ImageUploadManager().uploadImageaaaRemote(image:compressedImage!,UserId: userID){ data, error -> Void in
                if !data!.isEmpty{
                    print("profile image is :- ",data ?? "NIL")
                    if "failure"  == data {
                        
                    }
                    else{
                        SDImageCache.shared.clearMemory()
                        SDImageCache.shared.clearDisk()
                        UserDefaults.standard.set(data, forKey: Constants.userImg)
                        
                    }
                }
            }
        }
    }
    
    
    @objc func setToProfile(notification: Notification) {
                addImage()
    }
    
    @objc func getUserProfileData(notification: Notification) {
                getProfileAPI()
    }
    
    

}
extension MyProfileViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func addImage(){
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        let takePic = UIAlertAction(title: "Take Photo", style: .default,handler: {
            (alert: UIAlertAction!) -> Void in
            self.checkCameraAccess()
        })
        let choseAction = UIAlertAction(title: "Choose from Library",style: .default,handler: {
            (alert: UIAlertAction!) -> Void in
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
            myPickerController.modalPresentationStyle = .fullScreen
            self.present(myPickerController, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(takePic)
        optionMenu.addAction(choseAction)
        optionMenu.addAction(cancelAction)
        
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func checkCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        
        case .authorized:
            print("Authorized, proceed")
            DispatchQueue.main.async {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = UIImagePickerController.SourceType.camera
                myPickerController.modalPresentationStyle = .fullScreen
                myPickerController.showsCameraControls = true
                self.present(myPickerController, animated: true, completion: nil)
            }
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    print("Permission granted, proceed")
                    DispatchQueue.main.async {
                        let myPickerController = UIImagePickerController()
                        myPickerController.delegate = self
                        myPickerController.sourceType = UIImagePickerController.SourceType.camera
                        myPickerController.modalPresentationStyle = .fullScreen
                        myPickerController.showsCameraControls = true
                        self.present(myPickerController, animated: true, completion: nil)
                    }
                }
                else{
                    self.dismiss(animated: false, completion: nil)
                }
            }
        default:
            self.alertToEncourageCameraAccessInitially()
        }
    }
    
    func alertToEncourageCameraAccessInitially() {
        
        let alert = UIAlertController(
            title: "Alert",
            message: "ChefMiddleEast app requires to access your camera to capture image on your business profile and service.",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let originalImage = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage else { return }
        
        self.Profileimg = originalImage
        self.dataSource?.isUpdateImage = true
        self.dataSource?.userUpdateImage = originalImage
        self.tableView.reloadData()
        self.dismiss(animated: false, completion: { [weak self] in
        })
    }
}


