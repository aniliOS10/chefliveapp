//
//  MyProfileDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
import UIKit
@available(iOS 13.0, *)
class MyProfileDataSource:GenericDataSource<MockUp_MyProfileSection>,UITableViewDataSource,UITableViewDelegate{
    
                                                    
    var tableView:UITableView?
    var genderStr = ""
    var userFirstName = ""
    var userlastName = ""
    var userDOBDate = ""

    var isUpdateImage = false
    var isEmptyFirstName = "Yes"
    var isEmptyLastName = "Yes"

    var userUpdateImage = UIImage()
    var currentDate = Date()
    
    var didSelectRow: ((_ cell:UITableViewCell, _ indexPath:IndexPath) -> Void)?
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let object = self.data.value[section]
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileSectionTableViewCell") as! MyProfileSectionTableViewCell
        
        cell.titleLabel.text = object.title
        cell.profileButton.addTarget(self, action: #selector(UpdateProfileButton(button:)), for: .touchUpInside)
        if isUpdateImage {
            cell.profileImg.image = userUpdateImage
            
            cell.profileImg.layer.borderWidth = 1
            cell.profileImg.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
            cell.profileImg.layer.cornerRadius = cell.profileImg.frame.size.width/2
            
        }
        else{
            if let profileImg = UserDefaults.standard.string(forKey: Constants.userImg),!profileImg.isEmpty{
                
                cell.profileImg.sd_setImage(with: NSURL(string:(profileImg)) as URL?) { (image, error, cache, urls) in
                    if (error != nil) {
                        cell.profileImg.image = UIImage(named: "UserProfile")
                    } else {
                        cell.profileImg.image = image
                        cell.profileImg.layer.borderWidth = 1
                        cell.profileImg.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
                        cell.profileImg.layer.cornerRadius = cell.profileImg.frame.size.width/2
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MYProfileFooterTableViewCell") as! MYProfileFooterTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 4
            //self.data.value[section].row.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileRowTableViewCell") as! MyProfileRowTableViewCell
        
        cell.textField.isUserInteractionEnabled = true
        cell.textField.delegate = self

        
        Utility.shared.makeShadowsOfView_roundCorner(view: cell.cellRoundView, shadowRadius: 4.0, cornerRadius: 30, borderColor: UIColor.lightGray.withAlphaComponent(0.2))
        
        cell.cellRoundView.isHidden = false
        cell.lblGender.isHidden = true
        switch indexPath.row {
        case 0:
            cell.genderView.isHidden = true
            cell.dobView.isHidden = true
            cell.textField.tag = 1044
            
    
            if let firstName = UserDefaults.standard.string(forKey: Constants.firstName),!firstName.isEmpty{
                cell.textField.text = firstName.capitalized
            }
            else{
                cell.textField.placeholder = self.data.value[indexPath.section].row[indexPath.row].PlaceHolder
            }
            
            if userFirstName != ""{
                cell.textField.text = userFirstName
            }

            
            
        case 1:
            cell.genderView.isHidden = true
            cell.dobView.isHidden = true

            cell.textField.tag = 1033
            if let lastName = UserDefaults.standard.string(forKey: Constants.lastName),!lastName.isEmpty{
                cell.textField.text =  lastName.capitalized
            }
            else{
                cell.textField.placeholder = self.data.value[indexPath.section].row[indexPath.row].PlaceHolder
            }
            
            if userlastName != ""{
                cell.textField.text = userlastName
            }

            
        case 2:

            cell.genderView.isHidden = false
            cell.cellRoundView.isHidden = true
            cell.dobView.isHidden = true
            cell.lblGender.isHidden = false

            
            if genderStr == "" {
                if let gender = UserDefaults.standard.string(forKey: Constants.gender),!gender.isEmpty{
                    genderStr = gender
                }
                else{
                    genderStr = "Male"
                }
            }

            if genderStr == "Male"{
                cell.imgViewMRadio.image = UIImage(named: "selectedImage")
                cell.imgViewFRadio.image = UIImage(named: "unselected")
                cell.imgViewORadio.image = UIImage(named: "unselected")
            }
            else if (genderStr == "Female"){
                cell.imgViewFRadio.image = UIImage(named: "selectedImage")
                cell.imgViewMRadio.image = UIImage(named: "unselected")
                cell.imgViewORadio.image = UIImage(named: "unselected")
            }
            else{
                cell.imgViewORadio.image = UIImage(named: "selectedImage")
                cell.imgViewMRadio.image = UIImage(named: "unselected")
                cell.imgViewFRadio.image = UIImage(named: "unselected")
            }
            
            cell.btnViewM.addTarget(self, action: #selector(btnMale(button:)), for: .touchUpInside)
            cell.btnViewF.addTarget(self, action: #selector(btnFemale(button:)), for: .touchUpInside)
            cell.btnViewO.addTarget(self, action: #selector(btnOther(button:)), for: .touchUpInside)
            
        case 3:
            Utility.shared.makeShadowsOfView_roundCorner(view: cell.dobView, shadowRadius: 4.0, cornerRadius: 30, borderColor: UIColor.lightGray.withAlphaComponent(0.2))
            cell.genderView.isHidden = true
            cell.cellRoundView.isHidden = true
            cell.dobView.isHidden = false
            cell.btnDob.addTarget(self, action: #selector(dobdate(button:)), for: .touchUpInside)
                            
                if userDOBDate == "" {
                    if let dateSave = UserDefaults.standard.string(forKey: Constants.dob),!dateSave.isEmpty{
                      
                        if dateSave != nil{
                            let localDateString = dateSave.dobDateToDateString

                            userDOBDate = localDateString
                        }
                        else{
                            userDOBDate = dateSave
                        }
                     
                    }
                    else{
                        cell.textFieldDOB.placeholder = "DOB"
                    }
                }
            
            cell.textFieldDOB.text = userDOBDate

  
        default:
            cell.textField.text = self.data.value[indexPath.section].row[indexPath.row].title
            cell.textField.placeholder = self.data.value[indexPath.section].row[indexPath.row].PlaceHolder
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    
    
    @objc func UpdateProfileButton(button:UIButton){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserImageGet"), object: nil)
    }

    @objc func dobdate(button:UIButton){
   
        addPicker()
    }
    func addPicker(){
       
        let alert = UIAlertController(style: .alert, title: "Select Date of Birth")
        
        if #available(iOS 13.4, *) {
            alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = UIColor.white
            alert.setTitle(font: UIFont(name: FontName.Optima.Bold, size: 15)!, color: UIColor.black)
            alert.view.tintColor = UIColor.black
        }
        else{
            alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = .white
            alert.setTitle(font: UIFont(name: FontName.Optima.Bold, size: 15)!, color: UIColor.white)
            alert.view.tintColor = UIColor.white
        }
        
        let date = Calendar.current.date(byAdding: .year, value: -10, to: Date())
        currentDate = date ?? Date()
        
        
        if userDOBDate == ""
        {
            currentDate = date ?? Date()
        }
        else
        {
            
            let isoDate = userDOBDate
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "yyyy-MM-dd"
            currentDate = dateFormatter.date(from:isoDate) ?? Date()
            
        }
        
        alert.addDatePicker(mode: .date, date: currentDate, minimumDate: nil, maximumDate: date) { date in
            self.currentDate = date
            }
        alert.addAction( title: "OK", style: .default, isEnabled: true) { (action) in
            
            self.userDOBDate = "str".convertToYYYYMMDD(date:self.currentDate)
            self.tableView?.reloadData()

            
            }
        alert.addAction(title: "Cancel", style: .cancel){ (action) in
            
        }
        
        alert.show()
        
    }

    
    
    @objc func btnMale(button:UIButton){

        genderStr = "Male"
        tableView?.reloadData()
    }
    @objc func btnFemale(button:UIButton){

        genderStr = "Female"
        tableView?.reloadData()

    }
    @objc func btnOther(button:UIButton){

        genderStr = "Others"
        tableView?.reloadData()

    }
}
extension MyProfileDataSource : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .done {
            
            textField.resignFirstResponder()
        }
        else {
            
            let textTag = textField.tag + 1
            
            if let nextResponder = textField.superview?.superview?.superview?.superview?.superview?.viewWithTag(textTag) {
    
                nextResponder.becomeFirstResponder()
            }
            else {
                
                textField.resignFirstResponder()
            }
        }
        
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
    
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        

        if textField.tag == 1044 {
            
            if textField.text!.trimmingCharacters(in: .whitespaces).count > 0 {
                isEmptyFirstName = "Yes"
                userFirstName = textField.text!.trimmingCharacters(in: .whitespaces)

            }
            else {
                NotificationAlert().NotificationAlert(titles: "Enter First Name")
                isEmptyFirstName = "No"

                return
            }
        }
        else if textField.tag == 1033{
            
            if textField.text!.trimmingCharacters(in: .whitespaces).count > 0 {
                isEmptyLastName = "Yes"
                userlastName = textField.text!.trimmingCharacters(in: .whitespaces)

            }
            else {
                 NotificationAlert().NotificationAlert(titles: "Enter Last Name")
                 isEmptyLastName = "No"
                return
            }
        }
    }
}
