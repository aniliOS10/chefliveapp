//
//  FAQViewModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/11/21.
//

import Foundation
import UIKit
class FAQViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    weak var dataSource:GenericDataSource<MockUp_FAQ>?
    
    init(dataSource : GenericDataSource<MockUp_FAQ>?) {
        self.dataSource = dataSource    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(arrFAQ:[FAQModel]) {
        for value in arrFAQ {
            self.dataSource?.data.value.append( MockUp_FAQ.init(description: "", title: value.question ?? "", isSelect: false, row: [ MockUp_FAQRow.init(description: "Leorem", title: value.answer ?? "", image: UIImage.init(named: "arrowUP") ?? UIImage())]))
        }
    }
}
