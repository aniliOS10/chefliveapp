//
//  FAQViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/11/21.
//

import UIKit

@available(iOS 13.0, *)
class FAQViewController:BaseViewController {
    @IBOutlet weak var tableView : UITableView!
   
    @IBOutlet weak var titleString: UILabel!
    @IBOutlet weak var subTitleString: UILabel!
    @IBOutlet weak var backViewWhite: UIView!
    @IBOutlet weak var buttonPayment: UIButton!
 
    var arrFAQInfo = [FAQModel]()
   
   private let dataSource:FAQDataSource?
    
    lazy var viewModel:FAQViewModel = {
        let viewModel = FAQViewModel(dataSource: self.dataSource)
        return viewModel
    }()
    init(dashBoardViewDataSource:FAQDataSource)
    {
        self.dataSource = dashBoardViewDataSource
        super.init(nibName: nil, bundle: nil)
    }
  
    convenience init(dataSourceCollectionView1:FAQDataSource) {
        self.init(dashBoardViewDataSource: dataSourceCollectionView1)
    }
    required init?(coder: NSCoder) {
         self.dataSource = FAQDataSource()
         super.init(coder: coder)
     }
    
    //MARK:- VC Life Cycle
    override func viewWillAppear(_ animated: Bool) {
       // self.tabBarController?.tabBar.isHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.getFAQInfoAPI()
        self.setNavigation()
        
    }
    
    func setNavigation(){
        self.initBackButton()
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title:  AppMessage_FAQ.Placeholder.FAQ)
        
        self.backViewWhite .layer.masksToBounds = true
        self.backViewWhite.layoutIfNeeded()
        Utility.shared.roundParticular(corners: [.topLeft,.topRight], cornerRadius: 40, view1: backViewWhite)
    }
    
    @objc func menuButtonPressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
    
    @IBAction func buttonBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Get FAQ Info  Api
    func getFAQInfoAPI() {
        self.showLoader()
        EndPoint.GetFaqList(t: FAQModel.self) { result in
            self.hideLoader()
               switch result {
               case .onSuccess(let items):
                if items.count > 0 {
                    self.arrFAQInfo = items
                    self.tableView.delegate = self.dataSource
                    self.tableView.dataSource = self.dataSource
                    self.dataSource?.tableView = self.tableView
                    self.viewModel.dataSource?.data.addAndNotify(observer: self, completionHandler: { mockUp_Dashboard in
                        self.tableView.reloadData()
                    })
                    self.viewModel.makeDashboardList(arrFAQ: self.arrFAQInfo)
                    print(self.arrFAQInfo)
                }
               case .onFailure(let error):
                   print(error)
                   if error != "The request timed out."{
                       NotificationAlert().NotificationAlert(titles: error)
                   }
               }
           }
       }
}
