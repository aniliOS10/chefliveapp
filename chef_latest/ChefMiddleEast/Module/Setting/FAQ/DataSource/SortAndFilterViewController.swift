//
//  SortAndFilterViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/11/21.
//

import UIKit
import ObjectMapper

protocol FilterSelectionDelegate: AnyObject {
    func userSelectedFilter(info: [String:Any])
    func filterResetAcn()
    func filterCancelAcn()
}

class SortAndFilterViewController:BaseViewController {
    
    weak var delegate: FilterSelectionDelegate? = nil
    @IBOutlet weak var tblCategory : UITableView!
    @IBOutlet weak var search : UISearchBar!
    @IBOutlet weak var buttonPayment: UIButton!
    @IBOutlet weak var backViewWhite: UIView!
    @IBOutlet var btnCancel: UIButton!

   var arrAllProducts = [NewArrivalsModel]()
   var arrCategory = [String]()
   var arrSubCategory = [NewArrivalsModel]()
   var hiddenSections = Set<Int>()
   var filterDataArray = NSMutableArray()
   var allKeysArray = NSMutableArray()
   var filterSelection = [String:Any]()
   var filterSelectionOpen = [String:Any]()

    var selectedCategoryID = String()
    var selectedSubCategoryID = String()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.arrAllProducts = fetchAllProductLocalStorge()
        self.arrCategory = arrAllProducts.unique{$0.categoryId}.map({ $0.categoryId ?? "" })
        self.arrCategory = self.arrCategory.sorted {
            $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        
       //
        setupFilterData()
        self.navigationController?.navigationBar.isHidden = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnCancel.setTitle("Cancel", for: .normal)
       // self.setUpSearchBar()
        self.backViewWhite .layer.masksToBounds = true
        self.backViewWhite.layoutIfNeeded()
        Utility.shared.roundParticular(corners: [.topLeft,.topRight], cornerRadius: 40, view1: backViewWhite)
      
        self.tblCategory.delegate = self
        self.tblCategory.dataSource = self

        
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    func setupFilterData() {
        var brandName = [String]()
        let temp1  = arrAllProducts.filter{$0.categoryId == selectedCategoryID} as [NewArrivalsModel]
        let temp2  = temp1.filter{$0.subCategoryId == selectedSubCategoryID} as  [NewArrivalsModel]
        
        
        if selectedSubCategoryID != "" {
            let temp  = temp2.unique{$0.brand}.map({ $0.brand ?? "" }) as NSArray
            brandName = temp as! [String]
            brandName.removeAll(where: { $0.contains("n/a") })
            brandName.removeAll(where: { $0.contains("") })
            brandName = brandName.filter({ $0 != ""})
            brandName = brandName.sorted()
            
            let brandDict = ["Brand" : brandName]
            
            if brandName.count > 0 {
              filterDataArray.add(brandDict)
              allKeysArray.add("Brand")

            }
            
            var origin = [String]()
            let tempOrigin = temp2.unique{$0.origin}.map({ $0.origin ?? "" }) as NSArray
            origin = tempOrigin as! [String]
            origin.removeAll(where: { $0.contains("n/a") })
            origin.removeAll(where: { $0.contains("") })
            origin = origin.filter({ $0 != ""})
            origin = origin.sorted()
            let originDict = ["Origin" : origin]
            if origin.count > 0 {
                filterDataArray.add(originDict)
                allKeysArray.add("Origin")

            }
            
            var status = [String]()
            let tempStatus = temp2.unique{$0.status}.map({ $0.status ?? "" }) as NSArray
            status = tempStatus as! [String]
            status.removeAll(where: { $0.contains("n/a") })
            status.removeAll(where: { $0.contains("") })
            status = status.filter({ $0 != ""})
            status = status.sorted()
            
            let statusDict = ["Status" : status]
            
            if status.count > 0 {
                filterDataArray.add(statusDict)
                allKeysArray.add("Status")

            }
            
        } else {
            let temp  = temp1.unique{$0.brand}.map({ $0.brand ?? "" }) as NSArray

            brandName = temp as! [String]
            brandName.removeAll(where: { $0.contains("n/a")})
            brandName.removeAll(where: { $0.contains("") })
            brandName = brandName.filter({ $0 != ""})
            brandName = brandName.sorted()

            let brandDict = ["Brand" : brandName]
            if brandName.count > 0 {
                filterDataArray.add(brandDict)
                allKeysArray.add("Brand")

            }
            var origin = [String]()
            let tempOrigin = temp1.unique{$0.origin}.map({ $0.origin ?? "" }) as NSArray
            origin = tempOrigin as! [String]
            origin.removeAll(where: { $0.contains("n/a") })
            origin.removeAll(where: { $0.contains("") })
            origin = origin.filter({ $0 != ""})
            origin = origin.sorted()

            let originDict = ["Origin" : origin]
            if origin.count > 0 {
                filterDataArray.add(originDict)
                allKeysArray.add("Origin")

            }
            
            var status = [String]()
            let tempStatus = temp1.unique{$0.status}.map({ $0.status ?? "" }) as NSArray
            status = tempStatus as! [String]
            status.removeAll(where: { $0.contains("n/a") })
            status.removeAll(where: { $0.contains("") })
            status = status.filter({ $0 != ""})
            status = status.sorted()

            let statusDict = ["Status" : status]
            
            if status.count > 0 {
                filterDataArray.add(statusDict)
                allKeysArray.add("Status")

            }
            
        }
 
        
        
        allKeysArray.add("Price")
        
        allKeysArray.add("Caffeine")
        allKeysArray.add("Sugar Free")
        allKeysArray.add("Lactose")
        allKeysArray.add("Nuts")
        allKeysArray.add("Low Fat")
        allKeysArray.add("Low Sugar")
        allKeysArray.add("Organic")
        allKeysArray.add("Reduced Sugar")
        allKeysArray.add("Vegan")
        allKeysArray.add("Vegetarian")
        allKeysArray.add("Halal Suitable")
        allKeysArray.add("Kosher Suitable")
        allKeysArray.add("Gulten Free")
        
        
        hiddenSections = [0,1,2]
        print(filterDataArray)
        
        let emptyArray = NSArray()
        
        filterSelection = ["Brand" : emptyArray, "Origin" : emptyArray, "Status" : emptyArray, "Price" : "","Caffeine": false,"Sugar Free": false,"Lactose": false,"Nuts": false,"Low Fat": false,"Low Sugar": false,"Organic": false,"Reduced Sugar": false,"Vegan": false,"Vegetarian": false,"Halal Suitable": false,"Kosher Suitable": false,"Gulten Free": false]
        
        if filterSelectionOpen.count > 0 {
            filterSelection = filterSelectionOpen
            tblCategory.reloadData()
            
                
                let Brand = filterSelection["Brand"] as! NSArray
                if Brand.count > 0 {
                    var dict = NSDictionary()
                    if filterDataArray.count > 0 {
                    dict = filterDataArray[0] as! NSDictionary
                    if (dict["Brand"] != nil) {
                        let BrandOLD = dict["Brand"] as! NSArray
                            if BrandOLD.count > 0 {
                                openWithIndex(0)
                    }
                }
            }
        }
            tblCategory.reloadData()

            let Origin = filterSelection["Origin"] as! NSArray
            if Origin.count > 0 {
                var dict = NSDictionary()
                if filterDataArray.count > 1 {
                dict = filterDataArray[1] as! NSDictionary
                if (dict["Origin"] != nil) {
                    let BrandOLD = dict["Origin"] as! NSArray
                        if BrandOLD.count > 0 {
                            openWithIndex(1)
                }
            }
        }
    }
            tblCategory.reloadData()

            let Status = filterSelection["Status"] as! NSArray
            if Status.count > 0 {
                var dict = NSDictionary()
                if filterDataArray.count > 2 {
                dict = filterDataArray[2] as! NSDictionary
                if (dict["Status"] != nil) {
                    let BrandOLD = dict["Status"] as! NSArray
                        if BrandOLD.count > 0 {
                            openWithIndex(2)
                }
            }
        }
    }
    }
        
        print("filterSelection")
        print(filterSelection)
        
    }
    
    func fetchAllProductLocalStorge() -> [NewArrivalsModel] {
        if let syncContactsData = UserDefaults.standard.value(forKey: Constants.ProductModel) as? Data {
            if let aryProduct:[[String:Any]] = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(syncContactsData) as? [[String : Any]] {
                return Mapper<NewArrivalsModel>().mapArray(JSONArray: aryProduct)
            }
        }
        return []
    }
    
    //MARK:- Search Bar Setup
    func setUpSearchBar(){
        //search.searchBarStyle = .minimal
        search.backgroundColor = UIColor.white
        search.tintColor = UIColor.white
        if #available(iOS 13.0, *) {
            search.searchTextField.backgroundColor = UIColor().getSearchColor()
            search.searchTextField.textColor =  UIColor().getThemeColor()
            search.searchTextField.tintColor =  UIColor().getThemeColor()
            Utility.shared.makeRoundCorner(layer: search.searchTextField.layer, color:UIColor.white.withAlphaComponent(0.5), radius: 5)
            let white = UIColor.red
            search.searchTextField.background = UIImage.init(named: "imageV1")
        }
        else {
        }
    }
    
    //MARK:- Button Action
    @IBAction func buttonSaveAndMakePaymentPressed(_ sender: Any) {
        
    }
    @IBAction func buttonCancelPressed(_ sender: Any) {
        delegate?.filterCancelAcn()
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func buttonResetPressed(_ sender: Any) {
        let emptyArray = NSArray()
        filterSelection = ["Brand" : emptyArray, "Origin" : emptyArray, "Status" : emptyArray, "Price" : "Low to High","Caffeine": false,"Sugar Free": false,"Lactose": false,"Nuts": false,"Low Fat": false,"Low Sugar": false,"Organic": false,"Reduced Sugar": false,"Vegan": false,"Vegetarian": false,"Halal Suitable": false,"Kosher Suitable": false,"Gulten Free": false]
        filterSelectionOpen.removeAll()
        delegate?.filterResetAcn()
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func buttonApplyPressed(_ sender: Any) {
        delegate?.userSelectedFilter(info: filterSelection)
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func segmentControllerValueChanged(_ sender: Any) {
            
    }
    
    
}


