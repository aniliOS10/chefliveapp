//
//  FAQDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/11/21.
//

import Foundation
import UIKit
import WebKit
class FAQDataSource:GenericDataSource<MockUp_FAQ>,UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate, WKNavigationDelegate{
    var tableView:UITableView?
    var didSelectRow: ((_ cell:UITableViewCell, _ indexPath:IndexPath) -> Void)?
    
    
    var contentHeightsArray = NSMutableArray()


    func numberOfSections(in tableView: UITableView) -> Int {
        return self.data.value.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQSectionTableViewCell") as! FAQSectionTableViewCell
        
       
        let object = self.data.value[section]
        if object.isSelect{
            cell.lineView.isHidden = true
            cell.titleLabel.textColor = UIColor().getThemeColor()
            cell.arrowButton.setImage(UIImage.init(named: "arrowUP"), for: .normal)
        }else{
            cell.titleLabel.textColor = UIColor.black
            cell.lineView.isHidden = false
            cell.arrowButton.setImage(UIImage.init(named: "arrowDown"), for: .normal)
        }
        
        cell.titleLabel.text = object.title
        cell.selectionButton.tag = section
        cell.selectionButton.addTarget(self, action: #selector(self.selectionButtonPressed), for: .touchUpInside)
           
        return cell
      
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.data.value[section].row.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let object = self.data.value[indexPath.section].row[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQRowTableViewCell") as! FAQRowTableViewCell

        cell.webViewList.tag = indexPath.section
        let string = String(object.title)
        if  string.contains("<html"){
            DispatchQueue.main.async {
                cell.webViewList.loadHTMLString(object.title, baseURL: nil)
            }
            cell.webViewList.isHidden = false
            cell.webViewList.navigationDelegate = self
            cell.titleLabel.text = object.title.htmlToString
        }
        else{
            cell.titleLabel.text = object.title
            cell.webViewList.isHidden = true
        }

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let object = self.data.value[indexPath.section]
        if object.isSelect{
            
            let objectDict = self.data.value[indexPath.section].row[0]

            let string = String(objectDict.title)
            
            if  string.contains("<html"){
        
                var totals = 444.00

                for i in 0..<contentHeightsArray.count
                {
                    var dict = NSMutableDictionary()
                    dict = contentHeightsArray[i] as! NSMutableDictionary
                    var intValue =  0
                    intValue = dict.value(forKey: "index") as! Int
                    if intValue == indexPath.section {
                        totals = dict.value(forKey: "height") as? Double ?? 445.00
                        if totals < 10 {
                            totals = 445
//                            let cell : FAQRowTableViewCell = tableView.cellForRow(at: IndexPath(row: 0, section: indexPath.section)) as? FAQRowTableViewCell ?? FAQRowTableViewCell()
//                            totals = Double(cell.webViewList.scrollView.contentSize.height)

                        }
                        else{
                           print(totals)
                        }
                    }
                }
                
                return CGFloat(totals)

            }
            else{
                return UITableView.automaticDimension

                }
               
            }
        return 0.0

        }
        


    @objc func selectionButtonPressed(sender:UIButton){
        let object = self.data.value[sender.tag]
        object.isSelect = !object.isSelect
        self.tableView?.reloadData()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

        let object = self.data.value[webView.tag].row[0]
        let string = String(object.title)
        if  string.contains("<html"){
            self.contentHeightsArray.removeAllObjects()
            let dict = NSMutableDictionary()
            dict.setValue(webView.tag, forKey: "index")
            webView.layoutSubviews()

            var valueFloat =  Double()
            valueFloat = Double(webView.scrollView.contentSize.height)
            dict.setValue(valueFloat, forKey: "height")
            self.contentHeightsArray.add(dict)
            print(webView.scrollView.contentSize.height)


        }
       }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
