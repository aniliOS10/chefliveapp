//
//  SortAndFilterViewModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/11/21.
//

import Foundation
import UIKit
class SortAndFilterViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    weak var dataSource:GenericDataSource<MockUp_SortFilter>?
    
    init(dataSource : GenericDataSource<MockUp_SortFilter>?) {
        self.dataSource = dataSource    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(){
    
        var arrSubRow:[MockUp_SortFilter_SubRow] = []
        arrSubRow.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        arrSubRow.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        arrSubRow.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        
        var arrSubRow2:[MockUp_SortFilter_SubRow] = []
        arrSubRow2.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        arrSubRow2.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        arrSubRow2.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
       
        var arrRow:[MockUp_SortFilterRow] = []
        arrRow.append( MockUp_SortFilterRow.init(description: "Leorem", title: "Bakery", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage(), row: arrSubRow))
        arrRow.append( MockUp_SortFilterRow.init(description: "Leorem", title: "Bakery", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage(), row: arrSubRow2))
       
       
        var arrSubRowO:[MockUp_SortFilter_SubRow] = []
        arrSubRowO.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        arrSubRowO.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        arrSubRowO.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        
        var arrSubRowO2:[MockUp_SortFilter_SubRow] = []
        arrSubRowO2.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        arrSubRowO2.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        arrSubRowO2.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
       
        var arrRowO:[MockUp_SortFilterRow] = []
        arrRowO.append( MockUp_SortFilterRow.init(description: "Leorem", title: "Bakery", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage(), row: arrSubRowO))
        arrRowO.append( MockUp_SortFilterRow.init(description: "Leorem", title: "Bakery", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage(), row: arrSubRowO2))
       
        
        
        
        var arrSubRowM:[MockUp_SortFilter_SubRow] = []
        arrSubRowM.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        arrSubRowM.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        arrSubRowM.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        
        var arrSubRowM2:[MockUp_SortFilter_SubRow] = []
        arrSubRowM2.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        arrSubRowM2.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
        arrSubRowM2.append( MockUp_SortFilter_SubRow.init(description: "Leorem", title: "Rice pulse", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage()))
       
        var arrRowM:[MockUp_SortFilterRow] = []
        arrRowM.append( MockUp_SortFilterRow.init(description: "Leorem", title: "Bakery", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage(), row: arrSubRowM))
        arrRowM.append( MockUp_SortFilterRow.init(description: "Leorem", title: "Bakery", isSelect: false, image: UIImage.init(named: "arrowUP") ?? UIImage(), row: arrSubRowM2))
        
        
        
        
        
        let section = MockUp_SortFilter.init(description: "Leorem", title: "Category pulse", isSelect: false, row: arrRow)
        
        let section2 = MockUp_SortFilter.init(description: "Leorem", title: "offers", isSelect: false, row: arrRowO)
        
        let section3 = MockUp_SortFilter.init(description: "Leorem", title: "More", isSelect: false, row: arrRowM)
        
        
        self.dataSource?.data.value.append(section )
        self.dataSource?.data.value.append( section2)
        self.dataSource?.data.value.append(section3)
        
    }
}
