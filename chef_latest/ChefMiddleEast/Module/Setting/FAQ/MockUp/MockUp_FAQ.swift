//
//  MockUp_FAQ.swift
//  ChefMiddleEast
//
//  Created by rupinder singh on 03/11/21.
//

import Foundation
import UIKit
class MockUp_FAQ{
    var description : String!
    var title : String!
    var isSelect : Bool = false
    var row : [MockUp_FAQRow] = []
    init(description: String, title: String, isSelect: Bool, row: [MockUp_FAQRow])
    {
        self.description = description
        self.title = title
        self.isSelect = isSelect
        self.row = row
    }
}
class MockUp_FAQRow{
    var description : String!
    var title : String!
    var image : UIImage!
    init(description: String, title: String, image: UIImage)
    {
        self.description = description
        self.title = title
        self.image = image
    }
}
