//
//  CardModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 25/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class FAQModel: BaseResponse {
    
    var question : String?
    var answer : String?
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        question <- map["question"]
        answer <- map["answer"]
    }
}
