//
//  FAQRowTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 03/11/21.
//

import UIKit
import WebKit

class FAQRowTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var webViewList: WKWebView!

    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
