//
//  NotificationListDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
import UIKit

extension NotificaitonListViewController: UITableViewDataSource, UITableViewDelegate{
    
    
   // var tableView:UITableView?
   // var didSelectRow: ((_ cell:UITableViewCell, _ indexPath:IndexPath) -> Void)?
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
       /* let cell = tableView.dequeueReusableCell(withIdentifier: "TermsConditionSectionTableViewCell") as! TermsConditionSectionTableViewCell
        cell.titleLabel.text = AppMessage_TermCondition.Placeholder.TermsandConditions
        return cell
      */
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
       // return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return  arrNotification.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let object = arrNotification[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationListTableViewCell") as! NotificationListTableViewCell
        
        cell.titleLabel.text = object.title
        
        if let dateString = object.date, dateString != nil {
            if let range = dateString.range(of: "T") {
                let dateValue = dateString[dateString.startIndex..<range.lowerBound]
                cell.dateLabel.text =  String(dateValue)
            }
        }
        cell.descriptionLabel.text = object.notificationDescription
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
   
}

