//
//  NotificaitonListViewModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
class NotificaitonListViewModel:ViewModel {
    var compeltionBlockLocaliable: ((Any) -> Void)?
    var onCompletionHandling: ((Any) -> Void)?
    var onErrorHandling : ((ErrorResult?) -> Void)?
    weak var dataSource:GenericDataSource<MockUp_NotificationList>?
    
    init(dataSource : GenericDataSource<MockUp_NotificationList>?) {
        self.dataSource = dataSource    }
    func updateLocalizable(){
        self.compeltionBlockLocaliable!(Resetpassword_Appmessage())
    }
    func makeDashboardList(){
        self.dataSource?.data.value.append(
            MockUp_NotificationList.init(description: "Lorem", title: "New order is confirmed",image: "",date: "12 ago"))
        self.dataSource?.data.value.append(
            MockUp_NotificationList.init(description: "Lorem", title: "Order ID #456987 is delivered",image: "",date: "10 ago"))
        self.dataSource?.data.value.append(
            MockUp_NotificationList.init(description: "Lorem", title: "New order is confirmed",image: "",date: "12 ago"))
        self.dataSource?.data.value.append(
            MockUp_NotificationList.init(description: "Lorem", title: "Order ID #456987 is delivered",image: "",date: "10 ago"))
        
    }
}
