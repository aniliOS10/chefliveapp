//
//  MockUp_NotificationList.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import Foundation
class MockUp_NotificationList{
    var description : String!
    var title : String!
    var image : String!
    var date : String!
    init(description: String, title: String,image : String,date : String)
    {
        self.description = description
        self.title = title
        self.image = image
        self.date = date
    }
}
