//
//  NotificaitonListViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 05/11/21.
//

import UIKit

@available(iOS 13.0, *)
class NotificaitonListViewController:BaseViewController {
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var titleString : UILabel!
    @IBOutlet weak var backViewWhite: UIView!
    @IBOutlet weak var bgViewWhite: UIView!

    @IBOutlet weak var lbe_NoData : UILabel!
    @IBOutlet weak var img_NoData : UIImageView!

    var arrNotification = [NotificationsModel]()
    
    /*
    lazy var viewModel:NotificaitonListViewModel = {
        let viewModel = NotificaitonListViewModel(dataSource: self.dataSource)
        return viewModel
    }()
    init(dashBoardViewDataSource:NotificationListDataSource)
    {
        self.dataSource = dashBoardViewDataSource
        super.init(nibName: nil, bundle: nil)
    }
  
    convenience init(dataSourceCollectionView1:NotificationListDataSource) {
        self.init(dashBoardViewDataSource: dataSourceCollectionView1)
    }
    required init?(coder: NSCoder) {
         self.dataSource = NotificationListDataSource()
         super.init(coder: coder)
    }
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
      
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: AppMessage_NotificationList.Placeholder.Notification)
        //  self.titleString.text = AppMessage_NotificationList.Placeholder.Notification
        self.titleString.text = ""
        GetNotificationList()
        ReadNotificationList()
        cartCountAPI()
    }
   
    
    //MARK:- Get CountryList API
    func GetNotificationList(){
//        self.bgViewWhite.isHidden = true
//        self.backViewWhite.isHidden = true
        self.tableView.isHidden = true
        self.lbe_NoData.isHidden = false
        self.img_NoData.isHidden = false

        EndPoint.GetNotification(t: NotificationsModel.self) { [self] (result) in
            switch result {
            case .onSuccess(let items):
                if items.count > 0 {
                    self.arrNotification = items
                    print("Notification Array")
                    print(self.arrNotification)
                    
                    if self.arrNotification.count > 0 {
                        self.bgViewWhite.isHidden = false
                        self.backViewWhite.isHidden = false
                        self.tableView.isHidden = false
                        self.lbe_NoData.isHidden = true
                        self.img_NoData.isHidden = true
                        self.tableView.reloadData()

                    }
                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    func ReadNotificationList(){
        EndPoint.ReadNotification(t: NotificationsModel.self) { (result) in
            switch result {
            case .onSuccess(let items):
                if items.count > 0 {
                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    func cartCountAPI() {
        
        EndPoint.CartCount(t: CartCountModel.self) { result in
            switch result {
            case .onSuccess(let data):
            if data != nil{
                if let cartCount = data!.cartCount, cartCount != 0 {
                    self.setBagCountOnCart(count: cartCount)
                    if let tabItems = self.tabBarController?.tabBar.items {
                        // In this case we want to modify the badge number of the third tab:
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "\(cartCount)"
                    }
                }
                if let notificationCount = data!.notificationCount, notificationCount >= 0 {
                    self.setNotificationBagCount(count: notificationCount)
                    
                }
                
            }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
