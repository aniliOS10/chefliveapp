//
//  NotificationsModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 07/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class NotificationsModel : BaseResponse {
    
    var title : String?
    var image : String?
    var date : String?
    var notificationDescription : String?
    
    override init() {
         super.init()
    }
    convenience required init?(map: Map) {
        self.init()
    }
    override func mapping(map: Map) {
        notificationDescription <- map["description"]
        title <- map["title"]
        image <- map["image"]
        date <- map["createDate"]
    }
    
}
