//
//  CartConfirmationVC.swift
//  ChefMiddleEast
//
//  Created by Apple on 25/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit
import SwiftGifOrigin

class CartConfirmationVC: BaseViewController {

    
    @IBOutlet weak var lblTotalPrice : UILabel!
    @IBOutlet weak var btnConfirm : UIButton!
    @IBOutlet weak var imgItem : UIImageView!
    @IBOutlet weak var imgItem_1 : UIImageView!
    @IBOutlet weak var imgItem_2 : UIImageView!
    @IBOutlet weak var imgItem_3 : UIImageView!
    @IBOutlet weak var imgItem_4 : UIImageView!
    @IBOutlet weak var imgItem_5 : UIImageView!
    @IBOutlet weak var imgItem_6 : UIImageView!

    @IBOutlet weak var imgBasket : UIImageView!
    
    @IBOutlet weak var xImage_1: NSLayoutConstraint!
    @IBOutlet weak var yImage_1: NSLayoutConstraint!

    var arrCartProduct = [ProductModel]()
    var ClassName = ""
    
    var isAnimation = false
    var valueY = CGFloat()
    var valueX = CGFloat()
    
    var count = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupAnimation()

        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            let param = AddToCartModel()
            param.userId = userId
            self.getCartListAPI(Model: param)
        }
        
        btnConfirm.layer.cornerRadius = btnConfirm.frame.height / 2
        btnConfirm.addShadow()
        setUpNavigation()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    override func viewDidAppear (_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setupAnimation() {
        let jeremyGif = UIImage.gif(name: "basket")
        
        // imgBasket.image = jeremyGif

        // Set the images from the UIImage
        imgBasket.animationImages = jeremyGif?.images
        // Set the duration of the UIImage
        imgBasket.animationDuration = jeremyGif!.duration
        // Set the repetitioncount
        imgBasket.animationRepeatCount = 1
        // Start the animation
        imgBasket.image = jeremyGif?.images?.last

        imgBasket.startAnimating()
    
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        count = count + 1
        
        if !isAnimation {
            valueY = self.imgBasket.center.y
            valueX =  self.imgBasket.center.x
            if ClassName == "1TabBar"{
                
            }
            else{
                
                if count == 3 {
                    
                    
                    UIView.animate(withDuration: 1.0, delay: 0.0, options:[], animations: {
                        
                        
                       self.imgItem.transform = CGAffineTransform(translationX: 0  , y: self.imgBasket.center.y - 60)
                        
                        self.imgItem_1.transform = CGAffineTransform(translationX: -65 , y: self.imgBasket.center.y - 60)

                      self.imgItem_2.transform = CGAffineTransform(translationX: -40 , y: self.imgBasket.center.y - 0)

                        self.imgItem_3.transform = CGAffineTransform(translationX: +40 , y: self.imgBasket.center.y - 70)

                      self.imgItem_4.transform = CGAffineTransform(translationX: +20  , y: self.imgBasket.center.y - 0)

                        self.imgItem_5.transform = CGAffineTransform(translationX: +35  , y: self.imgBasket.center.y - 30 )

                       self.imgItem_6.transform = CGAffineTransform(translationX: +0  , y: self.imgBasket.center.y - 60)


                    }, completion: nil)
                }
             
              
            }
        }
        else {
          //  self.imgItem.center.y = 44

        }
    }
    
 
    //MARK:- SetUp Navigation
    func setUpNavigation(){
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title:"Cart Confirmation")
    }
    
    //MARK:- Get Cart List Api
    func getCartListAPI(Model: AddToCartModel) {
        EndPoint.GetCartList(params: Model,t: AddToCartModel.self) { [self] result in
            self.hideLoader()
            switch result {
            case .onSuccess(let data):
                if data != nil {
                    var totalSum : Double = 00.00
                    if data!.productList.count > 0 {
                        self.arrCartProduct =  data!.productList
                        totalSum = 00.00
                        for eachProduct in self.arrCartProduct{
                            //New Changes
                            if let startDate = eachProduct.fromDate,startDate != nil {
                                let fromDate : Date = startDate.dateFromString(startDate)
                                let endDate = eachProduct.toDate
                                let toDate : Date = endDate!.dateFromString(endDate!)
                                if Date().isBetween(fromDate, and: toDate) {
                                    if let agreementValue = eachProduct.agreementValue, agreementValue != nil {
                                       totalSum = totalSum + Double(eachProduct.agreementValue!) * Double(eachProduct.productQuantity!)
                                    } else {
                                        if let price = eachProduct.salesPrice,price != 0 {
                                           totalSum = totalSum + Double(eachProduct.salesPrice!) * Double(eachProduct.productQuantity!)
                                        }
                                    }
                                } else {
                                    if let price = eachProduct.salesPrice,price != 0 {
                                        totalSum = totalSum + Double(eachProduct.salesPrice!) * Double(eachProduct.productQuantity!)
                                    }
                                }
                            } else {
                                if let price = eachProduct.salesPrice,price != nil {
                                   totalSum = totalSum + Double(eachProduct.salesPrice!) * Double(eachProduct.productQuantity!)
                                }
                            }
                        }
                        
                        let x = totalSum
                        let roundedValue = round(x * 100) / 100.0
                        self.lblTotalPrice.text = String(format: "Total : AED %.2f", roundedValue)
                      //  self.lblTotalPrice.text = String("\("Total : AED ")\(roundedValue)")
                    }
                }
            case .onFailure(let error):
                print(error)
            }
        }
    }
  
    @IBAction func onClickConfirmAcn(){
        let controller:CartSummaryViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            controller.hidesBottomBarWhenPushed = false
            controller.screenCommingFrom = "ProductDetails"
        self.navigationController?.pushViewController(controller, animated: true)
        
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if vc.isKind(of: CartConfirmationVC.self) || vc.isKind(of: CartConfirmationVC.self) {
                return true
            } else {
                return false
            }
        })
    }
    
    func calculateTotalCost() {
        let controller:CartSummaryViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
        controller.hidesBottomBarWhenPushed = false
        controller.screenCommingFrom = "ProductDetails"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
