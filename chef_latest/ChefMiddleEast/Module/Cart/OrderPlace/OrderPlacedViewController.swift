//
//  OrderPlacedViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 10/11/21.
//

import UIKit

class OrderPlacedViewController: BaseViewController {

    @IBOutlet var lblOrderNo: UILabel!
    
    @IBOutlet var btnViewOrder: UIButton!
    
    @IBOutlet var btnContinueShopping: UIButton!
    
    //MArk:- VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigation()
        
        if let orderNo = UserDefaults.standard.string(forKey: Constants.orderNumber),!orderNo.isEmpty{
            self.lblOrderNo.text = "Order No:- " + orderNo
        }
        
        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            cartCountAPI()
        }
    }
    
    //MARK:- Cart Count Api
    func cartCountAPI() {
        
        EndPoint.CartCount(t: CartCountModel.self) { result in
            switch result {
            case .onSuccess(let data):
            if data != nil{
                if let cartCount = data!.cartCount, cartCount != 0 {
                    self.setBagCountOnCart(count: cartCount)
                    if let tabItems = self.tabBarController?.tabBar.items {
                        // In this case we want to modify the badge number of the third tab:
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "\(cartCount)"
                    }
                }
                else{
                    self.setBagCountOnCart(count: 0)

                    if let tabItems = self.tabBarController?.tabBar.items {
                        // In this case we want to modify the badge number of the third tab:
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "0"
                    }
                }
                if let notificationCount = data!.notificationCount, notificationCount >= 0 {
                    self.setNotificationBagCount(count: notificationCount)
                }
                else{
                    self.setNotificationBagCount(count:1)
                }
                
            }
                else{
                    self.setBagCountOnCart(count: 0)
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "0"
                    }
                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    
    //MARK:- SetUp Navigation
    func setUpNavigation(){
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        initMenuButton()
        menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
       
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [], title:AppMessage_DeliverySummary.Placeholder.DeliverySummary )
        }else {
            self.initBellButton()
            bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
            self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [], title:AppMessage_DeliverySummary.Placeholder.DeliverySummary )
        }
    }
    
    //MARK:- Menu Button
    @objc func menuButtonPressed(){
        self.sideMenuController?.revealMenu()
    }
    
    //MARK:- Notification Button
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARk:- Button Action
    @IBAction func buttonViewOrderPressed(_ sender: Any) {
      //  self.navigationController?.popToRootViewController(animated: true)
        let controller:MyOrdersViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
        controller.screenCommingFrom = "ViewOrderButtonTap"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func buttonContinueShopingPressed(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }

   

}
