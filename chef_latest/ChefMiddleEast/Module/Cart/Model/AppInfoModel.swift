//
//  AppInfoModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 19/01/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class AppInfoModel: BaseResponse {
    
    var appInfoId : Int?
    var deliveryAmountNote : String?
    var termsAndConditionUrl : String?
    var vatPercent : Int?
    var deliveryCharges : Int?
    var deliveryLimit : Int?
    var deliveryNote : String?
    var phone : String?
    var whatsAppNumber : String?
    var instaId : String?
    var email : String?
    var address : String?
    var vatNote : String?
    var profileId : String?
    var serverKey : String?
    var clientKey : String?
    var announcement : String?
   
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        appInfoId <- map["appInfoId"]
        deliveryAmountNote <- map["deliveryAmountNote"]
        termsAndConditionUrl <- map["termsAndConditionUrl"]
        vatPercent <- map["vatPercent"]
        deliveryCharges <- map["deliveryCharges"]
        deliveryLimit <- map["deliveryLimit"]
        deliveryNote <- map["deliveryNote"]
        phone <- map["phone"]
        whatsAppNumber <- map["whatsAppNumber"]
        instaId <- map["instaId"]
        email <- map["email"]
        address <- map["address"]
        vatNote <- map["vatNote"]
        profileId <- map["profileId"]
        serverKey <- map["serverKey"]
        clientKey <- map["clientKey"]
        announcement <- map["annoucement"]
    }
}
