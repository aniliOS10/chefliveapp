//
//  CouponModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 19/01/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class CouponModel: BaseResponse {
    var couponCode : String?
    var discountAmount : Float?
    var couponValidity : String?
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        couponCode <- map["couponCode"]
        discountAmount <- map["discountAmount"]
        couponValidity <- map["couponValidity"]
        
    }
}
