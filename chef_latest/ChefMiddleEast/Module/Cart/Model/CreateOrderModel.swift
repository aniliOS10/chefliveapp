//
//  CreateOrderModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 22/01/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class CreateOrderModel: BaseResponse{
    
    var userId : String?
    var salesOrderId : String?
    var couponDescription : String?
    var couponDiscount : Float?
    var custAccount : String?
    var dataOrigin : String?
    var deliveryAddress : String?
    var deliveryCity : String?
    var deliveryCountryRegionId : String?
    var deliveryDate : String?
    var email : String?
    var inclTax : String?
    var paymTermId : String?
    var shipCharges : String?
    var phoneMobile : String?
    var requestedDate : String?
    var totalAmount : Float?
    var subTotal : Float?
    var vat : Float?
    var paymentMode : String?
    var discount : Int?
    var selfPickup : String?
    var deliveryfee : String?
    
    
    
    var orderProductList = [OrderProductListModel]()
    
    override init() {
        super.init()
    }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        userId <- map["userId"]
        salesOrderId <- map["salesOrderId"]
        couponDescription <- map["couponDescription"]
        couponDiscount <- map["couponDiscount"]
        custAccount <- map["custAccount"]
        dataOrigin <- map["dataOrigin"]
        deliveryAddress <- map["deliveryAddress"]
        deliveryCity <- map["deliveryCity"]
        deliveryCountryRegionId <- map["deliveryCountryRegionId"]
        deliveryDate <- map["deliveryDate"]
        email <- map["email"]
        inclTax <- map["inclTax"]
        paymTermId <- map["paymTermId"]
        shipCharges <- map["shipCharges"]
        phoneMobile <- map["phoneMobile"]
        requestedDate <- map["requestedDate"]
        totalAmount <- map["totalAmount"]
        subTotal <- map["subTotal"]
        vat <- map["vat"]
        paymentMode <- map["paymentMode"]
        discount <- map["discount"]
        orderProductList <- map["orderProductList"]
        deliveryfee <- map["deliveryfee"]

    }
}
