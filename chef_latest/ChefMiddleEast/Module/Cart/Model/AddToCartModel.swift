//
//  AddToCartModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 13/01/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class AddToCartModel: BaseResponse {
    
    //------------------ this is Use for add to Cart  ---------------------//

    var userId : String?
    var product = [ProductModel]()

    //-----------this is Use for get cartlist  Response ---------------//
    var cartSubtotal : Float?
    var cartTotal : Float?
    var cartDiscount : Float?
    var discountVoucher : Float?
    var deliveryFee : Float?
    var productList = [ProductModel]()
    
    //-----------this is Use for update quantity ---------------//
    var productId : Int?
    var productQuantity : Int?
    var cartId : Int?
    
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        
        //------------------ this is Use for add to Cart  ---------------------//

        userId <- map["userId"]
        product <- map["product"]

        //-----------this is Use for get cartlist  Response ---------------//
        cartSubtotal <- map["cartSubtotal"]
        cartTotal <- map["cartTotal"]
        cartDiscount <- map["cartDiscount"]
        discountVoucher <- map["discountVoucher"]
        deliveryFee <- map["deliveryFee"]
        productList <- map["productList"]
        
        //-----------this is Use for update quantity ---------------//

        productId <- map["productId"]
        productQuantity <- map["productQuantity"]
        
        cartId <- map["cartId"]
    }
}
