

import Foundation
import ObjectMapper

class OnlinePaymentModel: BaseResponse {
	var userId : String?
	var dataOrigin : String?
	var strDescription : String?
	var eComJournalId : String?
	var journalLine : JournalLine?

    override init() {
        super.init()
    }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
		userId <- map["userId"]
		dataOrigin <- map["dataOrigin"]
        strDescription <- map["description"]
		eComJournalId <- map["eComJournalId"]
		journalLine <- map["journalLine"]
	}
}

class JournalLine : BaseResponse {
    var currencyCode : String?
    var custAccount : String?
    var journalLinedescription : String?
    var paymentAmount : Float?
    var paymentCode : String?
    var salesReference : String?
    var transDate : String?

    override init() {
        super.init()
    }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {

        currencyCode <- map["currencyCode"]
        custAccount <- map["custAccount"]
        journalLinedescription <- map["journalLinedescription"]
        paymentAmount <- map["paymentAmount"]
        paymentCode <- map["paymentCode"]
        salesReference <- map["salesReference"]
        transDate <- map["transDate"]
    }

}
