//
//  ProductModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 13/01/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class ProductModel: BaseResponse {
    
    var productId : Int?
    var productQuantity : Int?
    
    var productDescription : String?
    var productImage : String?
    var agreementValue : Float?
    var salesPrice : Float?
    var fromDate : String?
    var toDate : String?
   
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        productId <- map["productId"]
        productQuantity <- map["productQuantity"]
        
        productDescription <- map["productDescription"]
        productImage <- map["productImage"]
        agreementValue <- map["agreementValue"]
        salesPrice <- map["salesPrice"]
        fromDate <- map["fromDate"]
        toDate <- map["toDate"]
    }
}
