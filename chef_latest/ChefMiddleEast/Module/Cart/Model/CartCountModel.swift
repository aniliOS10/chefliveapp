//
//  CartCountModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 13/01/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class CartCountModel: BaseResponse{
    
    var cartCount : Int?
    var notificationCount : Int?
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        cartCount <- map["cartCount"]
        notificationCount <- map["notificationCount"]
        
    }
}
