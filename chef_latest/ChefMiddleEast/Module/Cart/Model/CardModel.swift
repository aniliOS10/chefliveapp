//
//  CardModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 25/12/21.
//

import Foundation
import Alamofire
import ObjectMapper

class CardModel: BaseResponse {
    
    var cardExpiryDate : String?
    var cardHolderName : String?
    var cardHolderNumber : String?
    var custAccount : String?
   
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        cardExpiryDate <- map["cardExpiryDate"]
        cardHolderName <- map["cardHolderName"]
        cardHolderNumber <- map["cardHolderNumber"]
        custAccount <- map["custAccount"]
    }
}
