//
//  MockUp_DeliverySummary.swift
//  ChefMiddleEast
//
//  Created by Apple on 08/11/21.
//

import Foundation
import UIKit
struct MockUp_DeliverySummary{
    var description : String!
    var title : String!
    var image : UIImage!

    init(title:String,description:String,image:UIImage){
        self.description = description
        self.title = title
        self.image = image
        }
}
