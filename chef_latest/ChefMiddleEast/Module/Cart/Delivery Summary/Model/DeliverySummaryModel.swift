//
//  DeliverySummaryModel.swift
//  ChefMiddleEast
//
//  Created by sandeep on 20/01/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class DeliverySummaryModel : BaseResponse {
    
    var address = AddressModel()
    var card : String?
    var expectedDeliveryDate: String?
    
    override init() {
         super.init()
     }
    convenience required init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        address <- map["address"]
        card <- map["card"]
        expectedDeliveryDate <- map["expectedDeliveryDate"]
        
    }
}
