//
//  DeliverySummaryDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 08/11/21.
//

import Foundation
import UIKit

extension DeliverySummaryViewController: UITableViewDataSource,UITableViewDelegate{
   
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliverySummarySectionTableViewCell") as! DeliverySummarySectionTableViewCell
        
        if section == 0{
            if isSelfPick {
                return nil
            }
            cell.titleLabel.text = AppMessage_DeliverySummary.Placeholder.SelectDeliveryAddress
            return cell
        }else if section == 1{
            cell.titleLabel.text = AppMessage_DeliverySummary.Placeholder.ExpectedDeliveryDate
            return cell
        }else if section == 3{
            cell.titleLabel.text = "Contact Details"
            return cell
        }
        else if section == 4{
            cell.titleLabel.text = AppMessage_DeliverySummary.Placeholder.PaymentMode
            return cell
        }
        
        
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if isSelfPick {
            if section == 0 {
                return 0.1
            } else if section == 1  || section == 3 || section == 4{
                return 40.0
            }
            return 0.1
        } else {
            if section == 0 || section == 1  || section == 3 || section == 4{
                return 40.0
            }
            return 0.1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliverySummaryAddressTableViewCell") as! DeliverySummaryAddressTableViewCell
            cell.cellViewRound.addShadowDash()
            cell.cellViewRound.layer.cornerRadius = 5.0
            cell.btnEdit.tag = 99999
            cell.btnEdit.addTarget(self, action: #selector(btnEditTap(sender:)), for: .touchUpInside)
            
            if let countryName = dictAddress.countryName,!countryName.isEmpty{
                cell.lblAddress3.text = countryName
                cell.btnEdit.isHidden = false
                cell.lblDeliveryMethod.isHidden = false
                cell.imgLine.isHidden = false
            } else {
                cell.btnEdit.isHidden = true
                cell.lblDeliveryMethod.isHidden = true
                cell.imgLine.isHidden = true
                cell.lblAddress3.text = ""
            }
            
            if let street = dictAddress.street,!street.isEmpty{
            
                cell.lblAddress1.text = String(format: "%@ %@", dictAddress.street!, dictAddress.address!)
                
                cell.btnEdit.isHidden = false
                cell.lblDeliveryMethod.isHidden = false
                cell.imgLine.isHidden = false
                cell.lblAddress1.lineBreakMode = .byWordWrapping
                cell.lblAddress1.numberOfLines = 2
            }
            
            if let city = dictAddress.city,!city.isEmpty{
                cell.lblAddress2.text = city
                cell.btnEdit.isHidden = false
                cell.lblDeliveryMethod.isHidden = false
                cell.imgLine.isHidden = false
            }
            
            
            if let AddAdresscountryId = dictAddress.AddAdresscountryId,!AddAdresscountryId.isEmpty{
                cell.lblAddress4.text = AddAdresscountryId
            }
            return cell
        }
        else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliverySummaryDeliveryDateTableViewCell") as! DeliverySummaryDeliveryDateTableViewCell
            cell.cellViewRound.addShadowDash()
            cell.cellViewRound.layer.cornerRadius = 5.0
            if let date = dictDeliverySummary.expectedDeliveryDate, !date.isEmpty {
                cell.btnDeliveryDayDate.setTitle(date.utcServerDateToDateString, for: .normal)
            }
            return cell
        }
        else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliverySummaryPayMentTotalTableViewCell") as! DeliverySummaryPayMentTotalTableViewCell
            cell.cellViewRound.addShadowDash()
            cell.cellViewRound.layer.cornerRadius = 5.0
            
            if totalSum != 0.0 && subTotal != 0.0 {
                
                cell.lblSubTotalAmnt.text =
                    String(format: "AED %.2f", subTotalNew)
                
                // New subTotal
                
                cell.lblTotalAmnt.text = String(format: "AED %.2f", totalSum)
                
                let vat = dictAppInfo.vatPercent
                if vat != nil {
                    cell.lblVat.text = "VAT (\(String(describing: vat!))%)"
                }
                cell.lblVatAmnt.text =  String(format: "AED %.2f", vatTotal)
               
                if discountAmount != 0.0{
                    cell.lblDiscountAmnt.text = String(format: "-AED %.2f", ShowDiscountAmount)
                    cell.lblDiscountAmnt.isHidden = false
                    cell.lblDiscount.isHidden = false
                    cell.topDiscount.constant  = 8
                    cell.lblDiscount.text = "Discount"
                    cell.heightDiscount.constant = 17.55

                }
                else{
                    
                    cell.lblDiscountAmnt.text = ""
                    cell.lblDiscount.text = ""
                    cell.lblDiscountAmnt.isHidden = true
                    cell.lblDiscount.isHidden = true
                    cell.topDiscount.constant  = 0
                    cell.heightDiscount.constant = 0
                    cell.layoutIfNeeded()
                    cell.contentView.layoutIfNeeded()
                }
                
                if isSelfPick {
                    cell.lblDeliveryFeeAmnt.text = "Free"
                    cell.lblDeInclVat.text = ""
                } else {
                    let deliveryLimt = dictAppInfo.deliveryLimit

                    if Int(subTotal) > deliveryLimt ?? 0 {
                        cell.lblDeliveryFeeAmnt.text = "Free"
                        cell.lblDeInclVat.text = ""

                    }
                    else if arrAppliedCoupanData.discountType == "Free Delivery"{
                        cell.lblDeliveryFeeAmnt.text = "Free"
                        cell.lblDeInclVat.text = ""

                    }
                    else{
                        let deliveryCharges = Float(dictAppInfo.deliveryCharges ?? 0)
                        var deliveryChargesValue : Float = 0.0

                        if vat != nil {
                            deliveryChargesValue = Float(Float(deliveryCharges)  / (1 + (Float(vat!)) / 100))
                            
                            cell.lblDeliveryFeeAmnt.text = String(format: "AED %.2f", Float(deliveryCharges ?? 0))
                            cell.lblDeInclVat.text = "(Incl VAT)"

                        } else {
                            cell.lblDeliveryFeeAmnt.text =  String(format: "AED %.2f", Float(deliveryCharges ?? 0))
                            cell.lblDeInclVat.text = "(Incl VAT)"

                        }
                        
                        if freeDelivery == "Free"{
                            cell.lblDeliveryFeeAmnt.text = "Free"
                            cell.lblDeInclVat.text = ""
                        }

                    }
                }
            }
            else{
                
            }
            return cell
        }
        else if indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliverySummaryContactTVC") as! DeliverySummaryContactTVC
            cell.vwBase.addShadowDash()
            cell.vwBase.layer.cornerRadius = 5.0
            cell.lblCountryCode.isHidden = false
            
            let emailId = UserDefaults.standard.string(forKey: Constants.email)
            let phone = UserDefaults.standard.string(forKey: Constants.phone)

            if emailId == nil || emailId == "" {
        
                cell.txtEmailAddress.isEnabled = true
                cell.btnUpdateEmail.setTitle("Verify", for: .normal)
                
                
            } else {
                cell.txtEmailAddress.isEnabled = false
                cell.btnUpdateEmail.setTitle("Update", for: .normal)
                cell.txtEmailAddress.text = emailId

            }
            
            if phone == nil || phone == ""{
                cell.txtPhoneNumber.isEnabled = true
                cell.btnUpdatePhone.setTitle("Verify", for: .normal)
            } else {
                cell.txtPhoneNumber.isEnabled = true
                cell.btnUpdatePhone.setTitle("Update", for: .normal)
                if cell.txtPhoneNumber.text == "" || cell.txtPhoneNumber.text == nil {
                    cell.txtPhoneNumber.text = phone
                }
                else{
                }
                
            }
            
         
            
            cell.lblCountryCodeWidth.constant = 50.0
            
            cell.btnUpdateEmail.addTarget(self, action: #selector(updateEmailMethodTap(sender:)), for: .touchUpInside)
            cell.btnUpdatePhone.addTarget(self, action: #selector(updatePhoneMethodTap(sender:)), for: .touchUpInside)
            
            cell.btnUpdatePhone.isHidden = true

            return cell
        }
        else if indexPath.section == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliverySummaryCardTableViewCell") as! DeliverySummaryCardTableViewCell
            cell.cellViewRound.addShadowDash()
            cell.cellViewRound.layer.cornerRadius = 5.0
            cell.btnCreditCard.addTarget(self, action: #selector(btnPaymentMethodsTap(sender:)), for: .touchUpInside)
            cell.btnCreditCardOnDelivery.addTarget(self, action: #selector(btnPaymentMethodsTap(sender:)), for: .touchUpInside)
            cell.btnCreditCard.isSelected = false
            cell.btnCreditCardOnDelivery.isSelected = false
            if strSelectedPaymentMethod == "CreditCard" {
                cell.btnCreditCard.isSelected = true
            }
            else {
                cell.btnCreditCardOnDelivery.isSelected = true
            }
            return cell
        }
        else if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliverySummaryTermsTVC") as! DeliverySummaryTermsTVC
           //
            //cell.isChecked = isChecked
            cell.vwBackground.layer.cornerRadius = 5.0
            cell.vwBackground.addShadowDash()
              //  .underlineStyle: NSUnderlineStyle.single.rawValue
            let btnAttributes: [NSAttributedString.Key: Any] = [
                  .font: UIFont(name: FontName.Optima.Bold, size: 15)!,
                  .foregroundColor: UIColor.black
            ]
            let attributeString = NSMutableAttributedString(
                    string: "I agree to T&C's and Privacy Policy",
                    attributes: btnAttributes
            )
            cell.btnTermCondition.setAttributedTitle(attributeString, for: .normal)
            
            
            if cell.isChecked {
                cell.btnCheckBox.setImage(UIImage(named: "checkBox"), for: .normal)
            } else {
                cell.btnCheckBox.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                
            }
            
            cell.btnCheckBox.addTarget(self, action: #selector(btnCheckBoxTap(sender:)), for: .touchUpInside)
            cell.btnTermCondition.addTarget(self, action: #selector(btnTermsConditionTap(sender:)), for: .touchUpInside)
            return cell
        }
        else if indexPath.section == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliverySummaryPayNowTableViewCell") as! DeliverySummaryPayNowTableViewCell
            cell.btnPayNow.setTitle("ORDER NOW", for: .normal)
            if strSelectedPaymentMethod == "CreditCard" {
                cell.btnPayNow.setTitle("PAY NOW", for: .normal)
            }
            return cell
        }
        
       
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if isSelfPick {
            if indexPath.section == 0 {
                return 0
            } else if indexPath.section == 5 {
                return 55.0
            }else if indexPath.section == 3 {
                return 130
            }
        } else {
            if indexPath.section == 0 {
                if let countryName = dictAddress.countryName,!countryName.isEmpty{
                    return 237.0
                } else {
                    return 83
                }
            }
            if indexPath.section == 5 {
                return 55.0
            }else if indexPath.section == 3 {
                return 130
            }
        }
        return UITableView.automaticDimension
    }
    
    
    //MARK: - Update Email Tap
    @objc func updateEmailMethodTap(sender: UIButton) {
        
        let cell : DeliverySummaryContactTVC = tableView.cellForRow(at: IndexPath(row: 0, section: 3)) as! DeliverySummaryContactTVC
        
        if sender.title(for: .normal) == "Update" {
            sender.setTitle("Verify", for: .normal)
            cell.txtEmailAddress.isEnabled = true
            cell.txtEmailAddress.becomeFirstResponder()
            
        } else {
            //Verify Email
            if cell.txtEmailAddress.text?.trimmingCharacters(in: .whitespaces).count == 0 {
                NotificationAlert().NotificationAlert(titles: "Please enter email")
                return
            } else {
                EndPoint.email = cell.txtEmailAddress.text!.trimmingCharacters(in: .whitespaces)
                self.verifyEmailSendOTP()
            }
        }
    }
    
    func verifyEmailSendOTP(){
        self.showLoader()
        EndPoint.VerifyEmailSendOTP(t: UserModel.self) { result in
            switch result {
            case .onSuccessWithIntegerValue(let strOtp):
                NotificationAlert().NotificationAlert(titles: "Verification code sent on email")
                UserDefaults.standard.set(strOtp, forKey: Constants.otp)
                let controller:OTPVerifyViewController = UIStoryboard(storyboard: .athentication).initVC()
                controller.isRegisterNewUser = false
                controller.isPhoneVerification = false
                controller.email = EndPoint.email
                self.navigationController?.pushViewController(controller, animated: true)
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
                self.hideLoader()
            }
        }
    }
    
    //MARK: - Update Phone Tap
    @objc func updatePhoneMethodTap(sender: UIButton) {
        let cell : DeliverySummaryContactTVC = tableView.cellForRow(at: IndexPath(row: 0, section: 3)) as! DeliverySummaryContactTVC
        if sender.title(for: .normal) == "Update" {
            cell.lblCountryCode.isHidden = false
            sender.setTitle("Verify", for: .normal)
            cell.txtPhoneNumber.isEnabled = true
            cell.txtPhoneNumber.becomeFirstResponder()
            cell.lblCountryCodeWidth.constant = 50.0
        } else {
            //Verify Phone
            if cell.txtPhoneNumber.text?.trimmingCharacters(in: .whitespaces).count == 0 {
                NotificationAlert().NotificationAlert(titles: "Please enter phone number")
                return
            }
            else if cell.txtPhoneNumber.text?.trimmingCharacters(in: .whitespaces).count ?? 0 < 9 {
                NotificationAlert().NotificationAlert(titles: "Please enter valid phone number")
                return
            }
            else {
                let number = cell.txtPhoneNumber.text!.trimmingCharacters(in: .whitespaces)
                EndPoint.phone = String("\("%2B971")\(number)")
                EndPoint.phoneNumber = String("\(number)")
                EndPoint.checkExisting = false
                self.verifyPhoneSendOTP()
            }
        }
    }
   
    func verifyPhoneSendOTP(){
        self.showLoader()
        EndPoint.VerifyPhoneSendOTP(t: UserModel.self) { result in
            switch result {
            case .onSuccessWithIntegerValue(let strOtp):
                NotificationAlert().NotificationAlert(titles: "Verification code sent on phone")
                UserDefaults.standard.set(strOtp, forKey: Constants.phoneOtp)
                let controller:OTPVerifyViewController = UIStoryboard(storyboard: .athentication).initVC()
                controller.isRegisterNewUser = false
                controller.isPhoneVerification = true
                self.navigationController?.pushViewController(controller, animated: true)
                self.hideLoader()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
                self.hideLoader()
            }
        }
    }
    
    //MARK:- Add Product Tap
    @objc func btnPaymentMethodsTap(sender: UIButton) {
        strSelectedPaymentMethod = "CreditCardOnDelivery"
        if sender.tag == 200 {
            strSelectedPaymentMethod = "CreditCard"
        }
        self.tableView.reloadData()
    }
    
    //MARK:- Edit Button Tap
    @objc func btnEditTap(sender: UIButton) {
        let controller:AddNewAddressViewController =  UIStoryboard(storyboard: .Address).initVC()
        controller.titleString = "Edit Address"
        controller.subTitleString = AppMessage_NewAddress.Placeholder.EditAddress
        controller.saveTitleString = AppMessage_NewAddress.Placeholder.UpdateAddress
        controller.dictAddress = dictAddress
        controller.dictAddress.customerAddressId = dictAddress.customerAddressId
        controller.screenComeFrom = "EditButtonTap"
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - CheckBox Button Tapped
     @objc func btnCheckBoxTap(sender: UIButton) {
         let cell : DeliverySummaryTermsTVC = tableView.cellForRow(at: IndexPath(row: 0, section: 5)) as! DeliverySummaryTermsTVC
         if cell.isChecked {
             cell.btnCheckBox.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
         } else {
             cell.btnCheckBox.setImage(UIImage(named: "checkBox"), for: .normal)
         }
         cell.isChecked = !cell.isChecked
     }
    
    // MARK: - Terms And Condition Button Tapped
    @objc func btnTermsConditionTap(sender: UIButton) {
        let controller:PrivacyPolicyAndTermsConditionVC =  UIStoryboard(storyboard: .Cartpayment).initVC()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
}
