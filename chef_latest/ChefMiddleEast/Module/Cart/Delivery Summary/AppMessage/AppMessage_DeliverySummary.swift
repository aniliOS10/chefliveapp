//
//  AppMessage_DeliverySummary.swift
//  ChefMiddleEast
//
//  Created by Apple on 08/11/21.
//

import Foundation
class AppMessage_DeliverySummary: NSObject {
    struct Placeholder {
        static let SelectDeliveryAddress = NSLocalizedString("SelectDeliveryAddress", comment: "")
        static let ExpectedDeliveryDate = NSLocalizedString("ExpectedDeliveryDate", comment: "")
        static let PaymentMode = NSLocalizedString("PaymentMode", comment: "")
        static let DeliverySummary = NSLocalizedString("DeliverySummary", comment: "")
       
    }
}
