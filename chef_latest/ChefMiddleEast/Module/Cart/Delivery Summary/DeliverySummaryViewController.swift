//
//  DeliverySummaryViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 08/11/21.
//

import UIKit
import PaymentSDK

@available(iOS 13.0, *)

class DeliverySummaryViewController:BaseViewController, AddressViewControllerDelegate, EditNewAddressViewControllerDelegate {
 
    @IBOutlet weak var tableView : UITableView!
    var arrAppliedCoupanData = AppliedCoupanDetail()

    var dictDeliverySummary = DeliverySummaryModel()
    var dictAddress = AddressModel()
    
    var titleString:String = Appmessage_NewArrivals.Placeholder.MyFavourites
    
    var subtitleString:String = Appmessage_NewArrivals.Placeholder.MyFavouritesProducts
    
    var strSelectedPaymentMethod = "CreditCardOnDelivery"
        
    var totalSum: Float = 0.0
    
    var discountAmount: Float = 0.0
    var ShowDiscountAmount: Float = 0.0

    var subTotal: Float = 0.0
    var subTotalDouble: Double = 0.0
    var subTotalNew: Float = 0.0

    var isSelfPick : Bool = false
    var coupanCodeString = ""
    var vatTotal: Float = 0.0
    var freeDelivery = ""
    
    var isOrderNow = false
    
    var arrCartProduct = [ProductModel]()
    var strOrderId:String?
    let profileID = "47422"
    let serverKey = "SRJNLKMGWB-J2W6666TDM-GDLDNDNWWW"
    let clientKey = "C7KMDV-D22N6M-VGGGG7-GPRQQP"
    let firstName = UserDefaults.standard.string(forKey: Constants.firstName)
    let lastName = UserDefaults.standard.string(forKey: Constants.lastName)
    var emailId = UserDefaults.standard.string(forKey: Constants.email)
    var phone = UserDefaults.standard.string(forKey: Constants.phone)

    var billingDetails: PaymentSDKBillingDetails! {
        return PaymentSDKBillingDetails(name: "\(firstName ?? "") \(lastName ?? "")",
                                     email: emailId,
                                     phone: phone,
                                     addressLine: "Street1",
                                     city: "Dubai",
                                     state: "Dubai",
                                     countryCode: "ae",
                                     zip: "12345")
    }
    
    var shippingDetails: PaymentSDKShippingDetails! {
        return PaymentSDKShippingDetails(name: "John Smith",
                                      email: "email@test.com",
                                      phone: "+9731111111",
                                      addressLine: "Street1",
                                      city: "Dubai",
                                      state: "Dubai",
                                      countryCode: "ae",
                                      zip: "12345")
    }
    var configuration: PaymentSDKConfiguration! {
        let theme = PaymentSDKTheme.default
        theme.logoImage = UIImage(named: "Notification_iconPay")
        theme.backgroundColor = .white
        theme.secondaryColor = .white
        theme.backButtonColor = .black
        theme.titleFontColor = .black
        theme.secondaryFontColor = .black
        theme.buttonColor = UIColor().getThemeColor()
        return PaymentSDKConfiguration(profileID: profileID,
                                       serverKey: serverKey,
                                       clientKey: clientKey,
                                       currency: "AED",
                                       amount: subTotalDouble,
                                       merchantCountryCode: "AE")
            .cartDescription("Flowers")
            .cartID(strOrderId ?? "")
            .screenTitle("Pay with Card")
            .theme(theme)
            .billingDetails(billingDetails)
    }


    
    //MARK:- VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let x = totalSum
        
        let roundedValue = round(x * 100) / 100.0


        var s = String(format: "%.2f", x)
        let resultOriginal = Double(Int(100*Double(s)!))/100

        while s != String(Double(s)!) {
            s = String(String(s).dropLast())
        }
        let goodResult = Double(Int(100*Double(s)!))/100

        print(resultOriginal, goodResult)
        
        totalSum = Float(roundedValue)
        
        subTotalDouble = Double(resultOriginal)
    
        self.setUpNavigation()
        
        
        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            EndPoint.userId = userId
            self.afterCart()
        }
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
    override func viewWillAppear(_ animated : Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    //MARK:- SetUp Navigation
    func setUpNavigation(){
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
        
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title:AppMessage_DeliverySummary.Placeholder.DeliverySummary )
        } else {
             self.initBellButton()
             bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
             self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title:AppMessage_DeliverySummary.Placeholder.DeliverySummary )
        }
        
       
    }
    
    //MARK:- Menu Button
    @objc func menuButtonPressed(){
        self.sideMenuController?.revealMenu()
    }
    
    //MARK:- Notification Button
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
   
    //MARK:- Custome delegate AddressViewController (Choose Address Tap)
    func chooseAddress(model: AddressModel) {
        self.navigationController?.popViewController(animated: true)
        dictAddress.street = model.street
        dictAddress.city = model.city
        dictAddress.AddAdresscountryId = model.AddAdresscountryId
        dictAddress.countryName = model.countryName
        dictAddress.customerAddressId = model.customerAddressId
        dictAddress.address = model.address
        tableView.reloadData()
    }
    
    //MARK:- Custome delegate Add AddressViewController (Edit Tap)
    func EditAddress(model: AddressModel) {
        
        dictAddress.street = model.street
        dictAddress.city = model.city
        dictAddress.AddAdresscountryId = model.AddAdresscountryId
        dictAddress.countryName = model.countryName
        dictAddress.customerAddressId = model.customerAddressId
        dictAddress.address = model.address

        tableView.reloadData()
    }
    
    
    //MARK:- Button Action
    @IBAction func buttonDeliveryAddressPressed(_ sender: Any) {
        
        let controller:AddressViewController =  UIStoryboard(storyboard: .Address).initVC()
        controller.screenCommingFrom = "DeliverySummaryChooseAddress"
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func buttonPayNowPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        
        
        if isSelfPick {
        } else {
            
            if self.dictAddress.address == "" || self.dictAddress.address == nil  {
                NotificationAlert().NotificationAlert(titles: "Please choose address")
                return
            }
            
//            if self.dictAddress.city == "" || self.dictAddress.city == nil {
//                NotificationAlert().NotificationAlert(titles: "Please choose address")
//                return
//            }
        }
        
        
        
        let termsTVC : DeliverySummaryTermsTVC = tableView.cellForRow(at: IndexPath(row: 0, section: 5)) as! DeliverySummaryTermsTVC
    
        let cell : DeliverySummaryContactTVC = tableView.cellForRow(at: IndexPath(row: 0, section: 3)) as! DeliverySummaryContactTVC
       
            if cell.txtPhoneNumber.text?.trimmingCharacters(in: .whitespaces).count == 0 {
                NotificationAlert().NotificationAlert(titles: "Please enter phone number")
                return
            }
            else if cell.txtPhoneNumber.text?.trimmingCharacters(in: .whitespaces).count ?? 0 < 9 {
                NotificationAlert().NotificationAlert(titles: "Please enter valid phone number")
                return
            }
            else {
                let number = cell.txtPhoneNumber.text!.trimmingCharacters(in: .whitespaces)
                EndPoint.phone = String("\("%2B971")\(number)")
                EndPoint.phoneNumber = String("\(number)")
                UserDefaults.standard.set(EndPoint.phoneNumber, forKey: Constants.phone)
            }
    
        emailId = UserDefaults.standard.string(forKey: Constants.email)
        phone = UserDefaults.standard.string(forKey: Constants.phone)
        
        if emailId == nil || emailId == ""{
            NotificationAlert().NotificationAlert(titles: "Please verify email ID")
        } else if phone == nil || phone == ""{
            NotificationAlert().NotificationAlert(titles: "Please enter phone number")
        } else if !termsTVC.isChecked {
            NotificationAlert().NotificationAlert(titles: "Please agree to terms & condition")
        } else {
            
            if strSelectedPaymentMethod == "CreditCard" {
                if self.isOrderNow == false {
                    self.generateSalesOrderId()
                }
            }
            else {
                if self.isOrderNow == false {
                  self.createOrderWhileDeliveryOrCreditCard(paymentMethod: "ON DELIVER")
                }
            }
        }
    }
    
    
    @IBAction func buttonReturnToCartd(_ sender: Any) {
        let controller:SortAndFilterViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.present(controller, animated: true, completion: nil)
    }
    
    func createOrderWhileDeliveryOrCreditCard(paymentMethod:String) {
        
        let orderModel = CreateOrderModel()
        
        let deliveryLimt = dictAppInfo.deliveryLimit
        var deliveryCharges = Int()
    
        if Int(subTotal) > deliveryLimt! {
            deliveryCharges = 0
        }
        else{
            deliveryCharges = dictAppInfo.deliveryCharges!
        }
         
        if let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            orderModel.userId = userId
        }
         emailId = UserDefaults.standard.string(forKey: Constants.email)
         phone = UserDefaults.standard.string(forKey: Constants.phone)
        
        if isSelfPick {
            orderModel.selfPickup = "Yes"
            orderModel.deliveryAddress = "Self pickup"
            orderModel.deliveryCity = "dubai"
            orderModel.shipCharges =  "0.0"
            orderModel.deliveryfee = "0.0"
        } else {
            orderModel.selfPickup = "Chef Delivery"
            orderModel.deliveryAddress =  String(format: "%@ %@",  self.dictAddress.address ?? "",  self.dictAddress.street ?? "")
            orderModel.deliveryCity = self.dictAddress.city
            orderModel.shipCharges =  String(deliveryCharges)
            orderModel.deliveryCity = "dubai"

            let deliveryLimt = dictAppInfo.deliveryLimit

            if Int(self.subTotal) > deliveryLimt! {
                orderModel.deliveryfee = "0.0"
            }
            else  {
                let deliveryCharges = Float(dictAppInfo.deliveryCharges ?? 0)
                var deliveryChargesValue : Float = 0.0
                let vat = dictAppInfo.vatPercent

                if vat != nil {
                    deliveryChargesValue = Float(Float(deliveryCharges)  / (1 + (Float(vat!)) / 100))
                    
                    orderModel.deliveryfee = String(format: "%.2f", deliveryChargesValue)
                } else {
                    orderModel.deliveryfee =  String(format: "%.2f", deliveryChargesValue)
                }
                
                if arrAppliedCoupanData.discountType == "Free Delivery"{
                    orderModel.deliveryfee = "0.0"
                }
                
                if freeDelivery == "Free"{
                    orderModel.deliveryfee = "0.0"
                }
            }
        }
        
        orderModel.email = emailId
        orderModel.subTotal = self.subTotal
        orderModel.totalAmount = self.totalSum
        orderModel.vat = self.vatTotal
        orderModel.couponDescription = coupanCodeString
        orderModel.couponDiscount = self.discountAmount
        orderModel.discount = Int(self.discountAmount)
        orderModel.deliveryDate = self.dictDeliverySummary.expectedDeliveryDate?.utcServerDateToDateString
        orderModel.deliveryCountryRegionId = "uae"
        orderModel.dataOrigin = "Mobile"
        orderModel.paymentMode = "ON DELIVER"
        orderModel.paymTermId = "ON DELIVER"
        orderModel.inclTax = ""
        if UserDefaults.standard.bool(forKey: Constants.guestUser){
            
            orderModel.custAccount = String(UserDefaults.standard.integer(forKey: Constants.custAccount))

        }
        else{
            orderModel.custAccount = String(UserDefaults.standard.integer(forKey: Constants.custAccount))
        }
        orderModel.phoneMobile = phone
        orderModel.requestedDate = "currentDate".utcServerDateToDateString
        orderModel.salesOrderId = strOrderId
        for info in arrCartProduct {
            let orderedProduct = OrderProductListModel()
            orderedProduct.itemId = info.productId
            orderedProduct.salesPrice = info.salesPrice
            orderedProduct.salesQty = info.productQuantity
            orderedProduct.eComInventTransId = ""
            orderedProduct.eComSalesId = ""
            orderedProduct.fOCItem = 0
            orderedProduct.taxRate = dictAppInfo.vatPercent
            orderedProduct.discAmountPerQty = "0.0"
            orderedProduct.inventTransId = ""
            orderedProduct.productTitle = info.productDescription
            orderedProduct.productImageStr = info.productImage
            orderModel.orderProductList.append(orderedProduct)
        }
        
        print("createOrderAPI")
        print(orderModel.toJSON())

        self.createOrderAPI(Model: orderModel)
    }
   
    //MARK:- Update Quantity Api
    func afterCart() {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        EndPoint.currentDateTime = dateString
        
        self.showLoader()
        EndPoint.AfterCart(t: DeliverySummaryModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccess(let data):
                if data != nil {
                    self.dictDeliverySummary = data!
                    self.dictAddress = data!.address
                    self.tableView.reloadData()
                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- Creat Order Api
    func createOrderAPI(Model: CreateOrderModel) {
        isOrderNow = true
        self.showLoader()
        EndPoint.CreateOrder(params: Model,t: CreateOrderModel.self) { result in
            self.hideLoader()
            self.isOrderNow = false
            switch result {
            case .onSuccessWithStringValue(let strData):
                NotificationAlert().NotificationAlert(titles: strData)
                let controller:OrderPlacedViewController =  UIStoryboard(storyboard: .Cartpayment).initVC()
                self.navigationController?.pushViewController(controller, animated: true)
            case .onFailure(let error):
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- OnlinePaymentAPI
    func onlinePaymentAPI(Model: OnlinePaymentModel) {
        self.showLoader()
        EndPoint.OnlinePayment(params: Model,t: CreateOrderModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccessWithStringValue(let strData):
                
                NotificationAlert().NotificationAlert(titles: strData)
            
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.createOrderWhileDeliveryOrCreditCard(paymentMethod: "PAID")
                }
            case .onFailure(let error):
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- Order ID API
    func generateSalesOrderId() {
        isOrderNow = true
        self.showLoader()
        EndPoint.GenerateSalesOrderId(t: CountryListModel.self) {
            (result) in
            self.hideLoader()
            self.isOrderNow = false
            switch result {
            case .onSuccessWithStringValue(let strData):
                print(strData)
                self.strOrderId = strData
                PaymentManager.startCardPayment(on: self, configuration: self.configuration, delegate: self)
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
}

extension DeliverySummaryViewController: PaymentManagerDelegate {
    
    func paymentManager(didFinishTransaction transactionDetails: PaymentSDKTransactionDetails?, error: Error?) {
        if let transactionDetails = transactionDetails {
            if transactionDetails.isSuccess() {
                let paymentModel = OnlinePaymentModel()
                if let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
                    paymentModel.userId = userId
                }
                paymentModel.dataOrigin = "Mobile"
                paymentModel.strDescription = transactionDetails.transactionReference ?? ""
                paymentModel.eComJournalId = ""
                
                let journalModel = JournalLine()
                journalModel.currencyCode = "AED"
                journalModel.custAccount = "13629"
                journalModel.journalLinedescription = self.strOrderId
                journalModel.paymentAmount = self.totalSum
                journalModel.paymentCode = "Credit card"
                journalModel.salesReference = transactionDetails.transactionReference ?? ""
                journalModel.transDate = transactionDetails.paymentResult?.transactionTime ?? ""
                paymentModel.journalLine = journalModel
                self.onlinePaymentAPI(Model: paymentModel)
            }
        } else if let error = error {
            NotificationAlert().NotificationAlert(titles: error.localizedDescription)
            print(error.localizedDescription)
        }
    }
}
