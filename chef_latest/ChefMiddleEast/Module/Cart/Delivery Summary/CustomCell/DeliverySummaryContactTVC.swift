//
//  DeliverySummaryContactTVC.swift
//  ChefMiddleEast
//
//  Created by Apple on 07/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class DeliverySummaryContactTVC: UITableViewCell {

    @IBOutlet weak var lblCountryCodeWidth : NSLayoutConstraint!
    @IBOutlet weak var lblCountryCode : UILabel!
    @IBOutlet weak var txtPhoneNumber : UITextField!
    @IBOutlet weak var txtEmailAddress : UITextField!
    @IBOutlet weak var btnUpdatePhone : UIButton!
    @IBOutlet weak var btnUpdateEmail : UIButton!
    @IBOutlet weak var vwBase : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
