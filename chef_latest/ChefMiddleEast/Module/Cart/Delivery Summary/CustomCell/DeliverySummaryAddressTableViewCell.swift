//
//  DeliverySummaryAddressTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 08/11/21.
//

import UIKit

class DeliverySummaryAddressTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var lblAddress1: UILabel!
    @IBOutlet var lblAddress2: UILabel!
    @IBOutlet var lblAddress3: UILabel!
    @IBOutlet var lblAddress4: UILabel!
    @IBOutlet var lblDeliveryMethod: UILabel!
    @IBOutlet var imgLine: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
