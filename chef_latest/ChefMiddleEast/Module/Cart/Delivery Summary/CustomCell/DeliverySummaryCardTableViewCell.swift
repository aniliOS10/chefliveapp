//
//  DeliverySummaryCardTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 08/11/21.
//

import UIKit

class DeliverySummaryCardTableViewCell: UITableViewCell {
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var btnCreditCardOnDelivery: UIButton!
    @IBOutlet weak var btnCreditCard: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
