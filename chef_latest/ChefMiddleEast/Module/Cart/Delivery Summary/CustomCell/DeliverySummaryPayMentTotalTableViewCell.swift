//
//  DeliverySummaryPayMentTotalTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 08/11/21.
//

import UIKit

class DeliverySummaryPayMentTotalTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellViewRound: UIView!
    
    @IBOutlet  var lblSubTotalAmnt: UILabel!
    @IBOutlet  var lblVatAmnt: UILabel!
    @IBOutlet  var lblDiscountAmnt: UILabel!
    @IBOutlet  var lblDeliveryFeeAmnt: UILabel!
    @IBOutlet  var lblTotalAmnt: UILabel!
    @IBOutlet  var lblSubTotal: UILabel!
    @IBOutlet  var lblVat: UILabel!
    @IBOutlet  var lblDiscount: UILabel!
    @IBOutlet  var lblDelivery: UILabel!
    @IBOutlet  var lblTotal: UILabel!

    @IBOutlet weak var lblDeInclVat: UILabel!
    @IBOutlet weak var topDiscount: NSLayoutConstraint!
    @IBOutlet weak var heightDiscount: NSLayoutConstraint!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
