//
//  CardDetailViewController.swift
//  ChefMiddleEast
//
//  Created by rupinder singh on 02/11/21.
//

import UIKit

@available(iOS 13.0, *)
class CardDetailViewController: BaseViewController {

    @IBOutlet var lblTotalMain: UILabel!
    
    @IBOutlet var lblTotal: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
       // self.initBackButton()
    //backButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
      //  self.initBellButton()
      // bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
       
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: AppMessage_CardDetail.Placeholder.CartDetail)
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
       
        self.tabBarController?.tabBar.isHidden = true
    }
    @IBAction func buttonContinuePressed(_ sender: Any) {
        let controller:CartSummaryViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
