//
//  CartSummaryDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import Foundation
import UIKit

extension CartSummaryViewController: UITableViewDataSource,UITableViewDelegate, DiscountCartSummaryTableViewCellDelegate{
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.isBuyOneGetOneCoupon {
            return 6
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if self.isBuyOneGetOneCoupon {
            if section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartSummaryFreeProductsSectionTVC") as! CartSummaryFreeProductsSectionTVC
                cell.titleLabel.text = "Free Products"
                return cell
            }
             else if section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartSummarySectionTableViewCell") as! CartSummarySectionTableViewCell
                 cell.titleLabel.text = "Cart Details"
                 return cell
            }
        } else {
            
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartSummarySectionTableViewCell") as! CartSummarySectionTableViewCell
            cell.titleLabel.text = "Cart Details"
            return cell
        }
            else{
               return UIView()
                
            }
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        if self.isBuyOneGetOneCoupon {
            if section == 0 || section == 1 {
                return 30.0
            } else {
                return 0.1
            }
        } else {
            if section == 0 {
                return 30.0
            }
            else{
                return 0.1
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        
        if self.isBuyOneGetOneCoupon {
            if section == 0 {
                return arrCartProduct.count
            } else if section == 1{
                return arrAppliedCoupanData.freeProducts.count
            }
        } else {
            if section == 0 {
                return arrCartProduct.count
            }
            else if section == 1{
                return 1
            }
            else if section == 2{
                return 1
            }
            else if section == 3{
                return 1
            }
        }
        return 1
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if self.isBuyOneGetOneCoupon {
            
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartSummaryTableViewCell") as! CartSummaryTableViewCell
                cell.cellViewRound.addShadowDash()
                cell.cellViewRound.layer.cornerRadius = 8
                cell.imageV.layer.cornerRadius = 8
                cell.vwOfferApplied.isHidden = true
                if let imgUrl = arrCartProduct[indexPath.row].productImage, !imgUrl.isEmpty {
                    
                    let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

                    cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                        if (error != nil) {
                            cell.imageV.image = UIImage(named: "placeholder")
                        } else {
                            cell.imageV.image = image
                        }
                    }
                }
                else {
                    cell.imageV.image = UIImage(named: "placeholder")
                }
                
                
                if couponCodeDiscount != 0 || isFreeDeliveryCoupon || isBuyOneGetOneCoupon{
                    let tempProductID  =   arrCartProduct[indexPath.row].productId
                    if let appliedProductIDs = arrAppliedCoupanData.products, appliedProductIDs != nil {
                        let tempProductsArray : NSArray = appliedProductIDs.components(separatedBy: ",") as NSArray
                        if tempProductsArray.contains(String("\(tempProductID!)") ) {
                                cell.vwOfferApplied.isHidden = true
                        } else {
                                cell.vwOfferApplied.isHidden = true
                        }
                    }
                }
                
                cell.vwSelectQty.addShadowDash()
                cell.vwSelectQty.layer.cornerRadius = 8
                cell.titleLabel.text = arrCartProduct[indexPath.row].productDescription ?? ""
                
                if let price = arrCartProduct[indexPath.row].salesPrice,price != 0 {
                    cell.desLabel.text = String(format: "AED %.2f", price)
                }
                else {
                    cell.desLabel.text = "AED 0.0"
                }
                
                
                if let startDate = arrCartProduct[indexPath.row].fromDate,startDate != nil {
                    
                    let fromDate : Date = startDate.dateFromString(startDate)
                    let endDate = arrCartProduct[indexPath.row].toDate
                    let toDate : Date = endDate!.dateFromString(endDate!)
                    
                    if Date().isBetween(fromDate, and: toDate) {
                        if let agreementValue = arrCartProduct[indexPath.row].agreementValue, agreementValue != nil {
                            cell.desLabel.isHidden = false
                            let agreementValue = arrCartProduct[indexPath.row].agreementValue
                            cell.desLabel.text = String(format: "AED %.2f", agreementValue!)
                        } else {
                            if let price = arrCartProduct[indexPath.row].salesPrice,price != 0 {
                                cell.desLabel.text = String(format: "AED %.2f", price)
                            }
                        }
                    } else {
                        if let price = arrCartProduct[indexPath.row].salesPrice,price != 0 {
                            cell.desLabel.text = String(format: "AED %.2f", price)
                        }
                    }
                } else {
                    if let price = arrCartProduct[indexPath.row].salesPrice,price != nil {
                        cell.desLabel.text = String(format: "AED %.2f", price)
                    }
                }
            
                
                cell.lblQty.text = "\(arrCartProduct[indexPath.row].productQuantity!)"
                cell.btnAddProduct.tag = indexPath.row
                cell.btnRemoveProduct.tag = indexPath.row
                
                cell.btnAddProduct.addTarget(self, action: #selector(btnAddProductTap(sender:)), for: .touchUpInside)
                
                cell.btnRemoveProduct.addTarget(self, action: #selector(btnRemoveProductTap(sender:)), for: .touchUpInside)
                return cell
            } else if indexPath.section == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartSummaryFreeProductsTVC") as! CartSummaryFreeProductsTVC
                cell.baseView.addShadowDash()
                cell.baseView.layer.cornerRadius = 8.0
                cell.imageV.layer.cornerRadius = 8.0
                
                
                if let imgUrl = arrAppliedCoupanData.freeProducts[indexPath.row].productImage, !imgUrl.isEmpty {
                    
                    let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

                    
                    cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                        if (error != nil) {
                            cell.imageV.image = UIImage(named: "placeholder")
                        } else {
                            cell.imageV.image = image
                        }
                    }
                }
                else {
                    cell.imageV.image = UIImage(named: "placeholder")
                }
                
                
                cell.titleLabel.text = arrAppliedCoupanData.freeProducts[indexPath.row].productDescription ?? ""
                cell.qtyLabel.text = "Free"
                return cell
                
            } else if indexPath.section == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountCartSummaryTableViewCell") as! DiscountCartSummaryTableViewCell
             
                cell.cellViewRound.addShadowDash()
                cell.cellViewRound.layer.cornerRadius = 8.0
                
                
                
                cell.delegate = self
                cell.txtEnterDiscountCoupen.autocapitalizationType = .allCharacters
                if couponCodeDiscount != 0 || isBuyOneGetOneCoupon || isFreeDeliveryCoupon{
                    
                    if isBuyOneGetOneCoupon {
                        cell.txtEnterDiscountCoupen.text = String("\("Buy One Get One  ")\(EndPoint.couponCode)")
                    } else if isFreeDeliveryCoupon {
                        cell.txtEnterDiscountCoupen.text = String("\("Free Delivery  ")\(EndPoint.couponCode)")
                    } else {
                     
                        cell.txtEnterDiscountCoupen.text = String(format: "Coupon discount added AED %@", percentCouponCodeDiscount.clean)

                        if arrAppliedCoupanData.discountType == "Percent" {
                            cell.txtEnterDiscountCoupen.text = String(format: "Coupon discount added %@%%", percentCouponCodeDiscount.clean)

                        }

                    }
                    
                    cell.txtEnterDiscountCoupen.textColor = #colorLiteral(red: 0.6274509804, green: 0.01960784314, blue: 0.1529411765, alpha: 1)
                    cell.txtEnterDiscountCoupen.isUserInteractionEnabled = false
                    cell.btnApply.isHidden = true
                    cell.btnClearCode.isHidden = false
                }
                else {
                    cell.txtEnterDiscountCoupen.text = ""
                    cell.txtEnterDiscountCoupen.placeholder = "Enter discount code?"
                    cell.txtEnterDiscountCoupen.isUserInteractionEnabled = true
                    cell.btnApply.isHidden = false
                    cell.btnClearCode.isHidden = true
                    cell.txtEnterDiscountCoupen.textColor = .black

                }
                cell.txtEnterDiscountCoupen.tag = indexPath.row
                cell.btnApply.tag = indexPath.row
                cell.btnApply.addTarget(self, action: #selector(btnApplyTap(sender:)), for: .touchUpInside)
                cell.btnClearCode.tag = indexPath.row
                cell.btnClearCode.addTarget(self, action: #selector(btnClearCodeTap(sender:)), for: .touchUpInside)
                
                return cell
            } else if indexPath.section == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelfPickupCartSummaryTVC") as! SelfPickupCartSummaryTVC
                
                cell.selectionStyle = .none
                cell.isChecked = false
              
                
                cell.btnCheck.addTarget(self, action: #selector(btnSelfServiceTap(sender:)), for: .touchUpInside)
                cell.btnByChefCheck.addTarget(self, action: #selector(btnByChefTap(sender:)), for: .touchUpInside)

                
                if  isByPickUp {
                    cell.btnCheck.setImage(UIImage(named: "checkBox"), for: .normal)
                    cell.btnByChefCheck.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    self.isSelfPickUp = true

                }
                else{
                    cell.btnCheck.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.btnByChefCheck.setImage(UIImage(named: "checkBox"), for: .normal)
                    self.isSelfPickUp = false

                }
                
                
                cell.vwBack.addShadowDash()
                cell.vwBack.layer.cornerRadius = 8.0
                
                return cell
            } else if indexPath.section == 4{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartSummaryTotalTableViewCell") as! CartSummaryTotalTableViewCell
                                
                cell.vwMainBg.addShadowDash()
                cell.vwMainBg.layer.cornerRadius = 8
                if couponCodeDiscount == 0 {
                    cell.lblDiscount.isHidden = true
                    cell.lblDiscountTitle.isHidden = true
                    cell.lblHeightDiscountTitle.constant = 0
                    cell.lblTopDeliveryTitle.constant = 0
                    cell.lblDiscount.text = ""
                    cell.lblDiscountTitle.text = ""
                }
                else{
                    cell.lblDiscount.isHidden = false
                    cell.lblDiscountTitle.isHidden = false
                    cell.lblHeightDiscountTitle.constant = 17
                    cell.lblTopDeliveryTitle.constant = 8
                    cell.lblDiscount.text = String(format: "-AED %.2f", showDiscountValue)
                    cell.lblDiscountTitle.text = "Discount"

                }
                
                if totalSum != -1{
                    
                    if let vat = dictAppInfo.vatPercent, vat != 0 {
                        cell.lblVatTitle.text = "VAT (\(vat)%)"
                    }
                    else{
                        cell.lblVatTitle.text = "VAT (5%)"
                    }
                    
                    cell.lblVat.text =  String(format: "AED %.2f", self.vatTotal)
                    
                    self.totalSum = 0.0
                    for eachProduct in self.arrCartProduct{
                        //New Changes
                        if let startDate = eachProduct.fromDate,startDate != nil {
                            
                            let fromDate : Date = startDate.dateFromString(startDate)
                            let endDate = eachProduct.toDate
                            let toDate : Date = endDate!.dateFromString(endDate!)
                            
                            if Date().isBetween(fromDate, and: toDate) {
                                if let agreementValue = eachProduct.agreementValue, agreementValue != nil {
                                    let agreementValue = eachProduct.agreementValue
                                    
                                    self.totalSum = self.totalSum + Float(eachProduct.agreementValue!) * Float(eachProduct.productQuantity!)
                                    
                                } else {
                                    if let price = eachProduct.salesPrice,price != 0 {
                                        self.totalSum = self.totalSum + Float(eachProduct.salesPrice!) * Float(eachProduct.productQuantity!)
                                    }
                                }
                            } else {
                                if let price = eachProduct.salesPrice,price != 0 {
                                    self.totalSum = self.totalSum + Float(eachProduct.salesPrice!) * Float(eachProduct.productQuantity!)
                                }
                            }
                        } else {
                            if let price = eachProduct.salesPrice,price != nil {
                                self.totalSum = self.totalSum + Float(eachProduct.salesPrice!) * Float(eachProduct.productQuantity!)
                            }
                        }
                    }
                    
                    var subTotalNew: Float = 0
                    subTotalNew = self.totalSum
                    subTosubTotalNewF = subTotalNew
      
                        self.vatTotal = 0
                        var discountVat  : Float = 0.0
                        if isByPickUp {
                        cell.lblDeliveryCharges.text = "Free"
                        cell.lblDeInclVat.text = ""

                        if let vat = dictAppInfo.vatPercent, vat != 0 {
                            self.subTotal = totalSum / (1 + Float(vat) / 100)
                            self.vatTotal = self.totalSum - self.subTotal
                            discountVat = Float(Float(self.finalCouponCodeDiscount)  / (1 + (Float(vat)) / 100))
                            self.vatTotal = self.vatTotal - (self.finalCouponCodeDiscount - discountVat)
                        }
                        else{
                            self.subTotal = totalSum / (1 + Float(5) / 100)
                            self.vatTotal = self.totalSum - self.subTotal
                        }
                        cell.lblVat.text =  String(format: "AED %.2f", self.vatTotal)
                        self.totalSum = self.subTotal + self.vatTotal - discountVat
                    }
                    else {
                        var flag : Bool = true
                        if couponCodeDiscount == 0 {
                            if arrAppliedCoupanData.discountType == "Free Delivery" {
                                flag = false
                                cell.lblDeliveryCharges.text = "Free"
                                cell.lblDeInclVat.text = ""

                                if let vat = dictAppInfo.vatPercent, vat != 0 {
                                    self.subTotal = totalSum / (1 + Float(vat) / 100)
                                    self.vatTotal = self.totalSum - self.subTotal
                                    discountVat = Float(Float(self.finalCouponCodeDiscount)  / (1 + (Float(vat)) / 100))
                                    
                                    self.vatTotal = self.vatTotal - (self.finalCouponCodeDiscount - discountVat)
                                }
                                else{
                                    self.subTotal = totalSum / (1 + Float(5) / 100)
                                    self.vatTotal = self.totalSum - self.subTotal
                                }
                                cell.lblVat.text =  String(format: "AED %.2f", self.vatTotal)
                                self.totalSum = self.subTotal + self.vatTotal - discountVat
                               
                            }
                        }
                        if flag {
                            print("finalCouponCodeDiscount")
                            print(finalCouponCodeDiscount)
                            
                            self.vatTotal = 0
                            
                           
                            
                            if let vat = dictAppInfo.vatPercent, vat != 0 {
                                //self.vatTotal = (Float(vat) * totalSum)/100
                                self.subTotal = totalSum / (1 + Float(vat) / 100)
                                self.vatTotal = self.vatTotal + (self.totalSum - self.subTotal)
                            }
                            else{
                                self.subTotal = totalSum / (1 + Float(5) / 100)
                                self.vatTotal = self.vatTotal + (self.totalSum - self.subTotal)
                            }
                            
                            
                            if let deliveryLimt = dictAppInfo.deliveryLimit, deliveryLimt != 0 {
                                if Int(subTotal) > deliveryLimt {
                                    cell.lblDeliveryCharges.text = "Free"
                                    cell.lblDeInclVat.text = ""

                                  }
                                  else{
                                      let deliveryCharges = dictAppInfo.deliveryCharges
                                      var deliveryChargesValue : Float = 0.0
                                      
                                      if let vat = dictAppInfo.vatPercent, vat != 0 {
           
                                          deliveryChargesValue = Float(Float(deliveryCharges!)  / (1 + (Float(vat)) / 100))
         
                                          let  deliveryChargesAmt = Float(Float(deliveryCharges!)  - deliveryChargesValue)
                                          
                                        discountVat = Float(Float(self.finalCouponCodeDiscount)  / (1 + (Float(vat)) / 100))
                                          
                                        self.vatTotal = self.vatTotal + Float(deliveryChargesAmt) - (finalCouponCodeDiscount - discountVat)
                                      } else {
                                          self.vatTotal = self.vatTotal + ( totalSum / (1 + Float(5) / 100) )
                                      }
                                      
                                      // New
                                     // deliveryChargesValue
                                      let vD = Float(dictAppInfo.deliveryCharges ?? Int(0))

                                      cell.lblDeliveryCharges.text = String(format: "AED %.2f", vD ?? 0.00)
                                      cell.lblDeInclVat.text = "(Incl VAT)"

                                      
                                    //  cell.lblDeliveryCharges.text = String(format: "AED %.2f", deliveryChargesValue)
                                      self.totalSum = self.subTotal + self.vatTotal + Float(deliveryChargesValue) - discountVat
                                  }
                            }
                        }
                    }
                    cell.lblSubTotal.text = String(format: "AED %.2f", subTotalNew)
                    cell.lblTotal.text = String(format: "AED %.2f", self.totalSum)
                    nextAllTotals = self.totalSum
                    cell.lblVat.text =  String(format: "AED %.2f", self.vatTotal)
                }
                
                return cell
            } else  {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SaveCancelCartSummaryTableViewCell") as! SaveCancelCartSummaryTableViewCell
                
                return cell
            }
            
        } else {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartSummaryTableViewCell") as! CartSummaryTableViewCell
                cell.cellViewRound.addShadowDash()
                cell.cellViewRound.layer.cornerRadius = 8
                cell.imageV.layer.cornerRadius = 8

                cell.vwOfferApplied.isHidden = true
                if let imgUrl = arrCartProduct[indexPath.row].productImage, !imgUrl.isEmpty {
                    
                    let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

                    
                    cell.imageV?.sd_setImage(with: URL.init(string:(urlString))) { (image, error, cache, urls) in
                        if (error != nil) {
                            cell.imageV.image = UIImage(named: "placeholder")
                        } else {
                            cell.imageV.image = image
                        }
                    }
                }
                else {
                    cell.imageV.image = UIImage(named: "placeholder")
                }
                
                
                if couponCodeDiscount != 0 || isFreeDeliveryCoupon || isBuyOneGetOneCoupon{
                    let tempProductID  =   arrCartProduct[indexPath.row].productId
                    if let appliedProductIDs = arrAppliedCoupanData.products, appliedProductIDs != nil {
                        let tempProductsArray : NSArray = appliedProductIDs.components(separatedBy: ",") as NSArray
                        if tempProductsArray.contains(String("\(tempProductID!)") ) {
                                cell.vwOfferApplied.isHidden = true
                        } else {
                                cell.vwOfferApplied.isHidden = true
                        }
                    }
                }
       
                cell.vwSelectQty.addShadowDash()
                cell.vwSelectQty.layer.cornerRadius = 8
                cell.titleLabel.text = arrCartProduct[indexPath.row].productDescription ?? ""
                
                if let price = arrCartProduct[indexPath.row].salesPrice,price != 0 {
                    cell.desLabel.text = String(format: "AED %.2f", price)
                }
                else {
                    cell.desLabel.text = "AED 0.0"
                }
                
                
                if let startDate = arrCartProduct[indexPath.row].fromDate,startDate != nil {
                    
                    let fromDate : Date = startDate.dateFromString(startDate)
                    let endDate = arrCartProduct[indexPath.row].toDate
                    let toDate : Date = endDate!.dateFromString(endDate!)
                    
                    if Date().isBetween(fromDate, and: toDate) {
                        if let agreementValue = arrCartProduct[indexPath.row].agreementValue, agreementValue != nil {
                            cell.desLabel.isHidden = false
                            let agreementValue = arrCartProduct[indexPath.row].agreementValue
                            cell.desLabel.text = String(format: "AED %.2f", agreementValue!)
                        } else {
                            if let price = arrCartProduct[indexPath.row].salesPrice,price != 0 {
                                cell.desLabel.text = String(format: "AED %.2f", price)
                            }
                        }
                    } else {
                        if let price = arrCartProduct[indexPath.row].salesPrice,price != 0 {
                            cell.desLabel.text = String(format: "AED %.2f", price)
                        }
                    }
                } else {
                    if let price = arrCartProduct[indexPath.row].salesPrice,price != nil {
                        cell.desLabel.text = String(format: "AED %.2f", price)
                    }
                }
                
                cell.lblQty.text = "\(arrCartProduct[indexPath.row].productQuantity!)"
                cell.btnAddProduct.tag = indexPath.row
                cell.btnRemoveProduct.tag = indexPath.row
                
                cell.btnAddProduct.addTarget(self, action: #selector(btnAddProductTap(sender:)), for: .touchUpInside)
                
                cell.btnRemoveProduct.addTarget(self, action: #selector(btnRemoveProductTap(sender:)), for: .touchUpInside)
                return cell
            }  else if indexPath.section == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountCartSummaryTableViewCell") as! DiscountCartSummaryTableViewCell
                                
                cell.cellViewRound.addShadowDash()
                cell.cellViewRound.layer.cornerRadius = 8.0
                
                cell.delegate = self
                cell.txtEnterDiscountCoupen.autocapitalizationType = .allCharacters

                if couponCodeDiscount != 0 || isFreeDeliveryCoupon{
                    
                    cell.txtEnterDiscountCoupen.text = String(format: "Coupon discount added AED %@", percentCouponCodeDiscount.clean)

                    
                    if isFreeDeliveryCoupon {
                        cell.txtEnterDiscountCoupen.text = String("\("Free Delivery  ")\(EndPoint.couponCode)")
                    }
                    
                    if arrAppliedCoupanData.discountType == "Percent" {
                        cell.txtEnterDiscountCoupen.text = String(format: "Coupon discount added %@%%", percentCouponCodeDiscount.clean)

                    }
                    
                    cell.txtEnterDiscountCoupen.textColor = #colorLiteral(red: 0.6274509804, green: 0.01960784314, blue: 0.1529411765, alpha: 1)
                    cell.txtEnterDiscountCoupen.isUserInteractionEnabled = false
                    cell.btnApply.isHidden = true
                    cell.btnClearCode.isHidden = false
                }
                else {
                    cell.txtEnterDiscountCoupen.text = ""
                    cell.txtEnterDiscountCoupen.placeholder = "Enter discount code?"
                    cell.txtEnterDiscountCoupen.isUserInteractionEnabled = true
                    cell.btnApply.isHidden = false
                    cell.btnClearCode.isHidden = true
                    cell.txtEnterDiscountCoupen.textColor = .black

                }
                cell.txtEnterDiscountCoupen.tag = indexPath.row
                cell.btnApply.tag = indexPath.row
                cell.btnApply.addTarget(self, action: #selector(btnApplyTap(sender:)), for: .touchUpInside)
                cell.btnClearCode.tag = indexPath.row
                cell.btnClearCode.addTarget(self, action: #selector(btnClearCodeTap(sender:)), for: .touchUpInside)
                
                return cell
            } else if indexPath.section == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelfPickupCartSummaryTVC") as! SelfPickupCartSummaryTVC
                
                cell.selectionStyle = .none
                cell.isChecked = false
                self.isSelfPickUp = false
                cell.btnCheck.addTarget(self, action: #selector(btnSelfServiceTap(sender:)), for: .touchUpInside)
                cell.btnByChefCheck.addTarget(self, action: #selector(btnByChefTap(sender:)), for: .touchUpInside)

                if  isByPickUp {
                    cell.btnCheck.setImage(UIImage(named: "checkBox"), for: .normal)
                    cell.btnByChefCheck.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    self.isSelfPickUp = true

                }
                else{
                    cell.btnCheck.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
                    cell.btnByChefCheck.setImage(UIImage(named: "checkBox"), for: .normal)
                    self.isSelfPickUp = false

                }
                
                
                cell.vwBack.addShadowDash()
                cell.vwBack.layer.cornerRadius = 8.0
                
                return cell
            } else if indexPath.section == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartSummaryTotalTableViewCell") as! CartSummaryTotalTableViewCell
                            
                cell.vwMainBg.addShadowDash()
                cell.vwMainBg.layer.cornerRadius = 8
                if couponCodeDiscount == 0 {
                    cell.lblDiscount.isHidden = true
                    cell.lblDiscountTitle.isHidden = true
                    cell.lblHeightDiscountTitle.constant = 0
                    cell.lblTopDeliveryTitle.constant = 0
                    cell.lblDiscount.text = ""
                    cell.lblDiscountTitle.text = ""
                }
                else{
                    cell.lblDiscount.isHidden = false
                    cell.lblDiscountTitle.isHidden = false
                    cell.lblHeightDiscountTitle.constant = 17
                    cell.lblTopDeliveryTitle.constant = 8
                    cell.lblDiscount.text = String(format: "-AED %.2f", showDiscountValue)
                    cell.lblDiscountTitle.text = "Discount"

                }
                
                if totalSum != -1{
                    print("totalSum--- ",totalSum)
                    if let vat = dictAppInfo.vatPercent, vat != 0 {
                        cell.lblVatTitle.text = "VAT (\(vat)%)"
                    }
                    else{
                        cell.lblVatTitle.text = "VAT (5%)"
                    }
                    

                    cell.lblVat.text =  String(format: "AED %.2f", self.vatTotal)
                    
                    print("VAT--- ",self.vatTotal)

                    
                    self.totalSum = 0.0
                    for eachProduct in self.arrCartProduct{
                        //New Changes
                        if let startDate = eachProduct.fromDate,startDate != nil {
                            
                            let fromDate : Date = startDate.dateFromString(startDate)
                            let endDate = eachProduct.toDate
                            let toDate : Date = endDate!.dateFromString(endDate!)
                            
                            if Date().isBetween(fromDate, and: toDate) {
                                if let agreementValue = eachProduct.agreementValue, agreementValue != nil {
                                    let agreementValue = eachProduct.agreementValue
                                    
                                    self.totalSum = self.totalSum + Float(eachProduct.agreementValue!) * Float(eachProduct.productQuantity!)
                                    
                                } else {
                                    if let price = eachProduct.salesPrice,price != 0 {
                                        self.totalSum = self.totalSum + Float(eachProduct.salesPrice!) * Float(eachProduct.productQuantity!)
                                    }
                                }
                            } else {
                                if let price = eachProduct.salesPrice,price != 0 {
                                    self.totalSum = self.totalSum + Float(eachProduct.salesPrice!) * Float(eachProduct.productQuantity!)
                                }
                            }
                        } else {
                            if let price = eachProduct.salesPrice,price != nil {
                                self.totalSum = self.totalSum + Float(eachProduct.salesPrice!) * Float(eachProduct.productQuantity!)
                            }
                        }
                    }
                    
                    print("totalSum--- ",self.totalSum)
                    var subTotalNew: Float = 0
                    subTotalNew = self.totalSum
                    subTosubTotalNewF = subTotalNew

                    

                if arrAppliedCoupanData.discountType != "Percent" {
                    self.vatTotal = 0
                }

            
                if isByPickUp {
                        var discountVat  : Float = 0.0
                        cell.lblDeliveryCharges.text = "Free"
                        cell.lblDeInclVat.text = ""
                        var FreeVat  : Float = 0.0

                        if let vat = dictAppInfo.vatPercent, vat != 0 {
                            
                            self.subTotal = totalSum / (1 + Float(vat) / 100)
                            
                            self.vatTotal = self.totalSum - self.subTotal

                            if arrAppliedCoupanData.discountType != "Percent" {

                            discountVat = Float(Float(self.finalCouponCodeDiscount)  / (1 + (Float(vat)) / 100))
                            
                          //  self.vatTotal = self.vatTotal - (self.finalCouponCodeDiscount - discountVat)
                                
                                
                                FreeVat = self.vatTotal - (self.finalCouponCodeDiscount - discountVat)

                                
                                self.vatTotal = self.vatTotal + (self.finalCouponCodeDiscount - discountVat)
                            }
                           
                        }
                        else{
                            self.subTotal = totalSum / (1 + Float(5) / 100)
                            if arrAppliedCoupanData.discountType != "Percent" {
                               self.vatTotal = self.totalSum - self.subTotal
                               FreeVat = self.vatTotal

                            }
                        }
                    
                        cell.lblVat.text =  String(format: "AED %.2f", self.vatTotal)
                    
                    
                        if arrAppliedCoupanData.discountType != "Percent" {

                          self.totalSum = self.subTotal + FreeVat - discountVat
                            
                        }
                    else {
                        
                        self.totalSum = self.subTotal + self.vatTotal - couponCodeDiscount - percentDiscountAddInVat
                        self.finalCouponCodeDiscount = couponCodeDiscount
                      }
                            
                    }
                    else {
                        var flag : Bool = true
                        var discountVat  : Float = 0.0
                        if couponCodeDiscount == 0 {
                            if arrAppliedCoupanData.discountType == "Free Delivery" {
                                flag = false
                                cell.lblDeliveryCharges.text = "Free"
                                cell.lblDeInclVat.text = ""

                                if let vat = dictAppInfo.vatPercent, vat != 0 {
                                    self.subTotal = totalSum / (1 + Float(vat) / 100)
                                    
                                    if arrAppliedCoupanData.discountType != "Percent" {

                                    self.vatTotal = self.totalSum - self.subTotal
                                    discountVat = Float(Float(self.finalCouponCodeDiscount)  / (1 + (Float(vat)) / 100))
                                    
                                    self.vatTotal = self.vatTotal - (self.finalCouponCodeDiscount - discountVat)
                                        
                                    }
                                }
                                else{
                                    self.subTotal = totalSum / (1 + Float(5) / 100)
                                    if arrAppliedCoupanData.discountType != "Percent" {

                                      self.vatTotal = self.totalSum - self.subTotal
                                        
                                    }
                                }
                                cell.lblVat.text =  String(format: "AED %.2f", self.vatTotal)
                                
                                if arrAppliedCoupanData.discountType != "Percent" {

                                self.totalSum = self.subTotal + self.vatTotal - discountVat
                                    
                                }
                                else{
                                    self.totalSum = self.subTotal + self.vatTotal - couponCodeDiscount
                                    
                                    self.finalCouponCodeDiscount = couponCodeDiscount
                                }
                                
                                print("discountVat--- ",discountVat)
                            }
                            
                            print("totalSum--- ",self.totalSum)

                            print("vatTotal--- ",self.vatTotal)


                        }
                        if flag {
                            if arrAppliedCoupanData.discountType != "Percent" {

                                self.vatTotal = 0.0
                            }
                            if let vat = dictAppInfo.vatPercent, vat != 0 {
                                self.subTotal = totalSum / (1 + Float(vat) / 100)
                                if arrAppliedCoupanData.discountType != "Percent" {

                                self.vatTotal = self.vatTotal + (self.totalSum - self.subTotal)
                                }
                                else{
                                    self.vatTotal = self.totalSum - self.subTotal
                                    
                                }
                            }
                            else{
                                self.subTotal = totalSum / (1 + Float(5) / 100)
                                if arrAppliedCoupanData.discountType != "Percent" {

                                self.vatTotal = self.vatTotal + (self.totalSum - self.subTotal)
                                }
                            }
                            
                            print("vatTotal--- ",self.vatTotal)
                            print("subTotal--- ",self.subTotal)


                            if let deliveryLimt = dictAppInfo.deliveryLimit, deliveryLimt != 0 {
                                if Int(self.totalSum) > deliveryLimt {
                                    cell.lblDeliveryCharges.text = "Free"
                                    cell.lblDeInclVat.text = ""
                                    var FreeVat  : Float = 0.0

                                    if let vat = dictAppInfo.vatPercent, vat != 0 {
                                        self.subTotal = totalSum / (1 + Float(vat) / 100)
                                        if arrAppliedCoupanData.discountType != "Percent" {

                                        self.vatTotal = self.totalSum - self.subTotal
                                        discountVat = Float(Float(self.finalCouponCodeDiscount)  / (1 + (Float(vat)) / 100))
                                            
                                            FreeVat = self.vatTotal - (self.finalCouponCodeDiscount - discountVat)

                                            
                                            self.vatTotal = self.vatTotal + (self.finalCouponCodeDiscount - discountVat)

                                     //   Anil +
                                        }
                                    }
                                    else{
                                        self.subTotal = totalSum / (1 + Float(5) / 100)
                                        if arrAppliedCoupanData.discountType != "Percent" {

                                            self.vatTotal = self.totalSum - self.subTotal
                                            FreeVat = self.vatTotal
                                        }
                                    }
                                    cell.lblVat.text =  String(format: "AED %.2f", self.vatTotal)
                                    
                                    
                                    if arrAppliedCoupanData.discountType != "Percent" {

                                    self.totalSum = self.subTotal + FreeVat - discountVat
                                        
                                    }
                                    else{
                                        self.totalSum = self.subTotal + self.vatTotal - couponCodeDiscount - percentDiscountAddInVat
                                        
                                        self.finalCouponCodeDiscount = couponCodeDiscount

                                    }
                                    
                                    print("discountVat--- ",discountVat)
                                  }
                                  else{
                                      let deliveryCharges = dictAppInfo.deliveryCharges
                                      var deliveryChargesValue : Float = 0.0
                                      var vatDiscount : Float = 0.0

                                      
                                      if let vat = dictAppInfo.vatPercent, vat != 0 {
           
                                        deliveryChargesValue = Float(Float(deliveryCharges!)  / (1 + (Float(vat)) / 100))
         
                                        let deliveryChargesAmt = Float(Float(deliveryCharges!)  - deliveryChargesValue)
                                          
                                    if arrAppliedCoupanData.discountType != "Percent" {
                                        discountVat = Float(Float(self.finalCouponCodeDiscount)  / (1 + (Float(vat)) / 100))
                                        

                                        
                                        vatDiscount =  self.vatTotal + Float(deliveryChargesAmt) - (finalCouponCodeDiscount - discountVat)

                                        // Anil
                                        self.vatTotal = self.vatTotal + Float(deliveryChargesAmt) + (finalCouponCodeDiscount - discountVat)
                                        
                                        
                                              
                                    }
                                          else {
                                              self.vatTotal =  self.vatTotal + Float(deliveryChargesAmt)

                                          }
                                      } else {
                                          if arrAppliedCoupanData.discountType != "Percent" {

                                          self.vatTotal = self.vatTotal + ( totalSum / (1 + Float(5) / 100) )
                                              
                                          }
                                      }
                                      // New
                                     // deliveryChargesValue
                                      let vD = Float(dictAppInfo.deliveryCharges ?? Int(0))

                                      cell.lblDeliveryCharges.text = String(format: "AED %.2f", vD)
                                      cell.lblDeInclVat.text = "(Incl VAT)"

                                      
                                      if arrAppliedCoupanData.discountType != "Percent" {
                                          
                                          // Anil
                                          

                                          
                                          


                                          
                                          
                                          
                                        self.totalSum = self.subTotal + vatDiscount +  Float(deliveryChargesValue) - discountVat
                                          
                                    
                                          
                                          
                                          
                                      }
                                      else{
                                          self.totalSum = self.subTotal + self.vatTotal + Float(deliveryChargesValue) - couponCodeDiscount - percentDiscountAddInVat
                                          
                                          self.finalCouponCodeDiscount = couponCodeDiscount
                                      }
                                      
                                  }
                            }
                        }
                    }
                    
                    self.vatTotal = self.vatTotal + percentDiscountAddInVat
                    
                    print("subTotal--- ",self.subTotal)
                    print("totalSum--- ",self.totalSum)
                    print("vatTotal--- ",self.vatTotal)

                    cell.lblSubTotal.text = String(format: "AED %.2f", subTotalNew)
                    cell.lblTotal.text = String(format: "AED %.2f", self.totalSum)
                    nextAllTotals = self.totalSum
                    cell.lblVat.text =  String(format: "AED %.2f", self.vatTotal)
                }
                
                return cell
            } else  {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SaveCancelCartSummaryTableViewCell") as! SaveCancelCartSummaryTableViewCell
                
                return cell
            }
        }
  
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.isBuyOneGetOneCoupon {
            if indexPath.section == 1 {
                return 120
            }
            if indexPath.section == 3 {
                return 120
            }
        } else {
            if indexPath.section == 2 {
                return 105
            }
        }
        return UITableView.automaticDimension
    }
    
    //MARK:- Add Product Tap
    @objc func btnAddProductTap(sender: UIButton) {
        
        if !arrCartProduct.isEmpty {
            addQty = arrCartProduct[sender.tag].productQuantity ?? 0
            if addQty < 15 {

            }else{
                NotificationAlert().NotificationAlert(titles: "You have reached maximum order quantity")
                return
            }
            
            addQty = addQty + 1
            let param = AddToCartModel()
            if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                param.userId = userId
            }
            param.productId = arrCartProduct[sender.tag].productId
            param.productQuantity = addQty
            self.updateQuantity(Model: param)
        }
    }
    
    
    
    
    
    @objc func btnByChefTap(sender: UIButton) {
        
        if self.isBuyOneGetOneCoupon {
            let cell : SelfPickupCartSummaryTVC = tableView.cellForRow(at: IndexPath(row: 0, section: 3)) as! SelfPickupCartSummaryTVC

            self.isByPickUp = false
            self.isSelfPickUp = false

            cell.btnByChefCheck.setImage(UIImage(named: "checkBox"), for: .normal)
            cell.btnCheck.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
            
            tableView.beginUpdates()
            tableView.reloadSections(IndexSet(integer: 4), with: .none)
            tableView.endUpdates()
            
        }
        else{
            let cell : SelfPickupCartSummaryTVC = tableView.cellForRow(at: IndexPath(row: 0, section: 2)) as! SelfPickupCartSummaryTVC

            self.isByPickUp = false
            self.isSelfPickUp = false

            cell.btnByChefCheck.setImage(UIImage(named: "checkBox"), for: .normal)
            cell.btnCheck.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
            
            tableView.beginUpdates()
            tableView.reloadSections(IndexSet(integer: 3), with: .none)
            tableView.endUpdates()
            
            
            
        }
 
    }
    
    
    
    @objc func btnSelfServiceTap(sender: UIButton) {
        
        if self.isBuyOneGetOneCoupon {
            let cell : SelfPickupCartSummaryTVC = tableView.cellForRow(at: IndexPath(row: 0, section: 3)) as! SelfPickupCartSummaryTVC
            self.isByPickUp = true
            self.isSelfPickUp = true

            cell.btnCheck.setImage(UIImage(named: "checkBox"), for: .normal)
            cell.btnByChefCheck.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
            
            tableView.beginUpdates()
            tableView.reloadSections(IndexSet(integer: 4), with: .none)
            tableView.endUpdates()
            


            self.tableView.reloadData()

        }
        else{
            let cell : SelfPickupCartSummaryTVC = tableView.cellForRow(at: IndexPath(row: 0, section: 2)) as! SelfPickupCartSummaryTVC
            self.isByPickUp = true
            self.isSelfPickUp = true

            cell.btnCheck.setImage(UIImage(named: "checkBox"), for: .normal)
            cell.btnByChefCheck.setImage(UIImage(named: "squareEmptyBlack"), for: .normal)
            
            tableView.beginUpdates()
            tableView.reloadSections(IndexSet(integer: 3), with: .none)
            tableView.endUpdates()
            

         

            
            self.tableView.reloadData()

        }
     
        
    }
    
    //MARK:- Remove Product Tap
    @objc func btnRemoveProductTap(sender: UIButton) {
        
        if !arrCartProduct.isEmpty {
            addQty = arrCartProduct[sender.tag].productQuantity ?? 0
            addQty = addQty - 1

            let param = AddToCartModel()
            if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                param.userId = userId
            }
            param.productId = arrCartProduct[sender.tag].productId
            param.productQuantity = addQty
            self.updateQuantity(Model: param)
        }
    }
    
    //MARK:- Apply coupon tap
    @objc func btnApplyTap(sender: UIButton) {
       // self.couponDiscountAPI()
        self.isVerifyCoupon = false
        self.applyCouponAPI()
    }
    
    @objc func btnClearCodeTap(sender: UIButton) {
        self.totalSum = self.totalSum + Float(self.finalCouponCodeDiscount)
        EndPoint.couponCode = ""
        finalCouponCodeDiscount = 0
        couponCodeDiscount = 0
        percentCouponCodeDiscount = 0
        self.arrAppliedCoupanData.discountType = ""
        self.isFreeDeliveryCoupon = false
        self.isBuyOneGetOneCoupon = false
        self.strCoupon = ""
        self.firstTimeCoupon = false
        self.percentDiscountAddInVat = 0.0
        self.showDiscountValue = 0.0

        tableView.reloadData()
    }
    
    func clearDataAndValueOnCart(){
        self.totalSum = 0
        EndPoint.couponCode = ""
        finalCouponCodeDiscount = 0
        couponCodeDiscount = 0
        percentCouponCodeDiscount = 0
        self.arrAppliedCoupanData.discountType = ""
        self.isFreeDeliveryCoupon = false
        self.isBuyOneGetOneCoupon = false
        self.strCoupon = ""
        self.firstTimeCoupon = false
        self.percentDiscountAddInVat = 0.0
        self.showDiscountValue = 0.0

        tableView.reloadData()
    }
    
    func enterCouponCode(couponCode: String) {
        EndPoint.couponCode = couponCode
    }
}
extension Float {
    var clean: String {
       return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
