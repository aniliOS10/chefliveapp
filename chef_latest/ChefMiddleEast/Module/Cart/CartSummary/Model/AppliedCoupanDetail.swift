//
//  AppliedCoupanDetail.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/03/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper


class AppliedCoupanDetail: BaseResponse {
    
    var discountAmount : Float = 0.0
    var discountType: String?
    var products: String?
    var salesPrice: String?

    var freeProducts = [ProductModel]()
    
    
    override init() {
         super.init()
    }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        discountAmount <- map["discountAmount"]
        discountType <- map["discountType"]
        products <- map["products"]
        freeProducts <- map["freeProducts"]
        salesPrice <- map["salesPrice"]

    }
}
