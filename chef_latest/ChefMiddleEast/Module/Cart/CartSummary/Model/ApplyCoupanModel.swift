//
//  ApplyCoupanModel.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/03/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper


class ApplyCoupanModel: BaseResponse {
    
    var cartId: Int?
    var couponCode: String?
    var userId: String?

    
    
    override init() {
         super.init()
    }
    convenience required init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        cartId <- map["cartId"]
        couponCode <- map["couponCode"]
        userId <- map["userId"]
    }
}
