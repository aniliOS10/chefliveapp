//
//  CartSummaryTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import UIKit
import FloatRatingView
class CartSummaryTableViewCell: UITableViewCell {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var cellViewRound: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    
    @IBOutlet var btnRemoveProduct: UIButton!
    @IBOutlet var btnAddProduct: UIButton!
    @IBOutlet var lblQty: UILabel!
    @IBOutlet var vwSelectQty: UIView!
    @IBOutlet var vwOfferApplied: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
