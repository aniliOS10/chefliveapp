//
//  CartSummaryTotalTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import UIKit

class CartSummaryTotalTableViewCell: UITableViewCell {

    @IBOutlet var vwMainBg: UIView!

    @IBOutlet var lblNote: UILabel!
    
    @IBOutlet var lblSubTotal: UILabel!
    
    @IBOutlet var lblVat: UILabel!
    @IBOutlet var lblVatTitle: UILabel!
    @IBOutlet var lblDeliveryCharges: UILabel!
    
    @IBOutlet var lblTotal: UILabel!
    
    @IBOutlet var lblDiscount: UILabel!
    
    @IBOutlet var lblDiscountTitle: UILabel!
    
    @IBOutlet var lblDeInclVat: UILabel!

    

    @IBOutlet var lblHeightDiscountValue: NSLayoutConstraint!
    
    @IBOutlet var lblHeightDiscountTitle: NSLayoutConstraint!
    
    @IBOutlet var lblTopDeliveryTitle: NSLayoutConstraint!
    
    @IBOutlet var lblTopDeliveryValue: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
