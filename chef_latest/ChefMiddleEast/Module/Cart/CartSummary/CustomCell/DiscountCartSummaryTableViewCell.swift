//
//  DiscountCartSummaryTableViewCell.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import UIKit

protocol DiscountCartSummaryTableViewCellDelegate {
    
    func enterCouponCode(couponCode: String)
}

class DiscountCartSummaryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellViewRound: UIView!
    
    @IBOutlet var btnApply: UIButton!
    
    @IBOutlet var btnClearCode: UIButton!
    
    @IBOutlet var txtEnterDiscountCoupen: UITextField!
    
    @IBOutlet var btnDeliveryCharges: UIButton!
    
    var delegate : DiscountCartSummaryTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func txtEnterCouponCode(_ sender: UITextField) {
        
        txtEnterDiscountCoupen.resignFirstResponder()
        
        if  let code = txtEnterDiscountCoupen.text?.trimmingCharacters(in: .whitespaces), code.count > 0 {
            
            delegate.enterCouponCode(couponCode: code)
        }
        
    }
    
    @IBAction func btnApplyCoupon(_ sender: UIButton) {
        
        
    }
    
    @IBAction func btnClearCoupon(_ sender: UIButton) {
        
    
        
    }
    
}
