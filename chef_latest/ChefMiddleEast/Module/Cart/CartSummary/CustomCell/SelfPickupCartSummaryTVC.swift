//
//  SelfPickupCartSummaryTVC.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class SelfPickupCartSummaryTVC: UITableViewCell {

    @IBOutlet weak var vwBack : UIView!
    @IBOutlet weak var btnCheck : UIButton!
    @IBOutlet weak var btnByChefCheck : UIButton!

    var isChecked : Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
