//
//  CartSummaryViewController.swift
//  ChefMiddleEast
//
//  Created by Apple on 02/11/21.
//

import UIKit

@available(iOS 13.0, *)
class CartSummaryViewController:BaseViewController {
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var search : UISearchBar!
    @IBOutlet weak var buttonPayment: UIButton!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var lblYourCartIsEmpty: UILabel!
    @IBOutlet weak var lblEmptyDescription: UILabel!
    @IBOutlet weak var vwShopNowbutton: UIView!
    @IBOutlet weak var shopNowbutton: UIButton!
    @IBOutlet weak var imgCartEmpty: UIImageView!
    
    @IBOutlet var tableViewBottom: NSLayoutConstraint!


    var screenCommingFrom = String()
    var shouldDisplayHome:Bool = true
    var arrCartProduct = [ProductModel]()
    var arrAppliedCoupanData = AppliedCoupanDetail()
    var dictCartData = AddToCartModel()
    var addQty = Int()
    
    var couponCodeDiscount = Float()
    var finalCouponCodeDiscount = Float()
    var percentCouponCodeDiscount = Float()
    var showDiscountValue = Float()

    var percentDiscountAddInVat = Float()
    var totalSum: Float = 0
    var discountAmount: Float = 0
    var subTotal: Float = 0
    var vatTotal: Float = 0
    var subTosubTotalNewF: Float = 0.0
    var nextAllTotals: Float = 0.0

    var isSelfPickUp : Bool = false
    var isVerifyCoupon : Bool = false
    var isFreeDeliveryCoupon : Bool = false
    var isBuyOneGetOneCoupon : Bool = false
    var isByPickUp : Bool = false
    var firstTimeCoupon : Bool = false

    var strCoupon = ""
    var freeDelivery = ""

    //MARK:- VC Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigation()
        
        tableView.showsVerticalScrollIndicator = false
        self.navigationController?.navigationBar.isHidden = false
        
        if screenCommingFrom == "ProductDetails" {
            self.tabBarController?.tabBar.isHidden = false
            tableViewBottom.constant = 1
            self.view.layoutSubviews()
        }
        else{
            self.tabBarController?.tabBar.isHidden = false
        }
        
        
        if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
            let param = AddToCartModel()
            param.userId = userId
            if dictAppInfo.deliveryLimit == 0 || dictAppInfo.deliveryLimit == nil{
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.getAppInfoAPI()
            }
            self.getCartListAPI(Model: param)
            self.cartCountAPI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        tableView.contentInsetAdjustmentBehavior = .never
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0.0
        }
        self.showLoader()
    }
    
    //MARK:-  Set Up Navigation
    func setUpNavigation(){
        
        initBackButton()
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
       // backButton.addTarget(self, action:#selector(backButton1Pressed), for: .touchUpInside)
    
        self.initMenuButton()
        menuButton.addTarget(self, action:#selector(menuButtonPressed), for: .touchUpInside)
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
            if self.shouldDisplayHome{
                if screenCommingFrom == "ProductDetails" {
                    self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: "My Cart")
                }
                else {
                    self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [], title: AppMessage_CartSummary.Placeholder.CartSummary)
                }
            }
            else{
                self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: AppMessage_CartSummary.Placeholder.CartSummary)
            }
        }else {
            initBellButton()
            self.initBellButton()
            bellButton.addTarget(self, action:#selector(bellButtonPressed), for: .touchUpInside)
            if self.shouldDisplayHome{
                if screenCommingFrom == "ProductDetails" {
                    self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [self.bellBarButton], title: "My Cart")
                }
                else {
                    self.setNavigationControllerDisplay(rightItems: [self.menuBarButton], leftItems: [bellBarButton], title: AppMessage_CartSummary.Placeholder.CartSummary)
                }
            }
            else{
                self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: AppMessage_CartSummary.Placeholder.CartSummary)
            }
        }
    }
    
    
    //MARK:- Menu button
    @objc func menuButtonPressed(){
        self.sideMenuController?.revealMenu()
    }
    
    //MARK:- Back button
    @objc func backButton1Pressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Notification button
    @objc func bellButtonPressed(){
        let controller:NotificaitonListViewController =  UIStoryboard(storyboard: .Setting).initVC()
        self.navigationController?.pushViewController(controller, animated: true)    }
    
    //MARK:-  Button Action
    @IBAction func buttonSaveAndMakePaymentPressed(_ sender: Any) {
        let controller:DeliverySummaryViewController =  UIStoryboard(storyboard: .Cartpayment).initVC()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func segmentControllerValueChanged(_ sender: Any) {
        
        
    }
    
    @IBAction func buttonSaveAddressPressed(_ sender: Any) {
        
       

        
        if self.isBuyOneGetOneCoupon{
            if  let cell : CartSummaryTotalTableViewCell = tableView.cellForRow(at: IndexPath(row: 0, section: 4)) as? CartSummaryTotalTableViewCell{
                
                
                if cell.lblDeliveryCharges.text == "Free"{
                    
                    if arrAppliedCoupanData.discountType == "Free Delivery"{

                    }
                    else{
                      //  self.isSelfPickUp = true
                        freeDelivery = "Free"

                    }
                }
                else {
                    freeDelivery = "No"

                }
                
            }
        }
        else{
            if  let cell : CartSummaryTotalTableViewCell = tableView.cellForRow(at: IndexPath(row: 0, section: 3)) as? CartSummaryTotalTableViewCell{
                

                if cell.lblDeliveryCharges.text == "Free"{
                    
                    if arrAppliedCoupanData.discountType == "Free Delivery"{
                    }
                    else{
                      //  self.isSelfPickUp = true
                        freeDelivery = "Free"
                    }
                }
                else {
                    freeDelivery = "No"
                }

            }
        }
      
        let controller:DeliverySummaryViewController =  UIStoryboard(storyboard: .Cartpayment).initVC()
        controller.discountAmount = Float(couponCodeDiscount)
        controller.ShowDiscountAmount = Float(showDiscountValue)

        controller.subTotal = subTotal
        controller.totalSum = nextAllTotals
        controller.vatTotal = vatTotal
        controller.arrCartProduct = self.arrCartProduct
        controller.arrAppliedCoupanData = self.arrAppliedCoupanData
        controller.isSelfPick = isSelfPickUp
        controller.coupanCodeString = strCoupon
        controller.freeDelivery = freeDelivery
        controller.subTotalNew = subTosubTotalNewF

        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func buttonCancelPressed(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name("HomeViewShow"), object: nil)

    }
    
    @IBAction func buttonShopNow(_ sender: UIButton) {
       
        NotificationCenter.default.post(name: Notification.Name("HomeViewShow"), object: nil)

    }
    
    @IBAction func clearCartAction(_ sender: UIButton) {
        showAlert()
    }
    
    func showAlert(){
        let alert = UIAlertController(title: nil, message:"Are you sure you want to Clear the cart?", preferredStyle: .alert)
        
        let No = UIAlertAction(title:"No", style: .default, handler: { action in
        })
        alert.addAction(No)
        
        let Yes = UIAlertAction(title:"Yes", style: .default, handler: { [self] action in
            clearCartAPI()
        })
        alert.addAction(Yes)
        
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    
    //MARK:- Get Cart List Api
    func getCartListAPI(Model: AddToCartModel) {
        
        if dictAppInfo.deliveryLimit == 0 || dictAppInfo.deliveryLimit == nil{
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getAppInfoAPI()
        }
        
        
        EndPoint.GetCartList(params: Model,t: AddToCartModel.self) { [self] result in
            self.hideLoader()
            switch result {
            case .onSuccess(let data):
                
                if data != nil {
                    var deliveryChargesAmt : Float = 0.0
                    self.dictCartData = data!
                    EndPoint.cartId = data?.cartId ?? 0
                    
                    if data!.productList.count > 0 {
                        self.arrCartProduct =  data!.productList
                        
                        self.totalSum = 0
                        for eachProduct in self.arrCartProduct{
                            if let startDate = eachProduct.fromDate,startDate != nil {
                                
                                let fromDate : Date = startDate.dateFromString(startDate)
                                let endDate = eachProduct.toDate
                                let toDate : Date = endDate!.dateFromString(endDate!)
                                
                                if Date().isBetween(fromDate, and: toDate) {
                                    if let agreementValue = eachProduct.agreementValue, agreementValue != nil {
                                        let agreementValue = eachProduct.agreementValue
                                        
                                        self.totalSum = self.totalSum + Float(eachProduct.agreementValue!) * Float(eachProduct.productQuantity!)
                                        
                                    } else {
                                        if let price = eachProduct.salesPrice,price != 0 {
                                            self.totalSum = self.totalSum + Float(eachProduct.salesPrice!) * Float(eachProduct.productQuantity!)
                                        }
                                    }
                                } else {
                                    if let price = eachProduct.salesPrice,price != 0 {
                                        self.totalSum = self.totalSum + Float(eachProduct.salesPrice!) * Float(eachProduct.productQuantity!)
                                    }
                                }
                            } else {
                                if let price = eachProduct.salesPrice,price != nil {
                                    self.totalSum = self.totalSum + Float(eachProduct.salesPrice!) * Float(eachProduct.productQuantity!)
                                }
                            }
                        }
                        
                        
                        if let vat = dictAppInfo.vatPercent, vat != 0 {
                            self.subTotal = totalSum / (1 + Float(vat) / 100)
                            self.vatTotal = self.totalSum - self.subTotal
                        }
                        else{
                            self.subTotal = totalSum / (1 + Float(5) / 100)
                            self.vatTotal = self.totalSum - self.subTotal
                        }
                        
                        print("vatTotal--->> ",self.vatTotal)
                        
                        var deliveryChargesValue : Float = 0.0
                        if  let deliveryCharges = dictAppInfo.deliveryCharges, deliveryCharges != 0 {
                            if let vat = dictAppInfo.vatPercent, vat != 0 {
                                 
                                 deliveryChargesValue = Float(Float(deliveryCharges)  / (1 + (Float(vat)) / 100))
                              
                                 deliveryChargesAmt = Float(Float(deliveryCharges)  - deliveryChargesValue)
                                
                                self.vatTotal = self.vatTotal + Float(deliveryChargesAmt)
                            } else {
                                deliveryChargesValue = Float(deliveryCharges)
                            }
                        }

                        print("vatTotal--->> ",self.vatTotal)

                        if  let deliveryLimt = dictAppInfo.deliveryLimit, deliveryLimt != 0 {
                            self.totalSum  =  self.totalSum + deliveryChargesValue
                        }
                       
                        if finalCouponCodeDiscount != 0{
                            
                            if self.arrAppliedCoupanData.discountType != "Percent" {
                                totalSum = totalSum - Float(self.finalCouponCodeDiscount)
                                if let vat = dictAppInfo.vatPercent, vat != 0 {
                                    let discountVat = Float(Float(self.finalCouponCodeDiscount)  / (1 + (Float(vat)) / 100))
                                    self.couponCodeDiscount = discountVat
                                    self.vatTotal = self.vatTotal - (self.finalCouponCodeDiscount - discountVat)
                            }
                            else{
                               
                            }
                                print("vatTotal--->> ",self.vatTotal)

                    }
                    else {
                           if self.firstTimeCoupon {
                                    self.applyCouponAPI()
                                }
                            }
                        }
                      
                        self.tableView.dataSource = self
                        self.tableView.delegate = self
                        self.tableView.reloadData()
                        self.tableView.isHidden = false
                        self.vwShopNowbutton.isHidden = true
                        self.lblEmptyDescription.isHidden = true
                        self.lblYourCartIsEmpty.isHidden = true
                        self.imgCartEmpty.isHidden = true
                    }
                    else{
                        self.tableView.isHidden = true
                        self.vwShopNowbutton.isHidden = false
                        self.lblEmptyDescription.isHidden = false
                        self.lblYourCartIsEmpty.isHidden = false
                        self.imgCartEmpty.isHidden = false
                        clearDataAndValueOnCart()
                    }
                }
                else{
                    
                    self.tableView.isHidden = true
                    self.vwShopNowbutton.isHidden = false
                    self.lblEmptyDescription.isHidden = false
                    self.lblYourCartIsEmpty.isHidden = false
                    self.imgCartEmpty.isHidden = false
                    clearDataAndValueOnCart()
                }
            case .onFailure(let error):
                print(error)
                self.tableView.isHidden = true
                self.vwShopNowbutton.isHidden = false
                
                self.lblEmptyDescription.isHidden = false
                self.lblYourCartIsEmpty.isHidden = false
                self.imgCartEmpty.isHidden = false
                clearDataAndValueOnCart()
                if error != "The request timed out."{
                    if error != "No item exists in cart for this user."{
                        NotificationAlert().NotificationAlert(titles: error)
                    }
                }
            }
        }
    }
    
    //MARK:- Update Quantity Api
    func clearCartAPI() {
        self.showLoader()
        EndPoint.clearCart(t: AddToCartModel.self) { result in
            self.hideLoader()
            switch result {
            case .onSuccessWithStringValue(let msg):
                NotificationAlert().NotificationAlert(titles: msg)
                if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                    let param = AddToCartModel()
                    param.userId = userId
                    self.getCartListAPI(Model: param)
                    self.cartCountAPI()
                    self.clearDataAndValueOnCart()
                }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- Update Quantity Api
    func updateQuantity(Model: AddToCartModel) {
        EndPoint.UpdateQuantity(params: Model,t: AddToCartModel.self) { result in
            switch result {
            case .onSuccessWithStringValue(let msg):
                self.isVerifyCoupon = true
              
                if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                    let param = AddToCartModel()
                    param.userId = userId
                    self.getCartListAPI(Model: param)
                    self.cartCountAPI()
                }
                if self.arrAppliedCoupanData.discountType != "Percent" {
                    self.hideLoader()
                    if self.firstTimeCoupon {
                        self.applyCouponAPI()
                    }
                }
            case .onFailure(let error):
                print(error)
                self.hideLoader()
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:-  Coupon Discount Api
    func couponDiscountAPI() {
        self.view.endEditing(true)
        self.showLoader()
        EndPoint.CouponDiscount(t: CouponModel.self) { [weak self] result in
            switch result {
            case .onSuccess(let data):
                self?.hideLoader()
                if data != nil {
                    self?.couponCodeDiscount = Float(data!.discountAmount!)
                    self?.finalCouponCodeDiscount = Float(data!.discountAmount!)
                    self?.totalSum =  self!.totalSum - Float(self!.couponCodeDiscount)
                    
                    if let vat = dictAppInfo.vatPercent, vat != 0 {
                        let discountVat = Float(Float(self!.couponCodeDiscount)  / (1 + (Float(vat)) / 100))
                        self!.couponCodeDiscount = Float(discountVat)
                    }
                    
                    self?.vatTotal = self!.vatTotal - (self!.finalCouponCodeDiscount - self!.couponCodeDiscount)
                    self?.tableView.reloadData()
                    
                }
            case .onFailure(let error):
                self?.hideLoader()
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
            
    //MARK:- Apply Coupon Api
    func applyCouponAPI() {
        self.view.endEditing(true)
        let param = ApplyCoupanModel()
        
        if self.isBuyOneGetOneCoupon {
            
            if tableView.cellForRow(at: IndexPath(row: 0, section: 2)) as? DiscountCartSummaryTableViewCell == nil
            {
                return
            }
            
            let cell : DiscountCartSummaryTableViewCell = tableView.cellForRow(at: IndexPath(row: 0, section: 2))! as! DiscountCartSummaryTableViewCell
            print(cell.txtEnterDiscountCoupen.text!)
            EndPoint.couponCode = cell.txtEnterDiscountCoupen.text!
            param.couponCode = EndPoint.couponCode
            if EndPoint.couponCode == "" {
                if  finalCouponCodeDiscount == 0{
                    NotificationAlert().NotificationAlert(titles: "Enter Discount Code")
                    return
                }
            }
            
            if self.strCoupon == ""{
                param.couponCode = EndPoint.couponCode
            }
            else{
                param.couponCode = self.strCoupon
                EndPoint.couponCode = self.strCoupon
            }
        } else {
            if tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? DiscountCartSummaryTableViewCell == nil
            {
                return
            }
            let cell : DiscountCartSummaryTableViewCell = tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as! DiscountCartSummaryTableViewCell
            EndPoint.couponCode = cell.txtEnterDiscountCoupen.text!
            print(cell.txtEnterDiscountCoupen.text!)
            if EndPoint.couponCode == "" {
                if  finalCouponCodeDiscount == 0{
                    NotificationAlert().NotificationAlert(titles: "Enter Discount Code")
                    return
                }
            }
            
            if self.strCoupon == ""{
                param.couponCode = EndPoint.couponCode
            }
            else{
                param.couponCode = self.strCoupon
                EndPoint.couponCode = self.strCoupon
            }
        }
 
        param.cartId = EndPoint.cartId
        param.userId = EndPoint.userId
        self.showLoader()
        EndPoint.ApplyCoupon(params: param, t: AppliedCoupanDetail.self) { [weak self] result in
            switch result {
            case .onSuccess(let data):
                self?.hideLoader()
                if data != nil {
                    self!.arrAppliedCoupanData = data!
                    self?.strCoupon = param.couponCode ?? ""
                    self?.firstTimeCoupon = true
                    if data?.discountType == "Free Delivery" {
                        self!.isFreeDeliveryCoupon = true
                        self?.strCoupon = param.couponCode ?? ""
                    } else {
                        self!.isFreeDeliveryCoupon = false
                    }
                    if data?.discountType == "BOGO" {
                        self!.isBuyOneGetOneCoupon = true
                        self?.strCoupon = param.couponCode ?? ""
                    } else {
                        self!.isBuyOneGetOneCoupon = false
                    }
                    
                    if data?.discountType == "Percent"{
                    var totalCountAmount = Float()
                    var productsArray = [String]()
                    var salesPriceArray = [String]()

                    if data?.products != nil {
                        productsArray = data?.products?.components(separatedBy: ",") ?? []
                        
                        if data?.salesPrice != nil {
                            salesPriceArray = data?.salesPrice?.components(separatedBy: ",") ?? []
                            for item in self!.arrCartProduct {
                                if item.productId != nil {
                                    if productsArray.count > 0 {
                                        let i1 = productsArray.firstIndex(where: {$0 == String(item.productId ?? 0)})
                                        if i1 != nil {
                                            if salesPriceArray.count > 0 {
                                                let valueAmount = salesPriceArray[i1 ?? 0]
                                                totalCountAmount = totalCountAmount + Float(valueAmount)!
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                        else{
                            for item in self!.arrCartProduct {
                                if item.productId != nil {
                                    
                                    if let startDate = item.fromDate,startDate != nil {
                                        
                                        let fromDate : Date = startDate.dateFromString(startDate)
                                        let endDate = item.toDate
                                        let toDate : Date = endDate!.dateFromString(endDate!)
                                        
                                        if Date().isBetween(fromDate, and: toDate) {
                                            if var agreementValue = item.agreementValue, agreementValue != nil {
                                                
                                                
                                            agreementValue =  Float(item.agreementValue!) * Float(item.productQuantity!)
                                                totalCountAmount = totalCountAmount + agreementValue
                                            } else {
                                                if var price = item.salesPrice,price != 0 {
                                                    
                                                    price = price * Float(item.productQuantity!)
                                                    totalCountAmount = totalCountAmount + price
                                                }
                                            }
                                        } else {
                                            if var price = item.salesPrice,price != 0 {
                                                
                                                price = price * Float(item.productQuantity!)

                                                totalCountAmount = totalCountAmount + price
                                            }
                                        }
                                    } else {
                                        if var price = item.salesPrice,price != nil {
                                            
                                            price = price * Float(item.productQuantity!)

                                            totalCountAmount = totalCountAmount + price
                                        }
                                    }
                                }
                            }
                        }
                    
                    let discountAmountPercent = Float(data!.discountAmount)
                        
                        
                    let pValue = (Float(discountAmountPercent) / 100) * totalCountAmount
                        
                        print("p--- ",pValue)
                        self?.showDiscountValue = Float(pValue)


                    if let vat = dictAppInfo.vatPercent, vat != 0 {
                        let discountVat = Float(Float(pValue)  / (1 + (Float(vat)) / 100))
                            print("vatDic--- ",discountVat)
                        
                        self?.vatTotal = self!.vatTotal + pValue - discountVat
                        
                        data!.discountAmount = discountVat
                        print("discountAmount--- ", data!.discountAmount)
                        
                        self?.percentDiscountAddInVat = pValue - discountVat
                      
                        }
                        
                        print(self?.vatTotal ?? 0.0)
                        
                        print("totalSum--- ", self?.totalSum ?? 0.0)


                        self?.totalSum =  self!.totalSum - Float(pValue)
                        
                        print("totalSum--- ", self?.totalSum ?? 0.0)
                        
                    
                        self?.couponCodeDiscount = Float(data!.discountAmount)

                        self?.finalCouponCodeDiscount = Float(data!.discountAmount)

        
                    self?.percentCouponCodeDiscount = discountAmountPercent
                    self!.strCoupon = param.couponCode ?? ""
                        
                    self?.tableView.reloadData()

                    }
                    else{
                        self?.percentCouponCodeDiscount = Float(data!.discountAmount)
                    }
                    
                    
                    
                    /////////////////////////////////////////////////////////////
                    
                   
                    if data?.discountType != "Percent"{
                        
                        self?.couponCodeDiscount = Float(data!.discountAmount)
                        self?.showDiscountValue = Float(data!.discountAmount)
                        self?.finalCouponCodeDiscount = Float(data!.discountAmount)
                        
                        print("totalSum--- ", self!.totalSum)
                        
                        self?.totalSum =  self!.totalSum - Float(self!.couponCodeDiscount)
                        
                        print("totalSum--- ", self?.totalSum ?? 0.0)

                    if let vat = dictAppInfo.vatPercent, vat != 0 {
                        let discountVat = Float(Float(self!.couponCodeDiscount)  / (1 + (Float(vat)) / 100))
                        
                        print("discount--- ",discountVat)
                        self!.couponCodeDiscount = Float(discountVat)
                    }
                    
                    // ANIL -
                    self?.vatTotal = self!.vatTotal - (self!.finalCouponCodeDiscount - self!.couponCodeDiscount)
                    
                    print("vatTotal--- ",self?.vatTotal ?? 0.0)
                        
                    }

                    self?.tableView.reloadData()
                }
            case .onFailure(let error):
                self?.hideLoader()
                self!.couponCodeDiscount = 0
                self!.finalCouponCodeDiscount = 0
                self?.arrAppliedCoupanData.discountType = ""
                self!.isFreeDeliveryCoupon = false
                self!.isBuyOneGetOneCoupon = false
                self!.percentCouponCodeDiscount = 0
                self!.strCoupon = ""
                self?.firstTimeCoupon = false
                self?.percentDiscountAddInVat = 0.0
                self?.showDiscountValue = 0.0
                print(error)
                if !(self!.isVerifyCoupon) {
                    NotificationAlert().NotificationAlert(titles: error)
                }
                print(error)
                self?.tableView.reloadData()
            }
        }
    }
    
    //MARK:- Cart Count Api
    func cartCountAPI() {
        
        EndPoint.CartCount(t: CartCountModel.self) { result in
            switch result {
            case .onSuccess(let data):
            if data != nil{
                if let cartCount = data!.cartCount, cartCount != 0 {
                    self.setBagCountOnCart(count: cartCount)
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "\(cartCount)"
                    }
                }
                else{
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "0"
                    }
                }
                if let notificationCount = data!.notificationCount, notificationCount >= 0 {
                    self.setNotificationBagCount(count: notificationCount)
                }
                
            }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
}
