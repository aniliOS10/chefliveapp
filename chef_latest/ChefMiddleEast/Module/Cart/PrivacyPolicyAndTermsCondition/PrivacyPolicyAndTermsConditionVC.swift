//
//  PrivacyPolicyAndTermsConditionVC.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit

class PrivacyPolicyAndTermsConditionVC: BaseViewController {

    
    @IBOutlet weak var tblView : UITableView!
    var strTerms = String()
    var strPrivacy = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title:"T&C's and Privacy Policy")
        
        setupTableView()
        privacyPolicyAPI()
        termAndConditionAPI()
    }
    
    func setupTableView()  {
        tblView.delegate = self
        tblView.dataSource = self
    }
    
    //MARK:- PrivacyPolicy API
    func privacyPolicyAPI(){
        //self.showLoader()
        EndPoint.GetPrivacyPolicy(t: CountryListModel.self) { (result) in
            self.hideLoader()
            switch result {
            case .onSuccessWithStringValue(let strData):
                print(strData.html)
                self.strPrivacy = strData.html2String
                self.tblView.reloadData()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
    //MARK:- Terms And Condition API
    func termAndConditionAPI(){
        self.showLoader()
        EndPoint.GetTermsAndConditions(t: CountryListModel.self) {
            (result) in
            self.hideLoader()
            switch result {
            
            case .onSuccessWithStringValue(let strData):
                print(strData.html)
                self.strTerms = strData.html2String
                self.tblView.reloadData()
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }

}
