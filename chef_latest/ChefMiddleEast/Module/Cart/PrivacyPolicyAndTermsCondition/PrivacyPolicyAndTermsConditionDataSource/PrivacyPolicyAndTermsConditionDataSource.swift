//
//  PrivacyPolicyAndTermsConditionDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import UIKit

extension PrivacyPolicyAndTermsConditionVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrivacyPolicyTermsConditionSectionTVC") as! PrivacyPolicyTermsConditionSectionTVC
       
        if section == 0 {
            cell.lblTitle.text = "Terms & Conditions"
        } else {
            cell.lblTitle.text = AppMessage_TermsAndPrivacy.Placeholder.privacyPolicy
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrivacyPolicyTermsConditionTVC") as! PrivacyPolicyTermsConditionTVC
        if indexPath.section == 0 {
            cell.lblTitle.text = self.strTerms
        } else {
            cell.lblTitle.text = self.strPrivacy
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
}
