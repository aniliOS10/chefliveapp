//
//  AppMessage_TermsAndPrivacy.swift
//  ChefMiddleEast
//
//  Created by Apple on 04/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation

class AppMessage_TermsAndPrivacy : NSObject {
    struct Placeholder {
        static let privacyPolicy = NSLocalizedString("Privacy Policy", comment: "")
        static let termsCondition = NSLocalizedString("Terms and Condition", comment: "")
        static let title = NSLocalizedString("T & C and Privacy Policy", comment: "")
    }
}
