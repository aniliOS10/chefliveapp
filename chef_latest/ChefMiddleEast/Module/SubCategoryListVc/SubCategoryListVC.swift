//
//  SubCategoryListVC.swift
//  ChefMiddleEast
//
//  Created by Apple on 23/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import UIKit
import ObjectMapper

class SubCategoryListVC: BaseViewController {

    @IBOutlet weak var listView : UICollectionView!
    var arrCategory = [NewArrivalsModel]()
    var arrProduct = [NewArrivalsModel]()
    var arrAllProduct = [NewArrivalsModel]()
    var selectedCategoryID = String()
    var arrFavourit = [FavouriteModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.arrAllProduct = fetchAllProductLocalStorge()
        
        if arrAllProduct.count > 0 {
            arrProduct = arrAllProduct.filter{$0.categoryId != nil && $0.categoryId == selectedCategoryID}
        }
        
        setupCollectionView()
        setNavigationButton()
    }
    
    //MARK:- Fetch All Product
    func fetchAllProductLocalStorge() -> [NewArrivalsModel] {
        if let syncContactsData = UserDefaults.standard.value(forKey: Constants.ProductModel) as? Data {
            if let aryProduct:[[String:Any]] = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(syncContactsData) as? [[String : Any]] {
                return Mapper<NewArrivalsModel>().mapArray(JSONArray: aryProduct)
            }
        }
        return []
    }
    
    func setNavigationButton() {
        self.initBackButton(image: UIImage.init(named: "BlackArrow") ?? UIImage.init())
        
        self.setNavigationControllerDisplay(rightItems: [self.backBarButton], leftItems: [], title: selectedCategoryID)
    }

    func setupCollectionView() {
        listView.delegate = self
        listView.dataSource = self
    }

}
