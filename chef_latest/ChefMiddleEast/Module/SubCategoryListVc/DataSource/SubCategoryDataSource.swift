//
//  SubCategoryDataSource.swift
//  ChefMiddleEast
//
//  Created by Apple on 23/02/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation
import UIKit

extension SubCategoryListVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell: NewArrivalsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewArrivalsCollectionViewCell", for: indexPath) as! NewArrivalsCollectionViewCell
        
        cell.cellViewRound.addShadow()
        cell.cellViewRound.layer.cornerRadius = 10
        cell.imageV.layer.cornerRadius = 10
        cell.btnAdd.layer.cornerRadius = 5
        
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(btnAddTap(sender:)), for: .touchUpInside)
    
            
        cell.titleLabel.text = arrProduct[indexPath.row].productDescription
        cell.imageV.image = UIImage(named: "placeholder")
        
        
        if (arrProduct[indexPath.row].productImage != nil) {
            
            if (arrProduct[indexPath.row].productImage)?.count ?? 0 > 0 {
                let imageData = arrProduct[indexPath.row].productImage![0] as! [String : Any]
                
                let imagePath = String("\(EndPoint.BASE_API_PRODUCT_IMAGE_URL)\(imageData["productDetailImagePath"] as! String)")
                
                let urlString = imagePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
                
                cell.imageV?.sd_setImage(with: URL.init(string:("\(urlString)"))) { (image, error, cache, urls) in
                    if (error != nil) {
                        cell.imageV.image = UIImage(named: "placeholder")
                    } else {
                        cell.imageV.image = image
                    }
                }
            }
        }else {
            cell.imageV.image = UIImage(named: "placeholder")
        }
  
            cell.salePriceLabel.isHidden = true
            cell.btnFav.isHidden = true
           
       
           
       //Display agreementValue
        if let startDate = arrProduct[indexPath.row].fromDate,startDate != nil {
            
            let fromDate : Date = startDate.dateFromString(startDate)
            let endDate = arrProduct[indexPath.row].toDate
            let toDate : Date = endDate!.dateFromString(endDate!)
            
            if Date().isBetween(fromDate, and: toDate) {
                if let agreementValue = arrProduct[indexPath.row].agreementValue, agreementValue != nil {
                   
                    cell.salePriceLabel.isHidden = false
                    let agreementValue = arrProduct[indexPath.row].agreementValue
                    
                    cell.salePriceLabel.text = String(format: "AED %.2f", agreementValue!)
                    
                    if let price = arrProduct[indexPath.row].salesPrice,price != nil {
                        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: String(format: "AED %.2f", price))
                        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
                        cell.desLabel.isHidden = false
                        cell.desLabel.attributedText = attributeString
                    }
                }
            } else {
                cell.salePriceLabel.isHidden = true
            }
        } else {
            if let price = arrProduct[indexPath.row].salesPrice,price != nil {
                cell.desLabel.text = String(format: "AED %.2f", price)
            }
        }
        
        
        //Display Coming soon
        let availableQty = arrProduct[indexPath.row].availableQuantity
        if ((availableQty) != nil )  {
            if (availableQty)! > 5 {
                cell.btnAdd.isHidden = false
                cell.comingSoonLabel.isHidden = true
                cell.salePriceLabel.isHidden = false
                cell.desLabel.isHidden = false
                
                if let price = arrProduct[indexPath.row].salesPrice,price > 0 {
                }
                else{
                    cell.btnAdd.isHidden = true
                    cell.comingSoonLabel.isHidden = false
                    cell.salePriceLabel.isHidden = true
                    cell.desLabel.isHidden = true
                    cell.desLabel.text = ""
                }
                
            } else {
                 cell.btnAdd.isHidden = true
                 cell.comingSoonLabel.isHidden = false
                 cell.salePriceLabel.isHidden = true
                 cell.desLabel.isHidden = true
            }
        }else {
            cell.btnAdd.isHidden = true
            cell.comingSoonLabel.isHidden = false
            cell.salePriceLabel.isHidden = true
            cell.desLabel.isHidden = true
        }
        
      
        

     
        return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
       
        let width = (UIScreen.main.bounds.size.width/2) - 15
        let size = CGSize(width: width, height: width + 112)
        
        return size
    }
    
    
   
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
            let controller:CategoryListingViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
            controller.shouldDisplayHome = false
            controller.selectedSubCatIdName = ""
            self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    //MARK:- Add Button Tap
    @objc func btnAddTap(sender:UIButton){
        
      //  if screenCommingFrom == "SubCategory" {
            
            if !arrProduct.isEmpty {
                guard let productId = arrProduct[sender.tag].productId else {return}
                if productId != 0 {
                    let param = AddToCartModel()
                    if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                        param.userId = userId
                    }
                    let paramProduct = ProductModel()
                    paramProduct.productId = productId
                    paramProduct.productQuantity = 1
                    param.product.append(paramProduct)
                    self.addToCartAPI(Model: param)
                }
            }
      //  }
     /*   else{
            if !arrFavourit.isEmpty {
                guard let favouriteId = arrFavourit[sender.tag].favouriteId else {return}
                if favouriteId != 0 {
                    let param = AddToCartModel()
                    if let userId = UserDefaults.standard.string(forKey: Constants.userId),!userId.isEmpty {
                        param.userId = userId
                    }
                    let paramProduct = ProductModel()
                    paramProduct.productId = favouriteId
                    paramProduct.productQuantity = 1
                    param.product.append(paramProduct)
                    self.addToCartAPI(Model: param)
                }
            }
        }*/
        
    }
    
    
    //MARK:- Fav Button Tap
    @objc func btnFavouritTap(button:UIButton){
        button.isSelected = !button.isSelected
        arrFavourit[button.tag].isFavourite = button.isSelected
        EndPoint.recipeId = "\(arrFavourit[button.tag].recipeId!)"
        if button.isSelected == true {
            EndPoint.favouriteStatus = "true"
        }
        else {
            EndPoint.favouriteStatus = "false"
        }
        if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
            EndPoint.userId = userId
            markRecipeFavouriteAPI()
        }
    }
    
    // MARK: - Recipe List API
    func markRecipeFavouriteAPI(){
       
        EndPoint.MarkRecipeFavourite(t: RecipeListModel.self) { result in
            switch result {
            case .onSuccess(_ ):
                NotificationAlert().NotificationAlert(titles: "Sucess Response")
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    

//MARK:- Add to Cart Api
func addToCartAPI(Model: AddToCartModel) {
    
    EndPoint.AddToCart(params: Model,t: AddToCartModel.self) { result in
        self.hideLoader()
        switch result {
        case .onSuccessWithStringValue(let msg):
            if  let userId = UserDefaults.standard.string(forKey: Constants.userId), !userId.isEmpty {
                EndPoint.userId = userId
                self.cartCountAPI()
            }
            NotificationAlert().NotificationAlert(titles: msg)
        case .onFailure(let error):
            print(error)
            if error != "The request timed out."{
                NotificationAlert().NotificationAlert(titles: error)
            }
        }
    }
}
    
    
    func cartCountAPI() {
        
        EndPoint.CartCount(t: CartCountModel.self) { result in
            switch result {
            case .onSuccess(let data):
            if data != nil{
                if let cartCount = data!.cartCount, cartCount != 0 {
                    self.setBagCountOnCart(count: cartCount)
                    if let tabItems = self.tabBarController?.tabBar.items {
                        // In this case we want to modify the badge number of the third tab:
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = "\(cartCount)"
                    }
                }
                if let notificationCount = data!.notificationCount, notificationCount >= 0 {
                    self.setNotificationBagCount(count: notificationCount)
                    
                }
                
            }
            case .onFailure(let error):
                print(error)
                if error != "The request timed out."{
                    NotificationAlert().NotificationAlert(titles: error)
                }
            }
        }
    }
    
}
