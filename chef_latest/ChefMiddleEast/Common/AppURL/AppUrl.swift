//
//  AppUrl.swift
//  BaseProject
//
//  Created by rupinder singh on 17/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import Foundation
enum BaseUrl:String {
    case localUrl = "http://petcarewebapi.us-west-2.elasticbeanstalk.com/"
    case liveUrl = ""
    case scheme = "http"
    case hostLocal = "petcarewebapi.us-west-2.elasticbeanstalk.com"
}
enum SubUrl:String {
    case loginUrl = "/api/Auth/UserLogin"
    case emailPhoneVerifyCode =  "/api/Auth/SendEmailPhoneVerifyCode"
    case verifyPhone = "/api/Auth/VerifyPhone"
    case registerUser = "/api/Auth/RegisterUser"
    case forgotPassword = "/api/Auth/ForgotPassword"
    case resetPassword = "/api/Auth/ResetPassword"
    case sendEmailCode = "/api/Auth/SendEmailCode"
    case updateAddress = "/api/Auth/UpdateAddress"
    
}
struct APIUrl {
        static let baseUrl = BaseUrl.localUrl.rawValue
        

    
    struct LoginUrl {
        let pathValue:String = SubUrl.loginUrl.rawValue
        var parameter:[String:String]?
        var url:String{
             get{
                URLComponentT.shared.pathValue = pathValue
                URLComponentT.shared.parameter = parameter
                return URLComponentT.shared.urlComponents.url?.absoluteString ?? ""
             }
        }
        }
    struct SendOtpUrl {
        let pathValue:String = SubUrl.emailPhoneVerifyCode.rawValue
        var parameter:[String:String]?
        var url:String{
             get{
                URLComponentT.shared.pathValue = pathValue
                URLComponentT.shared.parameter = parameter
                return URLComponentT.shared.urlComponents.url?.absoluteString ?? ""
             }
        }
        }
    struct verifyPhone {
        let pathValue:String = SubUrl.verifyPhone.rawValue
        var parameter:[String:String]?
        var url:String{
             get{
                URLComponentT.shared.pathValue = pathValue
                URLComponentT.shared.parameter = parameter
                return URLComponentT.shared.urlComponents.url?.absoluteString ?? ""
             }
        }
        }
    struct registerUser {
        let pathValue:String = SubUrl.registerUser.rawValue
        var parameter:[String:String]?
        var url:String{
             get{
                URLComponentT.shared.pathValue = pathValue
                URLComponentT.shared.parameter = parameter
                return URLComponentT.shared.urlComponents.url?.absoluteString ?? ""
             }
        }
        
    }
    
    
    struct ForgorPassword {
        let pathValue:String = SubUrl.forgotPassword.rawValue
        var parameter:[String:String]?
        var url:String{
             get{
                URLComponentT.shared.pathValue = pathValue
                URLComponentT.shared.parameter = parameter
                return URLComponentT.shared.urlComponents.url?.absoluteString ?? ""
             }
        }
        }
    
    struct ResetPassword {
        let pathValue:String = SubUrl.resetPassword.rawValue
        var parameter:[String:String]?
        var url:String{
             get{
                URLComponentT.shared.pathValue = pathValue
                URLComponentT.shared.parameter = parameter
                return URLComponentT.shared.urlComponents.url?.absoluteString ?? ""
             }
        }
        }
    struct SendEmailCode {
        let pathValue:String = SubUrl.sendEmailCode.rawValue
        var parameter:[String:String]?
        var url:String{
             get{
                URLComponentT.shared.pathValue = pathValue
                URLComponentT.shared.parameter = parameter
                return URLComponentT.shared.urlComponents.url?.absoluteString ?? ""
             }
        }
        }
    struct UpdateAddress {
        let pathValue:String = SubUrl.updateAddress.rawValue
        var parameter:[String:String]?
        var url:String{
             get{
                URLComponentT.shared.pathValue = pathValue
                URLComponentT.shared.parameter = parameter
                return URLComponentT.shared.urlComponents.url?.absoluteString ?? ""
             }
        }
        }
    
    
}



     class URLComponentT{
        static let shared: URLComponentT = URLComponentT()
        var pathValue:String = ""
        var parameter:[String:String]?
        var urlComponents:URLComponents{
             get{
            var urlComponents = URLComponents()
            urlComponents.scheme = BaseUrl.scheme.rawValue
            urlComponents.host = BaseUrl.hostLocal.rawValue
            urlComponents.path = pathValue
            if let par = parameter{
                    urlComponents.setQueryItems(with: par)
            }
            return urlComponents
        }
    }
}
extension URLComponents {
    
    mutating func setQueryItems(with parameters: [String: String]) {
        self.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
    }
}
