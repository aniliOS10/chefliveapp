//
//  AppMessage.swift
//  BaseProject
//
//  Created by rupinder singh on 31/08/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import UIKit

class AppMessage: NSObject {
    struct Alert {
        static let error = NSLocalizedString("Error", comment: "")
        static let Success = NSLocalizedString("Success", comment: "")
        static let cancel = NSLocalizedString("Cancel", comment: "")
        static let Ok = NSLocalizedString("Ok", comment: "")
        static let yes = NSLocalizedString("Yes", comment: "")
        static let no = NSLocalizedString("No", comment: "")
        static let Alert = NSLocalizedString("Alert", comment: "")
    }
    
    struct Profile {
        static let firstName = NSLocalizedString("FirstName", comment: "")
    }
    
}
