import UIKit
import Foundation

// MARK: - Designable Extension

extension UIView {
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var cornerRadius: Double {
        get {
            return Double(self.layer.cornerRadius)
        }set {
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    
    @IBInspectable
    /// Should the corner be as circle
    public var circleCorner: Bool {
        get {
            return min(bounds.size.height, bounds.size.width) / 2 == uiCornerRadius
        }
        set {
            uiCornerRadius = newValue ? min(bounds.size.height, bounds.size.width) / 2 : uiCornerRadius
        }
    }
    
    /// Corner radius of view; also inspectable from Storyboard.
    public var uiCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = circleCorner ? min(bounds.size.height, bounds.size.width) / 2 : newValue
            //abs(CGFloat(Int(newValue * 100)) / 100)
        }
    }
    
    @IBInspectable
    /// Shadow color of view; also inspectable from Storyboard.
    public var shadowColor: UIColor? {
        get {
            guard let color = layer.shadowColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
        set {
            layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    /// Shadow offset of view; also inspectable from Storyboard.
    public var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    /// Shadow opacity of view; also inspectable from Storyboard.
    public var shadowOpacity: Double {
        get {
            return Double(layer.shadowOpacity)
        }
        set {
            layer.shadowOpacity = Float(newValue)
        }
    }
    
    @IBInspectable
    /// Shadow radius of view; also inspectable from Storyboard.
    public var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    /// Shadow path of view; also inspectable from Storyboard.
    public var shadowPath: CGPath? {
        get {
            return layer.shadowPath
        }
        set {
            layer.shadowPath = newValue
        }
    }
    
    @IBInspectable
    /// Should shadow rasterize of view; also inspectable from Storyboard.
    /// cache the rendered shadow so that it doesn't need to be redrawn
    public var shadowShouldRasterize: Bool {
        get {
            return layer.shouldRasterize
        }
        set {
            layer.shouldRasterize = newValue
        }
    }
    
    @IBInspectable
    /// Should shadow rasterize of view; also inspectable from Storyboard.
    /// cache the rendered shadow so that it doesn't need to be redrawn
    public var shadowRasterizationScale: CGFloat {
        get {
            return layer.rasterizationScale
        }
        set {
            layer.rasterizationScale = newValue
        }
    }
    
    @IBInspectable
    /// Corner radius of view; also inspectable from Storyboard.
    public var maskToBounds: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }
}


// MARK: - Properties

public extension UIView {
    
    /// Size of view.
    var size: CGSize {
        get {
            return self.frame.size
        }
        set {
            self.width = newValue.width
            self.height = newValue.height
        }
    }
    
    /// Width of view.
    var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            self.frame.size.width = newValue
        }
    }
    
    /// Height of view.
    var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
}

// MARK: - UIView

extension UIView {
    
    func searchVisualEffectsSubview() -> UIVisualEffectView? {
        if let visualEffectView = self as? UIVisualEffectView {
            return visualEffectView
        } else {
            for subview in subviews {
                if let found = subview.searchVisualEffectsSubview() {
                    return found
                }
            }
        }
        return nil
    }
    
    /// This is the function to get subViews of a view of a particular type
    /// https://stackoverflow.com/a/45297466/5321670
    func subViews<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        for view in self.subviews {
            if let aView = view as? T{
                all.append(aView)
            }
        }
        return all
    }
    
    
    /// This is a function to get subViews of a particular type from view recursively. It would look recursively in all subviews and return back the subviews of the type T
    /// https://stackoverflow.com/a/45297466/5321670
    func allSubViewsOf<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        func getSubview(view: UIView) {
            if let aView = view as? T{
                all.append(aView)
            }
            guard view.subviews.count>0 else { return }
            view.subviews.forEach{ getSubview(view: $0) }
        }
        getSubview(view: self)
        return all
    }
    
    func toImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }

    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    func addShadow(offSet:CGSize,radius:CGFloat=3.0,color:UIColor=UIColor(white: 0.67, alpha: 0.3)) {
        self.layer.masksToBounds = false; self.clipsToBounds = false;
        self.layer.shadowOpacity = 1.0
        self.layer.shadowColor = color.cgColor
        self.layer.shadowRadius = radius
        self.layer.shadowOffset = offSet
    }
    
    func rippleEffect(darker:Bool) {
        let origin = CGPoint.init(x: self.bounds.width*0.5, y: self.bounds.height*0.5)
        let color = (darker == true) ? self.backgroundColor?.darker(by:25) : self.backgroundColor?.lighter(by: 25)
        let duration = 0.33
        let fadeDelay = 0.75*duration
        let radius = (self.bounds.size.width > self.bounds.size.height) ? self.bounds.size.width : self.bounds.size.height
        
        rippleEffect(origin: origin, color: color!, duration: duration, radius: radius, fadeDelay: fadeDelay)
    }
    func rippleEffect(color:UIColor) {
        let origin = CGPoint.init(x: self.bounds.width*0.5, y: self.bounds.height*0.5)
        let duration = 0.40
        let fadeDelay = 0.75*duration
        let radius = (self.bounds.size.width > self.bounds.size.height) ? self.bounds.size.width * 0.55 : self.bounds.size.height  * 0.55
        rippleEffect(origin: origin, color: color, duration: duration, radius: radius, fadeDelay: fadeDelay)
    }
    
    func rippleEffect(origin:CGPoint,color:UIColor, duration:TimeInterval,radius:CGFloat,fadeDelay:TimeInterval) {
        let angleFull = Double.pi*2
        let layerName = "ripple"
        
        let bounds = self.bounds
        let x = bounds.width
        let y = bounds.height
        
        // Build an array with the four corners of the view.
        let corners = [CGPoint.init(x: 0, y: 0),
                       CGPoint.init(x: 0, y: y),
                       CGPoint.init(x: x, y: 0),
                       CGPoint.init(x: x, y: y)]
        // Calculate the corner closest to the origin and the one farther from it.
        // We might not need these values, but calculate them anyway so that the code
        // is clearer.
        var minDistance = CGFloat.greatestFiniteMagnitude; var maxDistance = CGFloat(-1);
        for cornerValue in corners {
            let distance = origin.distance(cornerValue)
            if distance < minDistance {
                minDistance = distance
            }
            if distance > maxDistance {
                maxDistance = distance
            }
        }
        
        // Calculate the start and end radius of our ripple effect.
        // If the ripple starts inside the view then the start radis is 0, if it
        // starts outside the view then make the radius the distance to the nearest corner.
        let originInside = origin.x > 0 && origin.x < x && origin.y > 0 && origin.y < y
        // Note that if 0 is used as a default value then the circle may look misshapen.
        let startRadius = (originInside == true) ? 0.1 : minDistance
        
        // If we set a radius use it, if not then use the distance to the farthest corner.
        let endRadius = (radius > 0) ? radius : minDistance
        
        // Create paths for out start and end circles.
        let startPath = UIBezierPath.init(arcCenter: origin, radius: startRadius, startAngle: 0, endAngle: CGFloat(angleFull), clockwise: true)
        let endPath = UIBezierPath.init(arcCenter: origin, radius: endRadius, startAngle: 0, endAngle: CGFloat(angleFull), clockwise: true)
        
        // Create a new layer to draw the ripple on.
        let rippleLayer = CAShapeLayer()
        rippleLayer.name = layerName
        self.layer.masksToBounds = true
        rippleLayer.fillColor = color.cgColor
        
        // Create the animation
        let rippleAnimation = CABasicAnimation.init(keyPath: "path")
        rippleAnimation.fillMode = CAMediaTimingFillMode.both
        rippleAnimation.duration = duration
        rippleAnimation.fromValue = startPath.cgPath
        rippleAnimation.toValue = endPath.cgPath
        rippleAnimation.isRemovedOnCompletion = false
        rippleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        
        // Set the ripple layer to be just above the bg.
        self.layer.insertSublayer(rippleLayer, at: 0)
        // Give the ripple layer the animation.
        rippleLayer.add(rippleAnimation, forKey: nil)
        
        // Enqueue blocks to handle animation ends.
        // We can use a delegate for this, but it complicates the code as handling
        // animation states is needed as well as @propertys to pass data around.
        // This may not be perfectly times but it is good enough.
        DispatchQueue.main.asyncAfter(deadline: .now() + fadeDelay) {
            let fadeAnimation = CABasicAnimation.init(keyPath: "opacity")
            fadeAnimation.fillMode = CAMediaTimingFillMode.both;
            fadeAnimation.duration = fadeDelay;
            fadeAnimation.fromValue = 1.0
            fadeAnimation.toValue = 0.0
            fadeAnimation.isRemovedOnCompletion = false
            rippleLayer.add(fadeAnimation, forKey: nil)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + fadeDelay) {
            rippleLayer.removeAllAnimations()
            rippleLayer.removeFromSuperlayer()
        }
    }
    
    func springPopAnimation() {
        self.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIView.AnimationOptions.allowUserInteraction,
                       animations: {
                        self.transform = CGAffineTransform.identity
                       },
                       completion: { Void in()  }
        )
    }
    
    func shakeAnimation() {
        UIView.animate(withDuration:0.2, delay:0, usingSpringWithDamping: 0.5, initialSpringVelocity:1.0, options:[.repeat,.allowUserInteraction,.autoreverse], animations: {
            UIView.animate(withDuration:0.1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: [.repeat,.allowUserInteraction,.autoreverse], animations: {
                self.transform = CGAffineTransform.init(rotationAngle:0.01)
            }, completion: { (completed1) in
            })
            UIView.animate(withDuration:0.1, delay: 0.1, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: [.repeat,.allowUserInteraction,.autoreverse], animations: {
                self.transform = CGAffineTransform.init(rotationAngle:(-0.01))
            }, completion: { (completed2) in
            })
        }) { (completed) in}
    }
    
    func pendulamAnimation(offSetX:CGFloat) {
        UIView.animate(withDuration:0.1, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.9, options:.transitionCrossDissolve, animations: {
            self.transform = CGAffineTransform.init(translationX: offSetX, y:0)
        }, completion: { (completed) in
            UIView.animate(withDuration:0.1, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.9, options:.transitionCrossDissolve, animations: {
                self.transform = CGAffineTransform.init(translationX: offSetX*(-1), y:0)
            }, completion: { (completed1) in
                UIView.animate(withDuration:0.1, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.9, options:.transitionCrossDissolve, animations: {
                    self.transform = CGAffineTransform.init(translationX: offSetX*0.5, y:0)
                }, completion: { (completed2) in
                    UIView.animate(withDuration:0.1, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.9, options:.transitionCrossDissolve, animations: {
                        self.transform = CGAffineTransform.init(translationX: offSetX*(-0.5), y:0)
                    }, completion: { (completed3) in
                        UIView.animate(withDuration:0.1, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.9, options:.transitionCrossDissolve, animations: {
                            self.transform = CGAffineTransform.identity
                        }, completion: { (completed4) in
                        })
                    })
                })
            })
        })
    }
    
    func blinkAnimation() {
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.2
        flash.fromValue = 1
        flash.toValue = 0.0
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 4
        layer.add(flash, forKey: nil)
    }
    
    func fadeIn(duration: TimeInterval = 0.4, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        self.alpha = 0.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        self.alpha = 1.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            self.alpha = 0.0
        }) { (completed) in
            self.isHidden = true
            completion(true)
        }
    }
    
    @discardableResult func showLoadingIndicator(superViewFrame:Bool=false) -> Bool {
        if let _:ProgressIndicator = self.viewWithTag(19518) as? ProgressIndicator {
            return false
        }
        else {
            ProgressIndicator.shared().show(at: self,superViewFrame:superViewFrame )
            return true
        }
    }
    
    func hideLoadingIndicator () {
        if let indicator:ProgressIndicator = self.viewWithTag(19518) as? ProgressIndicator {
            indicator.hide()
        }
    }
}
