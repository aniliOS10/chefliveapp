//
//  NotificationAlert.swift
//  ChefMiddleEast
//
//  Created by sandeep on 12/12/21.
//

import Foundation
import BRYXBanner
class NotificationAlert {
    
    func NotificationAlert(titles:String){
        let banner = Banner(title: "Chef Middle East", subtitle: titles, image: #imageLiteral(resourceName: "Notification_icon"), backgroundColor: #colorLiteral(red: 0.6274509804, green: 0.02089935914, blue: 0.1515867114, alpha: 1))
        banner.dismissesOnTap = true
        banner.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        banner.titleLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        banner.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        banner.imageView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        banner.imageView.image = #imageLiteral(resourceName: "Notification_icon")
        banner.show(duration: 3.0)
        }
   
//    func NotificationAlertWithTime(titles:String,time:TimeInterval){
//        let banner = Banner(title: "Alert", subtitle: titles, image: #imageLiteral(resourceName: "wopadu Logo"), backgroundColor: .black)
//        banner.dismissesOnTap = true
//        banner.show(duration: time)
//    }
   
   
}
