//
//  BaseViewController.swift
//  BaseProject
//
//  Created by rupinder singh on 30/08/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import UIKit
import CocoaTextField
import NVActivityIndicatorView
import SwiftGifOrigin

@available(iOS 13.0, *)
class BaseViewController: CustomNavigationController {
    var activityContainer:UIView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      

        // Do any additional setup after loading the view.
    }
    
    func showLoader() -> Void {
      
        activityContainer = UIView(frame: UIScreen.main.bounds)
        activityContainer.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        let jeremyGif = UIImage.gif(name: "Whisk")
        let imageView = UIImageView(image: jeremyGif)
        imageView.frame = CGRect(x: CGFloat(activityContainer.frame.width / 2) - 40, y: CGFloat(activityContainer.frame.height / 2) - 40, width: 80, height: 80)
        activityContainer.addSubview(imageView)
        UIApplication.shared.keyWindow?.addSubview(activityContainer)

    }
    
    
    func hideLoader() -> Void {
        
        activityContainer.removeFromSuperview()
    }


    func applyStyle(to v: CocoaTextField) {
    
        v.tintColor = .gray
        v.textColor = UIColor.black
        v.inactiveHintColor = .gray
        v.activeHintColor = .gray
        v.focusedBackgroundColor = UIColor.white
        v.defaultBackgroundColor = UIColor.white
        v.borderColor = UIColor.clear
        v.errorColor = .gray
    }
}
