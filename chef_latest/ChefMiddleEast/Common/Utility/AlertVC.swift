//
//  AlertVC.swift
//  BaseProject
//
//  Created by rupinder singh on 09/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import UIKit
enum AlertType: Int
{
    case errorA
    case successA
}
protocol AlertDelegate {
    func okButtonPressed(alertController:UIAlertController,alert:AlertVC)
    func cancelButtonPressed(alertController:UIAlertController,alert:AlertVC)
}

class AlertVC: NSObject {
    var type:AlertType = .successA
    var typeString:String = ""
    var delegateAlert: AlertDelegate?
    func withOkAndCancel(title:String = AppMessage.Alert.error,message:String,vc:UIViewController,okStyle:UIAlertAction.Style,cancelStyle:UIAlertAction.Style,controllerStyle:UIAlertController.Style,okTitle:String = AppMessage.Alert.Ok,cancelTitle:String = AppMessage.Alert.cancel) -> Void {
       
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: controllerStyle)
       
        alert.addAction(UIAlertAction(title: cancelTitle, style: cancelStyle, handler: { (action: UIAlertAction!) in
            self.delegateAlert?.cancelButtonPressed(alertController: alert, alert: self)
        }))
        alert.addAction(UIAlertAction(title: okTitle, style: okStyle, handler: { (action: UIAlertAction!) in
            self.delegateAlert?.okButtonPressed(alertController: alert, alert: self)

        }))

        
        vc.present(alert, animated: true, completion: nil)
    }
    func withOk(title:String = AppMessage.Alert.Alert
                ,message:String,vc:UIViewController,okStyle:UIAlertAction.Style,controllerStyle:UIAlertController.Style,okTitle:String = AppMessage.Alert.Ok) -> Void {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: controllerStyle)
        alert.addAction(UIAlertAction(title: okTitle, style: okStyle, handler: { (action: UIAlertAction!) in
            self.delegateAlert?.okButtonPressed(alertController: alert, alert: self)

        }))

        DispatchQueue.main.async {
        vc.present(alert, animated: true, completion: nil)
        }
    }
}
