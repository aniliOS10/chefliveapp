//
//  CustomNavigationController.swift
//  PetCare
//
//  Created by rupinder singh on 05/10/21.
//

import UIKit

@available(iOS 13.0, *)
class CustomNavigationController: UIViewController {
    var backBarButton:UIBarButtonItem = UIBarButtonItem()
    let backButton = UIButton(type: UIButton.ButtonType.custom)

    var menuBarButton:UIBarButtonItem = UIBarButtonItem()
    let menuButton = UIButton(type: UIButton.ButtonType.custom)
    
    var bellBarButton:UIBarButtonItem = UIBarButtonItem()
    let bellButton = UIButton(type: UIButton.ButtonType.custom)
    
    var cartBarButton:UIBarButtonItem = UIBarButtonItem()
    let cartButton = UIButton(type: UIButton.ButtonType.custom)


    var titleImageView:UIImageView = UIImageView.init(image: UIImage.init(named: ""))
    
    
    var lblBadge = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblBadge = UILabel.init(frame: CGRect.init(x: 20, y: -5, width: 20, height: 20))
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent

        // Do any additional setup after loading the view.
    }
    
    func setTitleImageOfNavigationController(image:UIImage?){
        if image != nil {
             titleImageView.image = image
        }
        
        self.navigationItem.titleView = titleImageView
    }
    
    func initBackButton(image:UIImage = UIImage(named: "ArrowRight") ?? UIImage.init()){
        
        backButton.setImage(image, for: .normal)
        backButton.addTarget(self, action:#selector(backButtonPressed), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backBarButton = UIBarButtonItem(customView: backButton)
    }
    
    func initMenuButton(){
        
        menuButton.setBackgroundImage(UIImage(named: "Sidemenu"), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 27, height: 17)
        menuBarButton = UIBarButtonItem(customView: menuButton)
    }
    
    func initCartButton(){
        
        cartButton.setBackgroundImage(UIImage(named: "shoppingCartTabbar"), for: .normal)
        cartButton.frame = CGRect(x: 0, y: 0, width: 30, height: 19)
        cartBarButton = UIBarButtonItem(customView: cartButton)
       
    }
    
    func initBellButton(){
        if #available(iOS 13.0, *) {
            let homeSymbolConfiguration = UIImage.SymbolConfiguration(pointSize: 28, weight: .regular)
            
            bellButton.setImage(UIImage.init(systemName: "bells_ic",withConfiguration: homeSymbolConfiguration), for: .normal)

        } else {
            // Fallback on earlier versions
        }
        bellButton.setBackgroundImage(UIImage(named: "bells_ic"), for: .normal)

        Utility.shared.setColorOfButtonImage(btn: bellButton, color: .black, image: bellButton.image(for: .normal) ?? UIImage.init())
        
         bellButton.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
         bellBarButton = UIBarButtonItem(customView: bellButton)
    }
    
    @objc func backButtonPressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationControllerDisplay(rightItems:[
        UIBarButtonItem],leftItems:[UIBarButtonItem],title:String,titleColor:UIColor = UIColor.black){
        self.navigationItem.leftBarButtonItems = rightItems
        self.navigationItem.rightBarButtonItems = leftItems
        self.title = title
        let textAttributes = [NSAttributedString.Key.foregroundColor:titleColor,NSAttributedString.Key.font:UIFont(name: FontName.Optima.Bold, size: 18)!]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
            
        let border_GreenishTealThree = UIColor(red: 232.0 / 255.0, green: 232.0 / 255.0, blue: 232.0 / 255.0, alpha: 1.0)

        self.navigationController?.navigationBar.barTintColor = border_GreenishTealThree
        self.navigationController?.navigationBar.backgroundColor = border_GreenishTealThree
    }
    
    
    func setNavigationControllerDisplayWhite(rightItems:[
        UIBarButtonItem],leftItems:[UIBarButtonItem],title:String,titleColor:UIColor = UIColor.white){
        self.navigationItem.leftBarButtonItems = rightItems
        self.navigationItem.rightBarButtonItems = leftItems
        self.title = title
        let textAttributes = [NSAttributedString.Key.foregroundColor:titleColor,NSAttributedString.Key.font:UIFont(name: FontName.Optima.Bold, size: 18)!]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }

    
    
    func setBagCountOnCart(count: Int){
        let lblBadge = UILabel.init(frame: CGRect.init(x: 20, y: -5, width: 20, height: 20))
        lblBadge.backgroundColor = .red
        lblBadge.clipsToBounds = true
        lblBadge.layer.cornerRadius = 10
        lblBadge.textColor = UIColor.white
        lblBadge.textAlignment = .center
        lblBadge.text = String(count)
        lblBadge.font = .boldSystemFont(ofSize: 12)
        cartButton.addSubview(lblBadge)
    }
    
    func setNotificationBagCount(count: Int){
        lblBadge.backgroundColor  = #colorLiteral(red: 0.6274509804, green: 0.01960784314, blue: 0.1529411765, alpha: 1)
        lblBadge.clipsToBounds = true
        lblBadge.layer.cornerRadius = 10
        lblBadge.textColor = UIColor.white
        lblBadge.textAlignment = .center
        lblBadge.text = String(count)
        lblBadge.font = .boldSystemFont(ofSize: 12)
        
        if count > 0 {
            bellButton.addSubview(lblBadge)
        }
        else{
            for bellButton in self.view.subviews {
                lblBadge.removeFromSuperview()
            }
        }
    }
}
