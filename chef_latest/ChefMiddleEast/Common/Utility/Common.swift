//
//  Common.swift
//  ChefMiddleEast
//
//  Created by sandeep on 13/01/22.
//  Copyright © 2022  Bafila Sandeep (9560531379). All rights reserved.
//

import Foundation


class Common: NSObject {
    
    class func setTotalForBuyerList(dictList: [String: AnyObject]) -> [String: AnyObject] {
        
        var dictListInfo = dictList
        
        print(dictListInfo)
        
        if let arrProds = dictListInfo["product_details"] as? [[String: AnyObject]],!arrProds.isEmpty {
        
        var total: Double = 0.0
        
        var totalCommission : Double = 0.0
        
        for dictProd in arrProds {
            
            var price: Double = 0.0
            
            if let strPrice = dictProd["priceWithComission"] as? String, !strPrice.isEmpty {
                
                price = (strPrice as NSString).doubleValue
            }
            
            let qty = Double(dictProd["qty"] as! String)
            
             total = total + (price * qty!)
            
            print(total)
        }
        
        dictListInfo["total"] = String(format: "%.2f", total) as AnyObject
        
        }
        return dictListInfo
    }
    
    
    struct Currency {
        
        private static let formatter: NumberFormatter = {
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            //formatter.currencyCode = "INR"
            formatter.currencySymbol = "$"
            return formatter
        }()
        
        static func stringFrom(_ decimal: Decimal, currency: String? = nil) -> String {
            return self.formatter.string(from: decimal as NSDecimalNumber)!
        }
    }

}


extension Float {
    var asLocaleCurrency:String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        
        formatter.currencyCode = Locale.current.currencyCode
        formatter.currencySymbol = Locale.current.currencySymbol
        //        formatter.currencyCode = "USD"
        //        formatter.currencySymbol = "$"
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        return formatter.string(from: NSNumber(value: self))!
    }
    
    var asLocaleCredits:String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        
        formatter.currencyCode = ""
        formatter.currencySymbol = ""
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        return formatter.string(from: NSNumber(value: self))!
    }
}
