//
//  MenuViewController.swift
//  SideMenuExample
//
//  Created by kukushi on 11/02/2018.
//  Copyright © 2018 kukushi. All rights reserved.
//

import UIKit
import SideMenuSwift
import SDWebImage

struct SideMenuRows {
    var title:String?
    var image:UIImage?
}

class Preferences {
    static let shared = Preferences()
    var enableTransitionAnimation = false
}

@available(iOS 13.0, *)
class MenuViewController: UIViewController, AlertDelegate {
    
    var isDarkModeEnabled = false
    
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var profilePicButton: UIButton!
    @IBOutlet weak var logOutButton: UIButton!
    @IBOutlet weak var profilePicView: UIView!
    @IBOutlet weak var lblDoctorName: UILabelX!
    @IBOutlet weak var lblAddress: UILabelX!
    @IBOutlet weak var imgProfilepic: UIImageView!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .none
        }
    }
    @IBOutlet weak var selectionTableViewHeader: UILabel!

    @IBOutlet weak var btn_RgNow: UIButton!
    @IBOutlet weak var selectionMenuTrailingConstraint: NSLayoutConstraint!
    private var themeColor = UIColor.white
    var arrayRows:[SideMenuRows] = []
    
    
    //MARK:- VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

       // Utility.shared.makeRoundCorner(layer: profilePicView.layer, color: UIColor.lightGray, radius: 12.0)
        
     //   Utility.shared.makeRoundCorner(layer: logOutButton.layer, color: UIColor.black, radius: 12.0)
        
        Utility.shared.makeRoundCorner(layer: logOutButton.layer, color: UIColor.black, radius: 12.0, borderWidth: 0.1)
    
        self.logOutButton.setTitle(SideMenu_Appmessage.Placeholder.Logout, for: .normal)
        
        if !UserDefaults.standard.bool(forKey: Constants.guestUser) {
            imgProfilepic.isHidden = false
            lblAddress.isHidden = false
            self.logOutButton.isHidden = false
            self.btn_RgNow.isHidden = true

            self.arrayRows.append(SideMenuRows.init(title: SideMenu_Appmessage.Placeholder.MyOrders, image: UIImage.init(named: "SideOrders"))
            )
       

            self.arrayRows.append(SideMenuRows.init(title: SideMenu_Appmessage.Placeholder.DeliveryAddress, image: UIImage.init(named: "SideDelivery")))
            
            
            self.arrayRows.append(SideMenuRows.init(title: SideMenu_Appmessage.Placeholder.Recipes, image: UIImage.init(named: "SideRe")))

                                  
            self.arrayRows.append(SideMenuRows.init(title: SideMenu_Appmessage.Placeholder.ContactUs, image: UIImage.init(named: "SideContactUs"))
                                  )
            
            self.arrayRows.append(SideMenuRows.init(title: SideMenu_Appmessage.Placeholder.Settings, image: UIImage.init(named: "SideSettings"))
                                  )
            
        } else {
            self.logOutButton.isHidden = true
            imgProfilepic.isHidden = true
            lblAddress.isHidden = true
            
            self.btn_RgNow.isHidden = false

            self.arrayRows.append(SideMenuRows.init(title: SideMenu_Appmessage.Placeholder.Recipes, image: UIImage.init(named: "SideRe")))
            
            self.arrayRows.append(SideMenuRows.init(title: SideMenu_Appmessage.Placeholder.ContactUs, image: UIImage.init(named: "SideContactUs")))
        }
   
     
        self.arrayRows.append(SideMenuRows.init(title: SideMenu_Appmessage.Placeholder.FAQ, image: UIImage.init(named: "SideFAQ"))
        )
//        self.arrayRows.append(SideMenuRows.init(title: SideMenu_Appmessage.Placeholder.PrivacyPolicy, image: UIImage.init(named: "SidePrivacyPolicy"))
//        )
        self.arrayRows.append(SideMenuRows.init(title: SideMenu_Appmessage.Placeholder.TermsConditions, image: UIImage.init(named: "SideTermsConditions"))
        )
        self.tableView.reloadData()
        
        if let version =  Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,let build =  Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        {
            self.lblVersion.text = "Version \(version) Build \(build)"
        }
        else{
            print("No")
        }
        
        isDarkModeEnabled = SideMenuController.preferences.basic.position == .under
        configureView()
        sideMenuController?.delegate = self
   
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setProfileDataFromUserDefault()
        print("[Example] Menu did appear")
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("[Example] Menu will disappear")
    }
    
    func setProfileDataFromUserDefault(){
       
        
        if let profileImg = UserDefaults.standard.string(forKey: Constants.userImg),!profileImg.isEmpty{
            
            let urlString = profileImg.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

            
            self.imgProfilepic.sd_setImage(with: NSURL(string:(urlString)) as URL?) { (image, error, cache, urls) in
                if (error != nil) {
                    self.imgProfilepic.image = UIImage(named: "UserProfile")
                } else {
                    self.imgProfilepic.image = image
                    self.imgProfilepic.layer.borderWidth = 0.4
                    self.imgProfilepic.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
                    self.imgProfilepic.layer.cornerRadius = self.imgProfilepic.frame.size.width/2
                }
            }
        }
        
        if let firstName = UserDefaults.standard.string(forKey: Constants.firstName),!firstName.isEmpty{
            
            if let lastName = UserDefaults.standard.string(forKey: Constants.lastName),!lastName.isEmpty{
                self.lblDoctorName.text = firstName + " " + lastName
            }
            
            if UserDefaults.standard.bool(forKey: Constants.guestUser) {
                self.lblDoctorName.text = "Guest Login"
            }
        }
        
        if let email = UserDefaults.standard.string(forKey: Constants.email),!email.isEmpty{
            self.lblAddress.text = email
        }
    }

    private func configureView() {
        if isDarkModeEnabled {
            themeColor = UIColor(red: 0.03, green: 0.04, blue: 0.07, alpha: 1.00)
            selectionTableViewHeader.textColor = .white
        } else {
            selectionMenuTrailingConstraint.constant = 0
            themeColor = UIColor(red: 0.98, green: 0.97, blue: 0.96, alpha: 1.00)
        }

        let sidemenuBasicConfiguration = SideMenuController.preferences.basic
        let showPlaceTableOnLeft = (sidemenuBasicConfiguration.position == .under) != (sidemenuBasicConfiguration.direction == .right)
        if showPlaceTableOnLeft {
            selectionMenuTrailingConstraint.constant = SideMenuController.preferences.basic.menuWidth - view.frame.width
        }

        view.backgroundColor = UIColor().getThemeColor()
        tableView.backgroundColor = UIColor().getThemeColor()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        let sideMenuBasicConfiguration = SideMenuController.preferences.basic
        let showPlaceTableOnLeft = (sideMenuBasicConfiguration.position == .under) != (sideMenuBasicConfiguration.direction == .right)
        selectionMenuTrailingConstraint.constant = showPlaceTableOnLeft ? SideMenuController.preferences.basic.menuWidth - size.width : 0
        view.layoutIfNeeded()
    }
}

@available(iOS 13.0, *)
extension MenuViewController: SideMenuControllerDelegate {
    func sideMenuController(_ sideMenuController: SideMenuController,
                            animationControllerFrom fromVC: UIViewController,
                            to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return BasicTransitionAnimator(options: .transitionFlipFromLeft, duration: 0.6)
    }

    func sideMenuController(_ sideMenuController: SideMenuController, willShow viewController: UIViewController, animated: Bool) {
        print("[Example] View controller will show [\(viewController)]")
    }

    func sideMenuController(_ sideMenuController: SideMenuController, didShow viewController: UIViewController, animated: Bool) {
        print("[Example] View controller did show [\(viewController)]")
    }

    func sideMenuControllerWillHideMenu(_ sideMenuController: SideMenuController) {
        print("[Example] Menu will hide")
    }

    func sideMenuControllerDidHideMenu(_ sideMenuController: SideMenuController) {
        print("[Example] Menu did hide.")
    }

    func sideMenuControllerWillRevealMenu(_ sideMenuController: SideMenuController) {
        print("[Example] Menu will reveal.")
    }

    func sideMenuControllerDidRevealMenu(_ sideMenuController: SideMenuController) {
        print("[Example] Menu did reveal.")
    }
    
    @IBAction func btnRegNow(_ sender: Any){
        CoreDataManager.shared.deleteObject_string(name: Entity.user.rawValue)
        UserDefaults.standard.removeObject(forKey: Constants.token)
        UserDefaults.standard.removeObject(forKey: Constants.userId)
        UserDefaults.standard.removeObject(forKey: Constants.userImg)
        UserDefaults.standard.removeObject(forKey: Constants.firstName)
        UserDefaults.standard.removeObject(forKey: Constants.lastName)
        UserDefaults.standard.removeObject(forKey: Constants.email)
        UserDefaults.standard.removeObject(forKey: Constants.phone)
        UserDefaults.standard.removeObject(forKey: Constants.custAccount)
        RootManager().welcomeRootClass()
    
    }
    
    @IBAction func btnLogOut(_ sender: Any)
    {
        ActionSheet()
    }
    //MARK: - Logout Message Action Sheet
        func ActionSheet()
        {
            let alertVC  = AlertVC()
            alertVC.delegateAlert = self
            DispatchQueue.main.async {
                alertVC.withOkAndCancel(title: "Confirm", message: "Are you sure to logout?", vc: self, okStyle: .default, cancelStyle: .destructive, controllerStyle: .alert,okTitle: AppMessage.Alert.yes,cancelTitle: AppMessage.Alert.no)
               
            }
           
        }
    func okButtonPressed(alertController: UIAlertController, alert: AlertVC) {
        CoreDataManager.shared.deleteObject_string(name: Entity.user.rawValue)
        UserDefaults.standard.removeObject(forKey: Constants.token)
        UserDefaults.standard.removeObject(forKey: Constants.userId)
        UserDefaults.standard.removeObject(forKey: Constants.userImg)
        UserDefaults.standard.removeObject(forKey: Constants.firstName)
        UserDefaults.standard.removeObject(forKey: Constants.lastName)
        UserDefaults.standard.removeObject(forKey: Constants.email)
        UserDefaults.standard.removeObject(forKey: Constants.phone)
        UserDefaults.standard.removeObject(forKey: Constants.custAccount)

        
        RootManager().welcomeRootClass()
    }
    
    func cancelButtonPressed(alertController: UIAlertController, alert: AlertVC) {
        
    }
}

@available(iOS 13.0, *)
extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.arrayRows.count
    }

    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenu", for: indexPath) as! SelectionCell
      //  cell.contentView.backgroundColor = UIColor().getThemeColor()
        let row = indexPath.row
       
        cell.titleLabel?.text = self.arrayRows[indexPath.row].title
        cell.imageIcon.image = self.arrayRows[indexPath.row].image
        //
        cell.titleLabel?.textColor  = UIColor.white
     //   cell.titleLabel?.textColor = isDarkModeEnabled ? .white : .black
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        
        
        if UserDefaults.standard.bool(forKey: Constants.guestUser) {
             if (row + 1) == 1{
                 
                 
                 let controller:RecipeListViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                 self.navigationController?.pushViewController(controller, animated: true)
                 
                 
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                  
                    navigation.pushViewController(controller, animated: true)
                }
            }
            else if (row + 1) == 2{
                
                let controller:ContactUsViewController =  UIStoryboard(storyboard: .Setting).initVC()
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                  
                    navigation.pushViewController(controller, animated: true)
                }
            }
            else if (row + 1) == 333{
                
                let controller:SettingViewController =  UIStoryboard(storyboard: .Setting).initVC()
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                  
                    navigation.pushViewController(controller, animated: true)
                }
            }
            else if (row + 1) == 3{
               
                let controller:FAQViewController =  UIStoryboard(storyboard: .Setting).initVC()
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                  
                    navigation.pushViewController(controller, animated: true)
                }
            }
            else if (row + 1) == 114{
                   let controller:PrivacyPolicyVC =  UIStoryboard(storyboard: .Setting).initVC()
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                  
                    navigation.pushViewController(controller, animated: true)
                }
            }
            else if (row + 1) == 4{
                   let controller:TermsConditionViewController =  UIStoryboard(storyboard: .Setting).initVC()
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                  
                    navigation.pushViewController(controller, animated: true)
                }
            }
            else{
                //sideMenuController?.setContentViewController(with: "\(row + 1)", animated: Preferences.shared.enableTransitionAnimation)
            }
        } else {
            if (row + 1) == 1{
               
                let controller:MyOrdersViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                  
                    navigation.pushViewController(controller, animated: true)
                }
            }
            else if (row + 1) == 2{
                 
                let controller:AddressViewController =  UIStoryboard(storyboard: .Address).initVC()
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                    controller.isTabBarHide = false
                    navigation.pushViewController(controller, animated: true)
                }
            }
            
            else if (row + 1) == 3 {
                let controller:RecipeListViewController =  UIStoryboard(storyboard: .DashBoard).initVC()
                self.navigationController?.pushViewController(controller, animated: true)
                
                
               if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
               {
                 
                   navigation.pushViewController(controller, animated: true)
               }
            }
            else if (row + 1) == 4{
                
                let controller:ContactUsViewController =  UIStoryboard(storyboard: .Setting).initVC()
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                  
                    navigation.pushViewController(controller, animated: true)
                }
            }
            else if (row + 1) == 5{
                
                let controller:SettingViewController =  UIStoryboard(storyboard: .Setting).initVC()
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                  
                    navigation.pushViewController(controller, animated: true)
                }
            }
            else if (row + 1) == 6{
               
                let controller:FAQViewController =  UIStoryboard(storyboard: .Setting).initVC()
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                  
                    navigation.pushViewController(controller, animated: true)
                }
            }
            else if (row + 1) == 117{
                   let controller:PrivacyPolicyVC =  UIStoryboard(storyboard: .Setting).initVC()
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                  
                    navigation.pushViewController(controller, animated: true)
                }
            }
            else if (row + 1) == 7{
                   let controller:TermsConditionViewController =  UIStoryboard(storyboard: .Setting).initVC()
                if let navigation  = DashTabBarViewController.sharedTabBar.selectedViewController as? UINavigationController
                {
                  
                    navigation.pushViewController(controller, animated: true)
                }
            }
            else{
                //sideMenuController?.setContentViewController(with: "\(row + 1)", animated: Preferences.shared.enableTransitionAnimation)
            }
        }
        
        sideMenuController?.hideMenu()

        if let identifier = sideMenuController?.currentCacheIdentifier() {
            print("[Example] View Controller Cache Identifier: \(identifier)")
        }
        
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
}

class SelectionCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
}
