//
//  SideMenu_Appmessage.swift
//  PetCare
//
//  Created by rupinder singh on 05/10/21.
//

import UIKit

class SideMenu_Appmessage: NSObject {
        struct Placeholder {
          
            static let MyOrders = NSLocalizedString("MyOrders", comment: "")
            static let DeliveryAddress = NSLocalizedString("DeliveryAddress", comment: "")
            static let Recipes = NSLocalizedString("Recipes", comment: "")
            static let ShippingAddress = NSLocalizedString("ShippingAddress", comment: "")
            static let PaymentMethods = NSLocalizedString("PaymentMethods", comment: "")
            static let ContactUs = NSLocalizedString("ContactUs", comment: "")
            static let Settings = NSLocalizedString("Settings", comment: "")
            static let FAQ = NSLocalizedString("FAQ", comment: "")
            static let PrivacyPolicy = NSLocalizedString("PrivacyPolicy", comment: "")
            static let TermsConditions = NSLocalizedString("TermsConditions", comment: "")
            static let Logout = NSLocalizedString("Log Out", comment: "")
            static let logoutMessage = NSLocalizedString("logoutMessage", comment: "")
            static let areYouSure = NSLocalizedString("areYouSure", comment: "")
            
        }
    
}
