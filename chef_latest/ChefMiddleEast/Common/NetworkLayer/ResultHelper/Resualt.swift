//
//  Resualt.swift
//  BaseProject
//
//  Created by rupinder singh on 04/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import Foundation
enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}
