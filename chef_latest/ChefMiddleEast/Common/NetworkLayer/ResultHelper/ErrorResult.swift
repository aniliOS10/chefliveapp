//
//  ErrorResult.swift
//  BaseProject
//
//  Created by rupinder singh on 04/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import Foundation
enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case custom(string: String)
}
