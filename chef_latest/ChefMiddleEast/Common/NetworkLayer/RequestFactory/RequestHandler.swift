//
//  RequestHandler.swift
//  BaseProject
//
//  Created by rupinder singh on 04/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import Foundation

class RequestHandler {
 
 let reachability = Reachability()!
 
 func networkResult<T: Parceable>(completion: @escaping ((Result<[T], ErrorResult>) -> Void)) ->
     ((Result<Data, ErrorResult>) -> Void) {
         
         return { dataResult in
             
            DispatchQueue.main.async {
                 switch dataResult {
                 case .success(let data) :
                    ResualtParser.parse(data: data, completion: completion)
                     break
                 case .failure(let error) :
                     print("Network error \(error)")
                     completion(.failure(.network(string: "Network error " + error.localizedDescription)))
                     break
                 }
             }
             
         }
 }
 
 func networkResult<T: Parceable>(completion: @escaping ((Result<T, ErrorResult>) -> Void)) ->
     ((Result<Data, ErrorResult>) -> Void) {
         
         return { dataResult in
            DispatchQueue.main.async {
                switch dataResult {
                case .success(let data) :
                   ResualtParser.parse(data: data, completion: completion)
                    break
                case .failure(let error) :
                    print("Network error \(error)")
                    completion(.failure(.network(string: "Network error " + error.localizedDescription)))
                    break
                }
            }
            
             
         }
 }
    
    
}
