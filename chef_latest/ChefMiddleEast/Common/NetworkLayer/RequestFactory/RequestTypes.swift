//
//  RequestTypes.swift
//  BaseProject
//
//  Created by rupinder singh on 04/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import Foundation
import Alamofire
final class RequestTypes {
    
    enum Method: String {
        case GET
        case POST
        case PUT
        case DELETE
        case PATCH
    }
    
    static func request(method: Method, url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        return request
    }
    
}
