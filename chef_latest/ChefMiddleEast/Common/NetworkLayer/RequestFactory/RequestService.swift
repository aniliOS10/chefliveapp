//
//  RequestService.swift
//  BaseProject
//
//  Created by rupinder singh on 04/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import Foundation
import Alamofire
final class RequestService {
    
    // todo add model
    func loadData_withURlSession(urlString: String, session: URLSession = URLSession(configuration: .default), completion: @escaping (Result<Data, ErrorResult>) -> Void) -> URLSessionTask? {
        
        guard let url = URL(string: urlString) else {
            completion(.failure(.network(string: "Wrong url format")))
            return nil
        }
        
        var request = RequestTypes.request(method: .GET, url: url)
        
        if let reachability = Reachability(), !reachability.isReachable {
            request.cachePolicy = .returnCacheDataDontLoad
        }
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(.failure(.network(string: "An error occured during request :" + error.localizedDescription)))
                return
            }
            
            if let data = data {
                completion(.success(data))
            }
        }
        task.resume()
        return task
    }
    func loadData_Alamofire(urlString: String,method:HTTPMethod,parameters:[String:Any]?, completion: @escaping (Result<Data, ErrorResult>) -> Void)-> DataRequest?
    {
      
        guard let url = URL.init(string: urlString) else {
            completion(.failure(.network(string: "Wrong url format")))
            return nil
        }
        
        let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                ]
       /*
        if let data = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted),
            let jsonString = String(data: data, encoding: .utf8) {
           
            AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        }
       */
        let request = Alamofire.request(url, method: method, parameters: parameters,encoding: JSONEncoding.default)
    
       
        
       /* if let reachability = Reachability(), !reachability.isReachable {
            request.cacheResponse(using: ResponseCacher.doNotCache)
        }*/
        request.responseJSON { (response) in
            
            if let error = response.error {
                completion(.failure(.network(string: "An error occured during request :" + error.localizedDescription)))
                return
            }
            
            if let data = response.data {
                completion(.success(data))
            }
            
        }
         return request
    }
    
}
