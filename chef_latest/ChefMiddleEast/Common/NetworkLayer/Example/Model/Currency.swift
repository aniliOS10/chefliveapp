//
//  Currency.swift
//  BaseProject
//
//  Created by rupinder singh on 05/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import Foundation

enum Currency : String {
    case EUR
    case GBP
    case USD
}

enum CurrencyLocale : String {
    case EUR = "fr_FR"
    case GBP = "en_UK"
}

struct CurrencyRate {
    
    let currencyIso : String
    let rate : Double
}
