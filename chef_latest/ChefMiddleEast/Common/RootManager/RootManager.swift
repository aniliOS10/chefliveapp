//
//  RootManager.swift
//  PetCare
//
//  Created by rupinder singh on 04/10/21.
//

import UIKit
import SideMenuSwift
@available(iOS 13.0, *)
class RootManager: NSObject {
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    //MARK: -  @Set RootView Controller
    func setRootController() {
       
        if let token = UserDefaults.standard.string(forKey: Constants.token), !token.isEmpty {
            self.SideRootScreen()
        }
        else{
            setLoginRoot()
        }
    }
    
    //MARK: -  @Push To RootController
    func PushToRootScreen(ClassName:UIViewController){
        UIApplication.shared.windows.first?.rootViewController = ClassName
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    func setLoginRoot(){
        let storyboard = UIStoryboard(name: "Athentication", bundle: nil)
        PushToRootScreen(ClassName: storyboard.instantiateViewController(withIdentifier: "LoginNavigationController") as! LoginNavigationController)
    }
    
    func welcomeRootClass(){
        let storyboard = UIStoryboard(name: "Athentication", bundle: nil)
        PushToRootScreen(ClassName: storyboard.instantiateViewController(withIdentifier: "WelcomeNavigationController") as! WelcomeNavigationController)
    }
    
    
    func SideRootScreen(){
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        PushToRootScreen(ClassName: storyboard.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController)
    }
    
}
