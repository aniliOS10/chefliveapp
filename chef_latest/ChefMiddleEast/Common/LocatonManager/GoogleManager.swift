//
//  GoogleManager.swift
//  PetCare
//
//  Created by rupinder singh on 13/10/21.
//

import Foundation
import GoogleSignIn
import GoogleMaps
import GooglePlaces

enum GoogleKey: String {
    case provided = "AIzaSyDCuXQn15xYlXu3glNIqIW_3LvAR4Zq3qg"
    case urlSchema = "comgooglemaps://"
    case appStoreUrl = "https://itunes.apple.com/in/app/google-maps-navigation-transport/id585027354?mt=8"
}
typealias Failure = (_ error: NSError) -> ()
class GoogleManager: NSObject, GIDSignInDelegate {

    //MARK:- SingleTon Instance
    static let shared = GoogleManager()

    var controller: UIViewController?

    /// This is the clouser that will return the current selected user through google login in
    var success: (_ user: MockUp_User) -> Void = { _ in }

    /// This clouser will return the error
    var failure : ((ErrorResult?) -> Void)?

    /// This method is used to handle the url call back
    ///
    /// - Parameters:
    ///   - application: application instance of application
    ///   - url: url that will handle by applicaiton
    ///   - options: applicaitonoption
    /// - Returns: true/false
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        let googleHandler = GIDSignIn.sharedInstance().handle(url)
        return googleHandler
    }

    //MARK:- Configuration

    /// This method will configure the google login the application
    ///
    /// - Parameters:
    ///   - failure: This call will return the reason for the failure while configuration of google login in the application
    ///   - vc: view controller on which the google login interface should be present
    func configure(failure: Failure, viewController vc: UIViewController) {
        //Configure Google Login.
        var error: NSError?
        //GGLContext.sharedInstance().configureWithError(&error)
        if let error = error {
            //failure(DIError(error: error))
            failure(error)
            return
        }
        //Set Google Login delegates
        controller = vc
        GIDSignIn.sharedInstance().delegate = self
        //GIDSignIn.sharedInstance().uiDelegate = self
    }

    //MARK:- Login

    /// This method is used to call google login interface
    func login () -> Void {
        logout()
        GIDSignIn.sharedInstance().signIn()
    }

    //MARK:- Logout

    /// This method will google the current user form system
    func logout() -> Void {
        GIDSignIn.sharedInstance().signOut()
    }

    //MARK:- Google SignIn Delegate
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        controller?.present(viewController, animated: true, completion: nil)

    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            //failure(parseError(error: error))
            failure?(ErrorResult.custom(string: error.localizedDescription))
            return
        }
        if let user = user {
            let currentuser = MockUp_User()
           // currentuser.email = user.profile.email
           // currentuser.snsID = user.userID
           // currentuser.sns = .google
            //currentuser.name = user.profile.givenName
            //currentuser.profilePicUrl = user.profile.imageURL(withDimension: 400).absoluteString
            success(currentuser)
        }

    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        //failure(parseError(error: error))
        failure?(ErrorResult.custom(string: error.localizedDescription))
    }
    /*
    func parseError(error: Error) -> DIError {
       
        switch error._code {
        case 3: return DIError.noTwitterEmailFound
        case -5: return DIError.googleRequestCanceled
        default: return DIError(error: error)
        }
    }
*/
    func initMap() {
        GMSServices.provideAPIKey(GoogleKey.provided.rawValue)
        GMSPlacesClient.provideAPIKey(GoogleKey.provided.rawValue)
    }
 

}
