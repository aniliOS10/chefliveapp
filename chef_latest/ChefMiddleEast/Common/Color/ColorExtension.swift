//
//  ColorExtension.swift
//  ChefMiddleEast
//
//  Created by Apple on 28/10/21.
//

import Foundation
import UIKit
extension UIColor {
    func getThemeColor()->UIColor{
        return UIColor.init(named: "ThemeColor") ?? UIColor.clear
    }
    func getSearchColor()->UIColor{
        return UIColor.init(named: "searchColor") ?? UIColor.clear
    }
    
    
    
    func imageWithColor(width: Int, height: Int) -> UIImage {
            let size = CGSize(width: width, height: height)
            return UIGraphicsImageRenderer(size: size).image { rendererContext in
                self.setFill()
                rendererContext.fill(CGRect(origin: .zero, size: size))
            }
        }
}
