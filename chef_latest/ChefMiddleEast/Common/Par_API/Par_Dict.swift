//
//  Par_Dict.swift
//  PetCare
//
//  Created by Apple on 02/09/21.
//

import Foundation
protocol DictionaryEncodable: Encodable {}

extension DictionaryEncodable {
    func dictionary() -> [String: AnyObject]? {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .millisecondsSince1970
        guard let json = try? encoder.encode(self),
            let dict = try? JSONSerialization.jsonObject(with: json, options: []) as? [String: AnyObject] else {
                return nil
        }
        return dict
    }
}
