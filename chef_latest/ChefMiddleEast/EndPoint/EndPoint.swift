//
//  EndPoint.swift
//  ChefMiddleEast
//
//  Created by sandeep on 12/12/21.
//

import Foundation

class EndPoint {
    static let BASE_API_URL = "http://20.107.115.133/api/"
   // static let BASE_API_URL = "http://20.71.160.226/api/"
   // static let BASE_API_URL = "http://192.168.1.20:5000/"
   //  static let BASE_API_URL = "http://192.168.1.6:5000/"
    
    static let BASE_API_IMAGE_URL = "http://20.107.115.133/"
    static let BASE_API_PRODUCT_IMAGE_URL = "https://cmenopcommerceimages.blob.core.windows.net/uat/"
    
    static var userId = ""
    static var email = ""
    static var Id = ""
    static var date = ""
    static var searchText = ""
    static var recipeId = ""
    static var favouriteStatus = ""
    static var bookmarkStatus = ""
    static var favouriteType = ""
    static var deviceType = ""
    static var couponCode = ""
    static var filterWith = ""
    static var salesOrderId = ""
    static var dataOrigin = ""
    static var phone = ""
    static var phoneNumber = ""
    static var fcmToken = ""
    static var cartId : Int = 0
    static var checkExisting : Bool = false
    static var currentDateTime = ""
}

