//
//  BaseResponse.swift
//  ChefMiddleEast
//
//  Created by sandeep on 12/12/21.
//

import Foundation
import ObjectMapper

class BaseResponse : NSObject, Mappable {
    
    var isSuccess: Bool?
    var pageNo: Int?
    var total: Int?
    var totalRecords: Int?
    var error :String?
    var message :String?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        isSuccess <- map["isSuccess"]
        error <- map["error"]
        pageNo <- map["pageNo"]
        total <- map["total"]
        totalRecords <- map["totalRecords"]
        message <- map["message"]
    }
}
