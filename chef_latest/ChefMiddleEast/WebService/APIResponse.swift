//
//  APIResponse.swift
//  ChefMiddleEast
//
//  Created by sandeep on 12/12/21.
//

import Foundation
import ObjectMapper
class APIResponse<T>: NSObject, Mappable where T: Mappable {
    
    var isSuccess: Bool!
    var data: T?
    var items = [T]()
    var error: String?
    var stringData: String?
    var message: String?
    var integerData: Int?
    var itemsData: T?

    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }

    func mapping(map: Map) {
        isSuccess <- map["success"]
        data <- map["data"]
        itemsData <- map["items"]
        stringData <- map["data"]
        message <- map["message"]
        error <- map["error"]
        items <- map["items"]
        integerData <- map["data"]
    }
}
