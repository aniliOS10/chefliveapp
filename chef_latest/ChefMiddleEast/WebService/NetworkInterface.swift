//
//  NetworkInterface.swift
//  ChefMiddleEast
//
//  Created by sandeep on 12/12/21.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

enum APIResult<T: BaseResponse> {
    case success(data: APIResponse<T>)
    case failure(error: String)
}

public enum NetworkMethod: String {
    case OPTIONS, GET, HEAD, POST, PUT, PATCH, DELETE, TRACE, CONNECT
}

struct Resource {
    let method: NetworkMethod
    let parameters: [String : AnyObject]?
}

protocol APIService {
    var path: String { get }
    var resource: Resource { get }
}

extension APIService {
    
    func request<T: Mappable>(t: T.Type, result: @escaping (APIResult<T>) -> Void) {
        
        debugPrint("**** API Request *****")
        debugPrint("Request URL:\(path)")
        debugPrint("Request resource: \(resource)")
        debugPrint("Request headers : \(UserDefaults.standard.value(forKey: Constants.token) ?? "")")
        let token = UserDefaults.standard.value(forKey: Constants.token)
        var tokenHeaders:[String:String] = [:]
        tokenHeaders = ["x-access-token" :"\(token ?? "")"]
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 180
        
        manager.request(path, method: alamofireMethodForMethod(method: resource.method),parameters: resource.parameters, encoding: JSONEncoding.default, headers: tokenHeaders)
        .validate()
        .responseObject(completionHandler: { (response: DataResponse<APIResponse<T>>) in

            debugPrint("Again URL:\(path)")
            debugPrint("**** API Response ****")
            debugPrint("\(response.debugDescription)")
                       
        switch response.result {
            case .success(let obj):
                if obj.isSuccess == true {
                    result(.success(data: obj))
                
                } else {
                    result(.failure(error: obj.message ?? "There was an error connecting to server.try again"))
                }
            case .failure(let error):
                if error.localizedDescription.contains("404") {
                    result(.failure(error: error.localizedDescription))
                }
                else if error.localizedDescription.contains("The request timed out") {
                    result(.failure(error: error.localizedDescription))
                }
                else{
                    result(.failure(error: error.localizedDescription))
                }
            }
        })
    }
    
    private func alamofireMethodForMethod(method: NetworkMethod) -> HTTPMethod {
        return Alamofire.HTTPMethod(rawValue: method.rawValue)!
    }

}
