//
//  CoreDataProcessor.swift
//  BaseProject
//
//  Created by rupinder singh on 31/08/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import UIKit
import CoreData
@available(iOS 13.0, *)
class CoreDataProcessor: NSObject {
    static let shared:CoreDataProcessor = CoreDataProcessor()
    let context =     Utility.shared.appDelegate.persistentContainer.viewContext
    // var userInfo:UserInfo?
    func insertEntity(entityName:String,mockUp:Any)  {
        let object =  NSEntityDescription.insertNewObject(forEntityName: entityName, into: Utility.shared.appDelegate.persistentContainer.viewContext)
        if let ob  = object as?  UserInfo{
            ob.saveDetailsInDB(object: mockUp)
        }
        
        Utility.shared.appDelegate.saveContext()
    }
    func fetchMultipleEntity(request:NSFetchRequest<NSFetchRequestResult>,predicate:NSPredicate?) -> [Any] {
        var arrResualt: [Any] = []
        if predicate != nil{
            request.predicate = predicate
        }
        do{
            arrResualt  =   try self.context.fetch(request)
        }
        catch{
            print("Error while fetching data")
        }
        return arrResualt
    }
    func deleteObject_string(name:String){
        let arr =   self.fetchMultipleEntity(request: NSFetchRequest<NSFetchRequestResult>.init(entityName: name), predicate: nil) as! [NSManagedObject]
        for obj in arr{
            self.deleleObject(info: obj)
        }
        do{
            try self.context.save()
        }catch{
            print("Error while saving context")
        }
    }
    func deleleObject(info:NSManagedObject)  {
        self.context.delete(info)
    }
    func deleleObjects()  {
        _ =  self.context.deletedObjects
        do{
            try self.context.save()
        }catch{
            print("Error while saving context")
        }
    }
    
    func getUserInfo() -> UserInfo {
        let arr =   self.fetchMultipleEntity(request: UserInfo.fetchRequest(), predicate: nil) as! [NSManagedObject]
        if arr.count > 0 {
            return arr[0] as! UserInfo
        }else{
            self.insertEntity(entityName: Entity.user.rawValue, mockUp: MockUp_User.init())
            let arr2 =   self.fetchMultipleEntity(request: UserInfo.fetchRequest(), predicate: nil) as! [NSManagedObject]
            return arr2[0] as! UserInfo
        }
        
        
    }
}
