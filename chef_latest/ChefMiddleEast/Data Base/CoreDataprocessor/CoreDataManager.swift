//
//  CoreDataManager.swift
//  BaseProject
//
//  Created by rupinder singh on 04/11/20.
//  Copyright © 2020 rupinder singh. All rights reserved.
//

import UIKit
import CoreData

//computed properties
@available(iOS 13.0, *)
struct UserInfoObj
{
   var lastObj:UserInfo = CoreDataProcessor.shared.getUserInfo()
   var userInfo:UserInfo
   {
       get{ return lastObj }
       set(newValue){ lastObj = newValue }
   }
}
// observer
/*
class UserInfoObj2 {
    var userInfo:UserInfo = CoreDataProcessor.shared.getUserInfo(){
        willSet(newValue){
            
        }
        didSet{
            print(oldValue)
        }
    }
    
}
*/
@available(iOS 13.0, *)
class CoreDataManager: NSObject {
    static let shared:CoreDataManager = CoreDataManager()
    let userInfoObj:UserInfoObj = UserInfoObj()
    
    
    func insertEntity(entityName:String,mockUp:Any)  {
        CoreDataProcessor.shared.insertEntity(entityName: entityName, mockUp: mockUp)
    }
    func fetchMultipleEntity(request:NSFetchRequest<NSFetchRequestResult>,predicate:NSPredicate?) -> [Any] {
       return CoreDataProcessor.shared.fetchMultipleEntity(request: request, predicate: predicate)
    }
    func deleleObject(info:NSManagedObject)  {
        CoreDataProcessor.shared.deleleObject(info: info)
    }
    func deleleObjects()  {
        CoreDataProcessor.shared.deleleObjects()
    }
    func deleteObject_string(name:String){
        CoreDataProcessor.shared.deleteObject_string(name: name)
    }
}
