//
//  UserInfo+CoreDataProperties.swift
//  
//
//  Created by Apple on 26/10/21.
//
//

import Foundation
import CoreData


extension UserInfo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserInfo> {
        return NSFetchRequest<UserInfo>(entityName: "UserInfo")
    }

    @NSManaged public var dialCode: String?
    @NSManaged public var email: String?
    @NSManaged public var facebookId: String?
    @NSManaged public var googleId: String?
    @NSManaged public var isEmailVerified: Int16
    @NSManaged public var isPhoneVerified: Int16
    @NSManaged public var isSocialUser: Int16
    @NSManaged public var linkedInId: String?
    @NSManaged public var passwordHash: String?
    @NSManaged public var phoneNumber: String?
    @NSManaged public var profilePic: String?
    @NSManaged public var registeredOn: String?
    @NSManaged public var twitterId: String?
    @NSManaged public var userID: String?
    @NSManaged public var userName: String?
    override  func saveDetailsInDB(object obj:Any){
            let object  = obj as! MockUp_User
            if object.userId != nil{
           // dialCode = object.dialCode
            email = object.email
            //facebookId = object.facebookId
         //   googleId  = object.googleId
            userID = object.userId
            isEmailVerified = Int16(object.isEmailVerified)
             //isPhoneVerified = Int16(object.isPhoneVerified)
            isSocialUser = Int16(object.isSocialUser)
           // linkedInId =  object.linkedInId
           //  passwordHash = object.passwordHash
            phoneNumber = String(object.phoneNumber)
          // profilePic = object.facebookId
         // registeredOn = object.registeredOn
          //  twitterId = object.twitterId
          userName = object.userName
            }
       }
}
